import * as AuthActions from './Auth/action';
import * as HospitalActions from './Hospital/action';
import * as ScheduleActions from './Schedule/action';
import * as CareActions from './Care/action';
import * as QuestActions from './Quest/action';
import * as AlertActions from './Alert/actions';
import * as LoadingActions from './Loading/actions';
import * as NavActions from './Navigation/actions';

export {
  AuthActions,
  AlertActions,
  HospitalActions,
  ScheduleActions,
  CareActions,
  QuestActions,
  LoadingActions,
  NavActions
};
