import { types } from './action';

const initialState = {};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.TYPE_HOME:
      return { ...state, ...payload };
    default:
      return { ...state };
  }
};
