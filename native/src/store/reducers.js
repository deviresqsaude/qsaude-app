import { combineReducers } from 'redux';

// Importing Reducers
import auth from './Auth/reducer';
import home from './Home/reducer';
import alert from './Alert/reducer';
import hospital from './Hospital/reducer';
import schedule from './Schedule/reducer';
import care from './Care/reducer';
import quest from './Quest/reducer';
import loadSystem from './Loading/reducer';
import navSystem from './Navigation/reducer';

const storeReducers = combineReducers({
  auth,
  home,
  alert,
  hospital,
  schedule,
  care,
  quest,
  loadSystem,
  navSystem
});

export default storeReducers;
