export const types = {
  OPEN_ALERT: 'OPEN_ALERT',
  CLOSE_ALERT: 'CLOSE_ALERT',
};

export const actions = {
  openAlert: options => ({
    type: types.OPEN_ALERT,
    payload: options,
  }),

  closeAlert: () => ({
    type: types.CLOSE_ALERT,
  }),
};

// export default actions;
