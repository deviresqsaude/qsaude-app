import { types } from './actions';

const initialState = {
  visible: false,
  title: '–',
  description: '–',
  isError: false,
  isSuccess: false,
  shouldRenderOkButton: false,
  closeAlert: () => {},
  // actionsToTake: [
  //   {
  //     label: 'Fechar',
  //     onPress: () => console.log(),

  //   }
  // ]
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.OPEN_ALERT:
      return {
        visible: true,
        shouldRenderOkButton: false,
        ...payload,
      };
    case types.CLOSE_ALERT:
      return {
        ...initialState,
        visible: false,
      };
    default:
      return { ...state };
  }
};
