import { types } from './actions';

const initialState = {
  visible: false,
  description: 'Carregando, aguarde um momento'
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.OPEN_LOADING:
      return {
        visible: true,
        ...payload,
      };
    case types.CLOSE_LOADING:
      return {
        ...initialState
      };
    default:
      return { ...state };
  }
};
