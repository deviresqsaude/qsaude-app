export const types = {
  OPEN_LOADING: 'OPEN_LOADING',
  CLOSE_LOADING: 'CLOSE_LOADING',
};

export const actions = {
  openLoading: options => ({
    type: types.OPEN_LOADING,
    payload: options,
  }),

  closeLoading: () => ({
    type: types.CLOSE_LOADING,
  }),
};
