export const types = {
  SET_APPOINMENTS: "SET_APPOINMENTS",
  SET_CURRENT_APPOINMENT: "SET_CURRENT_APPOINMENT",
  SET_EVENTS: "SET_EVENTS",
  SET_CURRENT_EVENT: "SET_CURRENT_EVENT",
  CLEAR_EVENTS: "CLEAR_EVENTS",
  CLEAR_APPOINMENTS: "CLEAR_APPOINMENTS",
  SET_MARKET_EVENTS: "SET_MARKET_EVENTS",
  UPDATE_MARKET_EVENTS: "UPDATE_MARKET_EVENTS",
  UPDATE_EVENTS: "UPDATE_EVENTS",
  SET_EXAMS: "SET_EXAMS",
  UPDATE_EXAMS: "UPDATE_EXAMS",
  SET_DATE: "SET_DATE",
};

export const actions = {
  setDate: date => ({
    type: types.SET_DATE,
    payload: date
  }),
  setAppoinments: appoinments => ({
    type: types.SET_APPOINMENTS,
    payload: appoinments
  }),
  setExams: exams => ({
    type: types.SET_EXAMS,
    payload: exams
  }),
  setCurrentAppoinment: appoinment => ({
    type: types.SET_CURRENT_APPOINMENT,
    payload: appoinment
  }),
  setEvents: events => ({
    type: types.SET_EVENTS,
    payload: events
  }),
  setCurrentEvent: event => ({
    type: types.SET_CURRENT_EVENT,
    payload: event
  }),
  setMarketEvents: events => ({
    type: types.SET_MARKET_EVENTS,
    payload: events
  }),
  upMarketEvents: events => ({
    type: types.UPDATE_MARKET_EVENTS,
    payload: events
  }),
  updateEvents: event => ({
    type: types.UPDATE_EVENTS,
    payload: event
  }),
  updateExams: exams => ({
    type: types.UPDATE_EXAMS,
    payload: exams,
  }),
  clearEvents: () => ({
    type: types.CLEAR_EVENTS
  }),
  clearAppoinments: () => ({
    type: types.CLEAR_APPOINMENTS
  }),
}
