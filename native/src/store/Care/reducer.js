import { types } from './action';
import R from 'ramda';

const initialState = {
  appoinments: [],
  currentAppoinment: {},
  events: [],
  currentEvent: {},
  marketEvents: {},
  exams: [],
  date: ""
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.SET_DATE:
      return { ...state, date: payload };
    case types.SET_APPOINMENTS:
      return { ...state, appoinments: [...payload] };
    case types.SET_EXAMS:
      return { ...state, exams: [...payload] };
    case types.UPDATE_EXAMS:
      return { ...state, exams: [...state.exams, ...payload]}
    case types.SET_CURRENT_APPOINMENT:
      return { ...state, currentAppoinment: payload };
    case types.SET_EVENTS:
      return { ...state, events: payload };
    case types.UPDATE_EVENT:
        return { ...state, events: [...payload] };
    case types.SET_CURRENT_EVENT:
      return { ...state, currentEvent: payload };
    case types.SET_MARKET_EVENTS:
      return { ...state, marketEvents: {...payload} };
    case types.UPDATE_EVENTS:
      return { ...state, events: R.uniq([...state.events, ...payload]) };
    case types.UPDATE_MARKET_EVENTS:
      return { ...state, marketEvents: {...state.marketEvents, ...payload} };
    case types.CLEAR_EVENTS:
      return { ...state, ...initialState};
    case types.CLEAR_APPOINMENTS:
      return { ...state, ...initialState};
    default:
      return { ...state };
  }
};
