export const types = {
  SET_CURRENT_PAGE: 'SET_CURRENT_PAGE',
};

export const actions = {
  setCurrentPage: page => ({
    type: types.SET_CURRENT_PAGE,
    payload: page,
  }),
};
