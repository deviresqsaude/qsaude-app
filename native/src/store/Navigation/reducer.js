import { types } from './actions';

const initialState = {
  currentPage: {},
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.SET_CURRENT_PAGE:
      return {
        ...state, 
        currentPage: payload,
      };
    default:
      return { ...state };
  }
};
