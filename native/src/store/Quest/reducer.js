import R from 'ramda';
import { types } from "./action";

const initialState = {
  list: [],
  quest: {},
  questionPage: 0,
  guide: {},
  allGuides: [],
  questions: [],
  currentQuestion: {},
  answers: {},
  conditions: [],
  loading: false,
  questCompleted: []
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.QUESTS_LIST: return {...state, list: [...payload]}
    case types.QUESTION_PAGE: return {...state, questionPage: payload}
    case types.SET_ALL_GUIDES: return {...state, allGuides: [...payload]}
    case types.CURRENT_QUEST: return {...state, quest: {...payload}}
    case types.UPDATE_QUEST: return {...state, quest: {...payload}}
    case types.UPDATE_GUIDE: return {...state, allGuides: [...payload]}
    case types.CURRENT_GUIDE: return {...state, guide: {...payload}}
    case types.CURRENT_QUESTIONS_LIST: return {...state, questions: [...payload]}
    case types.CURRENT_QUESTION: return {...state, currentQuestion: {...payload}}
    case types.ADD_ANSWER: return {...state, answers: {...payload}}
    case types.REMOVE_ANSWER: return {...state, answers: [...payload]}
    case types.ADD_CONDITIONAL: return {...state, conditions: R.union(state.conditions, payload)}
    case types.REMOVE_CONDITIONAL: return {...state, conditional: [...payload]}
    case types.SET_LOADING: return {...state, loading: payload}
    case types.ADD_QUEST_COMPLETED: return {...initialState, questCompleted: [...payload]}
    case types.CLEAR_QUEST_STATE: return {...initialState}
    default: return { ...state };
  }
};
