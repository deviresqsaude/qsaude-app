export const types = {
  QUESTS_LIST: "QUESTS_LIST",
  QUESTION_PAGE: "QUESTION_PAGE",
  CURRENT_QUEST: "CURRENT_QUEST",
  CURRENT_GUIDE: "CURRENT_GUIDE",
  CURRENT_QUESTIONS_LIST: "CURRENT_QUESTIONS_LIST",
  CURRENT_QUESTION: "CURRENT_QUESTION",
  UPDATE_QUESTION: "UPDATE_QUESTION",
  UPDATE_GUIDE: "UPDATE_GUIDE",
  ADD_ANSWER: "ADD_ANSWER",
  REMOVE_ANSWER: "REMOVE_ANSWER",
  ADD_CONDITIONAL: "ADD_CONDITIONAL",
  REMOVE_CONDITIONAL: "REMOVE_CONDITIONAL",
  SET_LOADING: "SET_LOADING",
  UPDATE_QUEST: "UPDATE_QUEST",
  SET_ALL_GUIDES: "SET_ALL_GUIDES",
  CLEAR_QUEST_STATE: "CLEAR_QUEST_STATE",
  ADD_QUEST_COMPLETED: "ADD_QUEST_COMPLETED",
};

export const actions = {
  setQuestionPage: page => ({
    type: types.QUESTION_PAGE,
    payload: page
  }),
  setQuestsList: data => ({
    type: types.QUESTS_LIST,
    payload: [...data]
  }),
  setAllGuides: data => ({
    type: types.SET_ALL_GUIDES,
    payload: [...data]
  }),
  setCurrentQuest: data => ({
    type: types.CURRENT_QUEST,
    payload: data
  }),
  updateCurrentQuest: data => ({
    type: types.UPDATE_QUEST,
    payload: data
  }),
  setCurrentGuide: data => ({
    type: types.CURRENT_GUIDE,
    payload: data
  }),
  updateCurrentGuide: data => ({
    type: types.UPDATE_GUIDE,
    payload: data
  }),
  setCurrentQuestionsList: data => ({
    type: types.CURRENT_QUESTIONS_LIST,
    payload: data
  }),
  setCurrentQuestion: data => ({
    type: types.CURRENT_QUESTION,
    payload: data
  }),
  updateAnswers: data => ({
    type: types.ADD_ANSWER,
    payload: data
  }),
  removeAnswers: data => ({ // TODO: Verificar se será implementada essa funcao
    type: types.REMOVE_ANSWER,
    payload: data
  }),
  addConditional: data => ({
    type: types.ADD_CONDITIONAL,
    payload: data
  }),
  removeConditional: data => ({
    type: types.REMOVE_CONDITIONAL,
    payload: data
  }),
  setLoading: data => ({
    type: types.SET_LOADING,
    payload: data
  }),
  updateQuestCompleted: (data) => ({
    type: types.ADD_QUEST_COMPLETED,
    payload: data
  }),
  clearQuestState: () => ({
    type: types.CLEAR_QUEST_STATE
  }),
};
