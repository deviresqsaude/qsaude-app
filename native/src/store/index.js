import { createStore } from 'redux';
import storeReducers from './reducers';
import storeMiddlewares from './middlewares';
/**
 * Init Store
 * @param {Object} preloadState - it's not required
 */
const initStore = storePreloadState => createStore(
  storeReducers,
  storePreloadState || {},
  storeMiddlewares,
);

export default initStore;
