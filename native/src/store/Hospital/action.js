export const types = {
  SET_HOSPITALS: "SET_HOSPITALS",
};

export const actions = {
  setHospitals: hospitals => ({
    type: types.SET_HOSPITALS,
    payload: hospitals
  }),
};
