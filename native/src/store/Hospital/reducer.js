import { types } from "./action";

const initialState = {
  hospitals: []
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.SET_HOSPITALS:
      return {
        ...state,
        hospitals: payload,
      }
    default:
      return { ...state };
  }
};
