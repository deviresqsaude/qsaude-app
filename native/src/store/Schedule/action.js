export const types = {
  SET_ALL_DATA: 'SET_ALL_DATA',
  RESET_STATE: 'RESET_STATE',
  SET_SCHEDULE_MODE: 'SET_SCHEDULE_MODE',
  SET_SCHEDULE_STEP: 'SET_SCHEDULE_STEP',
  LOADING_SCHEDULE: 'LOADING_SCHEDULE',
  SET_CURRENT_DOCTOR: 'SET_CURRENT_DOCTOR',
  SET_AVAILABLE_DAYS: 'SET_AVAILABLE_DAYS',
  SET_AVAILABLE_SLOTS: 'SET_AVAILABLE_SLOTS',
  SET_CURRENT_DAY: 'SET_CURRENT_DAY',
  SET_CURRENT_SLOT: 'SET_CURRENT_SLOT',
  SET_FETCHING: 'SET_FETCHING',
  ADD_LOADED_MONTHS: 'ADD_LOADED_MONTHS',
  CLEAR_ALL_CALENDAR: 'CLEAR_ALL_CALENDAR',
  UPDATE_PERIOD: 'UPDATE_PERIOD',
  CURRENT_COMPANY: 'CURRENT_COMPANY',
  CURRENT_UNIT: 'CURRENT_UNIT',
  UNITS_LIST: 'UNITS_LIST',
  SET_ADDRESS: 'SET_ADDRESS',
};

export const actions = {
  setAddress: address => ({
    type: types.SET_ADDRESS,
    payload: {...address}
  }),
  setUnits: units => ({
    type: types.UNITS_LIST,
    payload: units
  }),
  setCurrentUnit: unit => ({
    type: types.CURRENT_UNIT,
    payload: unit
  }),
  setCurrentCompany: company => ({
    type: types.CURRENT_COMPANY,
    payload: company
  }),
  updatePeriod: period => ({
    type: types.UPDATE_PERIOD,
    payload: period
  }),
  setAllData: response => ({
    type: types.SET_ALL_DATA,
    payload: response
  }),
  resetState: () => ({
    type: types.RESET_STATE
  }),
  setScheduleMode: mode => ({
    type: types.SET_SCHEDULE_MODE,
    payload: mode
  }),
  setScheduleStep: step => ({
    type: types.SET_SCHEDULE_STEP,
    payload: step
  }),
  setLoading: loading => ({
    type: types.LOADING_SCHEDULE,
    payload: loading
  }),
  setDoctor: doctor => ({
    type: types.SET_CURRENT_DOCTOR,
    payload: doctor
  }),
  setAvailableDays: days => ({
    type: types.SET_AVAILABLE_DAYS,
    payload: days
  }),
  setAvailableSlots: slots => ({
    type: types.SET_AVAILABLE_SLOTS,
    payload: slots
  }),
  setCurrentDay: day => ({
    type: types.SET_CURRENT_DAY,
    payload: day
  }),
  setCurrentSlot: slot => ({
    type: types.SET_CURRENT_SLOT,
    payload: slot
  }),
  setFetching: fetch => ({
    type: types.SET_FETCHING,
    payload: fetch
  }),
  addMonths: month => ({
    type: types.ADD_LOADED_MONTHS,
    payload: month
  }),
  clearCalendar: () => ({
    type: types.CLEAR_ALL_CALENDAR
  }),
};
