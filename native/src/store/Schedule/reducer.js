import moment from 'moment';
import { types } from './action';

const initialState = {
  allData: [],
  mode: null,
  step: 0,
  loading: false,
  doctor: null,
  availableDays: null,
  availableSlots: null,
  currentDay: moment().format('YYYY-MM-DD'),
  currentSlot: null,
  isFeching: false,
  months: [],
  schedulePeriod: {},
  startPeriod: false,
  currentCompany: null,
  unitsList:  [],
  currentUnit: null,
  address: {}
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.SET_ADDRESS:
      return { ...state, address: {...payload} };
    case types.UNITS_LIST:
      return { ...state, unitsList: payload };
    case types.CURRENT_UNIT:
      return { ...state, currentUnit: payload };
    case types.CURRENT_COMPANY:
      return { ...state, currentCompany: payload };
    case types.UPDATE_PERIOD:
      return { ...state, schedulePeriod: {...payload} };
    case types.SET_ALL_DATA:
      return { ...state, allData: [...state.allData, ...payload] };
    case types.RESET_STATE:
      return { ...initialState };
    case types.SET_SCHEDULE_MODE:
      return { ...state, mode: payload };
    case types.SET_SCHEDULE_STEP:
      return { ...state, step: payload };
    case types.LOADING_SCHEDULE:
      return { ...state, loading: payload };
    case types.SET_CURRENT_DOCTOR:
      return { ...state, doctor: {...payload} };
    case types.SET_AVAILABLE_DAYS:
      return { ...state, availableDays: payload};
    case types.SET_AVAILABLE_SLOTS:
      return { ...state, availableSlots: payload};
    case types.SET_CURRENT_DAY:
      return { ...state, currentDay: payload};
    case types.SET_CURRENT_SLOT:
      return { ...state, currentSlot: payload};
    case types.SET_FETCHING:
      return { ...state, isFeching: payload};
    case types.ADD_LOADED_MONTHS:
      return { ...state, months: [...payload]};
    case types.CLEAR_ALL_CALENDAR:
      return { ...state, ...{
        availableDays: null,
        availableSlots: null,
        currentDay: moment().format('YYYY-MM-DD'),
        currentSlot: null,
        isFeching: false,
        months: [],
        allData: []
      }};
    default:
      return { ...state };
  }
};
