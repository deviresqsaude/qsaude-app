import { compose } from "redux";

const enhancerList = [];
let devToolsExtension = compose;

if (__DEV__) {
  devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

if (__DEV__ && typeof devToolsExtension === "function") {
  enhancerList.push(devToolsExtension());
}

const storeMiddleware = compose(...enhancerList);

export default storeMiddleware;
