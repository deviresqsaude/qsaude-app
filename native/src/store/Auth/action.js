export const types = {
  USER_PROFILE: "USER_PROFILE",
  AUTH_LOGIN: "AUTH_LOGIN",
  AUTH_LOGOUT: "AUTH_LOGOUT",
  AUTH_RESET_PASSWORD: "AUTH_RESET_PASSWORD",
  CLEAR_USER_PROFILE: "CLEAR_USER_PROFILE"
};

export const actions = {
  setUserProfile: profile => ({
    type: types.USER_PROFILE,
    payload: profile
  }),
  insertCredentials: token => ({
    type: types.AUTH_LOGIN,
    token
  }),
  removeCredentials: () => ({
    type: types.AUTH_LOGOUT
  }),
  resetPassword: () => {},
  clearUserProfile: () => ({
    type: types.CLEAR_USER_PROFILE,
  })
};
