import { types } from "./action";

const initialState = {
  token: null,
  userProfile: {
    // beneficiary: null
  }
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.USER_PROFILE:
      return {
        ...state,
        userProfile: {...state.userProfile, ...payload},
      }
    case types.AUTH_LOGIN:
      return { ...state, ...payload };
    case types.CLEAR_USER_PROFILE:
      return { ...initialState };
    default:
      return { ...state };
  }
};
