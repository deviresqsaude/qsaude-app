import R from 'ramda';
import SyncStorage from 'sync-storage';
import { Platform, Linking, Alert } from 'react-native';
import moment from 'moment';
import Config from 'react-native-config';
import MCReactModule from 'react-native-marketingcloudsdk';

export const findServerError = (errors=[]) => {
  let e = null;
  // eslint-disable-next-line no-restricted-syntax
  for (const error of errors) {
    const {extensions: {response}} = error;
    if (response && response.status === 404) {
      const {body} = response;
      if (body) {
        const {critics} = body;
        e = critics ? critics[0].code : null;
      }
    };
  };
  return e;
};

// export const hasMultiHealthCard = id => id.length === 11;
export const hasMultiHealthCard = (id, numCard) => id.length === 11 && numCard.length > 1;

export const queryFactory = (client, query, token, variables={}) => {
  return client.query({
    query,
    variables,
    context: {headers: { authorization: token }}
  });
};

export const getUserAge = birthday => moment().diff(birthday, 'years');

// eslint-disable-next-line radix
export const itsLegalAge = birthday => parseInt(getUserAge(birthday)) >= 18;

/**
 *
 * @param {String} personalId Cpf do usuario
 */
export const formatPersonalId = personalId => R.insert(3, '.', R.insert(6, '.', R.insert(9, '-', personalId))).join('');

/**
 * Login factory
 * @param {*} loginData
 */
export const loginFactory = async (values, deviceId, loginData, BeneficiaryAndDependents, PlanCardQuery, client) => {
  // first get token with numcards
  // const { login: { token, personalId, numCard } } = loginData; // Old Login
  const { loginV2: { token, personalId, numCard } } = loginData; // New Login
  const firstNumCard = numCard[0]; // * Select the first NumCard from List

  // Setting Salesforce Module into Reaqct
  const id = formatPersonalId(personalId);
  await MCReactModule.setContactKey(id);

  // Get User Profile to set into the cache
  const { data: {beneficiary, myDependents} } = await queryFactory(client, BeneficiaryAndDependents, token);

  // Get User Health plan Card
  const { data: {getHealthPlanCard: healthCard } } = await queryFactory(client, PlanCardQuery, token);

  const owner = R.omit(['dependents'], healthCard);
  const dependents = R.map(d => R.merge(d, R.pick(['segmentPlan', 'startingDate', 'healthPlanCode'], owner)), R.prop('dependents', healthCard) || []);

  // Get tipo de Beneficiario Dependente
  const user = R.find(({numCard: nc}) => R.includes(nc, [...numCard]) , beneficiary);
  // const user = R.find(R.propEq('numCard', firstNumCard))(beneficiary);

  const profileUser = {...user, segmentPlan: healthCard.segmentPlan, companyID: healthCard.companyID, legalAge: itsLegalAge(user.birthday), dependents: myDependents || [], owner: true};

  const fullBeneficiary = [{...owner, ...user}, ...dependents];

  return hasMultiHealthCard(values.user, numCard) ?
  {link: "MultiHealthCard", params: {token, personalId, profileUser, firstNumCard, beneficiary: fullBeneficiary, ...values, deviceId}} :
  {link: "TermsOfUse", params: {token, personalId, profileUser, healthCardNumber: numCard, beneficiary: fullBeneficiary}}
};

export const setAsyncStorage = ({ profileUser, healthCardNumber, token, user, beneficiary}) => {
  const isFirstAccess = SyncStorage.get(`isFirstAccess.${profileUser.cpf}`);
  SyncStorage.set('healthCard', healthCardNumber);
  SyncStorage.set('profileUser', profileUser);
  SyncStorage.set('dependents', profileUser.dependents || []);
  SyncStorage.set('token', token);
  SyncStorage.set('allHelathCards', beneficiary);
  SyncStorage.set('@form[login]:user', user);
  if (!isFirstAccess) {
    SyncStorage.set(`Tutorial.${profileUser.cpf}.Care`, true);
    SyncStorage.set(`Tutorial.${profileUser.cpf}.Health`, true);
    SyncStorage.set(`Tutorial.${profileUser.cpf}.CustomerCare`, true);
    SyncStorage.set(`Tutorial.${profileUser.cpf}.Account`, true);
    SyncStorage.set(`isFirstAccess.${profileUser.cpf}`, true);
  }
};

export const arrayGenerator = quantity => Array.from(Array(quantity), (item,key) => key + 1);

export const callEmergencyNumber = () => {
  const phone = `${Platform.OS !== 'android' ? 'telprompt' : 'tel'}:${Config.EMERGENCY_NUMBER}`;
  Linking.canOpenURL(phone)
  .then(success => { return success ? Linking.openURL(phone) :false; })
  .catch(e => {
    Alert.alert(
      'Atendimento',
      `Aconteceu um erro na ligação. Tente novamente mais tarde. ${e}`,
      [{text: 'OK', onPress: () => {}}]
    )
  });
};
/**
 * Lista todas as datas entre duas datas
 * @param {String} start
 * @param {Sting} end
 */
export const getBetweenDates = (start, end) => {
  const inverted = moment(start) > moment(end);
  let currentDate = null;
  let lastDate = null;
  let datesList = [];
  currentDate = moment(inverted ? end : start).add(1, "day");
  lastDate = moment(inverted ? start : end);
  while (currentDate < lastDate) {
    datesList = R.append(currentDate.format('YYYY-MM-DD'), datesList);
    currentDate = currentDate.add(1, 'day');
  };
  return datesList;
};

export const gerRandomNumber = () => {
  return Math.floor(Math.random() * 1000) + 1000;
};

/**
 *
 * @param {Object} googleAddress
 */

export const formataGoogleAddress = address => {
  if (!address) return null;
  // eslint-disable-next-line camelcase
  const {address_components} = address;
  const data = {
    address: R.prop('long_name', R.find(i => R.includes("route", i.types))(address_components)),
    neighborhood: R.prop('long_name', R.find(i => R.includes("sublocality", i.types))(address_components)),
    state: R.prop('long_name', R.find(i => R.includes("administrative_area_level_2", i.types))(address_components)),
    city: R.prop('long_name', R.find(i => R.includes("administrative_area_level_1", i.types))(address_components))
  };
  return data;
}
/**
 *
 * @param {String} address
 */
export const geolocation = (address="") => {
  // eslint-disable-next-line no-undef
  return fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${Config.GMAPS_API_KEY}`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  })
  .then(res => res.json())
  .then(resJson => {
    return resJson.status === "OK" ?
      formataGoogleAddress(R.last(resJson.results)) :
      {error: true, message: "Endereço não encontrado"};
  })
  .catch(e => ({error: true, message: e}));
}

/**
 *
 * @param {String} color
 * @param {Number} elevation
 * @param {Number} opacity
 */
export const elevationShadow = (color, elevation=5, opacity, shadowRadius) => {
  let minElevation = 5;
  if(shadowRadius) {
    minElevation = 20;
  }
  return {
    elevation: elevation < 5 ? minElevation : elevation,
    shadowColor: color || 'black',
    shadowOffset: {width: 0, height: 0.5 * elevation},
    shadowOpacity: opacity || 0.1,
    shadowRadius: .8 * elevation || shadowRadius
  }
}
/**
 *
 * @param {Number} weight
 * @param {Number} height
 */
export const calculateImc = (weight, height) => {
  return Number(weight) / (Number(height) / 100) ** 2;
}
/**
 *
 * @param {String} val
 */
export const replacePoint = (val) => {
  if(typeof val === "string") return R.replace(/\__/g, '.', val.toString());
  return val;
}
/**
 *
 * @param {Array} rawQuestions
 */
export const parseQuestQuestions = rawQuestions => {
  const questions = R.map(rawQuestion => {
    const typeOfQuestion = rawQuestion.type;
    switch (typeOfQuestion) {
      case "integer":
        return {
          linkId: replacePoint(rawQuestion.name),
          answer: R.map(v => ({ valueInteger: replacePoint(v) }), rawQuestion.val),
        };
      case "decimal":
        return {
          linkId: replacePoint(rawQuestion.name),
          answer: R.map(v => ({ valueDecimal: replacePoint(v) }), rawQuestion.val),
        };
      case "check":
      case "choice":
      case "openChoice":
        return {
          linkId: replacePoint(rawQuestion.name),
          answer: R.map(
            code => ({
              valueCoding: {
                code: replacePoint(code),
                system: "https://api-homologa.prontlife.com.br",
              },
            }),
            rawQuestion.val,
          ),
        };
      case "string":
        return {
          linkId: replacePoint(rawQuestion.name),
          answer: R.map(v => ({ valueString: replacePoint(v) }), rawQuestion.val),
        };
      default:
        return {};
    }
  }, rawQuestions);
  return questions;
};
/**
 *
 * @param {Number} ms
 * @param {Function} promise
 */
export const timeoutPromise = (ms, promise) => {
  const timeout = new Promise((reject) => {
    // eslint-disable-next-line prefer-promise-reject-errors
    setTimeout(() => reject(`Tempo de espera excedido, espere um momento e tente novamente`), ms);
  });

  return Promise.race({
    promise,
    timeout
  });
}

// dots color
export const dots = {
  medical: {key:'medical', color: '#6e56ed', selectedDotColor: '#6e56ed'},
  exam: {key:'exam', color: '#ff9d66', selectedDotColor: '#ff9d66'},
}
