import { responsiveFontSize } from 'react-native-responsive-dimensions';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Platform } from 'react-native';

export const Colors = {
  Silver: "#e4e7e8",
  Purple: '#6e56ed',
  PurpleDisabled: '#c6c1f8',
  Pink: '#B113EB',
  Rose: '#E75BFC',
  Magenta: '#E51766',
  MagentaDisabled: '#f9aac3',
  LightGreen: '#1BA39C',
  GreenSuccess: "#5cdbe5",
  SelectedGreen: '#83d4db',
  YellowishGreen: '#c8cd25',
  Red: '#ff6679',
  WineRed: '#b20d15',
  Ochre: '#e9bc00',
  Black: '#000000',
  Gray: '#8e8e93',
  LabelGray: '#525252',
  LightGray: '#e8e8e8',
  White: '#fff',
  Mortar: '#525252',
  WhiteSmoke: '#EBEBEB',
  Light: '#ffffff',
  Empress: '#757575',
  LightSmoke: '#f8f8f8',
  TextGray: '#7f7f7f',
  DataLabel: '#858585',
  ApriCor: '#ff9d66',
};

export const Spacing = {
  tabbarHeight: Platform.OS === 'ios' && getStatusBarHeight() > 24 ? 80 : 56,
  tabbarPadding: Platform.OS === 'ios' && getStatusBarHeight() > 24 ? 24 : 0
};

export const Dots = {
  styleDot: {borderWidth: 1, borderColor: Colors.Purple, width: 12, height: 12,borderRadius: 6, marginLeft: 3, marginRight: 3, marginTop: 8, marginBottom: 3},
  styleActiveDot: {backgroundColor: Colors.Purple, width: 12, height: 12,borderRadius: 6, marginLeft: 3, marginRight: 3, marginTop: 8, marginBottom: 3}
};

export const Sizing = {
  xxlarge: responsiveFontSize(2.8),
  xlarge: responsiveFontSize(2.6),
  medium: responsiveFontSize(2.3),
  small: responsiveFontSize(2),
  xsmall: responsiveFontSize(1.6),
  xxsmall: responsiveFontSize(1.2),
};

export const HeaderStyle = {
  height: 92,
  justifyContent: 'space-between',
  shadowOpacity: 0,
  elevation: 0,
  borderBottomWidth: 0,
};

export const HeaderSimpleStyle = {
  // height: 72,
  shadowOpacity: 0,
  elevation: 0,
  borderBottomWidth: 0,
};

export const HeaderBackStyle = {
  height: 32,
  // alignItems: 'flex-start',
  shadowOpacity: 0,
  elevation: 0,
  borderBottomWidth: 0
};

export const SystemFonts = {
  MonserratBold: 'Montserrat-Bold',
  MonserratSemiBold: 'Montserrat-SemiBold',
  MonserratRegular: 'Montserrat-Regular',
  RubikLigth: 'Rubik-Light',
  RubikMedium: 'Rubik-Medium',
  RubikRegular: 'Rubik-Regular',
  PlexSansBold: 'IBMPlexSans-Bold',
  PlexSansLight: 'IBMPlexSans-Light',
  PlexSansMedium: 'IBMPlexSans-Medium',
  PlexSansRegular: 'IBMPlexSans',
};

export const SystemIcons = {
  teleconsulta: require('../../assets/icons/qsaude/ic_teleconsulta.png'),
  schedule: require('../../assets/icons/qsaude/ic_schedule.png'),
  schedule_active: require('../../assets/icons/qsaude/ic_schedule_active.png'),
  email: require('../../assets/icons/qsaude/ic_mail.png'),
  errorMenuIcon: require('../../assets/icons/qsaude/ic_error_quest.png'),
  successMenuIcon: require('../../assets/icons/qsaude/ic_success_quest.png'),
  emptyMenuIcon: require('../../assets/icons/qsaude/ic_empty_quest.png'),
  vitalBlood: require('../../assets/icons/qsaude/ic_vital_blood.png'),
  vitalWeight: require('../../assets/icons/qsaude/ic_vital_weigth.png'),
  vitalHeight: require('../../assets/icons/qsaude/ic_vital_ruler.png'),
  vitalImc: require('../../assets/icons/qsaude/ic_vital_imc.png'),
  vitalPressure: require('../../assets/icons/qsaude/ic_vital_heart.png'),
  vitalGlicemia: require('../../assets/icons/qsaude/ic_vital_doctor.png'),
  map: require('../../assets/icons/qsaude/ic_map.png'),
  crm: require('../../assets/icons/qsaude/ic_crm_medico.png'),
  unidade: require('../../assets/icons/qsaude/ic_unidade.png'),
  manual: require('../../assets/icons/qsaude/ic_manual.png'),
  manual2: require('../../assets/icons/qsaude/ic_manual_2.png'),
  openEye: require('../../assets/icons/custom/ic_eye_opn.png'),
  closeEye: require('../../assets/icons/custom/ic_eye_clsd.png'),
  siren: require('../../assets/icons/qsaude/ic_siren.png'),
  close: require('../../assets/icons/qsaude/ic_close.png'),
  check: require('../../assets/icons/qsaude/ic_check.png'),
  startOutline: require('../../assets/icons/qsaude/star_outline.png'),
  startFill: require('../../assets/icons/qsaude/star_fill.png'),
  phone: require('../../assets/icons/custom/ic_phone.png'),
  phone_ring: require('../../assets/icons/custom/ic_phone_ring.png'),
  health: require('../../assets/icons/qsaude/ic_heart.png'),
  health_active: require('../../assets/icons/qsaude/ic_heart_active.png'),
  customerCare: require('../../assets/icons/qsaude/ic_chat.png'),
  customerCare_active: require('../../assets/icons/qsaude/ic_chat_active.png'),
  account: require('../../assets/icons/qsaude/ic_profile.png'),
  account_active: require('../../assets/icons/qsaude/ic_profile_active.png'),
  chevromRight: require('../../assets/icons/atomic/ic_chevron_right.png'),
  chevromLeft: require('../../assets/icons/atomic/ic_chevron_left.png'),
  chevromDown: require('../../assets/icons/atomic/ic_chevron_down.png'),
  navBarBackIcon: require('../../assets/icons/nav_bar/ic_back.png'),
  lock: require('../../assets/icons/qsaude/ic_lock.png'),
  healthCard: require('../../assets/icons/custom/ic_health_card.png'),
  identification: require('../../assets/icons/qsaude/ic_id.png'),
  // estetoscopio: require('../../assets/icons/custom/ic_estetoscopio.png'),
  share: require('../../assets/icons/qsaude/ic_share.png'),
  // payments: require('../../assets/icons/custom/ic_estetoscopio.png'),
  // notifications: require('../../assets/icons/custom/ic_estetoscopio.png'),
  doctor: require('../../assets/icons/qsaude/ic_doctor.png'),
  lab: require('../../assets/icons/qsaude/ic_lab.png'),
  whatsapp: require('../../assets/icons/qsaude/ic_whatsapp.png'),
  search: require('../../assets/icons/qsaude/ic_search.png'),
  document: require('../../assets/icons/qsaude/ic_document.png'),
  ficha: require('../../assets/icons/qsaude/ic_ficha.png'),
  medicine: require('../../assets/icons/qsaude/ic_medicine.png'),
  calendar: require('../../assets/icons/qsaude/ic_calendar.png'),
  orientation: require('../../assets/icons/qsaude/ic_healt_chat.png'),
  navigation: require('../../assets/icons/custom/ic_navigation.png'),
  summary: require('../../assets/icons/custom/ic_summary.png'),
  webLink: require('../../assets/icons/custom/ic_web.png'),
  payment: require('../../assets/icons/qsaude/ic_payments.png'),
  ellipsis: require('../../assets/icons/qsaude/ic_ellipsis.png'),
  telecuidado: require('../../assets/icons/qsaude/ic_telecuidado.png'),
  maternity: require('../../assets/icons/qsaude/ic_maternity.png'),
  drugBox: require('../../assets/icons/qsaude/ic_drug_box.png'),
  hospitalize: require('../../assets/icons/qsaude/ic_hospitalize.png'),
  chatEmergency: require('../../assets/icons/qsaude/ic_orientation.png'),
  web: require('../../assets/icons/custom/ic_web.png'),
  dataPaper: require('../../assets/icons/qsaude/ic_data.png'),
};

export const SystemImages = {
  emptyAvatar: require('./../../assets/images/avatar.jpg'),
  sessionTime: require('./../../assets/images/img_time.gif'),
  errorImage: require('./../../assets/images/img_error_message.png'),
  successImage: require('./../../assets/images/img_success_message.png'),
  sirenImage: require('./../../assets/images/img_siren.png'),
  qsaudeMini: require('./../../assets/images/img_qsaude_mini.png'),
  imgRecord: require('./../../assets/images/img_record.png'),
  offline: require('./../../assets/images/img_offline.png'),
  localizationOffline: require('./../../assets/images/img_localization_offline.png'),
  passwordCheck: require('./../../assets/images/img_password_check.png'),
  waitingCall: require('./../../assets/images/gif_call_waiting.gif'),
  calendarEmpty: require('./../../assets/images/ic_experts_calendar.png'),
  emptyBoleto: require('./../../assets/images/img_empty_boleto.png'),
  copySuccess: require('./../../assets/images/img_copy_success.png'),
  question: require('./../../assets/images/question.png'),
  blockedCall: require('./../../assets/images/img_blocked_call.png'),
  warningCall: require('./../../assets/images/gif_call_warning.gif'),
  finishCall: require('./../../assets/images/gif_call_finish.gif'),
};

export const TutorialImages = {
  Care: {
    calendar: require('./../../assets/images/imgTutorialCalendar.png'),
    users: require('./../../assets/images/imgTrocaUsuario.png'),
    telemedicine: require('./../../assets/images/imgTutorialTelecuidado.png'),
  },
  Health: {
    questionary: require('./../../assets/images/imgTutorialQuestionario.png'),
    healthPlan: require('./../../assets/images/imgTutorialFichaMedica.png'),
    drugs: require('./../../assets/images/imgTutorialMedicamentos.png'),
    orientation: require('./../../assets/images/imgTutorialOrientacao.png'),
  },
  CustomerCare: {
    schedule: require('./../../assets/images/imgTutorialAgendamento.png'),
    timeQ: require('./../../assets/images/imgTutorialTimeQ.png'),
    redeQ: require('./../../assets/images/imgTutorialRedeQ.png'),
  },
  Account: {
    personalData: require('./../../assets/images/imgTutorialAlteracaoDados.png'),
    medicalCard: require('./../../assets/images/imgTutorialCarterinha.png'),
    manual: require('./../../assets/images/imgTutorialManual.png'),
  }
}