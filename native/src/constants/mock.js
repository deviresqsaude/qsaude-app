import { SystemIcons } from './theme';
import { BOTTOMTABBAR, PROFILECUSTOMERCARE } from './TestIds';

export const companyGenerator = id => {
  switch (id) {
    case "003":
      return "QUALICORP"
    default:
      return "OTHERS"
  }
}

export const COMPANY_UNITS = [
  {
    name: "Hospital Alemão Oswaldo Cruz",
    id: "abcfjs192kd",
    units: [
      {
        id: "units_zsd_221",
        name: "DAVITA - UNIDADE INDIANÓPOLIS",
        address: "AV. INDIANÓPOLIS, 667 - 04062-001 - SÃO PAULO SP"
      },
      {
        id: "units_zsd_222",
        name: "DAVITA - UNIDADE PAULISTA",
        address: "PRAÇA OSWALDO CRUZ, 109 - 04004-030 - PARAÍSO, SÃO PAULO SP"
      },
      {
        id: "units_zsd_223",
        name: "DAVITA - UNIDADE SANTO AMARO",
        address: "Alameda Santo Amaro, 379 - 04745-000 - Santo AmarO, SÃO PAULO SP"
      }
    ]
  },
  {
    name: "Davita",
    id: "abcfjs192ks",
    units: [
      {
        id: "units_zsd_224",
        name: "DAVITA - UNIDADE INDIANÓPOLIS",
        address: "AV. INDIANÓPOLIS, 667 - 04062-001 - SÃO PAULO SP"
      },
      {
        id: "units_zsd_225",
        name: "DAVITA - UNIDADE PAULISTA",
        address: "PRAÇA OSWALDO CRUZ, 109 - 04004-030 - PARAÍSO, SÃO PAULO SP"
      }
    ]
  }
]

export const slotsMock = [
  {
    start: '2019-12-25T10:00:00.000-03:00',
    end: '2019-12-25T10:30:00.000-03:00',
    id: '70103-251220191000-251220191030',
    __typename: 'MyDoctorSlot'
  },
  {
    start: '2019-12-25T10:30:00.000-03:00',
    end: '2019-12-25T11:00:00.000-03:00',
    id: '70103-251220191030-251220191100',
    __typename: 'MyDoctorSlot'
  },
  {
    start: '2019-12-25T11:00:00.000-03:00',
    end: '2019-12-25T11:30:00.000-03:00',
    id: '70103-251220191100-251220191130',
    __typename: 'MyDoctorSlot'
  },
  {
    start: '2019-12-25T11:30:00.000-03:00',
    end: '2019-12-25T12:00:00.000-03:00',
    id: '70103-251220191130-251220191200',
    __typename: 'MyDoctorSlot'
  },
  {
    start: '2019-12-25T13:00:00.000-03:00',
    end: '2019-12-25T13:30:00.000-03:00',
    id: '70103-251220191300-251220191330',
    __typename: 'MyDoctorSlot'
  },
  {
    start: '2019-12-25T14:00:00.000-03:00',
    end: '2019-12-25T14:30:00.000-03:00',
    id: '70103-251220191400-251220191430',
    __typename: 'MyDoctorSlot'
  },
  {
    start: '2019-12-25T14:30:00.000-03:00',
    end: '2019-12-25T15:00:00.000-03:00',
    id: '70103-251220191430-251220191500',
    __typename: 'MyDoctorSlot'
  },
  {
    start: '2019-12-25T15:00:00.000-03:00',
    end: '2019-12-25T15:30:00.000-03:00',
    id: '70103-251220191500-251220191530',
    __typename: 'MyDoctorSlot'
  },
  {
    start: '2019-12-25T15:30:00.000-03:00',
    end: '2019-12-25T16:00:00.000-03:00',
    id: '70103-251220191530-251220191600',
    __typename: 'MyDoctorSlot'
  },
  {
    start: '2019-12-25T16:00:00.000-03:00',
    end: '2019-12-25T16:30:00.000-03:00',
    id: '70103-251220191600-251220191630',
    __typename: 'MyDoctorSlot'
  },
  {
    start: '2019-12-25T16:30:00.000-03:00',
    end: '2019-12-25T17:00:00.000-03:00',
    id: '70103-251220191630-251220191700',
    __typename: 'MyDoctorSlot'
  },
  {
    start: '2019-12-25T17:00:00.000-03:00',
    end: '2019-12-25T17:30:00.000-03:00',
    id: '70103-251220191700-251220191730',
    __typename: 'MyDoctorSlot'
  },
  {
    start: '2019-12-25T18:30:00.000-03:00',
    end: '2019-12-25T19:00:00.000-03:00',
    id: '70103-251220191830-251220191900',
    __typename: 'MyDoctorSlot'
  },
  {
    start: '2019-12-25T19:00:00.000-03:00',
    end: '2019-12-25T19:30:00.000-03:00',
    id: '70103-251220191900-251220191930',
    __typename: 'MyDoctorSlot'
  },
  {
    start: '2019-12-25T19:30:00.000-03:00',
    end: '2019-12-25T20:00:00.000-03:00',
    id: '70103-251220191930-251220192000',
    __typename: 'MyDoctorSlot'
  }
];

export const CARDS_GRADIENT_COLORS = [
  { start: '#6E56ED', end: '#8257F0' },
  { start: '#8257F0', end: '#9658F2' },
  { start: '#9658F2', end: '#AB59F5' },
  { start: '#AB59F5', end: '#BF59F7' },
  { start: '#BF59F7', end: '#E75BFC' },
  { start: '#BF59F7', end: '#E75BFC' },
  { start: '#E75BFC', end: '#fc5bf7' },
  { start: '#fc5bf7', end: '#fc5be1' },
  { start: '#fc5be1', end: '#fc5bb4' },
];

export const USER_PROFILE_NAME = 'Qsaude User';
export const USER_PROFILE_AVATAR = require('./../../assets/images/avatar.jpg');

export const PARENTS_LIST = [
  {
    avatar: USER_PROFILE_AVATAR,
    title: USER_PROFILE_NAME,
    subtitle: USER_PROFILE_NAME,
  },
  {
    avatar: USER_PROFILE_AVATAR,
    title: USER_PROFILE_NAME,
    subtitle: USER_PROFILE_NAME,
  },
];

export const HEALTH_MENU = [
  {
    icon: SystemIcons.document,
    title: 'Questionários',
    link: 'Questionnaire',
    testID: BOTTOMTABBAR.tab.health.itemQuestionario,
    // size: { height: 32, width: 34},
    legalAge: true
  },
  // { icon: SystemIcons.lock, title: "Dicas de saude", link: "HealthTips" },
  {
    icon: SystemIcons.ficha,
    title: 'Ficha médica',
    link: 'MedicalRecord',
    testID: BOTTOMTABBAR.tab.health.itemFichaMedica,
    // size: { height: 27, width: 34}
  },
  {
    icon: SystemIcons.medicine,
    title: 'Medicamentos',
    link: 'Medications',
    testID: BOTTOMTABBAR.tab.health.itemMedicamentos,
    // size: { height: 31, width: 23}
  },
  {
    icon: SystemIcons.orientation,
    title: 'Orientações de saúde',
    link: 'Orientation',
    testID: BOTTOMTABBAR.tab.health.itemOrientacoes,
    // size: { height: 25, width: 29}
  },
];

export const LABORATORY_DATA = [
  {
    title: 'Delboni',
    description:
      'Este parceiro ainda não possui agendamento online, contate pelo telefone e acesse o site para mais informações:',
    phone: '(11) 3049-6999',
    site: 'https://www.delboniauriemo.com.br',
    testID: PROFILECUSTOMERCARE.laboratory.labDelboni,
  },
  {
    title: 'Lavoiser',
    description:
      'Este parceiro ainda não possui agendamento online, contate pelo telefone e acesse o site para mais informações:',
    phone: '(11) 3047-4488',
    site: 'https://www.lavoisier.com.br',
    testID: PROFILECUSTOMERCARE.laboratory.labLavoiser,
  },
  {
    title: 'Salomão Zoppi',
    description:
      'Este parceiro ainda não possui agendamento online, contate pelo telefone e acesse o site para mais informações:',
    phone: '(11) 5576-7878',
    site: 'http://web.szd.com.br/',
    testID: PROFILECUSTOMERCARE.laboratory.labZoppi,
  },
];

export const QUEST_DATA = [
  {
    id: 'quest_1',
    title: 'Sua saude QS',
    subtitle: 'min: 5    respondido: 11',
    min: 5,
    respond: 11,
    questions: [
      {
        id: 'question_1',
        required: true,
        type: 'text',
        label: 'Hola que tal',
        enableWhen: null,
        answer: null,
        parameters: {
          interval: 0,
          min: 8,
        },
      },
      {
        id: 'question_2',
        required: true,
        type: 'radio',
        label: 'Hola radio selector',
        options: [
          { id: 'option_radio_1', label: 'Hola tu mamá', value: 2 },
          { id: 'option_radio_2', label: 'Peri que si', value: 3 },
          { id: 'option_radio_3', label: 'Peri que no', value: 4 },
        ],
        enableWhen: null,
      },
      {
        id: 'question_3',
        required: true,
        type: 'radio',
        label: 'Hola dependo de la question_2/option_radio_1',
        options: [
          { id: 'option_radio_2_1', label: 'Pero como estas caballero', value: 2 },
          { id: 'option_radio_2_2', label: 'Yo estoy bien', value: 3 },
          { id: 'option_radio_2_3', label: 'Pero yo no amiguito', value: 4 },
        ],
        enableWhen: {
          questionCode: 'question_2',
          questionAnswer: 'option_radio_1',
        },
      },
      {
        id: 'question_4',
        required: true,
        type: 'radio',
        label: 'Hola dependo de la question_3/option_radio_2_2',
        options: [
          { id: 'option_radio_3_1', label: 'Turururu Turururu', value: 2 },
          { id: 'option_radio_3_2', label: 'Turu turu turu', value: 3 },
          { id: 'option_radio_3_3', label: 'Plan plan plan plan plan', value: 4 },
        ],
        enableWhen: {
          questionCode: 'question_3',
          questionAnswer: 'option_radio_2_3',
        },
      },
      {
        id: 'question_5',
        required: false,
        type: 'check',
        label: 'Hola check selector',
        options: [
          { id: 'option_check_1', label: 'Hola tu mamá', value: 2 },
          { id: 'option_check_2', label: 'Peri que si', value: 3 },
          { id: 'option_check_3', label: 'Peri que no', value: 4 },
        ],
      },
    ],
  },
  {
    id: 'quest_2',
    title: 'Autocuidado QS',
    subtitle: 'min: 6    respondido: 6',
    min: 6,
    respond: 2,
    questions: [],
  },
  {
    id: 'questi_3',
    title: 'Hábito QS',
    subtitle: 'min: 9    respondido: 12',
    min: 9,
    respond: 0,
    questions: [],
  },
];

export const HEALTH_CARDS = [
  {
    coverage: "0",
    name: "YAGO RICARDO CESAR ASSUNCAO da silva",
    segmentPlan: "Ambulatorial",
    numCard: "00010001000065008",
    startingDate: "2019-11-20",
    birthday: "05/03/1950",
    network: null,
    contract: "APM",
    typeContract: "Adesao",
    cns: "271754258910002",
    accommodation: "Apartamento",
    scopePlan: null,
    contractor: "",
    lackRemaining: "",
    cpf: "39582010835",
    whatsup: "",
    healthPlanCode: "QS5",
    lack: [{}, {}, {}, {}, {}, {}],
    dependents: null,
    active: true,
  },
  {
    coverage: "1",
    name: "YAGO RICARDO CESAR ASSUNCAO da silva",
    segmentPlan: "Ambulatorial",
    numCard: "00010001000065008",
    startingDate: "2019-11-20",
    birthday: "05/03/1950",
    network: null,
    contract: "APM",
    typeContract: "Adesao",
    cns: "271754258910002",
    accommodation: "Apartamento",
    scopePlan: null,
    contractor: "",
    lackRemaining: "",
    cpf: "39582010835",
    whatsup: "",
    healthPlanCode: "QS5",
    lack: [{}, {}, {}, {}, {}, {}],
    dependents: null,
    active: false,
  },
  {
    coverage: "2",
    name: "YAGO RICARDO CESAR ASSUNCAO da silva",
    segmentPlan: "Ambulatorial",
    numCard: "00010001000065008",
    startingDate: "2019-11-20",
    birthday: "05/03/1950",
    network: null,
    contract: "APM",
    typeContract: "Adesao",
    cns: "271754258910002",
    accommodation: "Apartamento",
    scopePlan: null,
    contractor: "",
    lackRemaining: "",
    cpf: "39582010835",
    whatsup: "",
    healthPlanCode: "QS5",
    lack: [{}, {}, {}, {}, {}, {}],
    dependents: null,
    active: false,
  },
]
