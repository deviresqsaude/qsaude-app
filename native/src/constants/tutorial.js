import { TutorialImages} from './theme';

export default {
	Care: [
		{
			title: "Agenda",
			image: TutorialImages.Care.calendar,
			description: "Toda comodidade em um calendário no qual você consulta seus eventos em apenas um clique."
		},
		{
			title: "Troca de usuários",
			image: TutorialImages.Care.users,
			description: "Faça troca usuários a qualquer momento, consulte o TimeQ para saber mais detalhes sobre essa funcionalidade."
		},
		{
			title: "Telecuidado",
			image: TutorialImages.Care.telemedicine,
			description: "Neste botão você acessa o telecuidado e pode conversar por vídeo com médicos e enfermeiros que vão entender os seus sintomas e  oferecer o melhor cuidado."
		},
	],
	Health: [
		{
			title: "Questionário",
			image: TutorialImages.Health.questionary,
			description: "Responda nosso questionário de saúde, é importante a gente cuidar você! Com essas respostas o TimeQ vai conhecer seus hábitos e histórico de saúde."
		},
		{
			title: "Ficha Médica",
			image: TutorialImages.Health.healthPlan,
			description: "Na opção ficha médica você vê suas prescrições médicas de maneira rápida e fácil."
		},
		{
			title: "Medicamentos",
			image: TutorialImages.Health.drugs,
			description: "Controle com facilidade os horários e a frequência no uso dos seus medicamentos."
		},
		{
			title: "Orientações de saúde",
			image: TutorialImages.Health.orientation,
			description: "Em orientações de saúde você encontra dicas para ter uma vida com mais bem-estar e qualidade de vida."
		},
	],
	CustomerCare: [
		{
			title: "Agendamentos",
			image: TutorialImages.CustomerCare.schedule,
			description: "Veja como marcar suas consultas, procedimentos e exames laboratoriais. Separamos pelo seu perfil os melhores especialistas!"
		},
		{
			title: "TimeQ",
			image: TutorialImages.CustomerCare.timeQ,
			description: "Fale com nosso TimeQ pelo WhatsApp, para todo e qualquer tipo de dúvida estamos dispostos a cuidar de você!"
		},
		{
			title: "RedeQ",
			image: TutorialImages.CustomerCare.redeQ,
			description: "Encontre a rede credenciada mais próxima de você acessando uma lista com todos os nossos parceiros da saúde."
		},
	],
	Account: [
		{
			title: "Alteração de dados",
			image: TutorialImages.Account.personalData,
			description: "Aqui você consegue alterar seus dados pessoais e senha com segurança."
		},
		{
			title: "Ver carteirinha virtual",
			image: TutorialImages.Account.medicalCard,
			description: "Visualize sua carteirinha pelo aplicativo e realize suas consultas e procedimentos pelo Qsaúde com tranquilidade."
		},
		{
			title: "Manual do beneficiário",
			image: TutorialImages.Account.manual,
			description: "Aqui no Qsaude, disponibilizamos o manual do beneficiário, assim, você poderá entender melhor o plano que criamos pra sua saúde."
		}
	],
}