export const SHARE_MESSAGE = `Olá, quero indicar para você o Qsaúde – um plano pensado para conhecer, acompanhar e cuidar de perto das pessoas. Acho que você vai gostar. Acesse qsaude.com.br e entenda porque o Qsaúde é diferente.`;
export const SHARE_QSAUDE = {
  contentType: 'link',
  contentUrl: 'https://www.qsaude.com.br/',
  contentDescription: 'Olá, quero indicar para você o Qsaúde – um plano pensado para conhecer, acompanhar e cuidar de perto das pessoas. Acho que você vai gostar. Acesse qsaude.com.br e entenda porque o Qsaúde é diferente.',
};
