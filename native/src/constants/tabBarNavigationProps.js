import { SystemIcons } from './theme';

export default {
  Care: {
    title: "Agenda",
    icon: SystemIcons.schedule,
    activeIcon: SystemIcons.schedule_active
  },
  Health: {
    title: "Saúde",
    icon: SystemIcons.health,
    activeIcon: SystemIcons.health_active
  },
  CustomerCare: {
    title: "Atendimento",
    icon: SystemIcons.customerCare,
    activeIcon: SystemIcons.customerCare_active
  },
  Account: {
    title: "Perfil",
    icon: SystemIcons.account,
    activeIcon: SystemIcons.account_active
  }
};
