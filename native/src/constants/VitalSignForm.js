export default {
  Date: 'Quando',
  Error: 'Falha na adição',
  Validations: {
    Number: 'Precisa ser um número válido',
    DiastolicMin: 'Valor abaixo do plausível',
    DiastolicMax: 'Deve ser menor do que a pressão máxima',
    SystolicMin: 'Deve ser maior do que a pressão mínima',
    SystolicMax: 'Valor acima do plausível',
  },
  imc: {
    Title: 'Novo Imc',
    Labels: {
      weight: 'Peso (em kg)',
      height: 'Altura (em cm)',
      imc: 'IMC (kg/m²)',
    },
    Units: {
      weight: 'kg',
      height: 'cm',
      imc: 'kg/m²',
    },
    SubmitButtonText: 'Adicionar medidas',
  },
  bloodPressure: {
    Title: 'Nova pressão arterial',
    Labels: {
      diastolic: 'Pressão mínima (Diastólica) (mm[Hg])',
      systolic: 'Pressão máxima (Sistólica) (mm[Hg])',
      bloodPressure: 'Pressão arterial',
    },
    Unit: 'mm[Hg]',
    SubmitButtonText: 'Adicionar pressão',
  },
  glycemia: {
    Title: 'Nova glicemia',
    Labels: {
      glycemia: 'Glicemia (mg/dl)',
    },
    Unit: 'mg/dl',
    SubmitButtonText: 'Adicionar glicemia',
  },
};
