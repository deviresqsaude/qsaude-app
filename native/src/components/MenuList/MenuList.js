import React from 'react';
import PropTypes from 'prop-types';
import { FlatList } from 'react-native';
import MenuListItem from './MenuListItem';
import { MenuItemSeparetor } from './MenuListStyle';

/**
 * Menu List
 * @param {array} menuData - Lista de items para ser renderizado
 * @param {func} handleOnPress - Function para ser executa quando clicar no item do menu
 * @param {boolean} multiple - Determina se o item tem duas linhas para ser mostrado
 * @param {boolean} customIcon - Determina um único Icon para todos os items do menu
 */
const MenuList = props => {
  const { menuData, handleOnPress, multiple, customIcon, isProgressive } = props;
  return (
    <FlatList
      data={menuData.filter(({visible}) => (visible || visible === undefined))}
      keyExtractor={item => item.title}
      ItemSeparatorComponent={() => <MenuItemSeparetor />}
      renderItem={({ item }) => (
        <MenuListItem
          disabled={item.disabled}
          iconName={item.iconName}
          multiple={multiple}
          title={item.title}
          subtitle={item.subtitle}
          icon={item.icon || customIcon}
          isProgressive={isProgressive}
          progress={item.progress}
          notification={item.notification}
          onPress={() => handleOnPress(item.link, item.params || {})}
          loading={item.loading}
          tintColor={item.tintColor}
          noTintColor={item.noTintColor}
          sizeIcon={item.size}
          testID={item.testID}
        />
      )}
    />
  );
};

MenuList.propTypes = {
  menuData: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      icon: PropTypes.number,
      notification: PropTypes.number,
      link: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
    }),
  ),
  handleOnPress: PropTypes.func.isRequired,
  multiple: PropTypes.bool,
  customIcon: PropTypes.any,
  isProgressive: PropTypes.bool,
  progress: PropTypes.number,
};

MenuList.defaultProps = {
  menuData: [],
  multiple: false,
  customIcon: null,
  isProgressive: false,
  progress: 0,
};

export default MenuList;
