/* eslint-disable no-nested-ternary */
import React from 'react';
import { TouchableWithoutFeedback, View, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import {
  MenuItemWrapper,
  MenuItemInfoWrapper,
  MenuItemNotification,
  MenuItemMultWrapper,
} from './MenuListStyle';
import { ButtomIconWrapper, ButtomIcon, ButtomIconProgress, SystemIcon } from '../Icons';
import { SystemIcons, Colors, Sizing } from '../../constants/theme';

import QText from '../QText';

/**
 * Menu List Item
 * @param {Image} icon - Icone para renderizar no menu
 * @param {string} title - Texto principal do item
 * @param {string} subtitle - Texto secundario do item
 * @param {func} onPress - Function para ser executa
 * @param {string} notification - Define se o item tem notificacoes com uma bolinha vermelha
 * @param {bool} multiple - Determian se o item tem duas linhas
 * @param {bool} isProgressive - Determina se o icone tem efeito de progresso
 * @param {bool} loading - Determina se o item tiene estado de espera
 */
const MenuListItem = ({testID, ...props}) => {
  const {
    icon,
    isProgressive,
    progress,
    title,
    subtitle,
    onPress,
    notification,
    multiple,
    disabled,
    iconName,
    tintColor,
    noTintColor,
    sizeIcon,
    loading
  } = props;
  return (
    <TouchableWithoutFeedback onPress={onPress} testID={testID}>
      <MenuItemWrapper>
        <>
          <MenuItemInfoWrapper>
            {icon || iconName ? (
              isProgressive ? (
                <ButtomIconWrapper
                  backgroung={Colors.LightGray}
                  radius={39}
                  height={39}
                  width={39}
                  style={{ marginRight: 22 }}
                >
                  <ButtomIcon source={icon} tintColor={Colors.White} />
                  <ButtomIconProgress backgroung={Colors.Purple} progress={progress} />
                </ButtomIconWrapper>
              ) : (
                <ButtomIconWrapper style={{ marginRight: 22 }} height={50} width={50}>
                  {iconName ? (
                    <SystemIcon
                      resizeMode="contain"
                      name={iconName}
                      height={sizeIcon ? sizeIcon.height : 35}
                      width={sizeIcon ? sizeIcon.width : 35}
                      tintColor={!noTintColor ? (tintColor || Colors.Purple) : null}
                    />
                  ) : (
                    <SystemIcon
                      width={sizeIcon ? sizeIcon.width : 35}
                      height={sizeIcon ? sizeIcon.height : 35}
                      source={icon}
                      tintColor={!noTintColor ? (tintColor || Colors.Purple) : null} />
                  )}
                </ButtomIconWrapper>
              )
            ) : null}
            {!multiple ? (
              <View style={{ flex: 1, flexWrap: 'nowrap' }}>
                <QText preset="title" fontSize={Sizing.small} color={Colors.Empress}>
                  {title}
                </QText>
              </View>
            ) : (
              <MenuItemMultWrapper>
                <QText preset="title" color={Colors.Purple}>
                  {title}
                </QText>
                <QText preset="paragraph" color={Colors.Empress}>
                  {subtitle}
                </QText>
              </MenuItemMultWrapper>
            )}
          </MenuItemInfoWrapper>
          {notification ? (
            <MenuItemNotification>{notification}</MenuItemNotification>
          ) : null}
          {!disabled && !loading && (
            <ButtomIconWrapper>
              <ButtomIcon source={SystemIcons.chevromRight} tintColor={Colors.Purple} />
            </ButtomIconWrapper>
          )}
          {!disabled && loading && (
            <ActivityIndicator size="small" color={Colors.Purple}/>
          )}
        </>
      </MenuItemWrapper>
    </TouchableWithoutFeedback>
  );
};

MenuListItem.propTypes = {
  icon: PropTypes.oneOfType([PropTypes.element, PropTypes.number]),
  iconName: PropTypes.string,
  title: PropTypes.string,
  subtitle: PropTypes.string,
  onPress: PropTypes.func,
  notification: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  multiple: PropTypes.bool,
  disabled: PropTypes.bool,
  isProgressive: PropTypes.bool,
  progress: PropTypes.number,
  loading: PropTypes.bool,
  noTintColor: PropTypes.bool,
};

MenuListItem.defaultProps = {
  icon: null,
  title: '',
  subtitle: '',
  onPress: () => {},
  notification: null,
  disabled: false,
  multiple: false,
  isProgressive: false,
  progress: 0,
  iconName: null,
  loading: false,
  noTintColor: false
};

export default MenuListItem;
