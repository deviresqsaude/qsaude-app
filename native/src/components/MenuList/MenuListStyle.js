import styled from 'styled-components/native';
import { SystemFonts, Sizing, Colors } from './../../constants/theme';

export const MenuWrapper = styled.ScrollView({
  flexDirection: 'column',
  width: '100%',
  flex: 1
})

export const MenuItemWrapper = styled.View({
  alignItems: 'center',
  justifyContent: 'space-between',
  minHeight: 56,
  width: '100%',
  flexDirection: 'row',
  borderColor: Colors.WhiteSmoke,
  paddingLeft: 16,
  paddingRight: 16,
  paddingBottom: 10,
  paddingTop: 10
});

export const MenuItemInfoWrapper = styled.View({
  alignItems: 'center',
  flexDirection: 'row',
  flex: 1,
})

export const MenuItemText = styled.Text({
  fontSize: Sizing.small,
  fontFamily: SystemFonts.RubikRegular,
  color: Colors.Mortar
});

export const MenuItemMultWrapper = styled.View({
  flexDirection: "column"
});

export const MenuItemSeparetor = styled.View({
  backgroundColor: Colors.WhiteSmoke,
  flex: 1,
  height: 1
});

export const MenuItemNotification = styled.Text({
  backgroundColor: Colors.Red,
  borderRadius: 10,
  color: Colors.Light,
  height: 20,
  lineHeight: 20,
  position: 'absolute',
  right: 36,
  textAlign: 'center',
  width: 20
});
