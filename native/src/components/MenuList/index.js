import { compose, withHandlers } from 'recompose';
import { withNavigation } from 'react-navigation';
import MenuListComponent from './MenuList';

const enhance = compose(
  withNavigation,
  withHandlers({
    handleOnPress: ({ navigation: { navigate } }) => (link, params) => {
      if (!link) return;
      if (typeof link === 'function') link();
      if (typeof link === 'string') navigate(link, {...params});
    },
  }),
);

const MenuList = enhance(MenuListComponent);

export { MenuList };

export default MenuList;
