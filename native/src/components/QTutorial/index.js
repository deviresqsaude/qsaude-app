import { compose, withHandlers, withState } from 'recompose';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';
import SyncStorage from 'sync-storage';
import QTutorial from './QTutorial';
import { NavActions } from '../../store/actions';
import tutorial from '../../constants/tutorial';

const mapStateToProps = ({navSystem}) => ({
  navSystemState: {...navSystem}
});

const mapDispatchToProps = dispatch => ({
  setNavCurrentPage: state => dispatch(NavActions.actions.setCurrentPage(state)),
});

const withIndexSwiperState  = withState('indexSwiper', 'setIndexSwiper', 0)

const handlers = withHandlers({
	closeModal: ({setNavCurrentPage}) => page => {
		const profileUser = SyncStorage.get("profileUser");
		SyncStorage.set(`Tutorial.${profileUser.cpf}.${page}`, false);
		setNavCurrentPage({});
	},
	getTutorialData: () => index => {
		return tutorial[index];
	},
})

const enchance = compose(
	connect(
		mapStateToProps,
		mapDispatchToProps
	),
	withIndexSwiperState,
	handlers,
	withNavigation,
);

export default enchance(QTutorial);