import React from 'react';
import R from 'ramda';
import { View, Modal, Image, Dimensions } from 'react-native';
import Swiper from 'react-native-swiper';
import SyncStorage from 'sync-storage';
import { CardTutorial, TutorialContainer, CardContainer, CardInformation, ImageContainer, DescriptionContainer } from './QTutorialStyle';
import QText from '../QText';
import { QButton } from '../Button';
import { ModalOverlay } from '../ModalOverlay';
import { Dots } from '../../constants/theme';

const QTutorial = ({closeModal, indexSwiper=0, navSystemState: {currentPage}, getTutorialData}) => {
	if(R.isEmpty(currentPage)) return null;
	const tutorialData = getTutorialData(currentPage.routeName);
	const profileUser = SyncStorage.get("profileUser");
	const showTutorial = SyncStorage.get(`Tutorial.${profileUser.cpf}.${currentPage.routeName}`);
	const swiperConfig = {
		index: indexSwiper,
		loop: false,
		loadMinimal: true,
    dot: <View style={{...Dots.styleDot}}/>,
		activeDot: <View style={{...Dots.styleActiveDot}}/>,
		// scrollEnabled: false
	};
	return (
		<Modal
			animationType="fade"
			visible={(!R.isEmpty(currentPage) || !R.isEmpty(tutorialData)) && showTutorial}
			// style={{justifyContent: "center", alignItems: "center"}}
			transparent>
				<ModalOverlay closeAlert={() => console.log(`Overlay Tutorial`)}/>
				<TutorialContainer>
					<CardContainer>
						<Swiper {...swiperConfig}>
							{tutorialData.map((item, index) => {
								return (
									// eslint-disable-next-line react/no-array-index-key
									<CardTutorial key={`${currentPage.routeName}-${index}`}>
										<QText preset="navTitleSimple" fontSize={20} margin={{top: 9}}>{item.title}</QText>
										<CardInformation>
											<ImageContainer>
												<Image source={item.image} height="184" width="184"/>
											</ImageContainer>
											<DescriptionContainer>
												<QText preset="dataText" textCenter fontSize={15}>{item.description}</QText>
											</DescriptionContainer>
										</CardInformation>
									</CardTutorial>
								)
							})}
						</Swiper>
					</CardContainer>
					<QButton style={{width: 156, marginHorizontal: "25%"}}
						onPress={() => closeModal(currentPage.routeName)}
						text="Entendi"/>
					{/* <QButton style={{width: 156, marginHorizontal: "25%"}}
						onPress={() => tutorialData.length - 1 === indexSwiper ? closeModal() : setIndexSwiper(indexSwiper + 1)}
						text={tutorialData.length - 1 === indexSwiper ? "Entendi" : "Próxima"}/> */}
				</TutorialContainer>
		</Modal>
	)
}

export default QTutorial;
