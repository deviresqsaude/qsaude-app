import styled from 'styled-components/native';
import { Dimensions, Platform } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Colors } from '../../constants/theme';

export const hasDeviceNotch = () => {
	const h = Platform.OS === 'android' ? 20 : getStatusBarHeight();
	console.log(`getStatusBarHeight: `, h);
	return h;
}

export const TutorialContainer = styled.View`
	align-items: center;
	border-radius: 15;
	background-color: ${Colors.White};
	width: ${Dimensions.get('screen').width - 40};
	height: ${Platform.OS === 'android' ? "87%" : Dimensions.get('screen').height - (hasDeviceNotch()*3)};
	justify-content: space-between;
	margin-horizontal: 20;
	margin-top: ${hasDeviceNotch()};
	padding-left: 16;
	padding-right: 16;
	padding-bottom: 22;
`;

export const CardContainer = styled.View`
	flex-grow: 1;
	width: 100%;
	align-items: center;
	justify-content: center;
`;

export const CardInformation = styled.View`
	/* background-color: ${Colors.Magenta}; */
	align-items: center;
`;

export const CardTutorial = styled.View`
	margin-top: 10;
	align-items: center;
	width: 100%;
	height: 100%;
	text-align: center;
	/* background-color: ${Colors.LabelGray}; */
	/* justify-content: space-between; */
	padding-bottom: 25;
`;

export const ImageContainer = styled.View`
	height: 194;
	width: 100%;
	align-items: center;
	margin-top: 28;
	margin-bottom: 20;
`;

export const DescriptionContainer = styled.View`
	align-items: center;
	height: 150;
	justify-content: center;
	padding-left: 5;
	padding-right: 5;
`;