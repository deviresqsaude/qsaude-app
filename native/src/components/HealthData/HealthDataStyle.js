import React from 'react';
import { View } from 'react-native';
import styled from 'styled-components/native';
import { Placeholder, PlaceholderMedia, Fade } from 'rn-placeholder';
import { Colors, Sizing, SystemFonts } from '../../constants/theme';

export const HealthDataWrapper = styled.View({
  height: 150,
});

export const HealthDataCardWrapper = styled.TouchableOpacity({
  backgroundColor: 'transparent',
  borderRadius: 10,
  color: Colors.White,
  height: 120,
  paddingBottom: 6,
  paddingLeft: 15,
  paddingRight: 15,
  paddingTop: 6,
  position: 'relative',
  width: 180,
  justifyContent: 'space-between',
});

export const HealtDataLabel = styled.Text({
  color: Colors.White,
  fontSize: 16,
  fontFamily: SystemFonts.RubikMedium,
  height: 40,
  marginTop: 10
});

export const HealthDataContainer = styled.View({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
});

export const HealtDataValue = styled.Text({
  color: Colors.White,
  fontSize: Sizing.xlarge,
  fontFamily: SystemFonts.RubikMedium,
});

export const HealtDataUnit = styled.Text({
  color: Colors.White,
  fontSize: Sizing.small,
  fontFamily: SystemFonts.RubikRegular,
  alignItems: 'center',
  marginLeft: 5
})

export const HealtDataContent = styled.View({
  backgroundColor: Colors.Black,
  marginBottom: 15
});

export const HealtDataUpdated = styled.Text({
  color: Colors.White,
  fontSize: Sizing.xsmall,
  fontFamily: SystemFonts.RubikRegular,
});

export const HealthDataIcon = styled.Image({
  opacity: 0.5,
  position: 'absolute',
  right: 10,
  bottom: 5,
});

export const EmptyBar = styled.View({
  width: 50,
  height: 2,
  borderBottomWidth: 2,
  borderColor: "transparent",
});

export const HealthDataSeparator = styled.View({
  backgroundColor: Colors.White,
  height: '100%',
  width: 16,
});

export const HealhDataPlaceholder = () => (
  <Placeholder Animation={Fade}>
    <View style={{ flexDirection: 'row', alignItems: 'center', padding: 16 }}>
      <PlaceholderMedia style={{ width: 180, height: 130, marginRight: 16, borderRadius: 10 }} />
      <PlaceholderMedia style={{ width: 180, height: 130, marginRight: 16, borderRadius: 10, }} />
      <PlaceholderMedia style={{ width: 180, height: 130, marginRight: 16, borderRadius: 10, }} />
    </View>
  </Placeholder>
);
