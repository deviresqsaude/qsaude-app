import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import PropTypes from 'prop-types';
import { withNavigation } from 'react-navigation';
import {
  HealthDataCardWrapper,
  HealtDataLabel,
  HealtDataValue,
  HealtDataUpdated,
  HealthDataIcon,
  EmptyBar,
  HealthDataContainer,
  HealtDataUnit,
} from './HealthDataStyle';

const HealthDataCard = props => {
  const {
    label,
    value,
    unit,
    updated,
    icon,
    color: { start, end },
    onPress,
    chartVitalSign,
    vitalSign,
    link,
    navigation: { navigate },
    testID,
  } = props;

  const navigateToRoute = () =>
    typeof link === 'string'
      ? navigate(link, {
          vitalSign,
          chartVitalSign,
        })
      : alert('A navegaçāo falhou!');

  return (
    <LinearGradient
      colors={[start, end]}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 0 }}
      style={{
        borderRadius: 10,
      }}
    >
      <HealthDataCardWrapper testID={testID} onPress={onPress || navigateToRoute}>
        <HealtDataLabel>{label || "-" }</HealtDataLabel>
        <HealthDataContainer>
          <HealtDataValue>{value || <EmptyBar />}</HealtDataValue>
          {
            !!value && <HealtDataUnit>{unit}</HealtDataUnit> 
          }
        </HealthDataContainer>
        <HealtDataUpdated>{updated || 'Nenhum registro'}</HealtDataUpdated>
        {icon && <HealthDataIcon source={icon} />}
      </HealthDataCardWrapper>
    </LinearGradient>
  );
};

HealthDataCard.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  vitalSign: PropTypes.string,
  chartVitalSign: PropTypes.string,
  updated: PropTypes.string,
  icon: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  color: PropTypes.shape({
    start: PropTypes.string.isRequired,
    end: PropTypes.string.isRequired,
  }).isRequired,
  onPress: PropTypes.func,
  link: PropTypes.string,
  testID: PropTypes.string,
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired
};

HealthDataCard.defaultProps = {
  label: '',
  value: '',
  updated: 'Sem medidas',
  vitalSign: '',
  icon: null,
  onPress: null,
  link: null,
  chartVitalSign: '',
  testID: ''
};

export default withNavigation(HealthDataCard);
