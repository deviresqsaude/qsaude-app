import { compose, withProps, withState, withHandlers, branch, renderComponent } from 'recompose';
import moment from 'moment';
import R from 'ramda';
import { connect } from 'react-redux';
import { graphql } from '@apollo/react-hoc';
import { InsertVitalSign } from '../../service/mutations';
import HealthData from './HealthData';
import { SystemIcons } from '../../constants/theme';
import renderWhileLoading from '../PlaceHolderHOCs/RenderWhileLoading';
import { HealhDataPlaceholder } from './HealthDataStyle';
import { CARDS_GRADIENT_COLORS } from '../../constants/mock';

import renderEmptyData from '../PlaceHolderHOCs/RenderEmptyData';
import { VitalSignType } from '../../screens/VitalSignForm';
import { AlertActions } from '../../store/actions';

const withBloodPickerState = withState('bloodPickerOpen', 'togglePicker', false);
const withLoadingState = withState('loadingStatus', 'setLoading', false);
const withCardListState = withState('cardList', 'setCardList', []);
const withBloodTypeMutation = graphql(InsertVitalSign);

// Error Handling
const mapDispatchToProps = dispatch => ({
  onServiceFailed: ({ title, error: description }) =>
    dispatch(
      AlertActions.actions.openAlert({
        title,
        shouldRenderOkButton: true,
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        description,
        isError: true,
      }),
    ),
  onServiceSuccess: () =>
    dispatch(
      AlertActions.actions.openAlert({
        description: null,
        shouldRenderOkButton: true,
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        title: 'Alteração efetuada com sucesso!',
        isSuccess: true,
      }),
    ),
});

const withSelectBloodTypeHandler = withHandlers({
  selectBloodType: props => async bloodType => {
    const { mutate, onServiceFailed, onServiceSuccess, togglePicker, setLoading, loading } = props;
    setLoading(true);
    try {
      await mutate({
        refetchQueries: ['RecentVitalSigns'],
        variables: {
          data: [
            {
              type: VitalSignType.BloodType,
              measuredAt: moment().toISOString(),
              measure: {
                amountString: bloodType,
              },
            },
          ],
        },
      });
      setLoading(false);
      onServiceSuccess();
    } catch (error) {
      onServiceFailed({
        title: 'Erro ao alterar tipo sanguíneo',
        description: error.message,
      });
      setLoading(false);
    }
    // setLoading(false);
    togglePicker(false);
  },
});

// helpers
const extractMeanPressure = subMeasures => {

    if (!subMeasures) return { measure: {amount: '', unit: ''}};
  const systolicPressure = R.filter(R.propEq('type', 'systolicPressure'), subMeasures);
  const diastolicPressure = R.filter(R.propEq('type', 'diastolicPressure'), subMeasures);
  // const meanPressureFromServer = subMeasures ? R.filter(R.propEq('type', 'meanPressure'), subMeasures) : [];
  // if (meanPressureFromServer.length > 0) return meanPressureFromServer[0];
  // const minAndMax = R.map(item => item.measure.amount, subMeasures);

  return {
    measure: {
      amount: `${systolicPressure[0].measure.amount}/${diastolicPressure[0].measure.amount}`,
    },
  };
};

const getLabel = R.cond([
  [R.equals('weight'), R.always('Peso')],
  [R.equals('height'), R.always('Altura')],
  [R.equals('glycemia'), R.always('Glicemia')],
  [R.equals('bloodPressure'), R.always('Pressão Arterial')],
  [R.equals('bloodType'), R.always('Tipo Sanguíneo')],
  [R.equals('heartRate'), R.always('Batimento Cardíaco')],
  [R.equals('bodySurface'), R.always('Massa Corporal')],
  [R.T, R.always('IMC')],
]);

const getIcon = R.cond([
  [R.equals('weight'), R.always("vitalWeight")],
  [R.equals('height'), R.always("vitalHeight")],
  [R.equals('glycemia'), R.always("vitalGlicemia")],
  [R.equals('bloodPressure'), R.always("vitalPressure")],
  [R.equals('bloodType'), R.always("vitalBlood")],
  [R.T, R.always("vitalImc")],
])

const vitalList= ['bloodType', 'weight', 'height', 'imc', 'bloodPressure', 'glycemia'];

const parseItemToCardFormat = togglePicker => (item, index) => {
  const { type, measuredAt, subMeasures, measure } = item;
  const updated = measuredAt ? moment(measuredAt).format('DD/MM/YYYY') : null;
  const label = getLabel(type);
  const iconCard = SystemIcons[getIcon(type)];
  const link = type === 'bloodType' ? undefined : 'VitalSignChart';
  const vitalSign = type !== 'bloodPressure' && type !== 'glycemia' ? 'imc' : type;
  const onPress = type === 'bloodType' ? () => togglePicker(true) : undefined;
  const color = CARDS_GRADIENT_COLORS[index];

  const value = R.cond([
    [R.equals("bloodPressure"), () => {
      const {measure: resulMeasure} = extractMeanPressure(subMeasures);
      return resulMeasure.amount ? `${resulMeasure.amount}` : null;
    }],
    [R.equals("bloodType"), () => {
      return R.prop('amountString', measure);
    }],
    [R.T, () => {
      const { amount } = measure;
      return amount ? `${amount}` : null;
    }]
  ])(type);

  return {
    label,
    value: value || "",
    unit: R.prop('measureUnit', item) || "",
    updated,
    icon: iconCard,
    onPress,
    link,
    chartVitalSign: type,
    vitalSign,
    color,
  };
};

/**
 * HOCs
 */
const withDataParsedToCards = withProps(props => {
  const {
    data: { recentVitalSigns, loading },
    togglePicker,
  } = props;

  if (loading || !recentVitalSigns) return [];
  // inject togglePicker in the VitalSignFormat => CardFormat parser.
  const parseToCardWithPickerOpener = parseItemToCardFormat(togglePicker);
  const refactorVitalSigns = recentVitalSigns.filter(item => item  !== null && R.includes(item.type, vitalList));
  const orderedList = R.filter(item => !!item)(R.map(item => R.find(R.propEq('type', item), refactorVitalSigns), vitalList));
  const cardList = orderedList.map(parseToCardWithPickerOpener);

  return { cardList }
});

export default compose(
  connect(
    null,
    mapDispatchToProps,
  ),
  withCardListState,
  withLoadingState,
  withBloodPickerState,
  withBloodTypeMutation,
  withSelectBloodTypeHandler,
  withDataParsedToCards,
  renderWhileLoading(HealhDataPlaceholder, 'recentVitalSigns'),
  renderEmptyData(HealhDataPlaceholder, 'recentVitalSigns'),
)(HealthData);
