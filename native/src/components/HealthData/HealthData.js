import React from 'react';
import { FlatList } from 'react-native';

// import PropTypes from 'prop-types';
import HealthDataCard from './HealthDataCard';
import { HealthDataSeparator, HealthDataWrapper, HealhDataPlaceholder } from './HealthDataStyle';
import BloodTypeSelector from '../BloodTypeSelector/BloodTypeSelector';
import { BOTTOMTABBAR } from '../../constants/TestIds';

const HealthData = props => {
  const {
    data: { loading },
    cardList,
    bloodPickerOpen,
    selectBloodType,
    togglePicker,
    loadingStatus,
  } = props;

  const handleChange = value => {
    selectBloodType(value);
  };
  if (loading) {
    return <HealhDataPlaceholder/>
  }
  return (
    <>
      <BloodTypeSelector
        loading={loadingStatus}
        closePicker={() => togglePicker(false)}
        isOpen={bloodPickerOpen}
        onChange={handleChange}
      />
      <HealthDataWrapper>
        <FlatList
          testID={BOTTOMTABBAR.tab.health.dadosVitaisScrollView}
          contentContainerStyle={{
            paddingBottom: 12,
            paddingLeft: 16,
            paddingRight: 16,
            paddingTop: 8,
          }}
          horizontal
          showsHorizontalScrollIndicator={false}
          data={cardList}
          ItemSeparatorComponent={() => <HealthDataSeparator />}
          renderItem={({ item }) => <HealthDataCard {...item} testID={String(Math.random())}/>}
        />
      </HealthDataWrapper>
    </>
  );
};

HealthData.propTypes = {};

HealthData.defaultProps = {};

export default HealthData;
