import styled from 'styled-components';
import { Colors } from '../../constants/theme';

export const SwiperItem = styled.View({
  justifyContent: 'center',
  alignItems: 'center',
  paddingTop: 8,
  paddingBottom: 16
})

export const CardStatusWrapper = styled.View({
  alignSelf: 'flex-end',
  flexDirection: 'row', 
  alignItems: 'center'
})

export const CardStatusText = styled.Text(({status}) => ({
  color: status ? Colors.GreenSuccess : Colors.Red,
  fontWeight: 'bold',
  fontSize: 12,
}))

export const CardStatusImage = styled.Image({
  width: 22,
  height: 22,
  marginRight: 4
})