import React from 'react';
import { View } from 'react-native';
import Swiper from 'react-native-swiper';
import moment from 'moment';
import {
  HealthCardWrapper,
  HealthCardBody,
  HealthCardLogo,
} from '../../screens/HealthCard/HealthCardStyle';
import { QText } from '../index';
import { SwiperItem, CardStatusWrapper, CardStatusImage, CardStatusText } from './SwiperCardsStyle'; 
import { Colors, SystemImages } from '../../constants/theme';
import { elevationShadow } from '../../helpers';
import { PROFILEUSER } from '../../constants/TestIds';

const swiperConfig = {
  style: {height: 290},
  index: 0,
  loop: false,
  dot: <View style={{borderWidth: 1, borderColor: Colors.Purple, width: 12, height: 12,borderRadius: 6, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3,}} />,
  activeDot: <View style={{backgroundColor: Colors.Purple, width: 12, height: 12,borderRadius: 6, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3,}} />
}


const SwiperCards = props => {
  const { healthCards, onIndexChanged } = props;
  return (
    <Swiper {...swiperConfig} onIndexChanged={onIndexChanged}>
        {healthCards && healthCards.length > 0 && healthCards.map((card, index) => {
          const { numCard, registration, name, healthPlanCode, startingDate, segmentPlan, status, completeName, clientName } = card;
          return (
            <SwiperItem key={index}>
              <HealthCardWrapper style={{ ...elevationShadow('#000000', 8, 0.5) }}>
                <HealthCardBody>
                  <View
                    style={{
                      height: 40,
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}
                  >
                    <HealthCardLogo />
                    <QText
                      testID={PROFILEUSER.checkVcard.registration}
                      color={Colors.Purple}
                      fontSize={14}
                      preset="bold"
                    >
                      {numCard || registration || '---'}
                    </QText>
                  </View>
                  <View>
                    <View style={{ marginBottom: 4, width: '100%' }}>
                      <QText
                        testID={PROFILEUSER.checkVcard.clientName}
                        color={Colors.Purple}
                        fontSize={12}
                        preset="bold">
                        {name || completeName || clientName || '---'}
                      </QText>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                      <View style={{ flexDirection: 'column', flex: 1 }}>
                        <View>
                          <QText color={Colors.Gray} fontSize={10} preset="bold">
                            Meu plano
                          </QText>
                          <QText
                            testID={PROFILEUSER.checkVcard.healthPlanCode}
                            color={Colors.Purple}
                            fontSize={12}
                            preset="bold"
                          >
                            {healthPlanCode || '---'}
                          </QText>
                        </View>
                        <View>
                          <QText color={Colors.Gray} fontSize={12} preset="bold">
                            Inicio vigência
                          </QText>
                          <QText
                            testID={PROFILEUSER.checkVcard.startingDate}
                            color={Colors.Purple}
                            fontSize={12}
                            preset="bold"
                          >
                            {startingDate ? moment(startingDate).format('DD/MM/YY') : '---'}
                          </QText>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'column', flex: 1 }}>
                        <View>
                          <QText color={Colors.Gray} fontSize={10} preset="bold">
                            Segmentação
                          </QText>
                          <QText
                            testID={PROFILEUSER.checkVcard.segmentPlan}
                            color={Colors.Purple}
                            fontSize={12}
                            preset="bold"
                          >
                            {segmentPlan || '---'}
                          </QText>
                        </View>
                        <View />
                      </View>
                    </View>
                    <CardStatusWrapper>
                      <CardStatusImage source={ !status ? SystemImages.errorImage : SystemImages.successImage} />
                      <CardStatusText status={status}>{ !status ? "Inativo" : "Ativo" } </CardStatusText>
                  </CardStatusWrapper>
                  </View>
                </HealthCardBody>
              </HealthCardWrapper>
            </SwiperItem>
          );
        })}
      </Swiper>
  )

}

export default SwiperCards;