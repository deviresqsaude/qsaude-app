import React from 'react';
import { FlatList } from 'react-native';
import { ShadowBox, DetailRow, ItemContainer, ListHeader, LinkContainer, LinkItem } from './DetailsCardStyle';
import QText from '../QText';
import { Colors } from '../../constants/theme';
import { elevationShadow } from '../../helpers';

export default function DetailsCard({ data, title = '–', description, links }) {
  const renderItem = ({ item }) => <DetailRow {...item} key={item.label} />;

  return (
    <>
      <ShadowBox style={{...elevationShadow("#000000", 8, .5)}}>
        {(title || description) && (
          <ItemContainer center>
            <ListHeader>
              <QText textCenter margin={{ bottom: 4 }} preset="link">
                {title}
              </QText>
              {description && (
                <QText textCenter color={Colors.TextGray} preset="caption">
                  {description}
                </QText>
              )}
            </ListHeader>
          </ItemContainer>
        )}
        <FlatList {...{ data, renderItem }} showsHorizontalScrollIndicator={false} />
        {
          (links) && (
            <LinkContainer>
              <FlatList
                scrollEnabled={false}
                data={links}
                renderItem={({item}) => {
                  return (
                    <LinkItem>
                      <QText onPress={item.onPress} preset="link">{item.label}</QText>
                    </LinkItem>
                  )
              }} />
            </LinkContainer>
          )
        }
      </ShadowBox>
    </>
  );
}
