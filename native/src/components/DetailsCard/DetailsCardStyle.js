import React from 'react';
import styled from 'styled-components/native';
import QText from '../QText';
import { Colors } from '../../constants/theme';

export const ShadowBox = styled.View({
  shadowColor: '#000',
  backgroundColor: '#fff',
  // shadowOffset: { width: 10, height: 10 },
  // shadowOpacity: 0.3,
  // elevation: 3,
  // shadowRadius: 20,
  borderColor: Colors.LightGray,
  borderWidth: 1,
  borderRadius: 18,
  padding: 14,
  margin: 16,
});

export const DetailRow = ({ label, data, center }) => (
  <ItemContainer center={center}>
    {label && data ? (
      <>
        <QText color={Colors.TextGray} margin={{ right: 32 }}>
          {label}
        </QText>
        <QText style={{ maxWidth: '75%' }} preset="link">
          {data}
        </QText>
      </>
    ) : (
      <QText color="rgba(0,0,0,0.5)" preset="bold">
        {label}
      </QText>
    )}
  </ItemContainer>
);

export const ItemContainer = styled.View(({ center }) => ({
  paddingBottom: 14,
  paddingTop: 14,
  borderBottomWidth: 1,
  borderBottomColor: 'rgba(0, 0, 0, .2)',
  flexDirection: 'row',
  alignItems: 'center',
  flexWrap: 'wrap',
  justifyContent: center ? 'center' : 'space-between',
}));

export const ListHeader = styled.View({
  alignItems: 'center',
  justifyContent: 'center',
});

export const LinkContainer = styled.View({
  paddingTop: 16
})

export const LinkItem = styled.View({
  marginBottom: 16
})
