import React from 'react';
import moment from 'moment';
import { TouchableOpacity } from 'react-native';
import QText from '../QText';
import { Colors } from '../../constants/theme';
import { Wrapper, AppointmentInfo, DoctorInfo } from './MyDoctorCardStyles';

const MyDoctorCard = ({ name = '–', nextSlot, address, onPress }) => (
  <TouchableOpacity onPress={onPress} activeOpacity={0.9}>
    <Wrapper>
      <DoctorInfo>
        <QText preset="itemTitle" fontSize={14}>{name}</QText>
        <QText color={Colors.TextGray} fontSize={12}>{address}</QText>
      </DoctorInfo>
      {nextSlot && <AppointmentInfo>
        <QText color={Colors.TextGray} preset="caption" fontSize={12}>
          Disponível em:
        </QText>
        <QText color={Colors.TextGray} fontSize={12}>{moment(nextSlot).format('DD/MM HH:mm')}</QText>
      </AppointmentInfo>}
    </Wrapper>
  </TouchableOpacity>
);

export default MyDoctorCard;
