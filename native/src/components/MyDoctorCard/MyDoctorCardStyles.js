import styled from 'styled-components/native';
import { Colors } from '../../constants/theme';

export const Wrapper = styled.View({
  paddingVertical: 16,
  borderBottomColor: Colors.LightGray,
  borderBottomWidth: 1,
  flexDirection: 'row',
  alignItems: 'center',
});

export const DoctorInfo = styled.View({
  flex: 0.7,
});

export const AppointmentInfo = styled.View({
  flex: 0.3,
  alignItems: 'flex-end',
});
