import { compose, withProps } from "recompose";
import { withNavigation } from 'react-navigation';
// import { connect } from 'react-redux';
import SyncStorage from 'sync-storage';
import Profile from './Profile';

const profileProps = withProps(() => ({
  userProfile: SyncStorage.get("profileUser"),
  dependents: SyncStorage.get("dependents"),
}))

const ProfileInfo = compose(
  withNavigation,
  profileProps
)(Profile);


export { ProfileInfo };
