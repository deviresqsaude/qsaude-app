import React from 'react';
import PropTypes from 'prop-types';
import { TouchableHighlight } from 'react-native';
import { ProfileWrapper, ProfileInfoWrapper, ProfileText } from './ProfileStyle';
import { AvatarIcon, ButtomIconWrapper, ButtomIcon } from '../Icons';
import { SystemIcons, Colors } from '../../constants/theme';
import { USER_PROFILE_AVATAR } from "../../constants/mock";

const ProfileInfo = ({avatar, userName, userProfile, dependents, navigation}) => {
  return (
    <TouchableHighlight
      style={{ width: '100%', height: 42, paddingHorizontal: 1}}
      onPress={ dependents && !dependents.length ?  () => {} : () => navigation.navigate({
        routeName: 'Parents',
        params: { dependents }
      })}
      underlayColor="transparent">
      <ProfileWrapper>
        <ProfileInfoWrapper>
          {
            avatar &&
            <AvatarIcon
              height={37}
              width={37}
              source={avatar}
              style={{
                marginRight: 10,
              }}
            />
          }
          <ProfileText>{userProfile.completeName || userName}</ProfileText>
        </ProfileInfoWrapper>
        {
          dependents && !!dependents.length &&
          <ButtomIconWrapper>
            <ButtomIcon source={SystemIcons.chevromRight} tintColor={Colors.Gray} />
          </ButtomIconWrapper>
        }
      </ProfileWrapper>
    </TouchableHighlight>
  );
};

ProfileInfo.defaultProps = {
  avatar: USER_PROFILE_AVATAR,
  userName: '',
  dependents: []
};

ProfileInfo.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  avatar: PropTypes.any,
  userName: PropTypes.string,
  dependents: PropTypes.arrayOf(PropTypes.any),
};

export default ProfileInfo;
