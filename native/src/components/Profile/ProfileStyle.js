import styled from 'styled-components/native';
import { SystemFonts, Sizing, Colors } from "../../constants/theme";

export const ProfileWrapper = styled.View({
  alignItems: 'center',
  justifyContent: 'space-between',
  height: 56,
  flex: 1,
  flexDirection: 'row',
  paddingLeft: 16,
  paddingRight: 16,
});

export const ProfileInfoWrapper = styled.View({
  alignItems: 'center',
  flexDirection: 'row',
});

export const ProfileText = styled.Text({
  fontSize: Sizing.small,
  fontFamily: SystemFonts.PlexSansMedium,
  color: Colors.Empress,
  textTransform: "capitalize"
});

export const ProfileError = styled.View({
  alignItems: 'center',
  justifyContent: 'space-between',
  height: 56,
  flex: 1,
  flexDirection: 'row',
  width: "100%"
});

export const ProfileLoadingWrapper = styled.View({
  alignItems: 'center',
  justifyContent: 'space-between',
  height: 56,
  width: '100%',
  paddingLeft: 16,
  paddingRight: 16
});
