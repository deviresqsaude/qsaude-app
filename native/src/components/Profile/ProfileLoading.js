import React from 'react';
import { Placeholder, PlaceholderLine, PlaceholderMedia, Fade } from 'rn-placeholder';
import { ProfileLoadingWrapper } from './ProfileStyle';

const QuestLoading = () => {
  return (
    <ProfileLoadingWrapper>
      <Placeholder
        Animation={Fade}
        Left={PlaceholderMedia}>
        <PlaceholderLine width={100} />
        <PlaceholderLine width={60} />
      </Placeholder>
    </ProfileLoadingWrapper>
  )
}

export default QuestLoading;
