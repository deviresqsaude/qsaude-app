/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
// import Icon from "react-native-vector-icons/AntDesign";
import { TouchableWithoutFeedback } from 'react-native';
import { QCheckWrapper, QCheckBox, QCheckImage } from './QcheckboxStyles';
import { SystemIcons, Colors } from '../../../constants/theme';
import QText from '../../QText';

const QCheckbox = props => {
  const {
    testID,
    label,
    checkHandler,
    ischecked,
    preset
  } = props

  return (
    <TouchableWithoutFeedback
      testID={testID}
      onPress={() => checkHandler(!ischecked)}>
      <QCheckWrapper>
        <QCheckBox
          accessibilityRole="checkbox"
          selected={ischecked}
          color={preset}>
          <QCheckImage source={SystemIcons.check} />
        </QCheckBox>
        <QText
          fontSize={14}
          color={preset==="default"? Colors.Empress : Colors.Purple}>
          {label}
        </QText>
      </QCheckWrapper>
    </TouchableWithoutFeedback>
  );
};

QCheckbox.propTypes = {
  ischecked: PropTypes.bool,
  preset: PropTypes.oneOf(["primary", "default"]),
  label: PropTypes.string
};

QCheckbox.defaultProps = {
  ischecked: false,
  preset: "default",
  label: "QCheckbox"
};

export default QCheckbox;
