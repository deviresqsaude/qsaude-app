import { withHandlers, compose, lifecycle, withState } from 'recompose';
import QCheckbox from './QCheckbox';

const enhance = compose(
  withState('ischecked', 'setChecked', false),
  withHandlers({
    checkHandler: ({
      onFieldChange,
      setChecked,
      disabled
    }) => check => {
      if(disabled) return;
      setChecked(check);
      if(onFieldChange) onFieldChange(check);
    },
  }),
  lifecycle({}),
);

export default enhance(QCheckbox);
