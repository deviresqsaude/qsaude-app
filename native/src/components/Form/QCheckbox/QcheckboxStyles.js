import styled from 'styled-components/native';
import { Colors } from '../../../constants/theme';

export const QCheckWrapper = styled.View({
  flexDirection: "row",
  alignItems: "center",
  marginBottom: 8
});

export const QCheckBox = styled.View(({selected, color, disabled}) => ({
  backgroundColor: `${selected ? Colors.Purple : Colors.White}`,
  borderWidth: 2,
  borderColor: Colors.Purple,
  // borderColor: `${selected || color==="primary" ? Colors.Purple : Colors.Empress}`,
  borderRadius: 5,
  height: 20,
  marginRight: 8,
  width: 20,
  alignItems: "center",
  justifyContent: "center",
  opacity: `${disabled ? .5 : 1}`
}));

export const QCheckImage = styled.ImageBackground({
  height: 14,
  width: 14
});

// export const CheckboxBody = styled.View(({ selected, error }) => ({
//   width: 28,
//   height: 28,
//   marginRight: 10,
//   borderWidth: 1,
//   borderColor: error ? Colors.WineRed : Colors.Purple,
//   backgroundColor: selected ? Colors.Purple : Colors.White,
//   alignItems: 'center',
//   justifyContent: 'center',
// }));

// export const QCheckImage = styled.ImageBackground({
//   height: 14,
//   width: 14,
// });
