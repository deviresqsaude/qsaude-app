import React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import R from 'ramda';
import PropTypes from 'prop-types';
import {
  QCheckContainer,
  QCheckWrapper,
  QCheckBox,
  QCheckImage,
} from './CheckGroupStyle';
import QText from "../../QText";
import { Colors, SystemIcons } from '../../../constants/theme';

const CheckGroup = props => {
  const {
    testID,
    field: {value},
    label,
    options,
    checkHandler
  } = props;
  return (
    <>
      {label &&
      <QText
        preset="title"
        color={Colors.Purple}
        style={{ marginBottom: 20 }}>{label}
      </QText>}
      <QCheckContainer>
        {R.map(
          item => {
            if(!item.visible) return false;
            const itemTestID = {
              input: `${testID}_${item.value}_input`,
              label: `${testID}_${item.value}_label`,
            };
            return (
            <TouchableWithoutFeedback
             testID={testID}
              key={item.value}
              onPress={() => checkHandler(item.value)}>
              <QCheckWrapper>
                <QCheckBox
                  testID={itemTestID.input}
                  accessibilityRole="checkbox"
                  selected={R.includes(item.value, value || [])}>
                  <QCheckImage source={SystemIcons.check} />
                </QCheckBox>
                <QText testID={itemTestID.label}
                  color={Colors.Empress}
                  preset="paragraph"
                  style={{marginLeft: 5, paddingRight: 20}}>
                  {item.description || item.label}
                </QText>
              </QCheckWrapper>
            </TouchableWithoutFeedback>
          );
        }, options)}
      </QCheckContainer>
    </>
  );
};

CheckGroup.propTypes = {
  field: PropTypes.objectOf(PropTypes.any),
  label: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.any),
  checkHandler: PropTypes.func,
};

CheckGroup.defaultProps = {
  field: {},
  label: null,
  options: [],
  checkHandler: () => {}
};

export default CheckGroup;
