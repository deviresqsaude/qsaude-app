import { withHandlers, compose, lifecycle } from 'recompose';
import R from 'ramda';
import QCheckGroup from './CheckGroup';

const enhance = compose(
  withHandlers({
    checkHandler: ({
      field: { name, value },
      form: { setFieldValue },
      onFieldChange
    }) => item => {
      const exits = R.includes(item, !value ? [] : value);
      const checks = !exits
        ? R.concat(!value ? [] : value, [item])
        : R.remove(R.indexOf(item, value), 1, value);
      setFieldValue(name, checks);
      if(onFieldChange) onFieldChange(checks);
    },
  }),
  lifecycle({}),
);

export default enhance(QCheckGroup);
