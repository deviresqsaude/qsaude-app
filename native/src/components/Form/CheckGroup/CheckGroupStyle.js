import styled from 'styled-components/native';
import { Colors } from "../../../constants/theme";


export const QCheckContainer = styled.View({
  flexDirection: "column",
  paddingBottom: 15,
  paddingLeft: 5,
  paddingRight: 5,
});

export const QCheckWrapper = styled.View({
  flexDirection: "row",
  alignItems: "center",
  marginBottom: 20
});

export const QCheckBox = styled.View(({selected}) => ({
  backgroundColor: `${selected ? Colors.Purple : Colors.White}`,
  borderWidth: 2,
  borderColor: Colors.Purple,
  // borderColor: `${selected ? Colors.Purple : Colors.Empress}`,
  borderRadius: 2,
  height: 20,
  marginRight: 8,
  width: 20,
  alignItems: "center",
  justifyContent: "center"
}));

export const QCheckImage = styled.ImageBackground({
  height: 14,
  width: 14
});
