import styled from 'styled-components/native';
import { Colors, SystemFonts } from '../../../constants/theme';

const getColor = ({ error, isInputFocused, touched }) => {
  if (error && touched) return Colors.Magenta;
  if (isInputFocused) return Colors.Purple;
  return Colors.LabelGray;
};

export const BaseInputWrapper = styled.View``;

export const BaseInputLabel = styled.Text(
  ({ touched, bottom, error, isInputFocused }) => ({
    fontFamily: SystemFonts.PlexSansLight,
    fontSize: 14,
    color: getColor({ error, isInputFocused, touched }),
    lineHeight: 16,
    marginBottom: bottom ? 0 : 8,
    marginTop: 8,
    marginLeft: 4,
  }),
);
