import React from 'react';
import PropTypes from 'prop-types';
import { BaseInputWrapper, BaseInputLabel } from './BaseInputStyle';

const BaseInput = props => {
  const { isInputFocused, children, error, label, testID, touched } = props;
  return (
    <BaseInputWrapper>
      {label && label.length > 0 && <BaseInputLabel testID={`${testID}.label`} {...{ error, isInputFocused, touched }}>
        {label}
      </BaseInputLabel>}
      {children}
      <BaseInputLabel touched={touched}  testID={`${testID}.error`} bottom error={error}>
        {error && touched ? String(error) : ''}
      </BaseInputLabel>
    </BaseInputWrapper>
  );
};

BaseInput.propTypes = {
  isInputFocused: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
  error: PropTypes.string,
  label: PropTypes.string,
  touched: PropTypes.oneOfType([PropTypes.any, PropTypes.bool]),
  testID: PropTypes.string.isRequired,
};

BaseInput.defaultProps = {
  error: null,
  touched: false,
  label: null
};

export default BaseInput;
