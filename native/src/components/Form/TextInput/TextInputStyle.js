import styled from 'styled-components/native';
import { Colors, SystemIcons } from '../../../constants/theme';

export const TextInputContainer = styled.View`
  border-radius: 16px;
  height: 56px;
  width: 100%;
  flex-direction: row;
  align-items: center;
  border-color: ${({ error, isInputFocused, touched }) => {
    if (error && touched) return Colors.Magenta;
    if (isInputFocused) return Colors.Purple;
    return Colors.LightGray;
  }};
  border-width: 2;
  padding: 8px;
  position: relative;
`;

export const QTextInput = styled.TextInput(({ disabled }) => ({
  flex: 8,
  height: 48,
  color: disabled ? '#a3a3a3' : '#000',
}));

export const FakeTextInput = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9,
})({
  height: 48,
  width: '100%',
  paddingTop: 4,
  justifyContent: 'center'
});

export const QTextInputAddon = styled.Image.attrs(({ image }) => ({
  source: SystemIcons[image],
}))`
  position: absolute;
  right: 10;
`;
