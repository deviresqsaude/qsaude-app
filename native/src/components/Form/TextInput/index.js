import { withHandlers, withState, compose } from 'recompose';
import TextInput from './TextInput';

/**
 * Used to control the colors of the input
 */

const enhance = compose(
  // controls if the input is focused, used to color borders and label
  withState('isInputFocused', 'setInputFocused', false),
  // handler that toggles it..
  withHandlers({
    toggleInputFocusHandler: ({ isInputFocused, setInputFocused }) => () =>
      setInputFocused(!isInputFocused),
  }),
  /**
   * Middleware integration
   * functions that will execute aside formik onChance funcction.
   *
   * const mid1 = text => console.log(text)
   *
   * <TextInput onChangeMiddleware=[mid1] />
   */
  withHandlers({
    handleFieldBlur: ({
      toggleInputFocusHandler,
      validateOnBlur,
      field: { onBlur: formikOnBlur, value, name },
      form: { setFieldTouched },
    }) => () => {
      toggleInputFocusHandler();
      setFieldTouched(name, true);
      if (validateOnBlur) {
        validateOnBlur(value);
      }
      formikOnBlur();
    },
    handleFieldChange: ({
      form: { handleChange, setFieldTouched },
      field: { name },
    }) => text => {
      handleChange(name)(text);
      setFieldTouched(name, true);
    },
  }),
);

export default enhance(TextInput);
