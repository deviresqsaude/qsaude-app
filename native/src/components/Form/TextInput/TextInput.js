/* eslint-disable react/prop-types */
import React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';
import { TextInputMask } from 'react-native-masked-text';
import { QTextInput, QTextInputAddon, TextInputContainer } from './TextInputStyle';
import BaseInput from '../BaseInput';

/**
 * TextInputComponent implements BaseInput as core, wich controls
   label and error showing.
 */
const TextInputComponent = props => {
  const {
    rightAddon,
    rightAddonCallback,
    leftAddon,
    type,
    mask,
    inputMaskType,
    leftAddonCallback,
    testID,
    secureTextEntry,
    keyboardType,
    disabled,
    returnKeyType,
    form: { errors, touched },
    field: { name: fieldName, value },
    onFieldChange,
    // recompose state/handlers HOC's (index.js)
    isInputFocused,
    handleFieldBlur: onBlur,
    handleFieldChange: onChangeText,
    // focus "setState" toggle
    toggleInputFocusHandler: onFocus,
  } = props;

  const error = errors[fieldName];
  const wasTouched = touched[fieldName];
  return (
    <BaseInput touched={wasTouched} error={error} {...props}>
      <TextInputContainer {...{ error, isInputFocused, touched: wasTouched }}>
        {leftAddon && (
          <TouchableWithoutFeedback onPress={() => leftAddonCallback()}>
            <QTextInputAddon image={leftAddon} />
          </TouchableWithoutFeedback>
        )}
        {type === 'custom' ? (
          <TextInputMask
            type={inputMaskType || "custom"}
            {...{
              disabled,
              autoCapitalize: 'none',
              testID,
              onBlur,
              keyboardType,
              secureTextEntry,
              onFocus,
              onChangeText,
              returnKeyType,
              editable: !disabled,
            }}
            height
            options={{mask}}
            onChangeText={v => {
              onChangeText(v);
              onFieldChange(value);
            }}
            style={{
              flex: 8,
              height: 48,
              color: disabled ? '#a3a3a3' : '#000',
            }}
            value={value ? String(value) : undefined}
          />
        ) : (
          <QTextInput
            {...{
              disabled,
              autoCapitalize: 'none',
              testID,
              onBlur,
              keyboardType,
              secureTextEntry,
              onFocus,
              onChangeText,
              returnKeyType,
              editable: !disabled,
            }}
            onChange={() => {
              onChangeText();
              // onFieldChange(value);
            }}
            onEndEditing={() => onFieldChange(value)}
            value={value ? String(value) : undefined}
          />
        )}
        {rightAddon && (
          <TouchableWithoutFeedback onPress={() => rightAddonCallback()}>
            <QTextInputAddon image={rightAddon} />
          </TouchableWithoutFeedback>
        )}
      </TextInputContainer>
    </BaseInput>
  );
};

TextInputComponent.propTypes = {
  rightAddon: PropTypes.string,
  rightAddonCallback: PropTypes.func,
  leftAddon: PropTypes.string,
  leftAddonCallback: PropTypes.func,
  label: PropTypes.string,
  testID: PropTypes.string,
  onFieldChange: PropTypes.func,
  parameters: PropTypes.objectOf(PropTypes.object),
  onFieldPress: PropTypes.func,
};

TextInputComponent.defaultProps = {
  rightAddon: null,
  rightAddonCallback: () => {},
  leftAddon: null,
  leftAddonCallback: () => {},
  label: '',
  testID: '',
  onFieldChange: () => {},
  parameters: {},
  onFieldPress: () => {},
};

export default TextInputComponent;
