import React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import R from 'ramda';
import PropTypes from 'prop-types';
import {
  QCheckContainer,
  QCheckWrapper,
  QCheckBox,
  QCheckImage,
  QInputChoise
} from './OpenChoiceStyle';
import QText from "../../QText";
import { Colors, SystemIcons } from '../../../constants/theme';

const OpenChoice = props => {
  const {
    testID,
    field: {value},
    label,
    options,
    choiceHandler,
    othersChange
  } = props;
  return (
    <>
      {label && (
        <QText preset="title" color={Colors.Purple} style={{ marginBottom: 10 }}>{label}
        </QText>
      )}
      <QCheckContainer>
        {R.map(
          item => {
            const itemTestID = {
              input: `${testID}_${item.id}_input`,
              label: `${testID}_${item.id}_label`,
            };
            return (
            <TouchableWithoutFeedback
              testID={testID}
              key={item.id}
              onPress={() => choiceHandler(item.id)}>
              <QCheckWrapper>
                <QCheckBox
                testID={itemTestID.input}
                  accessibilityRole="checkbox"
                  selected={R.includes(item.id, value || [])}>
                  <QCheckImage source={SystemIcons.check} />
                </QCheckBox>
                <QText testID={itemTestID.label} color={Colors.Empress} preset="paragraph">
                  {item.label}
                </QText>
              </QCheckWrapper>
            </TouchableWithoutFeedback>
          );
        }, options)}
      </QCheckContainer>
      <QInputChoise placeholder="Outros" onChangeText={val => othersChange(val)}/>
    </>
  );
};

OpenChoice.propTypes = {
  field: PropTypes.objectOf(PropTypes.any),
  label: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.any),
  choiceHandler: PropTypes.func,
};

OpenChoice.defaultProps = {
  field: {},
  label: '',
  options: [],
  choiceHandler: () => {}
};

export default OpenChoice;
