import styled from 'styled-components/native';
import { Colors } from "../../../constants/theme";
import { padding } from 'polished';


export const QCheckContainer = styled.View({
  flexDirection: "column",
  paddingBottom: 15
});

export const QCheckWrapper = styled.View({
  flexDirection: "row",
  alignItems: "center",
  marginBottom: 8
});

export const QCheckBox = styled.View(({selected}) => ({
  backgroundColor: `${selected ? Colors.Purple : Colors.White}`,
  borderWidth: 2,
  borderColor: `${selected ? Colors.Purple : Colors.Empress}`,
  borderRadius: 2,
  height: 24,
  marginRight: 8,
  width: 24,
  alignItems: "center",
  justifyContent: "center"
}));

export const QCheckImage = styled.ImageBackground({
  height: 14,
  width: 14
});

export const QInputChoise = styled.TextInput(() => ({
  borderWidth: 2,
  borderColor: Colors.Purple,
  height: 46,
  borderRadius: 16,
  paddingLeft: 16,
  marginBottom: 10,
  fontSize: 14
}))
