/* eslint-disable react/prop-types */
import React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';
import {
  FakeTextInput,
  QTextInputAddon,
  TextInputContainer,
} from '../TextInput/TextInputStyle';
import BaseInput from '../BaseInput';
import { QText } from '../..';

/**
 * TextInputComponent implements BaseInput as core, wich controls
   label and error showing.
 */
const TextInputComponent = props => {
  const {
    rightAddon,
    rightAddonCallback,
    leftAddon,
    leftAddonCallback,
    onPress,
    form: { errors, touched },
    field: { name: fieldName },
    value,
    // recompose state/handlers HOC's (index.js)
    isInputFocused,
  } = props;

  const error = errors[fieldName];
  const wasTouched = touched[fieldName];

  return (
    <BaseInput touched={wasTouched} error={error} {...props}>
      <TextInputContainer {...{ error, isInputFocused, touched: wasTouched }}>
        {leftAddon && (
          <TouchableWithoutFeedback onPress={() => leftAddonCallback()}>
            <QTextInputAddon image={leftAddon} />
          </TouchableWithoutFeedback>
        )}
        <FakeTextInput onPress={onPress}>
          <QText>{value}</QText>
        </FakeTextInput>
        {rightAddon && (
          <TouchableWithoutFeedback onPress={() => rightAddonCallback()}>
            <QTextInputAddon image={rightAddon} />
          </TouchableWithoutFeedback>
        )}
      </TextInputContainer>
    </BaseInput>
  );
};

TextInputComponent.propTypes = {
  rightAddon: PropTypes.string,
  rightAddonCallback: PropTypes.func,
  leftAddon: PropTypes.string,
  leftAddonCallback: PropTypes.func,
  label: PropTypes.string,
  testID: PropTypes.string,
  onFieldChange: PropTypes.func,
  parameters: PropTypes.objectOf(PropTypes.object),
  onFieldPress: PropTypes.func,
};

TextInputComponent.defaultProps = {
  rightAddon: null,
  rightAddonCallback: () => {},
  leftAddon: null,
  leftAddonCallback: () => {},
  label: '',
  testID: '',
  onFieldChange: () => {},
  parameters: {},
  onFieldPress: () => {},
};

export default TextInputComponent;
