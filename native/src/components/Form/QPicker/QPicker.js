import React from 'react';
import R from 'ramda';
import { Picker, Modal, TouchableWithoutFeedback, Platform } from 'react-native';
import PropTypes from 'prop-types';
import { Container, PickerContainer, PickerInputWrapper, PickerOverlay, Overlay } from './QPickerStyle';
import QTextInput from '../TextInput';
import { SystemIcons } from '../../../constants/theme';

const QPicker = props => {
  const {
    field: { value=-1 },
    testID,
    options,
    picker,
    pickerChange,
    visible,
    setVisible,
    form,
    field,
    label
  } = props;
  return (
    <>
      <TouchableWithoutFeedback
        onPress={() => setVisible(!visible)}>
        <PickerInputWrapper>
          <PickerOverlay/>
          <QTextInput {...{form, label}}
            field={{name: field.name, value: picker}}
            rightAddon="chevromDown"/>
        </PickerInputWrapper>
      </TouchableWithoutFeedback>
      <Modal
        animationType="fade"
        transparent
        visible={visible}>
        <TouchableWithoutFeedback onPress={() => setVisible(!visible)}>
          <Container>
            <Overlay/>
            <PickerContainer
              selectedValue={value}
              onValueChange={item => pickerChange(item)}>
              <Picker.Item label="Selecione uma opção" name="none" value={-1}/>
              {
                R.map(i => <Picker.Item key={`${testID}_${i.id}`} label={i.name} value={i.id}/>, options)
              }
            </PickerContainer>
          </Container>
        </TouchableWithoutFeedback>
      </Modal>
    </>
  )
};

QPicker.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.any,
    label: PropTypes.string,
    value: PropTypes.any
  }))
};

QPicker.defaultProps = {
  options: [{id: "no_one", name: "Selecione uma opção", value: -1}]
}

export default QPicker;
