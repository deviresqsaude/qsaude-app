import styled from 'styled-components/native';
import { Colors } from '../../../constants/theme';

export const Container = styled.View({
  flex: 1,
  justifyContent: "flex-end",
});

export const PickerContainer = styled.Picker({
  width: "100%",
  backgroundColor: Colors.White
});

export const PickerInputWrapper = styled.View({
  position: "relative"
});

export const PickerOverlay = styled.View({
  position: "absolute",
  height: "100%",
  width: "100%",
  left: 0,
  top: 0,
  opacity: .5,
  zIndex: 9999
});

export const Overlay = styled.View({
  flex: 1,
  backgroundColor: 'rgba(0,0,0,0.4)'
})
