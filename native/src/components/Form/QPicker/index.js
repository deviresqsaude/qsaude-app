import { withHandlers, withState, compose, lifecycle } from 'recompose';
import R from 'ramda';
import QPicker from './QPicker';

const enhance = compose(
  withState('visible', 'setVisible', false),
  withState('picker', 'setPicker', false),
  withHandlers({
    pickerChange: ({
      field: { name },
      form: { setFieldValue, handleChange },
      onFieldChange,
      setPicker,
      options
    }) => item => {
      setPicker(item !== -1 ? R.find(R.propEq("id", item))(options).name : null);
      setFieldValue(name, item);
      handleChange(name);
      if(onFieldChange) onFieldChange(item);
    },
  }),
  lifecycle({
    componentDidMount() {}
  }),
);

export default enhance(QPicker);
