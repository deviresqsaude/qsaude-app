import R from 'ramda';
import React from 'react';
import { TouchableWithoutFeedback, Text } from 'react-native';
import PropTypes from 'prop-types';
import { QRadioContainer, QRadioWrapper, QRadio, QRadioPin } from './RadioGroupStyle';
import QText from '../../QText';
import { Colors } from '../../../constants/theme';

const QRadioGroup = props => {
  const {
    testID,
    field: { value = {} },
    label,
    options,
    setRadioValue,
  } = props;

  return (
    <>
      {label && (
        <QText preset="title" color={Colors.Purple} style={{ marginBottom: 10 }}>
          {label}
        </QText>
      )}
      <QRadioContainer accessibilityRole="radiogroup">
        {R.map(item => {
          const itemTestID = {
            input: `${testID}_${item.id}_input`,
            label: `${testID}_${item.id}_label`,
          };
          return (
            <TouchableWithoutFeedback
              testID={testID}
              key={item.id}
              onPress={() => setRadioValue(item.id)}
            >
              <QRadioWrapper accessibilityRole="radio" selected={value === item.id}>
                <QRadio testID={itemTestID.input}>
                  {value === item.id && <QRadioPin />}
                </QRadio>
                <QText
                  testID={itemTestID.label}
                  color={Colors.Empress}
                  preset="paragraph"
                  style={{ width: '90%' }}
                >
                  {item.label}
                </QText>
              </QRadioWrapper>
            </TouchableWithoutFeedback>
          );
        }, options)}
      </QRadioContainer>
    </>
  );
};

QRadioGroup.propTypes = {
  field: PropTypes.objectOf(PropTypes.any),
  label: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.any),
  setRadioValue: PropTypes.func,
};

QRadioGroup.defaultProps = {
  field: {},
  label: null,
  options: [],
  setRadioValue: () => {},
};

export default QRadioGroup;
