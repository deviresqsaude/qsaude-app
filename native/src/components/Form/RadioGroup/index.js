import { withHandlers, compose, lifecycle } from 'recompose';
import RadioGroup from './RadioGroup';

const enhance = compose(
  withHandlers({
    setRadioValue: ({
      field: { name },
      form: { setFieldValue, handleChange },
      onFieldChange
    }) => item => {
      setFieldValue(name, item);
      handleChange(name);
      if(onFieldChange ) onFieldChange(item);
    },
  }),
  lifecycle({}),
);

export default enhance(RadioGroup);
