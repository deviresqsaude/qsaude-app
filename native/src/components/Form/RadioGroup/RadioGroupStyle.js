import styled from 'styled-components/native';
import { Colors } from "../../../constants/theme";

export const QRadioContainer = styled.View({
  flexDirection: "column",
  paddingBottom: 15
});

export const QRadioWrapper = styled.View({
  flexDirection: "row",
  alignItems: "center",
  width: "100%",
  marginBottom: 8
});

export const QRadio = styled.View(() => ({
  backgroundColor: Colors.White,
  borderWidth: 2,
  borderColor: Colors.Empress,
  borderRadius: 12,
  height: 24,
  marginRight: 8,
  width: 24,
  alignItems: "center",
  justifyContent: "center"
}));


export const QRadioPin = styled.View({
  backgroundColor: Colors.Purple,
  borderRadius: 7,
  height: 14,
  width: 14
})
