import styled from 'styled-components/native';
import { Colors } from '../../constants/theme';

export const QAlertWrapper = styled.View(() => ({
  flex: 1,
  alignItems: "center",
  justifyContent: "center",
  position: "absolute",
  width: "100%",
  height: "100%",
  left: 0,
  top: 0
}));

export const QAlertOverlay = styled.View(() => ({
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  position: "absolute",
  width: "100%",
  height: "100%",
  backgroundColor: Colors.Black,
  opacity: .3
}));

export const QAlertBox = styled.View(() => ({
  width: "90%",
  backgroundColor: Colors.White,
  borderRadius: 15,
  padding: 12,
  paddingBottom: 16,
  textAlign: "center",
  alignItems: "center"
}));

export const QAlertImage = styled.Image(() => ({
  height: 92,
  width: 92,
  marginTop: 10
}));

export const QAlertRow = styled.View({
  flexDirection: 'row',
  width: '100%',
  justifyContent: 'space-between',
})
