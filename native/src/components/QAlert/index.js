import React from 'react';
import { Modal, TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';
import { QAlertImage, QAlertBox, QAlertWrapper, QAlertOverlay, QAlertRow } from './QAlertStyle';
import { elevationShadow } from '../../helpers';
import QText from '../QText';
import QButton from '../Button/Button';
import { SystemImages, Colors } from '../../constants/theme';

const QAlert = ({
  image,
  title,
  titleColor,
  description,
  visible,
  isError,
  isSuccess,
  children,
  shouldRenderImage,
  shouldRenderOkButton,
  closeAlert,
  goBack,
  doubleButton,
  firstOnPress,
  secondOnPress,
  firstButtonTitle,
  secondButtonTitle,
  link,
  linkTitle,
  linkOnPress,
  buttonTitle,
  buttonOnPress
}) => {
  let buttonText = 'Fechar';
  if(isSuccess) {
    buttonText = 'Ok'
  } else if (goBack) {
    buttonText = 'Voltar'
  } else if(buttonTitle) {
    buttonText = buttonTitle
  }
  return (
    <Modal animationType="fade" transparent visible={visible}>
      <QAlertWrapper>
        <TouchableWithoutFeedback onPress={() => closeAlert()}>
          <QAlertOverlay />
        </TouchableWithoutFeedback>
        <QAlertBox style={{ ...elevationShadow(null, 10, null) }}>
          {shouldRenderImage && (isError || isSuccess) && (
            <QAlertImage source={isError ? SystemImages.errorImage : SystemImages.successImage}/>
          )}
          {image && <QAlertImage source={image} />}
          {title && (
            <QText style={{ marginTop: 16 }} textCenter preset="title" fontSize={17} color={titleColor}>
              {title}
            </QText>
          )}
          {description && (
            <QText
              textCenter
              color={Colors.TextGray}
              fontSize={17}
              style={{ marginTop: 16}}>
              {description}
            </QText>
          )}
          {shouldRenderOkButton && (isSuccess || isError || goBack || (buttonTitle && (buttonOnPress || closeAlert))) && (
            <QButton
              testID="action.closeAlert"
              text={buttonText}
              inBlock={false}
              onPress={() => { buttonOnPress ? buttonOnPress() : closeAlert()}}
              style={{marginTop: 16}}
            />
          )}
          {doubleButton && firstOnPress && secondOnPress && firstButtonTitle && secondButtonTitle && (
            <QAlertRow>
              <QButton
                testID="action.closeAlert"
                text={firstButtonTitle}
                inBlock={false}
                onPress={firstOnPress}
                preset={'invert'}
                style={{marginTop: 16, width: '49%'}}
              />
              <QButton
                testID="action.closeAlert"
                text={secondButtonTitle}
                inBlock={false}
                onPress={secondOnPress}
                style={{marginTop: 16, width: '49%'}}
              />
            </QAlertRow>
          )}
          {
            link && linkTitle && linkOnPress && (
              <QText
                preset={'link'}
                onPress={linkOnPress}
                inBlock={false}
                style={{textDecorationLine: 'underline'}}
              >
                {linkTitle}
              </QText>
            )
          }
          {/* Quando nao for nem error e nem success render `children` */}
          {children}
        </QAlertBox>
      </QAlertWrapper>
    </Modal>
  );
};

QAlert.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  image: PropTypes.any,
  title: PropTypes.string,
  description: PropTypes.string,
  // eslint-disable-next-line react/forbid-prop-types
  children: PropTypes.any,
  visible: PropTypes.bool,
  shouldRenderImage: PropTypes.bool,
  shouldRenderOkButton: PropTypes.bool,
  isError: PropTypes.bool,
  isSuccess: PropTypes.bool,
  closeAlert: PropTypes.func,
};

QAlert.defaultProps = {
  image: null,
  title: null,
  description: null,
  children: null,
  visible: false,
  isError: false,
  shouldRenderImage: true,
  shouldRenderOkButton: true,
  isSuccess: false,
  closeAlert: () => {},
};

export default QAlert;
