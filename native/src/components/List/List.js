import React from 'react';
import { FlatList } from 'react-native';
import PropTypes from 'prop-types';
import ListItem from './ListItem';
import { ListItemSeparator } from './ListStyle';
import { Colors } from "../../constants/theme";

const SimpleList = (props) => {
  const { data } = props;
  if (!data.length) return null;
  return (
    <FlatList
      style={{
        borderBottomWidth: 1,
        borderTopWidth: 1,
        borderColor: Colors.WhiteSmoke,
      }}
      data={data}
      ItemSeparatorComponent={() => (
        <ListItemSeparator/>
      )}
      renderItem={({item: {avatar, title, subtitle, onPress}}) => (
        <ListItem
          {...{
            avatar,
            title,
            subtitle,
            onPress
          }}
        />
      )}/>
  )
};

SimpleList.propTypes = {
  data: PropTypes.arrayOf(PropTypes.any)
};

SimpleList.defaultProps = {
  data: []
};

export default SimpleList;

