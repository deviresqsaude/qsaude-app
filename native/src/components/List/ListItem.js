import React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';
import { ListItemWrapper,
  ListItemLeft,
  ListItemRight,
  ListItemParagraphWrapper,
  ListItemParagraphTitle,
  ListItemParagraphSubTitle} from './ListStyle';
import { USER_PROFILE_NAME, USER_PROFILE_AVATAR } from "../../constants/mock";
import {
  SystemIcons, Colors } from "../../constants/theme";
import { AvatarIcon, ButtomIconWrapper, ButtomIcon } from "../Icons";

const ListItem = props => {
  const { avatar, title, subtitle, right, onPress } = props;
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <ListItemWrapper>
        <ListItemLeft>
          {
            avatar ?
            <AvatarIcon
              height={36}
              width={36}
              source={avatar}
              style={{
                marginRight: 16
              }}/> : null
          }
          <ListItemParagraphWrapper>
            {title ? <ListItemParagraphTitle>{title}</ListItemParagraphTitle> : null}
            {subtitle ? <ListItemParagraphSubTitle>{subtitle}</ListItemParagraphSubTitle> : null}
          </ListItemParagraphWrapper>
        </ListItemLeft>
        {
          right &&
          <ListItemRight>
            <ButtomIconWrapper>
              <ButtomIcon
                source={SystemIcons.chevromRight}
                tintColor={Colors.Purple}/>
            </ButtomIconWrapper>
          </ListItemRight>
        }
      </ListItemWrapper>
    </TouchableWithoutFeedback>
  )
};

ListItem.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  avatar: PropTypes.any,
  title: PropTypes.string,
  subtitle: PropTypes.string,
  onPress: PropTypes.func
};

ListItem.defaultProps = {
  avatar: USER_PROFILE_AVATAR,
  title: USER_PROFILE_NAME,
  subtitle: '',
  onPress: () => {}
};

export default ListItem;
