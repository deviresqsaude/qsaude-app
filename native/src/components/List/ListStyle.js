import styled from 'styled-components/native';
import { SystemFonts, Colors } from '../../constants/theme';

export const ListItemWrapper = styled.View({
  alignItems: 'center',
  flexDirection: 'row',
  flex: 1,
  height: 75,
  justifyContent: 'space-between',
  paddingLeft: 16,
  paddingRight: 8
});

export const ListItemLeft = styled.View({
  alignItems: 'center',
  flexDirection: 'row'
});

export const ListItemRight = styled.View({
  alignItems: 'center',
  flexDirection: 'row'
});

export const ListItemParagraphWrapper = styled.View({
  flexDirection: 'column',
});

export const ListItemParagraphTitle = styled.Text({
  fontSize: 15,
  fontFamily: SystemFonts.PlexSansMedium,
  color: Colors.Empress
});

export const ListItemParagraphSubTitle = styled.Text({
  fontSize: 12,
  fontFamily: SystemFonts.PlexSansRegular,
  color: Colors.TextGray
});

export const ListItemSeparator = styled.View({
  backgroundColor: Colors.WhiteSmoke,
  flex: 1,
  height: 1
});
