/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components/native';
import { responsiveWidth } from 'react-native-responsive-dimensions';
import QText from '../QText';
import { SystemIcon } from '../Icons';
import { Colors } from '../../constants/theme';

export const Wrapper = styled.View(props => ({
  width: '100%',
  paddingLeft: 16,
  paddingRight: 16,
  paddingBottom: 8,
  paddingTop: 8,
  backgroundColor: '#fff',
  borderBottomColor: '#e2e2e2',
  borderBottomWidth: props.last ? 0 : 1,
}));

export const ChildrenContainer = styled.View({
  marginTop: 12,
});

const LinkContainer = styled.TouchableOpacity({
  marginBottom: 8,
  flexDirection: 'row',
  alignItems: 'center',
});

export const Link = ({ icon, onPress, label, disabled }) => {
  return (
    <LinkContainer onPress={onPress} disabled={disabled} >
      <SystemIcon
        source={icon}
        width={responsiveWidth(6)}
        tintColor={Colors.Purple}
        style={{ marginRight: 4 }}
      />
      <QText preset="link" >
        {label}
      </QText>
    </LinkContainer>
  );
};
