/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import QText from '../QText';
import { Wrapper, ChildrenContainer, Link } from './DoctorCardStyle';

const DoctorCard = ({ title, description, links, children, last }) => {
  return (
    <Wrapper last={last}>
      <QText color="#7f7f7f" preset="black">
        {title}
      </QText>
      <QText color="#7f7f7f">{description}</QText>
      <ChildrenContainer>
        {children && children}
        {links && links.length && links.map(item => {
          if (item.visible) {
            return <Link key={item.label} {...item} /> 
          }
          })}
      </ChildrenContainer>
    </Wrapper>
  );
};

// DoctorCard.propTypes = {
//   // title: PropTypes.string.isRequired,
//   // description: PropTypes.string.isRequired,
//   children: PropTypes.element,
//   links: PropTypes.array,
// };

// DoctorCard.defaultProps = {
//   title: '-',
//   description: '-',
//   children: null,
//   links: [],
// };

export default DoctorCard;
