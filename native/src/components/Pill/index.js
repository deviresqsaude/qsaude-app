import React from 'react';
import { PillWrapper, PillBody, PillText } from './PillStyles';

export default function Pill({ text, onPress, selected }) {
  return (
    <PillWrapper onPress={onPress}>
      <PillBody {...{ selected }}>
        <PillText {...{ selected }}>{text}</PillText>
      </PillBody>
    </PillWrapper>
  );
}
