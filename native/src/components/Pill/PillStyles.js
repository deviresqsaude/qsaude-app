import styled from 'styled-components/native';
import QText from '../QText';
import { Colors, Sizing } from '../../constants/theme';

export const PillWrapper = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9,
})({});

export const PillBody = styled.View(({ selected }) => ({
  width: 62,
  height: 34,
  alignItems: 'center',
  justifyContent: 'center',
  marginBottom: 12,
  borderColor: Colors.Purple,
  borderWidth: 1,
  borderRadius: 10,
  backgroundColor: selected ? Colors.Purple : Colors.White,
}));

export const PillText = styled(QText).attrs({
  preset: 'paragraph',
})(({ selected }) => ({
  color: selected ? Colors.White : Colors.Purple,
  fontSize: Sizing.small,
}));
