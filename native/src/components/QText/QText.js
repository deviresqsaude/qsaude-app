import styled from 'styled-components/native';
import PropTypes from 'prop-types';
import { Sizing, Colors, SystemFonts } from '../../constants/theme';

const getTextStyles = ({ color, fontSize, preset, textAlign }) => {
  const availablePresets = {
    caption: {
      color: Colors.Black,
      fontFamily: SystemFonts.RubikRegular,
      fontSize: Sizing.xsmall,
    },
    body: {
      color: Colors.TextGray,
      fontFamily: SystemFonts.PlexSansRegular,
      fontSize: Sizing.small,
    },
    dataLabel: {
      color: Colors.Purple,
      fontFamily: SystemFonts.PlexSansRegular,
      fontSize: Sizing.medium,
    },
    dataText: {
      color: Colors.DataLabel,
      fontFamily: SystemFonts.PlexSansRegular,
      fontSize: Sizing.medium,
    },
    black: {
      color: Colors.Black,
      fontFamily: SystemFonts.PlexSansBold,
      fontSize: Sizing.medium,
    },
    paragraph: {
      color: Colors.Black,
      fontFamily: SystemFonts.PlexSansRegular,
      fontSize: Sizing.small,
    },
    bold: {
      color: Colors.Black,
      fontFamily: SystemFonts.PlexSansBold,
      fontSize: Sizing.small,
    },
    link: {
      color: Colors.Purple,
      fontFamily: SystemFonts.PlexSansMedium,
      fontSize: Sizing.small,
    },
    navTitle: {
      color: Colors.Purple,
      fontSize: Sizing.xxlarge,
      fontFamily: SystemFonts.PlexSansBold,
    },
    itemTitle: {
      color: Colors.TextGray,
      fontSize: Sizing.xlarge,
      fontFamily: SystemFonts.PlexSansBold,
    },
    navTitleSimple: {
      color: Colors.Purple,
      fontSize: Sizing.medium,
      fontFamily: SystemFonts.PlexSansBold,
    },
    title: {
      color: Colors.Black,
      fontFamily: SystemFonts.PlexSansMedium,
      fontSize: Sizing.medium,
    },
    headline: {
      color: Colors.Purple,
      fontFamily: SystemFonts.PlexSansBold,
      fontSize: Sizing.xlarge,
    },
  };

  const style = availablePresets[preset];
  if (color) style.color = color;
  if (fontSize) style.fontSize = fontSize;
  if (textAlign) style.textAlign = textAlign;
  return style;
};

const QText = styled.Text(
  ({
    color,
    fontSize,
    preset,
    textCenter,
    margin: { top, left, horizontal, vertical, bottom, right },
  }) => ({
    ...getTextStyles({ color, preset, fontSize }),
    marginTop: vertical || top,
    marginBottom: vertical || bottom,
    marginLeft: horizontal || left,
    marginRight: horizontal || right,
    textAlign: textCenter ? 'center' : 'left',
  }),
);

QText.defaultProps = {
  margin: {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  preset: 'paragraph',
};

QText.propTypes = {
  preset: PropTypes.oneOf([
    'paragraph',
    'body',
    'itemTitle',
    'navTitle',
    'navTitleSimple',
    'link',
    'black',
    'headline',
    'title',
    'bold',
    'caption',
    'dataLabel',
    'dataText'
  ]),
  margin: PropTypes.shape({
    top: PropTypes.number,
    bottom: PropTypes.number,
    left: PropTypes.number,
    right: PropTypes.number,
  }),
  fontSize: PropTypes.number,
  color: PropTypes.string,
  textAlign: PropTypes.string,
};

export default QText;
