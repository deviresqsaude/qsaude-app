import React from 'react';
import R from 'ramda';
import moment from 'moment';
import QText from '../QText';
import {
  CardContainer,
  CardWrapper,
  StatusIndicator,
  CardInfo,
  getIconName,
  getPrimaryColor,
} from './EventCardStyles';
import { SystemIcon } from '../Icons';
import { Colors } from '../../constants/theme';

const getStatus = (status, date) => {
  return R.cond([
    [R.equals('cancelled'), R.always(`Cancelado`)],
    [R.equals('arrived'), R.always(`Realizado no dia ${moment(date).format('DD/MM')} às ${moment(date).format('HH:mm')}.`)],
    [R.equals('fulfilled'), R.always(`Realizado no dia ${moment(date).format('DD/MM')} às ${moment(date).format('HH:mm')}.`)],
    [R.equals('completed'), R.always(`Realizado no dia ${!!date ? moment(date).format('DD/MM') : '- '}.`)],
    [R.T, R.always(`Agendado para o dia ${moment(date).format('DD/MM')} às ${moment(date).format('HH:mm')}.`)],
  ])(status);
}

const EventCard = ({ name, onPress, start, status, type }) => {
  return (
    <CardContainer onPress={onPress}>
      <StatusIndicator status={status}>
        <SystemIcon
          height={12}
          tintColor={type  != 1 && status == 'completed' ? Colors.White : getPrimaryColor(status)}
          name={getIconName(status)}
        />
      </StatusIndicator>
      <CardWrapper status={status}>
        <CardInfo>
          <QText color={Colors.TextGray} numberOfLines={1}>{type != 1 && status == 'completed' ? `${name}` : `Dr.(a) ${name}`}</QText>
          <QText color={Colors.TextGray} preset="caption">
            {getStatus(status, start)}
          </QText>
        </CardInfo>

        <SystemIcon width={8} tintColor={Colors.DataLabel} name="chevromRight" />
      </CardWrapper>
    </CardContainer>
  );
};

export default EventCard;
