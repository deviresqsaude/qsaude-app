import R from 'ramda';
import styled from 'styled-components/native';
import { Colors } from '../../constants/theme';

export const getPrimaryColor = R.cond([
  [R.equals('Vencido'), R.always(Colors.Magenta)],
  [R.equals('error'), R.always(Colors.Magenta)],
  [R.equals('cancelled'), R.always('rgba(229, 23, 102, 0.5)')], // cancelado Icon=Magenda BG=White
  [R.equals('arrived'), R.always(Colors.White)], // concluído Icon=White BG=Purple\
  [R.equals('fulfilled'), R.always(Colors.White)], // realizado ,
  [R.equals('completed'), R.always(Colors.ApriCor)], //exam
  [R.T, R.always(Colors.Purple)], // agendado Icon=Purple BG=White
]);

export const getSecondaryColor = R.cond([
  [R.equals('cancelled'), R.always(Colors.White)], // cancelado Icon=Magenda BG=White
  [R.equals('arrived'), R.always(Colors.Purple)], // concluído Icon=White BG=Purple
  [R.equals('fulfilled'), R.always(Colors.Purple)], //realizado,
  [R.equals('completed'), R.always(Colors.ApriCor)], //exam
  [R.T, R.always(Colors.White)], // agendado Icon=Purple BG=White
]);

export const getIconName = R.cond([
  [R.equals('cancelled'), R.always('close')],
  [R.T, R.always('check')],
]);

export const getBorderColor =  R.cond([
  [R.equals('cancelled'), R.always('rgba(229, 23, 102, 0.5)')], // cancelado Icon=Magenda BG=White
  [R.equals('completed'), R.always(Colors.ApriCor)], //exam
  [R.T, R.always(Colors.Purple)], // agendado Icon=Purple BG=White
]);
// export const StatusIcon = styled(SystemIcon).attrs({
//   name: 'check',
// })``;

// export const StatusIcon = styled(SystemIcon).attrs(({ status }) => {
//   const name =""

//   return {
//     height: 12,
//     name,
//     tintColor: getPrimaryColor(status),
//   };
// })``;

// export const RightArrow = styled(SystemIcon).attrs({
//   width: 8,
//   tintColor: Colors.DataLabel,
//   name: 'chevromRight',
// })``;

export const CardContainer = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9,
})({
  width: '100%',
  alignItems: 'flex-end',
  height: 48,
  paddingLeft: 13,
  marginBottom: 12
});

export const CardWrapper = styled.View(({ status }) => ({
  width: '100%',
  borderRadius: 10,
  borderWidth: 1.5,
  height: '100%',
  borderColor: getBorderColor(status),
  opacity: 1,
  paddingLeft: 22,
  paddingRight: 16,
  paddingTop: 4,
  paddingBottom: 4,
  alignItems: 'center',
  flexDirection: 'row',
}));

export const CardInfo = styled.View({
  justifyContent: 'center',
  flex: 1,
});

export const StatusIndicator = styled.View(({ status }) => ({
  width: 26,
  zIndex: 4,
  height: 26,
  borderRadius: 26,
  backgroundColor: getSecondaryColor(status),
  borderColor: getPrimaryColor(status),
  borderWidth: 1.5,
  position: 'absolute',
  left: 0,
  top: 10,
  alignItems: 'center',
  justifyContent: 'center',
}));
