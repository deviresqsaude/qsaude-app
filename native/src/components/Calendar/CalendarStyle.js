import styled from 'styled-components/native';
import { Colors } from '../../constants/theme';

export const CalendarFooter = styled.View({
  flexDirection: 'row',
  paddingHorizontal: 16,
  paddingBottom: 5,
  justifyContent: 'flex-end',
  borderBottomColor: Colors.LightGray,
  borderBottomWidth: 1,
  marginBottom: 16,
  alignItems: 'center',
});

export const CalendarSubtitleMark = styled.View(({ color }) => ({
  width: 8,
  height: 8,
  backgroundColor: color || Colors.Purple,
  borderRadius: 4,
  marginRight: 4,
}));

export const PlaceholderWrapper = styled.View(({ loading }) => ({
  position: 'absolute',
  justifyContent: 'center',
  alignItems: 'center',
  flex: 1,
  alignSelf: 'stretch',
  height: 310,
  width: '100%',
  zIndex: 10,
  backgroundColor: 'rgba(255, 255, 255, .8)',
  display: loading ? 'flex' : 'none ',
}));

export const LoaderContainer = styled.View({
  justifyContent: 'center',
  alignItems: 'center',
  height: 80,
  width: 80,
  borderRadius: 16,
  backgroundColor: Colors.LightSmoke,
});

export const Loader = styled.ActivityIndicator.attrs({ size: 'large', color: Colors.Purple })``;
