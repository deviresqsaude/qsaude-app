import React from 'react';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import PropTypes from 'prop-types';
import { Colors, SystemFonts } from '../../constants/theme';
import QText from '../QText';
import {
  CalendarFooter,
  CalendarSubtitleMark,
  PlaceholderWrapper,
  LoaderContainer,
  Loader,
} from './CalendarStyle';

LocaleConfig.locales.pt = {
  monthNames: [
    'Janeiro',
    'Fevereiro',
    'Março',
    'Abril',
    'Maio',
    'Junho',
    'Julho',
    'Agosto',
    'Setembro',
    'Outubro',
    'Novembro',
    'Dezembro',
  ],
  monthNamesShort: [
    'jan',
    'fev',
    'mar',
    'abr',
    'mai',
    'jun',
    'jul',
    'ago',
    'set',
    'out',
    'nov',
    'dez',
  ],
  dayNames: [
    'Domingo',
    'Segunda-feira',
    'Terça-feira',
    'Quarta-feira',
    'Quinta-feira',
    'Sexta-feira',
    'Sábado',
  ],
  dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
  today: 'Hoje',
};
LocaleConfig.defaultLocale = 'pt';

const EventsCalendar = props => {
  const { markedDates, onSelectDay, loading, footerTag, minDate , markingType, examTag } = props;
  return (
    <>
      {loading && <CalendarPlaceholder loading={loading} />}
      <Calendar
        {...props}
        displayLoadingIndicator
        markingType={markingType}
        minDate={minDate || undefined}
        markedDates={markedDates}
        onDayPress={({ dateString }) => onSelectDay(dateString)}
        theme={{
          selectedDayBackgroundColor: Colors.Purple,
          selectedDayTextColor: Colors.White,
          todayTextColor: Colors.Purple,
          monthTextColor: Colors.Purple,
          textMonthFontFamily: SystemFonts.PlexSansBold,
          textMonthFontWeight: '700',
          arrowColor: Colors.Purple,
          'stylesheet.day.multiDot': {
            today: {
              backgroundColor: 'transparent',
            },
            selected: {
              borderRadius: 12,
              borderColor: Colors.SelectedGreen,
              borderWidth: 2,
            },
            selectedText: {
              color: Colors.Purple,
              fontWeight: "bold"
            },
            text: {
              color: Colors.TextGray,
              fontSize: 16
            },
            base: {
              width: 36,
              height: 36,
              borderWidth: 2,
              borderColor: Colors.White,
              borderRadius: 12,
              alignItems: 'center'
            },
            dot: {
              width: 6,
              height: 6,
              marginVertical: 2,
              marginLeft: 2,
              marginRight: 1,
              borderRadius: 3,
              opacity: 0
            },
            visibleDot: {
              opacity: 1,
              backgroundColor: Colors.Purple
            },
            selectedDot: {
              backgroundColor: Colors.Purple
            },
          },
          'stylesheet.day.basic': {
            selected: {
              borderRadius: 12,
              borderColor: Colors.SelectedGreen,
              borderWidth: 2,
            },
            selectedText: {
              color: Colors.TextGray,
            },
            text: {
              color: Colors.TextGray,
            },
            base: {
              width: 32,
              height: 32,
              borderWidth: 2,
              borderColor: Colors.White,
              borderRadius: 12,
              alignItems: 'center'
            }
          },
        }}
      />
      <CalendarFooter>
        {
          examTag &&
          <>
            <CalendarSubtitleMark color={Colors.ApriCor} />
            <QText fontSize={10} preset="dataText" style={{marginRight: 8}} >
              Exames
            </QText>
          </>
        }
        <CalendarSubtitleMark />
        <QText fontSize={10} preset="dataText">
          {footerTag}
        </QText>
      </CalendarFooter>
    </>
  );
};

EventsCalendar.propTypes = {
  markedDates: PropTypes.objectOf(PropTypes.any),
  onSelectDay: PropTypes.func.isRequired,
  footerTag: PropTypes.string,
  examTag: PropTypes.bool,
  markingType: PropTypes.string,
};

EventsCalendar.defaultProps = {
  markedDates: {},
  footerTag: 'Consultas',
  markingType: 'simple',
};

const CalendarPlaceholder = ({ loading }) => (
  <PlaceholderWrapper loading={loading}>
    <LoaderContainer>
      <Loader />
    </LoaderContainer>
  </PlaceholderWrapper>
);

export default EventsCalendar;
