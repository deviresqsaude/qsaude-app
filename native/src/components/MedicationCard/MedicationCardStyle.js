import React from 'react';
import styled from 'styled-components/native';
import LinearGradient from 'react-native-linear-gradient';
import { TouchableOpacity } from 'react-native-gesture-handler';

export const CardWrapper = ({ children, onPress, testID }) => {
  return (
    <CardShadow>
      <TouchableOpacity testID={testID} activeOpacity={0.9} onPress={onPress}>
        <LinearGradient
          colors={['#6e56ed', '#e75bfc']}
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          style={{
            borderRadius: 10,
            paddingLeft: 16,
            paddingRight: 16,
            paddingTop: 8,
            paddingBottom: 8,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          {children}
        </LinearGradient>
      </TouchableOpacity>
    </CardShadow>
  );
};

export const DataWrapper = styled.View({
  justifyContent: 'center',
  flex: 1,
  flexWrap: 'nowrap',
});

const CardShadow = styled.View({
  borderRadius: 10,
  backgroundColor: '#fff',
  shadowColor: '#000',
  shadowOffset: { width: 3, height: 5 },
  elevation: 3,
  shadowOpacity: 0.3,
  shadowRadius: 10,
  marginBottom: 16,
});
