import React from 'react';
import { SystemIcon } from '../Icons';
import QText from '../QText';
import { CardWrapper, DataWrapper } from './MedicationCardStyle';
import { SystemIcons } from '../../constants/theme';

// const hideIfNoData = hasNoData => branch(hasNoData, renderNothing);

// eslint-disable-next-line react/prop-types
const MedicineCard = ({ display, onPress, when, frequency, dosageInstruction, id }) => {
  return (
    <CardWrapper onPress={onPress} testID={id} >
      <DataWrapper>
        <QText preset="headline" color="#fff">
          {String(display).split('(')[0]}
        </QText>
        <QText margin={{ bottom: 4 }} preset="caption" color="rgba(255, 255, 255, .7)">
          {String(display).split('(')[1]}
        </QText>
        <QText margin={{ bottom: 2 }} preset="caption" color="#fff" >
          {`${frequency || '-'} (${when})`}
        </QText>
        <QText preset="caption" color="#fff">
          {dosageInstruction}
        </QText>
      </DataWrapper>
      <SystemIcon
        resizeMode="contain"
        // height={38}
        tintColor="#fff"
        source={SystemIcons.medicine}
      />
    </CardWrapper>
  );
};

// const enhance = compose(
//   hideIfNoData(
//     ({ display, when, frequency, dosageInstruction }) =>
//       !display || !when || !frequency || !dosageInstruction,
//   ),
// );

export default MedicineCard;
