import React from 'react';
import { Modal, TouchableWithoutFeedback, Platform } from 'react-native';
import PropTypes from 'prop-types';
import { ModalContainer, ModalBody, ModalHeader } from './ModalStyle';
import QText from '../QText';
import { Colors, SystemIcons, Sizing } from '../../constants/theme';
import { SystemIcon, BackButtonContainer } from '../Icons';

const QSModal = props => {
  const {
    title,
    animationType,
    visible,
    showCloseAction,
    onRequestClose,
    onShow,
    transparent,
    children,
    closeModal,
    testID
  } = props;
  return (
    <Modal
      animationType={animationType}
      visible={visible}
      onRequestClose={() => closeModal()}
      onShow={() => onShow()}
      transparent={transparent}
      testID={testID}>
      <ModalContainer testID={`${testID}-container`}>
        <TouchableWithoutFeedback testID={`${testID}-close`} onPress={() => closeModal()}>
          <BackButtonContainer>
            <SystemIcon
              source={SystemIcons.close}
              height={Sizing.small}
              width={Sizing.small}
              style={{ marginTop: Platform.OS ? 15 : 0,marginBottom: 16, marginLeft: 20, opacity: !showCloseAction ? 0 : 1 }}
            />
          </BackButtonContainer>
        </TouchableWithoutFeedback>
        <ModalHeader>
          <QText preset="navTitle" color={Colors.Purple}>
            {title}
          </QText>
        </ModalHeader>
        <ModalBody testID={`${testID}-body`}>{children}</ModalBody>
      </ModalContainer>
    </Modal>
  );
};

QSModal.propTypes = {
  title: PropTypes.string,
  animationType: PropTypes.string,
  visible: PropTypes.bool,
  showCloseAction: PropTypes.bool,
  onShow: PropTypes.func,
  transparent: PropTypes.bool,
  onRequestClose: PropTypes.func,
  // eslint-disable-next-line react/forbid-prop-types
  children: PropTypes.any,
  closeModal: PropTypes.func,
  testID: PropTypes.string
};

QSModal.defaultProps = {
  title: '',
  animationType: 'slide',
  onRequestClose: () => {},
  visible: PropTypes.bool || false,
  showCloseAction: true,
  children: null,
  onShow: () => {},
  closeModal: () => {},
  transparent: false,
  testID: "modal"
};

export default QSModal;
