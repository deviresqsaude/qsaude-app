import styled from 'styled-components/native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

export const ModalContainer = styled.View({
  flex: 1,
  paddingBottom: 24,
  // paddingLeft: 16,
  // paddingRight: 16
});

export const ModalHeader = styled.View({
  marginHorizontal: 20,
  flexDirection: "column"
})

export const ModalBody = styled.View({
  flex: 1,
  // paddingLeft: 16,
  // paddingRight: 16
});