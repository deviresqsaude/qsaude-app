import React from 'react';
import styled from 'styled-components/native';
// import { Dimensions } from 'react-native';
import QText from '../QText';
import { QButton } from '..';
import { Sizing, SystemImages } from '../../constants/theme';

const EmptyData = ({
  title = '-',
  description = '-',
  buttonText = '',
  buttonType = 'secondary',
  buttonBlock = false,
  onPress = () =>{},
  imageSource=SystemImages.imgRecord,
  renderImage=true,
  linkText,
  linkOnPress,
  children
}) => (
  <>
    <EmptyWrapper>
      {renderImage && <EmptyImage source={imageSource} resizeMode="contain" />}
      <QText fontSize={Sizing.medium} style={{ marginBottom: 4 }} textCenter preset="link">
        {title}
      </QText>
      <QText style={{ marginBottom: 16 }} textCenter preset="body">
        {description}
      </QText>
      {buttonText !== '' && (
        <QButton text={buttonText} preset={buttonType} inBlock={buttonBlock} onPress={onPress} />
      )}
      {
        linkText && <QText preset="link" style={{ textDecorationLine: 'underline', marginTop: 16}} onPress={linkOnPress}>{linkText}</QText>
      }
    </EmptyWrapper>
    {
      children
    }
  </>
);

const EmptyWrapper = styled.View({
  paddingLeft: 16,
  paddingRight: 16,
  alignItems: 'center',
  backgroundColor: '#fff',
  flex: 1,
  justifyContent: 'center',
  // height: Dimensions.get('window').height * 0.6,
});

const EmptyImage = styled.Image({
  marginBottom: 16,
  height: 150,
  // width: '100%',
});

export default EmptyData;
