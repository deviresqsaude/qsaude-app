import React from 'react';
import { Platform } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import { DatePickerContainer, DoneButtonContainer, SaveButton } from './DatePickerStyles';

export default function DateTimePickerComponent(props) {
  const { value, mode, onChange, closePicker, closeLabel } = props;

  if (Platform.OS === 'android') {
    return <DateTimePicker mode={mode} onChange={onChange} value={value} />;
  }

  return (
    <DatePickerContainer>
      <DoneButtonContainer>
        <SaveButton onPress={closePicker}>{closeLabel}</SaveButton>
      </DoneButtonContainer>
      <DateTimePicker mode={mode} onChange={onChange} value={value} />
    </DatePickerContainer>
  );
}

DateTimePickerComponent.defaultProps = {
  value: null,
  mode: 'date',
  onChange: () => {},
  closePicker: () => {},
  closeLabel: 'Salvar',
};
