import styled from 'styled-components/native';
import { Colors, Sizing } from '../../constants/theme';
import QText from '../QText';

export const DatePickerContainer = styled.View({
  height: 200,
  width: '100%',
});

export const DoneButtonContainer = styled.View({
  alignItems: 'flex-end',
  backgroundColor: Colors.LightSmoke,
  paddingHorizontal: 32,
  paddingVertical: 8,
});

export const SaveButton = styled(QText).attrs({
  preset: 'link',
  fontSize: Sizing.medium,
})({});
