import React from 'react';
import { Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import LineChart from '../Chart/line-chart';
import { Colors } from '../../constants/theme';

/**
 *
 * @param {Object} chartData
 */
const QVitalSignChartComponent = ({ chartData }) => (
  <>
    <LineChart
      data={chartData}
      width={Dimensions.get('window').width - 32} // from react-native
      height={320}
      fromZero
      chartConfig={{
        backgroundColor: Colors.Purple,
        backgroundGradientFrom: '#fff',
        backgroundGradientTo: '#fcfcfc',
        decimalPlaces: 2, // optional, defaults to 2dp
        color: (opacity = 1) => `rgba(120, 128, 242, ${opacity})`,
      }}
      onDataPointClick={params => {
        // console.log(params);
      }}
      bezier
      style={{
        marginVertical: 8,
        borderRadius: 16,
      }}
    />
  </>
);

QVitalSignChartComponent.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  chartData: PropTypes.object.isRequired,
};

export default QVitalSignChartComponent;
