import React from 'react';
import { compose, branch, renderComponent, withProps } from 'recompose';
import { Dimensions } from 'react-native';
import R from 'ramda';
import { Placeholder, PlaceholderMedia, Fade } from 'rn-placeholder';
import moment from 'moment';
import styled from 'styled-components/native';

import QVitalSignChartComponent from './QVitalSignChart';
import EMPTY_STATE from '../../../assets/images/img_record.png';
import QText from '../QText';

const EmptyWrapper = styled.View({
  marginLeft: 16,
  marginRight: 16,
  alignItems: 'center',
  justifyContent: 'center',
  height: Dimensions.get('window').height * 0.6,
});

const EmptyImage = styled.Image({
  marginBottom: 38,
  width: 300,
});

const ChartPlaceHolder = () => (
  <Placeholder style={{ marginBottom: 21 }} Animation={Fade}>
    <PlaceholderMedia style={{ width: '100%', height: 300 }} />
  </Placeholder>
);

export const ChartEmpty = () => (
  <EmptyWrapper>
    <EmptyImage source={EMPTY_STATE} resizeMode="contain" />
    <QText margin={{ bottom: 4 }} textCenter preset="link">
      Nenhum dado cadastrado
    </QText>
    <QText textCenter preset="caption">
      Pressione o botão abaixo para inserir uma nova medida.
    </QText>
  </EmptyWrapper>
);

// helpers
const extractPtBrFormatedDates = result =>
  R.map(item => moment(R.prop('measuredAt', item)).format('DD/MM'), result).reverse();

const exctractMeasures = (result, vitalSign) => {
  if (vitalSign === 'bloodPressure') {
    const extractAmountFromSubmeasures = minAndMax =>
      R.map(R.path(['measure', 'amount']), minAndMax); // ([{ type, measures: { amount } }, { type, measures: { amount } }]) => [120,80]

    const removeMeanPressureFromServer = R.map(
      item =>
        R.assoc(
          'subMeasures',
          R.filter(submeasure => submeasure.type !== 'meanPressure', item.subMeasures),
          item,
        ),
      result,
    );

    const extractedSubmeasures = R.map(
      R.path(['subMeasures']),
      removeMeanPressureFromServer,
    ); // returns = [[{ min }, { max }] ,[{ min }, { max }]]
    const minAndMaxPressures = R.map(extractAmountFromSubmeasures, extractedSubmeasures); // returns [[120,80], [100,90]]
    const meanPressures = R.map(pressure => R.mean(pressure), minAndMaxPressures);
    return meanPressures;
  }
  return R.map(R.path(['measure', 'amount']), result).reverse();
};

const withDataReadyForChart = withProps(
  ({ vitalSign, data: { vitalSign: result, loading } }) => {
    return {
      chartData: {
        // Chart horizontal label (Dates)
        labels: loading ? [] : extractPtBrFormatedDates(result),
        // Chart data itself
        datasets: loading ? [] : [{ data: exctractMeasures(result, vitalSign) }],
      },
    };
  },
);

// HOC that will "intercept" the rendering of our component in case of (!data && !!loading)
const renderWhileLoading = (component, propName = 'data') =>
  branch(
    props => R.path(['data', 'loading'], props) && !R.path(['data', propName], props), // predicated
    renderComponent(component),
  );

// HOC that will "intercept" the rendering of our component in case of Loading (!data && !loading)
const renderEmptyData = (component, propName = 'data') =>
  branch(
    props =>
      !R.path(['data', 'loading'], props) && R.isEmpty(R.path(['data', propName], props)), // predicated
    renderComponent(component),
  );

const enhance = compose(
  withDataReadyForChart,
  renderWhileLoading(ChartPlaceHolder, 'vitalSign'),
  renderEmptyData(ChartEmpty, 'vitalSign'),
);

export default enhance(QVitalSignChartComponent);
