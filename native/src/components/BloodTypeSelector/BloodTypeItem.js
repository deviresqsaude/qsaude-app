import React from 'react';
import styled from 'styled-components/native';
import { Colors } from '../../constants/theme';

const BloodCell = styled.TouchableOpacity({
  backgroundColor: Colors.Purple,
  width: 80,
  height: 80,
  marginRight: 16,
  borderRadius: 80 / 2,
  alignItems: 'center',
  justifyContent: 'center',
});

const BloodText = styled.Text({
  color: Colors.White,
  fontWeight: 700,
  fontSize: 20,
});

const BloodTypeItem = ({ value, label, onPress }) => {
  return (
    <BloodCell onPress={onPress}>
      <BloodText>{value || label}</BloodText>
    </BloodCell>
  );
};

export default BloodTypeItem;
