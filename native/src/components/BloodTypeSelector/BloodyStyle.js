import styled from 'styled-components/native';
// import { Colors } from '../../constants/theme';

export const Wrapper = styled.View({
  flex: 1,
  backgroundColor: 'rgba(0, 0, 0, .4)',
  alignItems: 'center',
  // justifyContent: 'center',
  padding: 16,
});

export const ListWrapper = styled.View({ flex: 1 });

export const List = styled.ScrollView({
  width: '100%',
  height: 100, 
});

export const IndicatorWrapper = styled.View({
  width: '100%',
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
})

List.defaultProps = {
  horizontal: true,
  contentContainerStyle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
};
