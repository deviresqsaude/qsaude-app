import React from 'react';
import PropTypes from 'prop-types';
import { Modal, ActivityIndicator } from 'react-native';
import { Wrapper, List, IndicatorWrapper } from './BloodyStyle';
import Item from './BloodTypeItem';
import { QButton } from '..';

const BLOOD_TYPES = ['A+', 'A-', 'B+', 'B-', 'O+', 'O-', 'AB+', 'AB-'];

const BloodTypeSelector = props => {
  const { closePicker, onChange, isOpen, loading } = props;
  return (
    <Modal transparent visible={isOpen} onRequestClose={() => closePicker()}>
      <Wrapper>
          {
            loading ? 
            <IndicatorWrapper>
              <ActivityIndicator color='#fff' /> 
            </IndicatorWrapper>
            : 
            <>
              <List>
                {BLOOD_TYPES.map(item => {
                    return (
                      <Item
                        onPress={() => {
                          onChange(item);
                        }}
                        key={item}
                        value={item}
                      />
                    );
                  })}
              </List>
              <QButton text="Fechar" onPress={closePicker} />
            </>
          }
      </Wrapper>
    </Modal>
  );
};

BloodTypeSelector.propTypes = {
  onChange: PropTypes.func,
};

BloodTypeSelector.defaultProps = {
  onChange: item => console.log(`${item} selected.`),
};

export default BloodTypeSelector;
