import React from 'react';
import { View, Animated, TouchableWithoutFeedback } from 'react-native';
import R from 'ramda';
import { Image } from 'react-native';
import MapView, { Marker, Callout } from 'react-native-maps';
import Q_MARKER from '../../../assets/images/img_pin.png';
import { DoctorCard, SystemIcon }from '../../components';
import EmptyData from '../EmptyData';
import QButton from '../Button/Button';
import { SystemImages, Colors } from '../../constants/theme';

const DoctorsContainer = {
  backgroundColor: Colors.White,
  borderTopRightRadius: 12,
  borderTopLeftRadius: 12,
  bottom: 0,
  height: 240,
  left: 0,
  position: "absolute",
  flexDirection: "column",
  width: "100%",
  zIndex: 9998
};

const extractLocationFromMarkerObj = ({ coordinate }) => {
  if(!!coordinate.latitude&& !!coordinate.longitude) {
    return coordinate
  }
};

const filterMarkers = markers => {
  return R.filter(item => !!item.coordinate.latitude&&!!item.coordinate.longitude, markers)
}

const getSelectedmarker = ({ markers, coordinate }) => {
  return R.find(R.propEq('coordinate', coordinate), markers);
}

export default class MapViewComponent extends React.Component {
  render() {
    const { region, markers, requestPermission, noDescription, unidade, setUnidade, setRegion, showUnity, animateDoctors, toggleDoctors, collapse, setCollapse } = this.props;
    const filteredMarkers = noDescription ? R.map(item => R.merge(item, { links: null, description: null }), filterMarkers(markers)) : filterMarkers(markers);
    if (!region.latitude || !region.longitude) {
      return (
        <>
          <EmptyData
            imageSource= {SystemImages.localizationOffline}
            title="Falha ao carregar o mapa"
            description="É necessário permissões de localização para visualizar o mapa"
          />

          <QButton
            style={{ marginHorizontal: 16, marginBottom: 42 }}
            text="Tentar novamente"
            onPress={requestPermission}
          />
        </>
      );
    }

    return (
      <>
        <MapView
          region={region}
          ref={ref => {
            this.mapRef = ref;
          }}
          onMapReady={() => {
            const markerData = filteredMarkers.map(extractLocationFromMarkerObj);
            this.mapRef.fitToCoordinates([...markerData, region], {
              edgePadding: { bottom: 100, top: 50, left: 50, right: 50 },
            });
          }}
          style={{ flex: 1 }}
          showsUserLocation
          showsMyLocationButton
          loadingEnabled
        >
          {filteredMarkers && filteredMarkers.map(marker => (
              <Marker {...marker} 
                stopPropagation={true}
                onSelect={(e)=> {
                  Animated.spring(animateDoctors, {
                    toValue: 40,
                  }).start();
                  setCollapse(true)
                  setRegion(e.nativeEvent.coordinate ? {...region, ...e.nativeEvent.coordinate} : {...region})
                  setUnidade(getSelectedmarker({ markers, coordinate: e.nativeEvent.coordinate}))
                }} 
                onPress={(e)=> {
                    Animated.spring(animateDoctors, {
                      toValue: 40,
                    }).start();
                    setCollapse(true)
                    setRegion(e.nativeEvent.coordinate ? {...region, ...e.nativeEvent.coordinate} : {...region})
                    setUnidade(getSelectedmarker({ markers, coordinate: e.nativeEvent.coordinate}))
                  }}
                key={marker.title}
                image={Q_MARKER}
              >
                <Callout style={{ width: 230 }}>
                  <DoctorCard last {...marker}/>
                </Callout>
              </Marker>
            ))}
        </MapView>
        {
          showUnity && unidade.title && 
          <Animated.View style={{...DoctorsContainer, paddingBottom: 80, transform: [{translateY: animateDoctors}] }}>
            <TouchableWithoutFeedback onPress={() => toggleDoctors()}>
              <View style={{width: "100%", height: 40, justifyContent: "center", alignItems: "center"}}>
                <View style={{height: 20, width: 20, justifyContent: "center", alignItems: "center", transform: [{rotate: `${collapse ? 0 : 180}deg`}]}}>
                  <SystemIcon name="chevromDown" tintColor={Colors.Purple}/>
                </View>
              </View>
            </TouchableWithoutFeedback>
            <DoctorCard {...unidade} last />
          </Animated.View>
        }
      </>
    );
  }
}
// -23.568613
// -46.648817
