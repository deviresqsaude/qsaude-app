import { compose, lifecycle, withHandlers, withState } from 'recompose';
import { Platform, Alert, Animated } from 'react-native';
import { request, PERMISSIONS, RESULTS, openSettings } from 'react-native-permissions';
import Geolocation from '@react-native-community/geolocation';
import MapView from './MapView';

const withUserLocationState = withState('region', 'setRegion', {
  latitudeDelta: 0.0143,
  longitudeDelta: 0.0134,
});

const withSelection = withState('unidade', 'setUnidade', {});

// get user position
const withMapController = withHandlers({
  getCurrentPosition: ({ setRegion, region }) => () => {
    const onPositionSuccess = ({ coords: { latitude, longitude } }) =>
      setRegion({
        ...region,
        latitude,
        longitude,
      });
    const onPositionFailed = e =>
      console.log(`failed to get user position \n ${JSON.stringify(e, null, 2)}`);

    Geolocation.getCurrentPosition(onPositionSuccess, onPositionFailed, {
      timeout: 5000,
      // enableHighAccuracy: true,
    });
  },
});

const withPermissionRequest = withHandlers({
  requestPermission: ({ getCurrentPosition }) => () => {
    const permissionString =
      Platform.OS === 'ios'
        ? PERMISSIONS.IOS.LOCATION_WHEN_IN_USE
        : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION;

    request(permissionString).then(result => {
      switch (result) {
        case RESULTS.GRANTED:
          getCurrentPosition();
          break;
        case RESULTS.BLOCKED:
          Alert.alert(
            'Permissões negadas',
            'Acesse as configurações do aplicativo e conceda as permissões necessárias',
            [
              {
                text: 'Configurações',
                onPress: () =>
                  openSettings().catch(e => console.log('failed to open settings', e)),
              },
              {
                text: 'Cancelar',
                style: 'destructive',
              },
            ],
          );
          break;
        default:
          break;
      }
    });
  },
});

const stateCollapse = withState('collapse', 'setCollapse', false);
const stateAnimateCard = withState('animateDoctors', 'setAnimateDoctors', new Animated.Value(40));

const withCardAnimations = withHandlers({
  toggleDoctors: ({animateDoctors, collapse, setCollapse}) => () => {
    Animated.spring(animateDoctors, {
      toValue: !collapse ? 40 : 200,
    }).start();
    setCollapse(!collapse);
  },
})

const withInitMap = lifecycle({
  componentDidMount() {
    this.props.getCurrentPosition();
  },
});

export default compose(
  stateCollapse,
  stateAnimateCard,
  withSelection,
  withUserLocationState,
  withMapController,
  withPermissionRequest,
  withInitMap,
  withCardAnimations,
)(MapView);
