import { branch, renderComponent } from 'recompose';
import R from 'ramda';

// HOC that will "intercept" the rendering of our component in case of (!data && !!loading)
const renderWhileLoading = (component, propName = 'data', loadingPath = null) =>
  branch(
    props =>
      R.path(loadingPath || ['data', 'loading'], props) &&
      !R.path(['data', propName], props), // predicated
    renderComponent(component),
  );

export default renderWhileLoading;
