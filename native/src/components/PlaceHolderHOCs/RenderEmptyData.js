import React from 'react';
import { branch, renderComponent } from 'recompose';
import R from 'ramda';

// HOC that will "intercept" the rendering of our component in case of Loading (!data && !loading)
const renderEmptyData = (Component, propName = 'data') =>
  branch(
    props =>
      !R.path(['data', 'loading'], props) &&
      (R.isEmpty(R.path(['data', propName], props)) || // not empty
      R.isNil(R.path(['data', propName], props))), // not null
    renderComponent(() => <Component/>),
  );

export default renderEmptyData;
