import React from 'react';
import { Container, InputLocation, InputLabel } from './GeolocationStyle';

const state = {
  reference: {}
};

const Geolocation = props => {
  const {
    loading,
    setFocus,
    focus,
    label,
    field: { name, value},
    form: { errors },
    handleFieldChange
  } = props;
  const error = errors[name];
  return (
    <Container>
      <InputLabel isFocused={focus}>{label}</InputLabel>
      <InputLocation
        value={value ? String(value) : undefined}
        ref={target => {state.reference = target}}
        placeholder="Digite deu cep aqui"
        keyboardType="number-pad"
        onChangeText={val => handleFieldChange(val, state.reference)}
        isFocused={focus}
        onFocus={() => setFocus(true)}
        onBlur={() => setFocus(false)}
        error={error}
        editable={!loading}/>
      {error ? <InputLabel error bottom>{error}</InputLabel> : null}
    </Container>
  )
}

export default Geolocation;
