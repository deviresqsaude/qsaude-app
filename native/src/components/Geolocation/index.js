import { withHandlers, withState, compose } from 'recompose';
import R from 'ramda';
import Geolocation from './Geolocation';
import { geolocation } from '../../helpers';

const enhance = compose(
  withState('loading', 'setLoading', false),
  withState('focus', 'setFocus', false),
  withHandlers({
    handleFieldChange: props => async (zipcode, ref) => {
      const {
        setLoading,
        field: { name },
        form: { setFieldValue, handleChange, setFieldError },
        dependent,
      } = props;
      handleChange(name)(zipcode);
      setFieldValue(name, zipcode); // Setting current input value
      if (zipcode.length === 8) {
        setLoading(true);
        const address = await geolocation(zipcode);
        if (address.error) setFieldError(name, address.message);
        R.forEach(d => setFieldValue(d, address[d]), dependent || []); // Setting dependent values
        setLoading(false);
        ref.focus();
      }
    },
  })
);

export default enhance(Geolocation);
