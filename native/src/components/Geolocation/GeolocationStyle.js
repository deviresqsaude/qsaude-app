import styled from 'styled-components/native';
import { Colors, SystemFonts } from '../../constants/theme';

const getColor = ({ error, isFocused }) => {
  if (error) return Colors.Magenta;
  if (isFocused) return Colors.Purple;
  return Colors.LabelGray;
};

export const Container = styled.View({
  marginBottom: 12,
  position: "relative",
  paddingBottom: 8
});

export const InputLabel = styled.Text(
  ({ bottom, error, isFocused }) => ({
    fontFamily: SystemFonts.PlexSansLight,
    fontSize: 14,
    color: getColor({ error, isFocused }),
    lineHeight: 16,
    marginBottom: bottom ? 0 : 8,
    marginTop: 8,
    marginLeft: 4,
  }),
);

export const InputLocation = styled.TextInput(
  ({isFocused, editable, error}) => {
    return {
      borderWidth: 2,
      // eslint-disable-next-line no-nested-ternary
      borderColor: error ? Colors.Magenta : isFocused ? Colors.Purple : Colors.LightGray,
      color: !editable ? '#a3a3a3' : '#000',
      height: 56,
      borderRadius: 16,
      paddingLeft: 8,
      paddingRight: 8,
      marginBottom: 4,
      fontSize: 14,
    }
  }
);
