import { compose, withProps, withHandlers } from 'recompose';
import Stepper from './Stepper';

const propsStepper = withProps(({numberSteps, currentStep})=> ({
  numberSteps,
  currentStep
}));

const handlers = withHandlers({
  setStep: ({onStepHandler}) => step => {
    onStepHandler(step);
  }
})

const enchance = compose(
  propsStepper,
  handlers
);

export default enchance(Stepper);
