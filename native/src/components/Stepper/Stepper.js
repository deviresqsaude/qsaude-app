import React from 'react';
import { View, TouchableWithoutFeedback } from 'react-native';
import { SystemIcon, QText } from '../index';
import { BreadCrumbsWrapper, CircleWrapper, Dot, DotContainer, Path, StepperCircle } from './StepperStyle';
import { Colors } from '../../constants/theme';

const FormBreadcrumb = ({ numberSteps, currentStep=0, setStep, style}) => {
  const breadcrumbList = Array.from(Array(numberSteps).keys());
  const renderBreadCrumbItem = step => (
    <React.Fragment key={`bread-crumb-${step}`}>
      <BreadcrumbStep
        setStep={setStep}
        step={step}
        current={step === currentStep}
        done={currentStep > step}
        label={step + 1}
      />
      {step !== numberSteps - 1 && <Path done={currentStep > step} />}
    </React.Fragment>
  )
  return (
    <BreadCrumbsWrapper style={{...style}}>{breadcrumbList.map(renderBreadCrumbItem)}</BreadCrumbsWrapper>
  );
};

const BreadcrumbStep = props => {
  const {done, current, label, setStep, step} = props;
  return (
    <View style={{ alignItems: 'center' }}>
      <TouchableWithoutFeedback onPress={() => setStep(step)}>
        <CircleWrapper>
          <StepperCircle {...props}>
            {done || current ? (
              done ? (
                <SystemIcon name="check" tintColor={Colors.GreenSuccess} height="14" width="14"/>
              ) : (
                <DotContainer>
                  <Dot />
                  <Dot />
                  <Dot />
                </DotContainer>
              )
            ) : null}
          </StepperCircle>
        </CircleWrapper>
      </TouchableWithoutFeedback>
      <QText
        color={
          !done && !current
          ? Colors.LightGray
          : done
          ? Colors.GreenSuccess
          : Colors.Purple
        }
        fontSize={14}
        preset="navTitle">
        {label}
      </QText>
    </View>
  )
};

export default FormBreadcrumb;
