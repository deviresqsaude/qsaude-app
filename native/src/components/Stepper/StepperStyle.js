import styled from 'styled-components/native';
import { Colors } from '../../constants/theme';

export const BreadCrumbsWrapper = styled.View({
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
  maxWidth: "100%"
});

export const DotContainer = styled.View({
  width: 16,
  flexDirection: "row",
  justifyContent: "space-between"
});

export const Dot = styled.View({
  backgroundColor: Colors.Purple,
  width: 4,
  borderRadius: 2,
  height: 4,
});

export const CircleWrapper = styled.View({
  width: 34,
  height: 34,
  justifyContent: 'center',
  alignItems: 'center',
});

export const StepperCircle = styled.View(({ done, current }) => ({
  padding: 4,
  minWidth: done || current ? 25 : 15,
  minHeight: done || current ? 25 : 15,

  borderColor:
  // !done && !current = gray | done: Purple | current: Green
  // eslint-disable-next-line no-nested-ternary
  !done && !current ? Colors.LightGray : done ? Colors.GreenSuccess : Colors.Purple,
  borderWidth: 1,
  borderRadius: done || current ? 30 / 2 : 20 / 2,
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
}));

export const Path = styled.View(props => ({
  height: 1,
  backgroundColor: props.done ? Colors.Purple : Colors.LightGray,
  flex: 1,
  marginLeft: 4,
  marginRight: 4,
  marginBottom: 25,
}));
