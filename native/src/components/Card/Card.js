import React from 'react'
import PropTypes from 'prop-types';
import {
  CardWrapper,
  CardInfoWrapper,
  CardInfoTitle,
  CardInfoSubtitle,
  CardNotification,
  CardDate
} from './CardStyle';
import { AvatarIcon } from '../Icons';
import { elevationShadow } from '../../helpers';
import { Colors } from '../../constants/theme';
import { TouchableWithoutFeedback } from 'react-native'

const Card = props => {
  const { onPress, avatar, title, subtitle, date, notification } = props;
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <CardWrapper style={{ ...elevationShadow(Colors.Black, 5) }}>
        {
          avatar ?
          <AvatarIcon
            width={36}
            height={36}
            source={avatar}
            style={{
              marginRight: 8
            }}/> :
          null
        }
        <CardInfoWrapper>
          <CardInfoTitle>{title}</CardInfoTitle>
          <CardInfoSubtitle>{subtitle}</CardInfoSubtitle>
        </CardInfoWrapper>
        <CardNotification>{notification}</CardNotification>
        <CardDate>{date}</CardDate>
      </CardWrapper>
    </TouchableWithoutFeedback>
  )
};

Card.propTypes = {
  avatar: PropTypes.any,
  title: PropTypes.string,
  subtitle: PropTypes.string,
  date: PropTypes.string,
  notification: PropTypes.string
};

Card.defaultProps = {
  avatar: null,
  title: "",
  subtitle: "",
  date: "",
  notification: ""
};

export default Card;
