import styled from 'styled-components/native';
import { Colors, Sizing, SystemFonts } from './../../constants/theme';

export const CardWrapper = styled.View({
  backgroundColor: Colors.Light,
  borderRadius: 16,
  flexDirection: "row",
  paddingLeft: 16,
  paddingRight: 16,
  paddingTop: 16,
  paddingBottom: 16,
  position: "relative",
  width: "100%"
});

export const CardInfoWrapper = styled.View({

});

export const CardInfoTitle = styled.Text({
  color: Colors.Black,
  fontFamily: SystemFonts.MonserratRegular,
  fontSize: Sizing.small,
  marginBottom: 8
});

export const CardInfoSubtitle = styled.Text({
  color: Colors.Mortar,
  fontFamily: SystemFonts.RubikRegular,
  fontSize: Sizing.small,
});

export const CardNotification = styled.Text({
  backgroundColor: Colors.Magenta,
  borderRadius: 8,
  color: Colors.Light,
  fontFamily: SystemFonts.RubikRegular,
  fontSize: Sizing.xsmall,
  lineHeight: 16,
  height: 16,
  position: "absolute",
  textAlign: "center",
  top: -4,
  right: 0,
  width: 16,
});

export const CardDate = styled.Text({
  color: Colors.Empress,
  fontFamily: SystemFonts.RubikRegular,
  fontSize: Sizing.xsmall,
  position: 'absolute',
  right: 16,
  top: 16
});
