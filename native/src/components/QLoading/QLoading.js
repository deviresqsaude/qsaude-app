import React from 'react';
import { Modal, ActivityIndicator } from 'react-native'
import PropTypes from 'prop-types';
import { QLoadingWrapper, QLoadingOverlay, QLoadingBox } from './QLoadingStyle'
import QText from '../QText';
import { Colors } from '../../constants/theme'

const QLoading = ({description, visible, simple=false}) => (
  <Modal animationType="fade" transparent visible={visible}>
    <QLoadingWrapper>
      <QLoadingOverlay/>
      {
        simple ?
        <ActivityIndicator size="large" color={Colors.White} style={{marginTop: 16}}/> :
        <QLoadingBox>
          <ActivityIndicator size="large" color={Colors.Purple} style={{marginTop: 16}}/>
          <QText style={{ marginTop: 16, marginBottom: 12 }} textCenter preset="title" fontSize={17}>{description}</QText>
        </QLoadingBox>
      }
    </QLoadingWrapper>
  </Modal>
);

QLoading.propTypes = {
  visible: PropTypes.bool,
  description: PropTypes.string
};

QLoading.defaultProps = {
  visible: false,
  description: "Carregando"
};

export default QLoading

