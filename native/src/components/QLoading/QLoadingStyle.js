import styled from 'styled-components/native';
import { Colors } from '../../constants/theme';

export const QLoadingWrapper = styled.View(() => ({
  flex: 1,
  alignItems: "center",
  justifyContent: "center",
  position: "absolute",
  width: "100%",
  height: "100%",
  left: 0,
  top: 0,
}));

export const QLoadingOverlay = styled.View(() => ({
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  position: "absolute",
  width: "100%",
  height: "100%",
  backgroundColor: Colors.Black,
  opacity: .3
}));

export const QLoadingBox = styled.View(() => ({
  width: "60%",
  backgroundColor: Colors.White,
  borderRadius: 15,
  padding: 12,
  textAlign: "center",
  alignItems: "center"
}));
