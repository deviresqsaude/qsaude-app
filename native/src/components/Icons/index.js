import React from 'react';
import styled from 'styled-components/native';
import { Colors, SystemIcons } from '../../constants/theme';
import { hasDeviceNotch } from '../HeaderNav/HeaderNavStyle';

export const BackButtonContainer = styled.View({
  width: 70,
  alignItems: 'flex-start',
  paddingTop: hasDeviceNotch(),
})

export const ButtomIconWrapper = styled.View(props => ({
  alignItems: 'center',
  backgroundColor: props.backgroung || 'transparent',
  borderRadius: props.radius || 0,
  height: props.height || 24,
  justifyContent: 'center',
  overflow: 'hidden',
  paddingTop: 5,
  paddingBottom: 5,
  width: props.width || 24,
}));

export const ButtomIcon = styled.Image.attrs({
  resizeMode: 'contain',
})`
  ${props => ({
    tintColor: props.tintColor || `${Colors.Black}`,
    height: props.height || 20,
    width: props.width || 20,
  })}
`;

export const SystemIcon = styled.Image.attrs(({ name, source }) => ({
  source: name ? SystemIcons[name] : source,
  resizeMode: 'contain',
}))`
  ${props => ({
    width: props.width || 20,
    height: props.height || 20,
    tintColor: props.tintColor || ``,
  })}
`;

SystemIcon.defaultProps = { resizeMode: 'contain' };

export const AvatarIcon = styled.ImageBackground(props => ({
  borderRadius: props.radius || 24,
  backgroundColor: Colors.LightGray,
  height: props.height || 24,
  overflow: 'hidden',
  width: props.width || 24,
}));

export const ButtomIconProgress = styled.View(props => ({
  backgroundColor: props.backgroung || 'transparent',
  bottom: 0,
  height: `${props.progress || 0}%`,
  left: 0,
  position: 'absolute',
  width: '100%',
  zIndex: '-1',
}));
