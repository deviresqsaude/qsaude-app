import React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import { withNavigation } from 'react-navigation';
import { HeaderSimpleWrapper, BackbuttonWrapper } from './HeaderNavStyle';
import { Colors, SystemIcons } from '../../constants/theme';
import { SystemIcon } from '../Icons';
import { LOGIN } from '../../constants/TestIds';

const HeaderNavBack = props => {
  const { navigation } = props;
  return (
    <HeaderSimpleWrapper>
      <TouchableWithoutFeedback testID={LOGIN.form.navBackButtonSimple} onPress={() => navigation.goBack()}>
        <BackbuttonWrapper>
          <SystemIcon
            source={SystemIcons.chevromLeft}
            tintColor={Colors.Purple}
            style={{
              marginRight: 8,
            }}
          />
        </BackbuttonWrapper>
      </TouchableWithoutFeedback>
    </HeaderSimpleWrapper>
  );
};
export default withNavigation(HeaderNavBack);
