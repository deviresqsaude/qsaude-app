import React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import { withNavigation } from 'react-navigation';
import PropTypes from 'prop-types';
import QText from '../QText';
import { HeaderSimpleWrapper, NavTitleWrapper, BackbuttonWrapper, NavSimpleTitleWrapper } from './HeaderNavStyle';
import { Colors, SystemIcons, Sizing } from '../../constants/theme';
import { SystemIcon } from '../Icons';
import { LOGIN } from '../../constants/TestIds';

const HeaderNavSimple = props => {
  const { title, navigation } = props;
  return (
    <HeaderSimpleWrapper>
      <TouchableWithoutFeedback testID={LOGIN.form.navBackButtonSimple} onPress={() => navigation.goBack()}>
        <BackbuttonWrapper>
          <SystemIcon
            // width={Sizing.large}
            height={Sizing.large}
            source={SystemIcons.chevromLeft}
            tintColor={Colors.Purple}
          />
        </BackbuttonWrapper>
      </TouchableWithoutFeedback>
      <NavSimpleTitleWrapper>
        <QText preset="navTitle" style={{flex: 1}}>{title}</QText>
      </NavSimpleTitleWrapper>
    </HeaderSimpleWrapper>
  );
};
HeaderNavSimple.propTypes = {
  title: PropTypes.string,
};
HeaderNavSimple.defaultProps = {
  title: 'Title',
};
export default withNavigation(HeaderNavSimple);
