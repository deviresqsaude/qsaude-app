/* eslint-disable react/prop-types */
/* eslint-disable react/forbid-prop-types */
import React from 'react';
import { withNavigation } from 'react-navigation';
import { TouchableWithoutFeedback, View, Share } from 'react-native';
import PropTypes from 'prop-types';

import {
  HeaderNavWrapper,
  NavTitleWrapper,
  NavSosWrapper,
  NavSos,
  NavSosText,
  BackbuttonWrapper,
} from './HeaderNavStyle';
import { SystemIcons, Colors } from '../../constants/theme';
import { SHARE_MESSAGE } from '../../constants/messages';
import { SystemIcon } from '../Icons';
import { elevationShadow } from '../../helpers';
import { QText } from '..';

const shareApp = async () => {
  try {
    await Share.share(
      {
        message: SHARE_MESSAGE,
        title: 'QSaude',
      },
      {
        excludedActivityTypes: [
          'com.apple.UIKit.activity.Print',
          'com.apple.UIKit.activity.MarkupAsPDF',
          'pinterest.ShareExtension',
        ],
      },
    );
  } catch (e) {
    console.log(`Error: ${e}`);
  }
};

const HeaderNav = ({ title, navigation: { isFirstRouteInParent, pop, state } }) => {
  return (
    <HeaderNavWrapper>
      <NavSosWrapper>
        {isFirstRouteInParent() ? (
          <View />
        ) : (
          <TouchableWithoutFeedback
            testID={`back_${state.routeName}`}
            activeOpacity={0.9}
            onPress={() => pop()}
          >
            <BackbuttonWrapper>
              <SystemIcon
                source={SystemIcons.chevromLeft}
                tintColor={Colors.Purple}
                style={{
                  marginRight: 8,
                }}
              />
            </BackbuttonWrapper>
          </TouchableWithoutFeedback>
        )}
        <TouchableWithoutFeedback
          activeOpacity={0.9}
          style={{ alignSelf: 'flex-end' }}
          onPress={() => shareApp()}
        >
          <NavSos style={{ ...elevationShadow(Colors.Black, 5) }}>
            <SystemIcon
              source={SystemIcons.share}
              width={18}
              height={18}
              tintColor={Colors.White}
              style={{
                marginRight: 8,
              }}
            />
            <NavSosText>Indique o Q</NavSosText>
          </NavSos>
        </TouchableWithoutFeedback>
      </NavSosWrapper>
      <NavTitleWrapper>
        <QText preset="navTitle">{title}</QText>
      </NavTitleWrapper>
    </HeaderNavWrapper>
  );
};

HeaderNav.defaultProps = {
  title: '',
};

HeaderNav.propTypes = {
  title: PropTypes.string,
};

export default withNavigation(HeaderNav);
