/* eslint-disable no-nested-ternary */
import styled from 'styled-components/native';
import { Platform } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Colors, Sizing, SystemFonts } from '../../constants/theme';

// export const hasDeviceNotch = () => {
//   return Platform.OS === 'android' && getStatusBarHeight() > 24 ? 50 :
//     Platform.OS === 'android' ? 0 :
//     Platform.OS === 'ios' && getStatusBarHeight() > 24 ? 44 : 20;
// }

export const hasDeviceNotch = () => {
  return Platform.OS === 'android' ? 20 :
    Platform.OS === 'ios' && getStatusBarHeight() > 24 ? 44 : 20;
}

export const HeaderSimpleWrapper = styled.View({
  width: '100%',
  height: '100%',
  flexDirection: 'row',
  // paddingBottom: 16,
  alignItems: 'center',
  justifyContent: 'flex-start',
  zIndex: 9999,
  // paddingTop: hasDeviceNotch(),
});

export const HeaderNavWrapper = styled.View({
  // flexDirection: 'column',
  width: '100%',
  alignItems: 'center',
  paddingTop: hasDeviceNotch(),
  // paddingTop: 20,
  paddingBottom: 16,
  zIndex: 9999,
});

export const BackbuttonWrapper = styled.View({
  justifyContent: 'center',
  paddingLeft: 16,
  width: 50,
});

export const NavSosWrapper = styled.View({
  flexDirection: 'row',
  justifyContent: 'space-between',
  width: '100%',
  alignItems: 'center',
  flexWrap: 'wrap',
});

export const NavSos = styled.View({
  alignItems: 'center',
  alignSelf: 'flex-end',
  backgroundColor: Colors.Purple,
  borderBottomLeftRadius: 18,
  borderTopLeftRadius: 18,
  flexDirection: 'row',
  height: 30,
  paddingRight: 14,
  paddingLeft: 10,
});

export const NavSosText = styled.Text({
  color: Colors.White,
  fontFamily: SystemFonts.RubikMedium,
  fontSize: Sizing.small,
});

export const NavTitleWrapper = styled.View({
  paddingLeft: 17,
  paddingRight: 17,
  width: '100%',
  flexDirection: "column",
});

export const NavSimpleTitleWrapper = styled.View({
  // border: 1,
  paddingVertical: 4,
  flex: 1,
  flexWrap: 'wrap',
  flexDirection: 'row'
})

// export const NavSimpleTitle = styled.Text({
//   color: `${Colors.Black}`,
//   fontSize: Sizing.medium,
//   fontFamily: SystemFonts.MonserratBold,
// });