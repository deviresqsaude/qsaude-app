import styled from 'styled-components/native';
import { Colors } from '../../constants/theme';

export const Overlay = styled.View(() => ({
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  position: "absolute",
  width: "100%",
  height: "100%",
  backgroundColor: Colors.Black,
  opacity: .3
}));