import React from 'react';
import {TouchableWithoutFeedback} from 'react-native';
import PropTypes from 'prop-types';
import { Overlay } from './ModalOverlayStyle'

const ModalOverlay = ({closeAlert}) => {
  return (
    <TouchableWithoutFeedback onPress={() => closeAlert()}>
      <Overlay />
    </TouchableWithoutFeedback>
  );
}

ModalOverlay.propTypes = {
  closeAlert: PropTypes.func
};

ModalOverlay.defaultProps = {
  closeAlert: () => {}
}

export default ModalOverlay;

