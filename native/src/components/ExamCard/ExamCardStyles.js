import styled from 'styled-components/native';
import QText from '../QText';
import { SystemIcon } from '../Icons';
import { Colors, SystemFonts } from '../../constants/theme';

export const ExamBody = styled.View({
  width: '100%',
  paddingLeft: 19,
  paddingRight: 14,
  paddingVertical: 15,
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  borderColor: Colors.Purple,
  borderWidth: 1,
  borderRadius: 16,
});

export const ExamLabel = styled(QText).attrs({
  preset: 'body',
})({
  fontFamily: SystemFonts.PlexSansBold,
  marginBottom: 0
});

export const ExamDetail = styled(QText).attrs({
  preset: 'body',
})({
});

export const ExamTextWrapper = styled.View({
  width: "90%",
  flexDirection: 'column'
})

export const ExamWrapper = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9,
})({
  marginBottom: 10,
  width: '100%',
});
