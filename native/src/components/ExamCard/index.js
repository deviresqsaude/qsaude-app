import React from 'react';
import { ExamBody, ExamLabel, ExamWrapper, ExamDetail, ExamTextWrapper} from './ExamCardStyles';
import { SystemIcon } from '../Icons';

export default function ExamCard(props) {
  const { name, onPress, description } = props;

  return (
    <ExamWrapper onPress={onPress}>
      <ExamBody>
        <ExamTextWrapper>
          {name && <ExamLabel>{name}</ExamLabel>}
          {description && <ExamDetail>{description}</ExamDetail>}
        </ExamTextWrapper>
        <SystemIcon name="chevromRight" tintColor="#707070" />
      </ExamBody>
    </ExamWrapper>
  );
}

ExamCard.defaultProps = {
  onPress: () => {},
  name: '–',
};
