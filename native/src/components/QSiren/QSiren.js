import React from 'react';
import { TouchableWithoutFeedback, Image } from 'react-native';
import PropTypes from 'prop-types';
import { QSirenWrapper } from './QSirenStyle';
import { elevationShadow } from '../../helpers';
import { SystemIcons, Sizing } from '../../constants/theme';
import QText from '../QText';

function QSiren({ callbackSiren }) {
  return (
    <TouchableWithoutFeedback
      onPress={() => callbackSiren()}
      style={{
        right: 0,
        bottom: 0,
        backgroundColor: '#000000',
        position: 'absolute',
      }}
    >
      <QSirenWrapper style={{ ...elevationShadow('#000000', 5, 0.4) }}>
        <Image style={{ marginBottom: 14, width: 50 }} source={SystemIcons.telecuidado} />
        <QText
          fontSize={10}
          textCenter
          color="#FFFFFF"
          style={{
            position: 'absolute',
            bottom: 14,
          }}
        >
          Telecuidado
        </QText>
      </QSirenWrapper>
    </TouchableWithoutFeedback>
  );
}

QSiren.propTypes = {
  callbackSiren: PropTypes.func,
  visible: PropTypes.bool,
};

QSiren.defaultProps = {
  callbackSiren: () => {},
  visible: true,
};

export default QSiren;
