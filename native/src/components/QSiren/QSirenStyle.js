import styled from 'styled-components/native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Colors, SystemIcons } from '../../constants/theme';

export const QSirenWrapper = styled.View.attrs({
  source: SystemIcons.siren,
})({
  backgroundColor: Colors.Red,
  height: 62,
  width: 62,
  borderRadius: 62 / 2,
  right: 26,
  alignItems: 'center',
  justifyContent: 'center',
  position: 'absolute',
  bottom: getStatusBarHeight() > 24 ? 98 : 74,
});
