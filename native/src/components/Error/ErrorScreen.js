import React from 'react';
import PropTypes from 'prop-types';
import { ErrorContainer, ErrorImage } from './ErrorStyle';
import { SystemIcon } from '../Icons';
import QText from "../QText";
import { Colors, SystemIcons } from "../../constants/theme";

const ErrorScreen = ({title, description, image}) => {
  return (
    <ErrorContainer>
      {image &&
      <ErrorImage>
        <SystemIcon source={image} height={40} width={40} tintColor={Colors.Purple}/>
      </ErrorImage>}
      <QText
        preset="title"
        color={Colors.TextGray}
        fontSize={16}
        textAlign="center"
        margin={{ left: 16, right: 16, top: 0, bottom: 8 }}>{title}</QText>
      <QText preset="paragraph">{description}</QText>
    </ErrorContainer>
  )
};

ErrorScreen.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  // eslint-disable-next-line react/forbid-prop-types
  image: PropTypes.any
};

ErrorScreen.defaultProps = {
  title: "Error",
  description: "",
  image: SystemIcons.close
}

export default ErrorScreen;
