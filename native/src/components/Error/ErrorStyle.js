import styled from 'styled-components/native';
import { Colors } from './../../constants/theme';

export const ErrorContainer = styled.View({
  flex: 1,
  alignItems: "center",
  justifyContent: "center",
  textAlign: "center"
});

export const ErrorImage = styled.View({
  backgroundColor: Colors.LightSmoke,
  borderRadius: 60,
  height: 120,
  width: 120,
  alignItems: "center",
  justifyContent: "center",
  marginBottom: 50
});
