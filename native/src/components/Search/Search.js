import React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';
import { Container, Input, Icon, } from './SearchStyle';

function Search({style, onChangeText, onEndEditing, text, focus, setFocus, placeholder, clear, onClear, noBorder}) {
  return (
    <Container {...{style}}>
      <Icon {...{name: "search", focus}}/>
      <Input
        {...{focus, placeholder, noBorder}}
        value={text || ""}
        onBlur={() => setFocus(false)}
        onFocus={() => setFocus(true)}
        onChangeText={t => onChangeText(t)}
        onEndEditing={() => onEndEditing(text)}/>
      {clear && !!text && <TouchableWithoutFeedback onPress={() => onClear()}><Icon {...{name: "close", clear}}/></TouchableWithoutFeedback>}
    </Container>
  )
}

Search.propTypes = {
  onSearch: PropTypes.func,
  onEndEditing: PropTypes.func,
  placeholder: PropTypes.string,
  clear: PropTypes.bool,
  noBorder: PropTypes.bool,
  text: PropTypes.string
};

Search.defaultProps = {
  onSearch: () => {},
  onEndEditing: () => {},
  placeholder: "Pesquisar",
  clear: false,
  noBorder: false,
  text: ""
}

export default Search

