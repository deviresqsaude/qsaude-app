import styled from 'styled-components/native';
import { Colors, SystemIcons } from '../../constants/theme';

export const Container = styled.View(() => ({
  position: "relative"
}));

export const Input = styled.TextInput.attrs({
  placeholderTextColor: Colors.TextGray
})(({focus, noBorder}) => ({
  flexGrow: 1,
  backgroundColor: Colors.White,
  borderWidth: noBorder ? 0 : 2,
  borderColor: focus ? Colors.Purple : Colors.LightGray,
  borderRadius: 15,
  height: 46,
  paddingLeft: 46
}));

export const Icon = styled.Image.attrs(({name}) => ({
  source: SystemIcons[name]
}))(({focus, clear}) => ({
  tintColor: focus || clear ? Colors.Purple : Colors.TextGray,
  flexGrow: 0,
  position: "absolute",
  zIndex: 9998,
  ...(() => {
    return clear ? {
      right: 15,
      height: 15,
      width: 15,
      top: 16
    } : {
      left: 15,
      height: 23,
      width: 21,
      top: 11,
    }
  })()
}));
