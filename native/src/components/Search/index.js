import { compose, withState, withHandlers } from 'recompose';
import Search from './Search';

const stateFocus = withState('focus', 'setFocus', false);
const stateText = withState('text', 'setText', "");

const handlers = withHandlers({
  onChangeText: ({setText, onSearch}) => text => {
    setText(text);
    if(onSearch) onSearch(text);
  },
  onClear: ({setText}) => () => {
    setText("");
  }
})

const enchance = compose(
  stateFocus,
  stateText,
  handlers
);

export default enchance(Search); // 🧙‍♀️
