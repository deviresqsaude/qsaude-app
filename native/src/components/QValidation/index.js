import { withHandlers, compose, lifecycle, withState } from 'recompose';
import QValidation from './QValidation';

const enhance = compose(
  withState('visible', 'setVisible', true),
  withState('rating', 'setRating', 1),
  withHandlers({
    sendRating: ({rating, onSendRating}) => () => {
      // console.log(`rating`, rating);
      onSendRating();
    },
  }),
  lifecycle({}),
);

export default enhance(QValidation);
