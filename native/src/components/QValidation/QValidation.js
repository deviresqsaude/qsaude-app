import React from 'react';
import { Modal } from 'react-native';
import { AirbnbRating } from 'react-native-ratings';
import PropTypes from 'prop-types';
import {
  Container,
  ValidationWrapper,
  StarList
} from './QValidationStyle';
import { AvatarIcon } from '../Icons';
import QText from '../QText';
import QButton from '../Button/Button';
import { SystemImages, Colors } from '../../constants/theme';

const QValidation = props => {
  const {
    visible,
    doctor,
    showComment,
    setRating,
    sendRating,
  } = props;
  return (
    <Modal
      transparent
      visible={visible}
      animationType="fade"
      testID="ValidationID">
      <Container>
        <ValidationWrapper>
          <AvatarIcon source={SystemImages.emptyAvatar} height={70} width={70} radius={35}/>
          <QText preset="black"
            color={Colors.TextGray}
            margin={{top: 16}}
            textCenter="center">Como foi sua consulta com {doctor}?</QText>
          <StarList>
            <AirbnbRating
              showRating={false}
              count={5}
              defaultRating={1}
              size={35}
              reviewColor={Colors.Purple}
              selectedColor={Colors.Purple}
              onFinishRating={r => setRating(r)}
              starContainerStyle={{
                flexDirection: "row",
                width: "100%",
                justifyContent: "space-between",
                paddingRight: 16
              }}
            />
          </StarList>
          <QButton testID="COMMENT" preset="link"
            text="Adicionar comentario"
            inBlock={false}
            style={{marginBottom: 10}}
            onPress={() => showComment()}/>
          <QButton testID="RATING"
            text="Avaliar"
            inBlock={false}
            margin={0}
            onPress={() => sendRating()}/>
        </ValidationWrapper>
      </Container>
    </Modal>
  )
};

QValidation.propTypes = {
  visible: PropTypes.bool,
  doctor: PropTypes.string,
  showComment: PropTypes.func,
  sendRating: PropTypes.func
};

QValidation.defaultProps = {
  visible: false,
  doctor: "Roberto Carlos",
  showComment: () => {},
  sendRating: () => {}
}

export default QValidation;
