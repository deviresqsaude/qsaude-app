import styled from 'styled-components/native';
import { Colors } from '../../constants/theme';

export const Container = styled.View({
  backgroundColor: "rgba(0,0,0,0.4)",
  flex: 1,
  justifyContent: "center",
  alignItems: "center"
});

export const ValidationWrapper = styled.View({
  backgroundColor: Colors.White,
  borderRadius: 15,
  padding: 12,
  paddingTop: 18,
  width: "90%",
  justifyContent: "center",
  alignItems: "center",
  flexDirection: "column"
});

export const StarList = styled.View({
  flexDirection: "row",
  width: "100%",
  justifyContent: "space-between",
  paddingLeft: 16,
  paddingRight: 16,
  marginVertical: 16,
});
