import React from 'react';
import { FlatList, TouchableWithoutFeedback } from 'react-native';
import R from 'ramda';
import PropTypes from 'prop-types';
import {
  AcordionWrapper,
  AcordionContent,
  AcordionHeader,
  AcordionBody,
  AcordionTitle,
  AcordionDot,
  AcordionSatusIndicator,
  AcordionContentWrapper,
} from './AcordionStyle';
import { SystemIcon } from '../Icons';
import { Colors } from '../../constants/theme'


const getIconColor = R.cond([
  [R.equals('Vencido'), R.always(Colors.Magenta)],
  [R.T, R.always(Colors.Purple)],
]);

const getIconName = R.cond([
  // [R.equals('cancelled'), R.always('close')],
  [R.equals('Pago'), R.always('check')],
  [R.equals('Vencido'), R.always('close')],
  [R.T, R.always('ellipsis')],
]);

const Acordion = props => {
  const { setActive, currentActive, RenderComponent, RenderSeparator, data, onCopySuccess } = props;
  return (
    <AcordionWrapper>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={data}
        ItemSeparatorComponent={() => <RenderSeparator />}
        renderItem={({ item, index }) => {
          const active = currentActive === index;
          return (
            <AcordionContent>
              <AcordionSatusIndicator status={item.status} active={active}>
                <SystemIcon
                  height={12}
                  width={12}
                  tintColor={getIconColor(item.status)}
                  name={getIconName(item.status)}
                />
              </AcordionSatusIndicator>
              <AcordionContentWrapper>
                <TouchableWithoutFeedback
                  testID={item.testID}
                  onPress={() => setActive(index)}
                >
                  <AcordionHeader active={active} borderColor={getIconColor(item.status)}>
                    <AcordionTitle active={active}>{item.title}</AcordionTitle>
                    {active ? (
                      <SystemIcon height={16} name={'chevromDown'} tintColor={Colors.Gray} />
                    ) : (
                      <SystemIcon width={16} name={'chevromRight'} tintColor={Colors.Gray}  />
                    )}
                  </AcordionHeader>
                </TouchableWithoutFeedback>
                {active ? (
                  <AcordionBody>
                    <RenderComponent {...item} onCopySuccess={onCopySuccess} />
                  </AcordionBody>
                ) : null}
              </AcordionContentWrapper>
            </AcordionContent>
          );
        }}
      />
    </AcordionWrapper>
  );
};

Acordion.propTypes = {
  data: PropTypes.array,
  RenderComponent: PropTypes.any,
  RenderSeparator: PropTypes.any,
};

Acordion.defaultProps = {
  data: [],
  RenderComponent: null,
  RenderSeparator: null,
};

export default Acordion;
