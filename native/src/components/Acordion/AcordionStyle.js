import styled from 'styled-components/native';
import { Colors, Sizing, SystemFonts } from '../../constants/theme';
import { StatusIndicator } from '../EventCard/EventCardStyles';

export const AcordionWrapper = styled.View({
  borderRadius: 20,
  flexDirection: 'column',
  width: '100%',
});

export const AcordionContent = styled.View({
  flexDirection: 'column',
});

export const AcordionContentWrapper = styled.View({
  // paddingLeft: 14,
  marginLeft: 14,
  // alignItems: 'flex-end',
  // width: '100%',
  backgroundColor: Colors.WhiteSmoke,
  borderRadius: 15,
})

export const AcordionHeader = styled.View(({ active, borderColor}) => ({
  backgroundColor: Colors.White,
  borderWidth: active ? 2 : 1,
  borderColor: borderColor,
  borderRadius: 15,
  flexDirection: 'row',
  height: 50,
  paddingLeft: 15,
  paddingRight: 15,
  alignItems: 'center',
  justifyContent: 'space-between'
}));

export const AcordionDot = styled.Text(({ active }) => ({
  backgroundColor: active ? Colors.Purple : Colors.White,
  borderRadius: 10,
  borderWidth: active ? 2 : 1,
  borderColor: Colors.Purple,
  color: Colors.White,
  height: 20,
  marginRight: 10,
  width: 20,
  lineHeight: 13,
  fontFamily: SystemFonts.RubikMedium,
  fontSize: 16,
  textAlign: 'center',
}));

export const AcordionTitle = styled.Text(({ active }) => ({
  color: Colors.Mortar,
  fontFamily: active ? SystemFonts.RubikMedium : SystemFonts.RubikRegular,
  fontSize: Sizing.small,
  width: '90%',
  // textTransform: 'uppercase',
}));

export const AcordionBody = styled.View({
  padding: 10,
});

export const AcordionSatusIndicator = styled(StatusIndicator)``;
