import { compose, withState, withHandlers } from "recompose";
import Acordion from "./Acordion";

const enhance = compose(
  withState("currentActive", "updateActive", -1),
  withHandlers({
    setActive: ({updateActive, currentActive}) => currentSelect => {
      updateActive(currentActive === currentSelect ? -1 : currentSelect);
    }
  })
);

export default enhance(Acordion);
