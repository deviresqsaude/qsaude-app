import React from 'react';
import { renderComponent } from 'recompose';
import { Header, Pill, TabContainer, Container } from './TabViewStyles';

// import { Container } from './styles';

export default function TabView(props) {
  const { selectedIndex, onChange, sceneList, tabList } = props;

  const renderSelectedTab = () => {
    const Component = sceneList[selectedIndex];
    return <Component />;
  };

  const renderPills = () =>
    tabList.map((label, index) => {
      const first = index === 0;
      const last = index === tabList.length - 1;
      const selected = index === selectedIndex;
      const onPress = () => onChange(index);

      return <Pill key={Math.random()} {...{ onPress, label, last, first, selected }} />;
    });

  return (
    <Container>
      <Header>{renderPills()}</Header>
      <TabContainer>{renderSelectedTab()}</TabContainer>
    </Container>
  );
}
