import React from 'react';
import styled from 'styled-components/native';
import { TouchableOpacity } from 'react-native';
import QText from '../QText';
import { Colors, Sizing } from '../../constants/theme';

export const Header = styled.View({
  width: '100%',
  paddingBottom: 16,
  backgroundColor: '#fff',
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
});

const PillText = styled(QText).attrs({
  preset: 'title',
})(({ selected }) => ({
  color: selected ? Colors.White : Colors.Purple,
  fontSize: Sizing.small,
}));

const PillContainer = styled.View(({ selected, first, last }) => ({
  paddingVertical: 5,
  alignItems: 'center',
  justifyContent: 'center',
  paddingHorizontal: 10,
  borderColor: Colors.Purple,
  borderWidth: 2,
  borderTopLeftRadius: first ? 50 : 0,
  borderBottomLeftRadius: first ? 50 : 0,
  borderTopRightRadius: last ? 50 : 0,
  borderBottomRightRadius: last ? 50 : 0,
  backgroundColor: selected ? Colors.Purple : Colors.White,
}));

export const Pill = props => (
  <TouchableOpacity activeOpacity={0.9} onPress={props.onPress}>
    <PillContainer {...props}>
      <PillText selected={props.selected}>{props.label}</PillText>
    </PillContainer>
  </TouchableOpacity>
);

export const TabContainer = styled.View({ flex: 1 });

export const Container = styled.View({ flex: 1 });
