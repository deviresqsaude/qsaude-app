import styled from 'styled-components/native';
import { Alert } from 'react-native';
import { Colors, SystemFonts, Sizing } from '../../constants/theme';

const getColor = ({ disabled, error, preset }) => {
  const colors = {
    color: Colors.White,
    background: '#eee',
  };

  if (error || preset === 'cancel') {
    colors.background = disabled ? Colors.MagentaDisabled : Colors.Red;
  } else {
    colors.background = disabled ? Colors.PurpleDisabled : Colors.Purple;
  }
  const aux = { ...colors };

  if (['secondary', 'link', 'cancel'].includes(preset)) {
    colors.color = aux.background;
    colors.background = aux.color;
  }

  if(preset === 'invert') {
    colors.color = disabled ? Colors.PurpleDisabled : Colors.Purple;
    colors.background = 'transparent';
  }

  return colors;
};
// width: ${({ inBlock }) => (inBlock ? '100%' : 'auto')};

export const Container = styled.View`
  height: 48;
  margin: ${({ margin }) => (!margin ? 0 : margin)};
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background-color: ${props => getColor(props).background};
  border-radius: 16px;
  border-width: ${props => (props.preset === 'secondary' || props.preset === 'invert' ? 2 : 0)};
  border-color: ${props => getColor(props).color};
  padding-left: 24;
  padding-right: 24;
  position: relative;
`;

export const ButtonText = styled.Text`
  font-size: ${Sizing.small};
  font-family: ${SystemFonts.RubikMedium};
  color: ${props => getColor(props).color};
  text-align: center;
`;

export const ButtonIcon = styled.Image({
  width: 17,
  height: 17,
  marginRight: 4
});

export const Badge = styled.Text({
  position: "absolute",
  right: 13,
  height: 18,
  width: 18,
  backgroundColor: Colors.Red,
  color: Colors.White,
  fontWeight: 600,
  textAlign: "center",
  borderRadius: 9
});
