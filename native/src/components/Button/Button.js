import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, ActivityIndicator } from 'react-native';
import { ButtonText, Container, ButtonIcon, Badge } from './ButtonStyle';
import { Colors } from '../../constants/theme';

/**
 * QSaude Button
 * @param {function} onPress OnTap Function
 * @param {string} text
 * @param {boolean} disabled
 * @param {string} error
 */
const Button = props => {
  const {
    onPress,
    text,
    disabled,
    error,
    preset,
    loading,
    testID,
    inBlock,
    margin,
    style,
    accessibilityLabel,
    renderIcon,
    badge
  } = props;

  return (
    <TouchableOpacity
      style={{
        minHeight: 56,
        alignSelf: inBlock ? 'stretch' : 'center',
        ...style,
      }}
      activeOpacity={0.9}
      disabled={disabled || loading}
      {...{
        testID,
        onPress,
        accessibilityLabel,
        // contentDescription: accessibilityLabel,
      }}
    >
      <Container disabled={disabled || loading} {...{ error, preset, inBlock, margin }}>
        {loading ? (
          <ActivityIndicator
            size={18}
            color={preset === 'primary' ? Colors.White : Colors.Purple}
          />
        ) : (
          <>
            {
              renderIcon && <ButtonIcon source={renderIcon} />
            }
            {
              badge && <Badge>{badge}</Badge>
            }
            <ButtonText {...{ error, disabled, preset }}>{text}</ButtonText>
          </>
        )}
      </Container>
    </TouchableOpacity>
  );
};

Button.propTypes = {
  inBlock: PropTypes.bool,
  onPress: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  text: PropTypes.string,
  disabled: PropTypes.bool,
  label: PropTypes.string,
  accessibilityLabel: PropTypes.string,
  preset: PropTypes.oneOf(['primary', 'secondary', 'cancel', 'link', 'white', 'invert']),
  testID: PropTypes.string,
  error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  margin: PropTypes.number,
  style: PropTypes.objectOf(PropTypes.any),
};

Button.defaultProps = {
  inBlock: true,
  disabled: false,
  preset: 'primary',
  error: false,
  loading: false,
  text: '–',
  margin: null,
  testID: null,
  style: {},
  label: '',
  accessibilityLabel: '',
};

export default Button;
