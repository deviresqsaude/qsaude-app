import styled from 'styled-components/native';
import { Platform } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

const hasDeviceNotch = () => {
  return Platform.OS === 'android' ? 0 :
    Platform.OS === 'ios' && getStatusBarHeight() > 24 ? 24 : 0;
}

export default styled.View({
  flex: 1,
  backgroundColor: '#FFFFFF',
  marginTop: hasDeviceNotch()
});
