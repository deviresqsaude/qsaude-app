import { compose } from 'recompose';
import { withNavigation } from 'react-navigation';
import BottomTabbar from "./BottomTabbar";

const enchance = compose(withNavigation);

export default enchance(BottomTabbar);
