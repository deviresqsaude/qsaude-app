import React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import { Colors } from '../../constants/theme';
import { BottomTabbarItemWrapper, ItemLabel } from './BottomTabbarStyle';
import { ButtomIconWrapper, ButtomIcon } from '../Icons';

// eslint-disable-next-line react/prop-types
export default ({ testID, ...props }) => {
  // eslint-disable-next-line react/prop-types
  const { icon, title, active, activeTintColor, onPress, activeIcon } = props;
  return (
    <TouchableWithoutFeedback onPress={onPress} testID={testID}>
      <BottomTabbarItemWrapper {...props}>
        <ButtomIconWrapper style={{ marginBottom: 2 }} height={28} width={32}>
          <ButtomIcon
            width={34}
            height={24}
            tintColor={active ? activeTintColor : Colors.Gray}
            source={active ? activeIcon : icon}
            {...props}
          />
        </ButtomIconWrapper>
        <ItemLabel {...props}>{title}</ItemLabel>
      </BottomTabbarItemWrapper>
    </TouchableWithoutFeedback>
  );
};
