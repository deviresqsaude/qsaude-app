// import React from "react";
import styled from "styled-components/native";
import { Colors, Spacing } from "../../constants/theme";

export const BottomTabbarWrapper = styled.View`
  height: ${Spacing.tabbarHeight};
  flex-direction: row;
  border-top-color: #eee;
  justify-content: center;
  width: 100%;
  padding-bottom: ${Spacing.tabbarPadding}
`;

export const BottomTabbarItemWrapper = styled.View`
  flex: 1;
  border-top-width: 1;
  min-width: 25;
  border-top-color: ${Colors.LightGray};
  align-items: center;
  justify-content: center;
`;

export const ItemLabel = styled.Text`
  font-size: 10;
  margin-top: 1;
  color: ${({ active, activeTintColor }) => (active ? activeTintColor : Colors.Gray)};
`;
