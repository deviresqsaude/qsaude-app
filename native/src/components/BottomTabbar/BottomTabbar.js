/* eslint-disable react/require-default-props */
/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';
import BottomTabbarItem from './BottomTabbarItem';
import tabBarProps from '../../constants/tabBarNavigationProps';
import { BottomTabbarWrapper } from './BottomTabbarStyle';
import { Colors } from '../../constants/theme';

const routeMapper = R.addIndex(R.map);

const BottomTabbar = props => {
  const {
    activeTintColor,
    navigation: {
      state: { routes, index: activeRouteIndex },
      navigate,
    },
  } = props;
  return (
    <BottomTabbarWrapper>
      {routes &&
        routeMapper(({ key: routeKey, routeName }, index) => {
          const active = index === activeRouteIndex;
          const onPress = () => {
            navigate(routeKey)
          };
          const { title, icon, activeIcon } = tabBarProps[routeName];
          const key = `BOTTOMTABBAR.tab.${routeKey}`;
          return (
            <BottomTabbarItem
              key={`BOTTOMTABBAR.tab.${index}`}
              testID={key}
              {...{ activeTintColor, title, icon, active, onPress, activeIcon }}
            />
          );
        }, routes)}
    </BottomTabbarWrapper>
  );
};

BottomTabbar.defaultProps = {
  activeTintColor: Colors.Purple,
  navigation: {},
};

BottomTabbar.propTypes = {
  routes: PropTypes.any,
  activeTintColor: PropTypes.string,
  navigation: PropTypes.any,
};

export default BottomTabbar;
