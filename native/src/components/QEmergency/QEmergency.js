import React from 'react';
import R from 'ramda';
import { TouchableWithoutFeedback, Modal, Linking } from 'react-native';
import Config from 'react-native-config';
import PropTypes from 'prop-types';
import { Field } from 'formik';
import { elevationShadow } from '../../helpers';
import { QuestContainer, Container, SymptomsError, WarningImage, WarningBody, WarningContainer } from './QEmergencyStyle';
import QEmergencyLoading from './QEmergencyLoading';
import { Colors, SystemImages, SystemIcons, Sizing } from '../../constants/theme';
import { QSiren } from '../QSiren';
import { QSModal } from '../Modal';
import { QButton } from '../Button';
import QCheckGroup from '../Form/CheckGroup';
import { QText } from '../index';
import {
  QAlertOverlay,
} from '../QAlert/QAlertStyle';
import { SystemIcon, BackButtonContainer } from '../Icons';

// const QEmergency = ({callEmergencyNumber}) => <QSiren callbackSiren={() => callEmergencyNumber()}/>;
const QEmergency = props => {
  const {handleEmergency, visible, isValid, isSubmitting, symptoms, handleSubmit, data: { refetch, loading }, warningVisible, setWarningVisible, isLoading } = props;

  const returnSymtoms = () => {
    return !symptoms 
      ?
      <SymptomsError refetch={refetch} />
      :
      <Container showsVerticalScrollIndicator={false}>
        <QuestContainer style={{ ...elevationShadow(Colors.Black, 5, 0.2) }}>
          <Field
          name="symptom"
          required
          label="Marque abaixo os sintomas que você tem sentido"
          options={R.map(item => ({visible, ...item}), symptoms)}
          component={QCheckGroup}/>
        </QuestContainer>
        <QButton
          disabled={isSubmitting || !isValid}
          text="Próximo"
          loading={isSubmitting}
          onPress={handleSubmit}
          style={{marginHorizontal: 20, marginTop: 16}}/>
      </Container>
  }
  return (
    <>
      <QSiren callbackSiren={() => setWarningVisible(true)}/>
      <QSModal
        visible={visible}
        closeModal={() => handleEmergency()}
        title="Sintomas">
          { loading
            ?
              <QEmergencyLoading />
            :
            returnSymtoms(symptoms)
          }
      </QSModal>
      <Modal animationType="fade" transparent visible={warningVisible} onRequestClose={() => setWarningVisible(false)}>
        <TouchableWithoutFeedback onPress={() => setWarningVisible(false)}>
          <QAlertOverlay />
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={() => setWarningVisible(false)}>
            <BackButtonContainer>
              <SystemIcon
                source={SystemIcons.close}
                height={Sizing.small}
                width={Sizing.small}
                style={{ marginBottom: 16, marginLeft: 20 }}
                tintColor='#fff'
              />
            </BackButtonContainer>
          </TouchableWithoutFeedback>
        <WarningContainer>
          <QuestContainer style={{ ...elevationShadow(null, 10, null), alignItems: 'center', marginTop: 0}}>
            <WarningImage source={SystemImages.warningCall} />
            <QText
              style={{ marginTop: 16, marginBottom: 8 }}
              textCenter
              preset="navTitleSimple"
              fontSize={17}
            >
              Bem vindo ao TeleCuidado!
            </QText>
            <WarningBody>
              <QText preset='body'>
                Utilize esse botão para situações médicas de pronto atendimento. Aqui você
                fala por vídeo com um profissional de saúde que vai entender seus sintomas e
                oferecer o melhor tratamento. Lembramos que esse atendimento não substitui a
                consulta presencial .{'\n'}No caso de dúvidas fale conosco,
              </QText>
              <QText
                preset='link'
                style={{ textDecorationLine: 'underline', marginBottom: 12 }}
                onPress={()=>Linking.openURL(
                  `https://api.whatsapp.com/send?phone=${Config.WHATSAPP_LINKNUMBER}`,
                )}
              >
                Fale com TimeQ.
              </QText>
              <QButton
                loading={isLoading}
                text='Iniciar TeleCuidado'
                onPress={() =>handleEmergency()}
              />
            </WarningBody>
          </QuestContainer>
        </WarningContainer>
      </Modal>
    </>
  )
}

QEmergency.propTypes = {
  // callEmergencyNumber: PropTypes.func,
  handleEmergency: PropTypes.func,
  visible: PropTypes.bool
}

QEmergency.defaultProps = {
  // callEmergencyNumber: () => {},
  handleEmergency: () => {},
  visible: false
}

export default QEmergency;
