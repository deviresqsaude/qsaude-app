import styled from 'styled-components/native';
import { EmptyData } from '../../components';
import { SystemImages } from '../../constants/theme';

export const Container = styled.ScrollView({
  flex: 1,
  // marginTop: 20
});

export const QuestContainer = styled.View({
  backgroundColor: "white",
  borderRadius: 14,
  marginTop: 18,
  paddingHorizontal: 20,
  paddingTop: 12,
  marginHorizontal: 20
});

export const SymptomsError = styled(EmptyData).attrs(props => ({
  title: 'Ops!!',
  description: 'Aconteceu um problema. Tente novamente, por favor.',
  buttonText: 'Tentar novamente',
  onPress: props.refetch,
  imageSource: SystemImages.errorImage,
}))``;

export const QEmergencyLoadingWrapper = styled.View({
  flex: 1,
  padding: 16
})

export const WarningImage = styled.Image({
  width: 208,
  height: 170,
})

export const WarningBody = styled.View({
  marginTop: 16
})

export const WarningContainer = styled.View({
  flex: 1,
  justifyContent: 'center',
})

// export const EmergencyWrapper = styled.View({
//   flex: 1,
//   alignItems: "center",
//   justifyContent: "center",
//   position: "absolute",
//   width: "100%",
//   height: "100%",
//   left: 0,
//   top: 0,
// });

// export const EmergencyBox = styled.View(() => ({
//   width: "90%",
//   padding: 12,
//   paddingBottom: 16,
// }));

// export const EmergencyButton = styled.View(() => ({
//   flexDirection: "row",
//   height: 42,
//   borderColor: Colors.White,
//   borderWidth: 1,
//   borderRadius: 10,
//   alignItems: "center",
//   justifyContent: "center",
// }));

// export const EmergencyIconWrapper = styled.View({
//   alignItems: "center",
//   justifyContent: "center",
//   marginBottom: 20,
//   height: 50
// })

// export const EmergencyIcon = styled.Image.attrs({
//   source: SystemImages.sirenImage
// })({});
