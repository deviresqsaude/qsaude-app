import React from 'react';
import { Placeholder, Fade, PlaceholderLine } from 'rn-placeholder';
import { elevationShadow } from '../../helpers';
import { QEmergencyLoadingWrapper, QuestContainer } from './QEmergencyStyle';
import  { Colors } from '../../constants/theme';

const QEmergencyLoading = () => {
  return (
    <QuestContainer style={{ ...elevationShadow(Colors.Black, 5, 0.2) }}>
      <Placeholder Animation={Fade}>
        <PlaceholderLine width={80} style={{marginBottom: 40}}/>
        <PlaceholderLine/>
        <PlaceholderLine width={80} style={{marginBottom: 20}}/>
        <PlaceholderLine width={40} style={{marginBottom: 20}}/>
        <PlaceholderLine width={80} style={{marginBottom: 20}}/>
        <PlaceholderLine width={80} style={{marginBottom: 20}}/>
        <PlaceholderLine width={40} style={{marginBottom: 20}}/>
        <PlaceholderLine width={80} style={{marginBottom: 20}}/>
      </Placeholder>
    </QuestContainer>
  )
}

export default QEmergencyLoading;
