import { Alert } from 'react-native';
import { connect } from 'react-redux';
import { graphql, withApollo } from '@apollo/react-hoc';
import { compose, withHandlers, withState, withProps } from 'recompose';
import { withFormik } from 'formik';
import SyncStorage from 'sync-storage';
import { withNavigation, NavigationActions } from 'react-navigation';
import  { GetOnGoingAttendance, TelemedicinaQuery } from '../../service/query';
import Emergency from './QEmergency';
import { InitTelemedicineQuery } from '../../service/mutations';
import { AlertActions } from '../../store/actions';




const mapDispatchToProps = dispatch => ({
  onServiceFailed: ({ title, error: description }) => 
    dispatch(
      AlertActions.actions.openAlert({
        title,
        shouldRenderOkButton: true,
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        description,
        isError: true,
      }),
    ),
});

const withForm = withFormik({
  validate: values => {
    const errors = { name: 'invalid length' };
    return values.symptom.length === 0 && errors;
  },
  // mapPropsToValues: props => {
  //   return {
  //     symptom: props.symptoms,
  //   };
  // },
  handleSubmit: async (values, formik) => {
    const {
      setSubmitting,
      setFieldValue,
      props: {
        client,
        onServiceFailed,
        navigation: { navigate },
        setVisible,
      },
    } = formik;

    try {
      const request = await client.mutate({
        mutation: InitTelemedicineQuery,
        variables: {
          symptoms: values.symptom,
        },
      });

      const { errors, data } = request;
      const hasError = errors ? errors.length > 0 : false;

      if (hasError) {
        // eslint-disable-next-line
        setSubmitting(false);
        setFieldValue('symptom', undefined, true);
        setVisible(false)
        onServiceFailed({
          title: 'Erro ao mandar sintomas e começar telecuidado!',
          error: errors[0].message,
        });
        return;
      }

      if(data && !data.initTelemedicine.includes("sucesso")) {
        setSubmitting(false);
        setFieldValue('symptom', undefined, true);
        setVisible(false)
        onServiceFailed({
          title: 'Erro ao começar telecuidado!',
          error: "já existe uma chamada na fila com este cpf!",
        });
        return;
      }
      
      setSubmitting(false);
      setFieldValue('symptom', undefined, true);
      setVisible(false)
      navigate('Telemedicine');

    } catch (e) {
      console.log(e);
    }
    setSubmitting(false);
  },
});

const withData = graphql(TelemedicinaQuery, {
  options: { notifyOnNetworkStatusChange: true },
});

const symptomsProps = withProps(({ data: { symptoms } }) => ({symptoms}));

const enhance = compose(
  connect(
    null,
    mapDispatchToProps
  ),
  withData,
  withState('isLoading', 'setLoading', false),
  withState('visible', 'setVisible', false),
  withState('warningVisible', 'setWarningVisible', false),
  symptomsProps,
  withNavigation,
  withApollo,
  withForm,
  withHandlers({
    handleEmergency: ({ visible, setVisible, client, setLoading, setWarningVisible, navigation: { reset }, onServiceFailed }) => async () => {
      setLoading(true)
      const  callId = SyncStorage.get('callId');

      if(callId) {
        const request = await client.query({
          query: GetOnGoingAttendance,
        });

        const { errors, data } = request;
        const hasError = errors ? errors.length > 0 : false;
        if (hasError) {
          onServiceFailed({
            title: 'Erro ao verificar chamada',
            error: errors[0].message,
          });
          return;
        }
        const { getOngoingAttendance } = data;

        if(getOngoingAttendance.hasAttendance) {
          setLoading(false)
          setVisible(!visible);
          Alert.alert('Você já tem chamada aberta! Redirecionando para sua chamada.')
          reset([NavigationActions.navigate({ routeName: 'Telemedicine', params: { meetingNumber: getOngoingAttendance.zoomMeetingId } })], 0);
        } else {
          setLoading(false)
          setVisible(!visible);
          SyncStorage.remove('callId');
          setWarningVisible(true)
        }
      } else {
        setLoading(false)
        setVisible(!visible);
        setWarningVisible(false)
      }
    },
  }),
);

const QEmergency = enhance(Emergency);

export { QEmergency };
