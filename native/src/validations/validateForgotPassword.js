export default ({ email }) => {
  const errors = {};
  if (!email) {
    errors.user = 'Campo obrigatório';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
    errors.user = 'Email ou domínio incorreto';
  }

  return errors;
};
