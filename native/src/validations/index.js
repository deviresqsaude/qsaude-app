import R from 'ramda';

export { default as validateForgotPasswordForm } from './validateForgotPassword';

export const validateLoginForm = ({ user, password, termsAcceptance }) => {
  const errors = {};
  if (user.length !== 11 && (user.length < 15 || user.length > 17)) {
    errors.user = 'Use 11 dígitos para CPF ou 16 dígitos para ID';
  }

  if (password.length === 0) {
    errors.password = 'Campo obrigatório';
  } else if (password.length < 6) {
    errors.password = 'Senha deve ter no mínimo 6 caracteres';
  }

  if (!termsAcceptance) {
    errors.termsAcceptance = 'Por favor, aceite os termos';
  }
  return errors;
};

const imcValidator = values => {
  const errors = {};
  if (!values.weight || values.weight === '' || values.weight === '0') {
    errors.weight = 'Campo obrigatório';
  }
  if (R.not(R.prop('height', values))) {
    errors.height = 'Campo obrigatório';
  }
  if (!values.imc || values.imc === '0.00') {
    errors.imc = 'Informe peso e altura';
  }
  return errors;
};

const bloodPressureValidator = values => {
  const { systolic, diastolic } = values;
  const errors = {};
  const MAX_DIASTOLIC = 150;
  const MAX_SYSTOLIC = 210;
  const MIN_DIASTOLIC = 40;
  const MIN_SYSTOLIC = 60;

  // sistolic validation
  if (systolic === '' || !systolic) {
    errors.systolic = 'Campo obrigatório';
  }
  if (systolic > MAX_SYSTOLIC) {
    errors.systolic = 'Valor acima do plausível';
  }
  if (systolic < MIN_SYSTOLIC && (!systolic && systolic !== '')) {
    errors.systolic = 'Valor abaixo do plausível';
  }

  // diastolic
  if (diastolic === '' || !diastolic) {
    errors.diastolic = 'Campo obrigatório';
  }
  if (diastolic > MAX_DIASTOLIC) {
    errors.diastolic = 'Valor acima do plausível';
  }
  if (diastolic < MIN_DIASTOLIC ) {
    errors.diastolic = 'Valor abaixo do plausível';
  }

  return errors;

  // DiastolicMin: 'Valor abaixo do plausível',
  // DiastolicMax: 'Deve ser menor do que a pressão máxima',
  // SystolicMin: 'Deve ser maior do que a pressão mínima',
  // SystolicMax: 'Valor acima do plausível',

  // this.setState({ minSystolic: Math.max(MIN_SYSTOLIC, Number(value)) });
  // this.setState({ maxDiastolic: Math.min(MAX_DIASTOLIC, Number(value)) });

  // label={InsertVitalSignStrings.BloodPressure.Labels.Systolic}
  // Validators.MinValue(this.state.minSystolic, InsertVitalSignStrings.Validations.SystolicMin),
  // Validators.MaxValue(MAX_SYSTOLIC, InsertVitalSignStrings.Validations.SystolicMax),
  // if ()
};
const glycemiaValidator = values => {
  const errors = {};
  if (!values.glycemia || values.glycemia === '') {
    errors.glycemia = 'Campo obrigatório';
  }
  return errors;
};

export const validateInsertVitalSignForm = (values, props) => {
  const {
    navigation: {
      state: {
        params: { vitalSign },
      },
    },
  } = props;

  // create validator function based in 'vitalSign' prop. oneOf[Imc, BloodPressure, Glycemia]
  const validateForm = R.cond([
    [R.equals('imc'), R.always(imcValidator)], // [predicated, return] R.always.. always return whats passed in param.
    [R.equals('bloodPressure'), R.always(bloodPressureValidator)],
    [R.equals('glycemia'), R.always(glycemiaValidator)],
    [R.T, R.always(() => {})],
  ])(vitalSign);

  const errors = validateForm(values);
  return errors;
};
