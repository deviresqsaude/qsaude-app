import React from 'react';
import SyncStorage from 'sync-storage';
import R from 'ramda';
import { Linking } from 'react-native';
import  { QText, SwiperCards, QButton } from '../../components';
import { Container, PaddingWrapper, CardsWrapper } from './OfflineCardStyle';
import { QLogo, BottomWaves } from '../Login/LoginStyle';
// import { HEALTH_CARDS } from '../../constants/mock';

const OfflineCardScreen = props => {
  const { screenProps: { refetch } } = props;
  const healthCards = R.map(x=>R.merge(x, { status: true }), SyncStorage.get('allHelathCards')) || [];
  // const healthCards = HEALTH_CARDS;
  return (
    <>
      <Container>
        <QLogo />
        <QText preset="body" style={{ textAlign: 'center', marginLeft: 16, marginRight: 16 }}>De acordo com os registros do aplicativo, salvamos para eventuais consultas as carteirinhas abaixos:</QText>
        <CardsWrapper>
          <SwiperCards healthCards={healthCards} />
        </CardsWrapper>
        <PaddingWrapper>
          <QButton text="Ligue para Central Clínica 24hrs" onPress={() => Linking.openURL('tel://08005229090')}/>
          <QText preset="link" onPress={()=>refetch()} style={{textAlign: 'center', textDecorationLine: 'underline'}} >Tentar novamente</QText>
        </PaddingWrapper>
      </Container>
      <BottomWaves />
    </>
  )
}

export default OfflineCardScreen;