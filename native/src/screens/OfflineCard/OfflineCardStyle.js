import styled from 'styled-components';

export const Container = styled.View({
  flex: 1,
  marginBottom: 77,
});

export const PaddingWrapper = styled.View({
  paddingHorizontal: 16
})

export const CardsWrapper = styled.View({
  height: 280,
})