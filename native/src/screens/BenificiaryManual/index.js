import React from 'react';
import { Text } from 'react-native';
import { Placeholder, PlaceholderMedia, Shine } from 'rn-placeholder';
import PDFView from 'react-native-view-pdf/lib/index';
import { compose, renderComponent, branch, withState } from 'recompose';
// import PDF from './pdfBase64.json';
// import PDF from 'https://image.qsaude.net/lib/fe3c157175640478761c71/m/1/8dafa2f9-8f3e-4000-9d59-0a5fc2bee664.pdf';


const resources = {
  url: 'https://image.qsaude.net/lib/fe3c157175640478761c71/m/1/8dafa2f9-8f3e-4000-9d59-0a5fc2bee664.pdf',
};

// eslint-disable-next-line react/prop-types
const BeneficiaryManualScreen = ({ toggleLoad, pdfLoaded }) => {
  const resourceType = 'url';

  return (
    <>
      <PDFView
        // resource={PDF.document}
        resource={resources[resourceType]}
        // resourceType="url"
        onLoad={() => {
          toggleLoad(true);
        }}
        style={{ flex: 1 }}
      />
    </>
  );
};

const withLoadingState = withState('pdfLoaded', 'toggleLoad', false);

const renderWhileLoading = component =>
  branch(props => props.pdfLoaded, renderComponent(component));

const PdfPlaceholder = () => (
  <Placeholder Animation={Shine}>
    <PlaceholderMedia style={{ width: '100%', height: '100%' }} />
  </Placeholder>
);

export default compose(
  withLoadingState,
  // renderWhileLoading(PdfPlaceholder),
)(BeneficiaryManualScreen);
