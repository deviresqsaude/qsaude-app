import { Dimensions } from 'react-native';
import styled from 'styled-components/native';
import { Colors } from '../../constants/theme';

export const Container = styled.View({
  flex: 1,
  paddingBottom: 38,
  paddingTop: 32,
  paddingLeft: 20,
  paddingRight: 20,
  backgroundColor: Colors.White
});

export const TokenWrapper = styled.View({
  flex: 1,
  paddingBottom: 38,
  paddingTop: 32,
  paddingLeft: 20,
  paddingRight: 20,
  backgroundColor: Colors.White
});

// export const

export const TokenTitle = styled.View({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  marginBottom: 15
})

export const TokenContainer = styled.View({
  display: "flex",
  alignItems: "flex-end",
  marginVertical: 20
});

export const TokenInput = styled.TextInput.attrs({
  placeholderTextColor: Colors.TextGray
})(({focus, noBorder}) => ({
  flexGrow: 1,
  backgroundColor: Colors.White,
  borderWidth: noBorder ? 0 : 2,
  borderColor: focus ? Colors.Purple : Colors.LightGray,
  borderRadius: 15,
  height: 46,
  textAlign: "center",
  width: "100%"
}));

export const SliderItem = styled.View({
  width: Dimensions.get('screen').width,
  // width: Dimensions.get('screen').width - 32,
  flex: 1
});

export const TokenListItem = styled.View({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  borderBottomWidth: 2,
  borderColor: Colors.LightGray,
  paddingVertical: 12,
  paddingHorizontal: 8
});
