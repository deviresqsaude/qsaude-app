import React from 'react';
import Config from 'react-native-config';
import { TouchableWithoutFeedback, Image, FlatList, Linking, View } from 'react-native';
import { Container, TokenContainer, TokenInput, TokenTitle, SliderItem, TokenListItem } from './SecurityTokenStyle';
import { QText, QButton } from '../../components';
import { Colors, SystemIcons } from '../../constants/theme';

const TITLES = {
  welcome: "Olá, Bem-vindo",
  other: "Verifique de outra maneira"
};
const DESCRIPTION = {
  token: "Como esta é a primeira vez que você acessa o seu plano Qsaúde, precisamos seguir algumas medidas de segurança, ok?",
  other: "Para confirmar a sua identidade. O Qsaúde disponibiliza outras formas de acesso."
};
const LABEL = "Insira aqui o código recebido e clique em Entrar.";
const BUTTONS = {
  primary: "Entrar",
  secondary: "Verificar de outra maneira",
  resend: "Reenviar código",
  token: "Voltar para o token"
};

const Token = ({sendToken, newToken, anotherWay, onChangeToken, token, focus, setFocus, goNavigate, loading}) => {
  return (
    <Container>
      <TouchableWithoutFeedback onPress={() => loading ? () => {} : goNavigate("back")}>
        <TokenTitle>
          <Image source={SystemIcons.close} width="22" height="22"/>
          <QText preset="navTitle" margin={{left: 10}} color={Colors.Purple}>{TITLES.welcome}</QText>
        </TokenTitle>
      </TouchableWithoutFeedback>
      <QText preset="paragraph" color={Colors.LabelGray} margin={{bottom: 10}}>{DESCRIPTION.token}</QText>
      <TokenContainer>
        <QText preset="paragraph" color={Colors.Purple} margin={{top: 0, bottom: 5}}>{LABEL}</QText>
        <TokenInput
          {...{focus}}
          disabled={loading}
          value={token || ""}
          onBlur={() => setFocus(false)}
          onFocus={() => setFocus(true)}
          onChangeText={t => onChangeToken(t)}
          keyboardType="number-pad"/>
        <TouchableWithoutFeedback onPress={() => newToken()}>
          <QText preset="link" margin={{top: 5, bottom: 15}}>{BUTTONS.resend}</QText>
        </TouchableWithoutFeedback>
      </TokenContainer>
      <QButton disabled={!token || loading} loading={loading} preset="primary" onPress={() => sendToken()} text={BUTTONS.primary}/>
      <QButton disabled={loading} preset="invert" onPress={() => loading ? () => {} : anotherWay()} text={BUTTONS.secondary}/>
    </Container>
  )
};

const Other = ({goToToken, goNavigate, sendEmail}) => {
  const OPTIONS = [
    {
      id: "email",
      icon: SystemIcons.email,
      title: "Receber um token de segurança no e-mail",
      link: () => sendEmail(),
      disabled: true
    },
    {
      id: "whatsapp",
      icon: SystemIcons.whatsapp,
      title: "Fale pelo WhatsApp com o TimeQ 3003-5503",
      link: () => Linking.openURL(`http://api.whatsapp.com/send?phone=${Config.WHATSAPP_LINKNUMBER}`),
      disabled: true
    },
    {
      id: "phone",
      icon: SystemIcons.phone,
      title: "Ligue para o Time Q 3003-5503",
      link: () => Linking.openURL(`tel:${Config.EMERGENCY_NUMBER}`),
      disabled: true
    },
  ];
  return (
    <Container>
      <TouchableWithoutFeedback onPress={() => goNavigate("back")}>
        <TokenTitle>
          <Image source={SystemIcons.close} width="22" height="22"/>
          <QText preset="navTitle" margin={{left: 10}} color={Colors.Purple}>{TITLES.other}</QText>
        </TokenTitle>
      </TouchableWithoutFeedback>
      <QText preset="paragraph" color={Colors.LabelGray} margin={{bottom: 10}}>{DESCRIPTION.other}</QText>
      <View style={{marginBottom: 40}}>
      {
        OPTIONS.map(({id, link, icon, title}) => {
          return (
            <TouchableWithoutFeedback onPress={() => link()} key={id}>
              <TokenListItem>
                <Image source={icon} width="22" height="22"/>
                <View style={{width: "80%"}}>
                  <QText preset="paragraph" margin={{left: 14}} color={Colors.Purple}>{title}</QText>
                </View>
              </TokenListItem>
            </TouchableWithoutFeedback>
          )
        })
      }
      </View>
      <QButton preset="invert" onPress={() => goToToken()} text={BUTTONS.token}/>
    </Container>
  )
};

const SecurityToken = props => {
  const {sliderRef} = props;
  return (
    <FlatList
      showsHorizontalScrollIndicator={false}
      ref={sliderRef}
      data={[Token, Other]}
      scrollEnabled={false}
      horizontal
      renderItem={({item}) => {
        const ItemComponent = item;
        return (
          <SliderItem>
            <ItemComponent {...props}/>
          </SliderItem>
        )
      }}
    />
  )
}

export default SecurityToken;
