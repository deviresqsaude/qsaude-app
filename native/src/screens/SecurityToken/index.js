import React from "react";
import { connect } from 'react-redux';
import { compose, withState, withHandlers, withProps, lifecycle } from "recompose";
import { withApollo } from '@apollo/react-hoc';
import { DeviceAddQuery, DeviceAtiveQuery, DeviceResendQuery, LoginV2Query, BeneficiaryAndDependents, PlanCardQuery} from "../../service/query";
import SecurityToken from './SecurityToken';
import { AlertActions } from '../../store/actions';
import { SystemImages } from '../../constants/theme';
import { loginFactory } from '../../helpers';

const TOKEN = {
  SMS: 1,
  EMAIL: 2
};

const ALERT = {
  error: {
    title: "Aconteceu um erro",
    description: "Tente novamente, ou entre em contato com o Time Q",
    isError: true
  },
  resend: {
    image: SystemImages.successImage,
    shouldRenderImage: true,
    title: "Token reenviado",
    isError: false,
    description: "Foi enviado um novo token, aguarde um momento porfavor"
  },
  success: {
    ttle: "",
    description: ""
  }
};

const errorFactory = errors => {
  let error = {};
  // eslint-disable-next-line no-restricted-syntax
  for (const e of errors) {
    error = {description: e.message};
  };
  return error;
};

const mapDispatchToProps = dispatch => ({
  showAlert: options => dispatch(AlertActions.actions.openAlert({...options, shouldRenderOkButton: true, closeAlert: () =>  dispatch(AlertActions.actions.closeAlert())}))
});

const tokenRefs = withProps(() => ({
  sliderRef: React.createRef()
}));

const stateFocus = withState('focus', 'setFocus', false);
const stateToken = withState('token', 'setToken', "");
const stateLoading = withState('loading', 'setLoading', false);

const handlerQuery = withHandlers({
  queryFactory: ({client}) => (query, variables) => {
    return client.query({
      query,
      variables
    });
  },
  togleLoading: ({loading, setLoading}) => () => {
    setLoading(!loading);
  },
  goNavigate: ({navigation: {navigate, goBack}, loading}) => (page, params={}) => {
    if(loading) return;
    if(page==="back") goBack(); else navigate(page, {...params});
  }
});

const handlers = withHandlers({
  sendEmail: ({sliderRef, showAlert, queryFactory, togleLoading, navigation: {state: {params: {twoFactorCodeId}}}}) => async () => {
    togleLoading();
    try {
      const {errors} = await queryFactory(DeviceResendQuery, {twoFactorCodeId, receiveType: TOKEN.EMAIL});
      togleLoading();
      if(errors) {showAlert(ALERT.error); return};
      sliderRef.current.scrollToIndex({animated: true, index: 0});
      showAlert(ALERT.resend);
    } catch (e) {
      togleLoading();
      showAlert(ALERT.error);
    }
    // const {errors, data} = await queryFactory(DeviceResendQuery, {twoFactorCodeId, receiveType: TOKEN.EMAIL});
    // togleLoading();
    // if(errors) {showAlert(ALERT.error); return};
    // showAlert(ALERT.resend);
    // console.log(`New Token`, data);
    // sliderRef.current.scrollToIndex({animated: true, index: 0});
  },
  sendToken: ({client, showAlert, queryFactory, token, togleLoading, navigation: {navigate, state: {params: {deviceId, user, password}}}}) => async () => {
    togleLoading();
    try {
      const userParams = user.length === 11 ? {cpf: user, cardNumber: null} : {cpf: null, cardNumber: user};
      const {errors: tokenError, data: dataToken} = await queryFactory(DeviceAtiveQuery, {deviceID: deviceId, ...userParams, code: token});
      // Send Token Error
      if(tokenError) { togleLoading(); showAlert({...ALERT.error, ...errorFactory(tokenError)}); return;};
      console.log(`Send token success`, dataToken);
      // Login Factory
      const {errors: loginError, data: dataLogin} = await queryFactory(LoginV2Query, {user, password, deviceId});
      // Login Error
      if(loginError) { togleLoading(); showAlert({...ALERT.error, ...errorFactory(tokenError)}); return;};
      // Login Factory
      const {link, params: paramsFactory} = await loginFactory({user, password}, deviceId, dataLogin, BeneficiaryAndDependents, PlanCardQuery, client);
      // Redirect
      navigate(link, {...paramsFactory});
    } catch (e) {
      togleLoading();
      showAlert({...ALERT.error});
    }
  },
  newToken: ({showAlert, queryFactory, togleLoading, navigation: {state: {params: {twoFactorCodeId}}}}) => async () => {
    togleLoading();
    try {
      const {errors} = await queryFactory(DeviceResendQuery, {twoFactorCodeId, receiveType: TOKEN.SMS});
      togleLoading();
      if(errors) {showAlert(ALERT.error); return};
      showAlert(ALERT.resend);
    } catch (e) {
      togleLoading();
      showAlert(ALERT.resend);
    }
    // if(errors) {showAlert(ALERT.error); return};
    // showAlert(ALERT.resend);
    // console.log(`New Token`, data);
  },
  goToToken: ({sliderRef}) => () => {
    sliderRef.current.scrollToIndex({animated: true, index: 0});
  },
  anotherWay: ({sliderRef}) => () => {
    sliderRef.current.scrollToIndex({animated: true, index: 1});
  },
  onChangeToken: ({setToken, onSearch}) => token => {
    setToken(token);
    if(onSearch) onSearch(token);
  },
  onClear: ({setToken}) => () => {
    setToken("");
  },
});

const lifeCycle = lifecycle({
  componentWillMount() {
    // const {queryFactory, navigation: {state: {params: {deviceId, user}}}} = this.props;
    // const userParams = user.length === 11 ? {cpf: user, cardNumber: null} : {cpf: null, cardNumber: user};
    // queryFactory(DeviceAddQuery, {deviceID: deviceId, ...userParams, receiveType: TOKEN.SMS});
  }
})

const enhance = compose(
  connect(
    null,
    mapDispatchToProps
  ),
  withApollo,
  tokenRefs,
  stateFocus,
  stateToken,
  stateLoading,
  handlerQuery,
  handlers,
  lifeCycle
);

export default enhance(SecurityToken);
