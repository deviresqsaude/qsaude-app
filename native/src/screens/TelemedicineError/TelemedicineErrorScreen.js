import React from 'react';
import { Container, WaitImage } from '../Telemedicine/TelemedicineStyle';
import { QButton } from '../../components/Button';
import QText from '../../components/QText';
import { QSModal } from '../../components/Modal';
import { SystemImages, Colors } from '../../constants/theme';

const TelemedicineErrorScreen = ({ visible, setVisible, confirmAction, navigation: { navigate }, loading, backToCall }) => {
  return (
    <QSModal
      visible={visible}
      // closeModal={() => goBack()}
      title="Chamada interrompida"
    >
      <Container>
        <WaitImage source={SystemImages.blockedCall}/>
        <QText style={{color: Colors.Purple, fontSize: 16, textAlign: "center", marginBottom: 20, marginHorizontal: 20}}>Parece que sua chamada foi interrompida por algum motivo!</QText>
        <QText style={{color: Colors.Empress, fontSize: 15, textAlign: "center", marginBottom: 25}}>Não fique preocupado! A chamada não foi finalizada, clique no botão para entrar novamente</QText>
        <QButton loading={loading} text="Voltar para chamada" onPress={() => {backToCall();}} />
        <QText preset="link" style={{textDecorationLine: 'underline'}} onPress={() => {setVisible(false); confirmAction({ navigate, setVisible })}}>cancelar</QText>
      </Container>
    </QSModal>
  )
}

TelemedicineErrorScreen.propTypes = {}

export default TelemedicineErrorScreen;
