import { Alert } from 'react-native';
import { withApollo } from '@apollo/react-hoc';
import { compose, withState, withHandlers } from 'recompose';
import { connect } from 'react-redux';
import { withNavigation, NavigationActions } from 'react-navigation';
import SyncStorage from 'sync-storage';
import { AlertActions } from '../../store/actions';
import TelemedicineErrorScreen from './TelemedicineErrorScreen';
import { SystemImages, Colors } from '../../constants/theme';
import { GetOnGoingAttendance } from '../../service/query';

const mapDispatchToProps = dispatch => ({
  onServiceFailed: ({ setVisible }) =>
  dispatch(
    AlertActions.actions.openAlert({
      title: 'Um Erro ocorreu verificando sua chamada por favor tente novamente',
      shouldRenderOkButton: true,
      closeAlert: () => {dispatch(AlertActions.actions.closeAlert()); setVisible(true)},
      description: '-',
      isError: true,
    }),
  ),
  confirmAction: ({navigate, setVisible}) =>
    dispatch(
      AlertActions.actions.openAlert({
        buttonTitle: 'Sim, quero cancelar',
        shouldRenderOkButton: true,
        buttonOnPress: () => {dispatch(AlertActions.actions.closeAlert()); SyncStorage.remove('callId'); navigate('App')},
        closeAlert: () => {dispatch(AlertActions.actions.closeAlert()); setVisible(true);},
        title: 'Cancelar consulta',
        image: SystemImages.question,
        titleColor: Colors.Purple,
        description: 'Caso cancele, a chamada de vídeo com o Profissional de saúde vai ser encerrada.',
        link: true,
        linkTitle: 'Voltar',
        linkOnPress: () => {dispatch(AlertActions.actions.closeAlert()); setVisible(true);},
      }),
    ),
});

const enhance = compose(
  connect(
    null, 
    mapDispatchToProps
  ),
  withState('visible', 'setVisible', true),
  withState('loading', 'setLoading', false),
  withNavigation,
  withApollo,
  withHandlers({
    backToCall: ({ navigation : { navigate, reset }, client, onServiceFailed, setLoading, setVisible }) => async () => {
      setLoading(true)
      const callId = SyncStorage.get('callId');
      console.log('error', callId)
      if(callId) {
        //check if call is still there
        const request = await client.query({
          query: GetOnGoingAttendance,
        });

        const { errors, data } = request;
        const hasError = errors ? errors.length > 0 : false;
        if (hasError) {
          setLoading(false)
          setVisible(false);
          onServiceFailed({setVisible});
          return;
        }

        const { getOngoingAttendance } = data;
        if(getOngoingAttendance.hasAttendance) {
          setVisible(false)
          reset([NavigationActions.navigate({ routeName: 'Telemedicine', params: { meetingNumber: getOngoingAttendance.zoomMeetingId } })], 0);
          return;
        } else {
          setVisible(false);
          Alert.alert('Sua sessão de telecuidado não existe mais, tente novamente');
          SyncStorage.remove('callId');
          navigate('App');
        }
        // return;
        // dispatch(resetAction);
        // navigate('Telemedicine', { meetingNumber: getOngoingAttendance.zoomMeetingId })
      } 
      setLoading(false)
      setVisible(false)
      Alert.alert('Sua sessão de telecuidado não existe mais, tente novamente');
      navigate('App')
    }
  })
)

export default enhance(TelemedicineErrorScreen);