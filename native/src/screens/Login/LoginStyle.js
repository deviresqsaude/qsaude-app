import { Dimensions } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import styled from 'styled-components/native';
import LOGO from '../../../assets/images/img_logo.png';
import WAVES from '../../../assets/images/img_waves.png';

import { Colors } from '../../constants/theme';

const { height } = Dimensions.get('window');

export const Version = styled.Text({
  // flex: 1,
  textAlign: "center",
  fontSize: 10,
  color: Colors.Gray
});

export const LoginScreenWrapper = styled.View({
  flex: 1
});

export const Container = styled.View({
  flex: 1,
  // paddingBottom: 100,
  justifyContent: 'center',
  maxHeight: height  - 100
});

// QLogo
export const QLogo = styled.Image.attrs({
  source: LOGO,
  resizeMode: 'contain',
})`
  width: 242px;
  height: 78;
  margin-top: 50px;
  margin-bottom: 30px;
  align-self: center;
`;

// Spacing
export const Spacing = styled.View`
  height: ${({ size }) => size || 0};
`;

// Waves BG
export const BottomWaves = styled.Image.attrs({
  source: WAVES,
  resizeMode: 'stretch',
})({
  width: '100%',
  height: 77,
  position: 'absolute',
  bottom: 0,
  zIndex: -1,
});

// Login Container
export const LoginContainer = styled(KeyboardAwareScrollView).attrs({
  width: '100%',
  paddingHorizontal: 16,
})``;

export const ForgotPasswordWrapper = styled.View({
  alignItems: 'flex-end',
  width: '100%',
  marginBottom: 24,
  paddingRight: 8,
});

export const TermsWrapper = styled.View(() => ({
  marginTop: 16,
  marginBottom: 16,
  flexDirection: 'row',
  flexWrap: 'wrap',
}));

export const PaddingWrapper = styled.View({
  paddingHorizontal: 16
})

export const CardStatusWrapper = styled.View({
  alignSelf: 'flex-end',
  flexDirection: 'row',
  alignItems: 'center'
})

export const CardStatusText = styled.Text(({status}) => ({
  color: status ? Colors.GreenSuccess : Colors.Red,
  fontWeight: 'bold',
  fontSize: 12,
}))

export const CardStatusImage = styled.Image({
  width: 22,
  height: 22,
  marginRight: 4
})

export const SelectCardContainer = styled.ScrollView({
  width: '50%',
  // flex: 1,
  paddingBottom: 16,
})

export const LinkContainer = styled.TouchableOpacity({
  width: '100%',
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'row'
})
