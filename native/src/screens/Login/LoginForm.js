import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'formik';
// import { KeyboardAvoidingView } from "react-native";
import { LoginContainer, ForgotPasswordWrapper } from './LoginStyle';
import { QTextInput, QButton, QText } from '../../components';
import { LOGIN } from '../../constants/TestIds';

const LoginForm = props => {
  // All props comes from withFormik HOC.
  const {
    isSubmitting,
    isValid,
    handleSubmit,
    showPassword,
    setShowPassword,
    navigation: { navigate },
    isLoggingIn
  } = props;

  return (
    <LoginContainer testID={LOGIN.form.form} showsVerticalScrollIndicator={false}>
      <Field
        testID={LOGIN.form.userInput}
        keyboardType="numeric"
        name="user"
        type="custom"
        mask="9999999999999999999"
        label="CPF ou carteirinha"
        disabled={isSubmitting}
        component={QTextInput}
      />
      <Field
        testID={LOGIN.form.passwordInput}
        secureTextEntry={showPassword}
        rightAddon={showPassword ? 'closeEye' : 'openEye'}
        rightAddonCallback={() => setShowPassword(!showPassword)}
        label="Senha"
        name="password"
        disabled={isSubmitting}
        component={QTextInput}
      />
      <ForgotPasswordWrapper>
        <QText
          testID={LOGIN.form.forgotPassword}
          onPress={() => navigate('ForgotPassword')}
          preset="link">
          Esqueceu a senha?
        </QText>
      </ForgotPasswordWrapper>
      <QButton
        accessible
        accessibilityLabel={isValid ? 'Entrar Habilitado' : 'Entrar Desabilitado'}
        testID={LOGIN.form.submitButton}
        loading={isSubmitting || isLoggingIn}
        disabled={!isValid}
        text="Entrar"
        onPress={() => handleSubmit()}
        // onPress={() => {setLogin(true); startLoginFormAnimation();}}
      />
    </LoginContainer>
  );
};

LoginForm.propTypes = {
  isSubmitting: PropTypes.bool.isRequired,
  isValid: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

export default LoginForm;
