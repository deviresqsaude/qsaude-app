import React from 'react';
import moment from 'moment';
import SyncStorage from 'sync-storage';
import { View, Linking, Image } from 'react-native';
import Swiper from 'react-native-swiper';
import Config from 'react-native-config';

import { SelectCardContainer, SwiperItem, PaddingWrapper, CardStatusText, CardStatusImage, CardStatusWrapper, LinkContainer } from './LoginStyle';
import { QText, QButton } from '../../components';
import {
  HealthCardWrapper,
  HealthCardBody,
  HealthCardLogo,
} from '../HealthCard/HealthCardStyle';
import { PROFILEUSER } from '../../constants/TestIds';
import { elevationShadow } from '../../helpers';
import { Colors, SystemIcons, SystemImages } from '../../constants/theme';
import HealthCardLoading from '../HealthCard/HealthCardLoading';

const SelectCard = ({ healthCards, selectedHealthCard, selectHealthCard, logOut, navigation: { navigate }, changeToken, isLoggedIn, isLoggingIn }) => {

  const swiperConfig = {
    onIndexChanged: (index) => selectHealthCard(healthCards[index]),
    style: {height: 290},
    index: 0,
    loop: false,
    dot: <View style={{borderWidth: 1, borderColor: Colors.Purple, width: 12, height: 12,borderRadius: 6, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3,}} />,
    activeDot: <View style={{backgroundColor: Colors.Purple, width: 12, height: 12,borderRadius: 6, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3,}} />
  }

  return (
    <SelectCardContainer showsVerticalScrollIndicator={false}>
      <PaddingWrapper>
      <QText color={Colors.Purple} fontSize={12} preset="bold">Dependentes: {healthCards.length}</QText>
      </PaddingWrapper>
      {
        isLoggedIn ?
        <Swiper {...swiperConfig} >
        {healthCards && healthCards.length > 0 && healthCards.map((card, index) => {
          const { numCard, name, healthPlanCode, startingDate, segmentPlan, status } = card;
          return (
            <SwiperItem key={index}>
              <HealthCardWrapper style={{ ...elevationShadow('#000000', 8, 0.5) }}>
                <HealthCardBody>
                  <View
                    style={{
                      height: 40,
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}
                  >
                    <HealthCardLogo />
                    <QText
                      testID={PROFILEUSER.checkVcard.registration}
                      color={Colors.Purple}
                      fontSize={14}
                      preset="bold"
                    >
                      {numCard || '---'}
                    </QText>
                  </View>
                  <View>
                    <View style={{ marginBottom: 4, width: '100%' }}>
                      <QText
                        testID={PROFILEUSER.checkVcard.clientName}
                        color={Colors.Purple}
                        fontSize={12}
                        preset="bold"
                      >
                        {name || '---'}
                      </QText>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                      <View style={{ flexDirection: 'column', flex: 1 }}>
                        <View>
                          <QText color={Colors.Gray} fontSize={10} preset="bold">
                            Meu plano
                          </QText>
                          <QText
                            testID={PROFILEUSER.checkVcard.healthPlanCode}
                            color={Colors.Purple}
                            fontSize={12}
                            preset="bold"
                          >
                            {healthPlanCode || '---'}
                          </QText>
                        </View>
                        <View>
                          <QText color={Colors.Gray} fontSize={12} preset="bold">
                            Inicio vigência
                          </QText>
                          <QText
                            testID={PROFILEUSER.checkVcard.startingDate}
                            color={Colors.Purple}
                            fontSize={12}
                            preset="bold"
                          >
                            {startingDate ? moment(startingDate).format('DD/MM/YY') : '---'}
                          </QText>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'column', flex: 1 }}>
                        <View>
                          <QText color={Colors.Gray} fontSize={10} preset="bold">
                            Segmentação
                          </QText>
                          <QText
                            testID={PROFILEUSER.checkVcard.segmentPlan}
                            color={Colors.Purple}
                            fontSize={12}
                            preset="bold"
                          >
                            {segmentPlan || '---'}
                          </QText>
                        </View>
                        <View />
                      </View>
                    </View>
                    <CardStatusWrapper>
                      <CardStatusImage source={ status == true ? SystemImages.successImage:  SystemImages.errorImage } />
                      <CardStatusText status={status}>{ status == true ? "Ativo" : "Inativo" } </CardStatusText>
                  </CardStatusWrapper>
                  </View>
                </HealthCardBody>
              </HealthCardWrapper>
            </SwiperItem>
          );
        })}
      </Swiper> : <View style={{paddingVertical: 22}}><HealthCardLoading /></View>
      }
      <PaddingWrapper>
        {/* {
          selectedHealthCard.active ?
            <QButton text={"Continuar"} onPress={() => changeToken()}/>
            :
            <QButton disabled text={"Continuar"} onPress={()=>{}}/>
        } */}
        <QButton text="Continuar" onPress={() => changeToken()} loading={isLoggingIn}/>
        <QButton error text="Cancelar" onPress={logOut} />
        <LinkContainer>
          <Image source={SystemIcons.whatsapp} style={{width: 16, height: 16, marginRight: 4}}/>
          <QText
            preset="link"
            onPress={() => Linking.openURL(
                `https://api.whatsapp.com/send?phone=${Config.WHATSAPP_LINKNUMBER}`,
              )}
            style={{ textDecorationLine: 'underline' }}
          >
            Carteirinha inativa?
          </QText>
        </LinkContainer>
      </PaddingWrapper>
    </SelectCardContainer>
  );
};

export default SelectCard;
