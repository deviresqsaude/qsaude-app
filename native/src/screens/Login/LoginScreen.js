/* eslint-disable react/prop-types */
import React from 'react';
import Config from 'react-native-config';
import { getReadableVersion } from 'react-native-device-info'
import PropTypes from 'prop-types';
import { Container, QLogo, BottomWaves, LoginScreenWrapper, Version } from './LoginStyle';
import LoginForm from './LoginForm';

const LoginScreen = props => {
  return (
    <LoginScreenWrapper>
      <Container>
        <QLogo />
        <LoginForm {...props} />
        {["Development", "Staging"].includes(Config.ENV_NAME ) && <Version>Versão para teste:  {getReadableVersion()}</Version>}
      </Container>
      <BottomWaves />
    </LoginScreenWrapper>
  );
};

LoginScreen.propTypes = {
  title: PropTypes.string,
};

LoginScreen.defaultProps = {
  title: `Login`,
};

export default LoginScreen;
