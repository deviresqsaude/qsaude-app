import { withFormik } from 'formik';
import { Animated } from 'react-native';
import * as yup from 'yup';
import { connect } from 'react-redux';
import SyncStorage from 'sync-storage';
import { compose, withState, withHandlers, lifecycle } from 'recompose';
import { withNavigation } from 'react-navigation';
import { withApollo } from '@apollo/react-hoc';
import DeviceInfo from 'react-native-device-info';
import { LoginQuery, LoginV2Query, BeneficiaryAndDependents, PlanCardQuery } from '../../service/query';
import { loginFactory } from '../../helpers';
import LoginScreen from './LoginScreen';
import { AlertActions } from '../../store/actions';
import { SystemImages } from '../../constants/theme';

const ALERTS = {
  invalid_user: {
    title: "Usuario não encontrado",
    description: "Esta carterinha não foi encontrada, tente novamente por favor",
    shouldRenderImage: true,
    isError: true,
  },
  inactive_user: {
    title: "Aconteceu um erro",
    shouldRenderImage: true,
    isError: true,
    buttonTitle: "Ok"
  },
  session_ended: {
    image: SystemImages.sessionTime,
    title: "Sessão expirada!",
    shouldRenderImage: false,
    description: "Sua sessão expirou, faça login novamente!",
    shouldRenderOkButton: true,
    buttonTitle: "Ok"
  },
  default_error: {
    shouldRenderImage: true,
    isError: true,
    title: "Aconteceu um Erro, tente novamente ou ligue para o TimeQ"
  },
  health_card: {
    title: "Erro ao selecionar carteirinha",
  }
};

// Error Handling
const mapDispatchToProps = dispatch => ({
  showAlert: options => dispatch(AlertActions.actions.openAlert({...options, shouldRenderOkButton: true, closeAlert: () =>  dispatch(AlertActions.actions.closeAlert())}))
});

const validateLogin = yup.object().shape({
  user: yup
    .string()
    .required('Campo obrigatório')
    .min(11, 'Mínimo de 11 dígitos'),
  password: yup
    .string()
    .required('Campo obrigatório')
    .min(6, 'Mínimo de 6 caracteres')
});

const handlers = withHandlers({
  loginFunction: ({client}) => ({user, password, deviceId}) => {
    return client.query({
      // query: LoginQuery,
      query: LoginV2Query,
      variables: {
        user,
        password,
        deviceId
      },
    });
  },
  queryFactory: ({client}) => (query, token) => {
    return client.query({
      query,
      context: {headers: { authorization: token }}
    });
  },
});

const withForm = withFormik({
  validationSchema: validateLogin,
  mapPropsToValues: () => {
    const user = SyncStorage.get('@form[login]:user');
    return {
      user,
      password: __DEV__ ? "" : undefined,
    };
  },
  handleSubmit: async (values, formik) => {
    const {
      setSubmitting,
      // setFieldValue,
      props: {
        client,
        showAlert,
        navigation: { navigate },
        loginFunction,
        setLoggingIn,
      },
    } = formik;
    try {
      const deviceId = DeviceInfo.getUniqueId();
      const { errors: loginError, data: loginData }  = await loginFunction({
        user: values.user,
        password: values.password,
        deviceId
      });
      // List of Errors
      const hasError = loginError ? loginError.length > 0 : false;
      if (hasError) {
        // eslint-disable-next-line no-restricted-syntax
        for (const e of loginError) {
          switch (e.extensions.code) {
            case "401": showAlert({...ALERTS.inactive_user, ...{description: e.message}}); break;
            case "404": showAlert({...ALERTS.invalid_user}); break;
            case "403": {
              const message = JSON.parse(e.message);
              navigate('SecurityToken', {...values, ...message, deviceId});
              break;
            }
            default: {
              showAlert({...ALERTS.default_error, ...{description: e.message}});
              break;
            }
          }
        };
        setSubmitting(false);
        setLoggingIn(false);
        return;
      };
      // Login Factory
      const {link, params: paramsFactory} = await loginFactory(values, deviceId, loginData, BeneficiaryAndDependents, PlanCardQuery, client);
      // Redirect
      navigate(link, {...paramsFactory});
      setSubmitting(false);
      setLoggingIn(false)
      return;
    } catch (e) {
      setSubmitting(false);
      setLoggingIn(false);
      showAlert({...ALERTS.default_error});
    };
  },
});

const lifecycleMethod = lifecycle({
  // everytime the screen loads, show loginform
  componentDidMount() {
    const { navigation, showAlert } = this.props;
    const loginType = navigation.getParam('loginType');
    switch (loginType) {
      case "IS_SESSION_ENDED":
        showAlert(ALERTS.session_ended)
        break;
      default:
        break;
    };
  }
});

export default compose(
  connect(
    null,
    mapDispatchToProps,
  ),
  withNavigation,
  withState('showPassword', 'setShowPassword', true),
  withState('password', 'setPassword', ''),
  withState('isLoggedIn', 'setLogin', false),
  withState('isLoggingIn', 'setLoggingIn', false),
  withState('loginFormAnimation', 'setLoginFormAnimation', new Animated.Value(0)),
  withState('healthCards', 'setHealthCards', []),
  withState('selectedHealthCard', 'selectHealthCard', {}),
  withState('profileUser', 'setProfileUser', {}),
  withApollo,
  handlers,
  withForm,
  lifecycleMethod,
)(LoginScreen);
