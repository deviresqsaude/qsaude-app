import React from 'react';
import styled from 'styled-components/native';
import { Placeholder, Fade, PlaceholderLine, PlaceholderMedia } from 'rn-placeholder';
import { Colors, SystemImages } from '../../constants/theme';
import { EmptyData } from '../../components';

const EmptyContent = styled.View({});

export const Container = styled.View({
  flex: 1,
});
export const Content = styled.ScrollView.attrs({
  contentContainerStyle: {},
})``;

export const Separator = styled.View({
  borderBottomWidth: 1,
  borderColor: Colors.WhiteSmoke,
  height: 10,
  width: "100%",
})

export const CalendarFooter = styled.View({
  flexDirection: 'row',
  marginRight: 16,
  marginLeft: 16,
  justifyContent: 'flex-end',
  borderBottomColor: Colors.LightGray,
  borderBottomWidth: 1,
  padding: 4,
  marginBottom: 16,
  alignItems: 'center',
});

export const CalendarSubtitleMark = styled.View(({ color }) => ({
  width: 8,
  height: 8,
  backgroundColor: color || Colors.Purple,
  borderRadius: 4,
  marginRight: 4,
}));

export const CalendarLoading = () => (
  <EmptyContent>
    <Placeholder
      Animation={Fade}
      Left={PlaceholderMedia}
      style={{paddingLeft: 16, paddingRight: 16}}>
      <PlaceholderLine width={100} />
      <PlaceholderLine width={60} />
    </Placeholder>
    <Placeholder Animation={Fade} style={{marginTop: 10, marginBottom: 10}}>
      <PlaceholderLine round width={100} height={400} style={{borderRadius: 0}}/>
    </Placeholder>
    <Placeholder Animation={Fade}>
      <PlaceholderLine round width={100} height={20} style={{borderRadius: 0}}/>
    </Placeholder>
  </EmptyContent>
);

export const CalendarError = styled(EmptyData).attrs(props => ({
  title: 'Ops!!',
  description: 'Aconteceu um problema, Tente novamente.',
  buttonText: 'Tentar novamente',
  onPress: props.refetch,
  imageSource: SystemImages.errorImage,
}))``;
