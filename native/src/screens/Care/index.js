import { withState, compose, branch, renderComponent, lifecycle, withHandlers } from 'recompose';
import { withApollo, graphql } from '@apollo/react-hoc';
import R from 'ramda';
import moment from 'moment';
import { connect } from 'react-redux';
// import syncStorage from 'sync-storage';
import CareScreen from './CareScreen';
import { ListAppointments, AppointmentDetails, ExamDetails, AllExams } from '../../service/query';
import { AuthActions, CareActions, NavActions } from '../../store/actions';
import { CalendarError, CalendarLoading } from './CareStyle';
import { dots } from '../../helpers';

const DATA_APPOINTMENTS = "dataAppointments";
const DATA_EVENTS = "dataEvents";

// Redux Handlers
const mapStateToProps = ({ care }) => {
  return { careState: {...care}}
};

const mapDispatchToProps = dispatch => ({
  onProfileLoaded: beneficiary => dispatch(AuthActions.actions.setUserProfile(beneficiary)),
  setAppoinments: events => dispatch(CareActions.actions.setAppoinments(events)),
  setMarketEvents: events => dispatch(CareActions.actions.setMarketEvents(events)),
  updateMarketEvents: events => dispatch(CareActions.actions.upMarketEvents(events)),
  setEvents: events => dispatch(CareActions.actions.setEvents(events)),
  updateEvents: events => dispatch(CareActions.actions.updateEvents(events)),
  setExams: exams => dispatch(CareActions.actions.setExams(exams)),
  clearEvents: () => dispatch(CareActions.actions.clearEvents()),
  setDate: date => dispatch(CareActions.actions.setDate(date)),
  setNavCurrentPage: state => dispatch(NavActions.actions.setCurrentPage(state)),
});

const withCurrentEventsId = withState('currentEventsId', 'setEventsId', []);
const withCurrentAppointsId = withState('currentAppointmentsId', 'setAppointmentsId', []);
const withDataLoaded = withState('isDataLoaded', 'setDataLoaded', false);

const withCurrentExamsId = withState('currentExamsId', 'setExamsId', []);

const getMarkedEvents = ({ events, flag }) => {
  const eventsParsedToCalendar = {};
  R.forEach(item => {
    const { start: eventDate } = item;
    eventsParsedToCalendar[eventDate] = R.merge(
      eventsParsedToCalendar[R.prop('start', item)],
      {
        marked: true,
        dots: [flag],
      },
    );
  }, events);
  return eventsParsedToCalendar;
};

const getMarkedExams = ({ exams, flag }) => {
  const eventsParsedToCalendar = {};
  R.forEach(item => {
    const { occurrenceDateTime: eventDate } = item;
    const formatDate = moment(eventDate).format('YYYY-MM-DD');
    eventsParsedToCalendar[formatDate] = R.merge(
      eventsParsedToCalendar[formatDate],
      {
        marked: true,
        dots: [flag],
      },
    );
  }, exams);
  return eventsParsedToCalendar;
}

const getEventsId = (events, date) => {
  return R.prop('events', R.head(R.filter(R.propEq('start', date), events))) || [];
};

const formatExams = (exams) => {
  return R.map(item => ({
    id: R.prop('id', item),
    doctorName: R.prop('requester', item).display,
    name: R.prop('code', item)[0].display,
    status: R.prop('status', item),
    start: moment(R.prop('occurrenceDateTime', item)).format('YYYY-MM-DD')
  }), exams) || [];
}

const withAppointmentsData = graphql(ListAppointments, {
  name: DATA_APPOINTMENTS,
  options: () => ({
    notifyOnNetworkStatusChange: true,
  }),
});

const withExamsDate = graphql(AllExams, {
  name: "allExams",
  options: ({ setExams }) => ({
    notifyOnNetworkStatusChange: true,
    variables: {
      params: {
        status: "completed",
        category: "108252007,363679005,71388002"
      },
      resultType: null
    },
    onCompleted: ({ exams }) => {
      setExams(formatExams(exams))
    }
  }),
});

const withEventsData = graphql(AppointmentDetails, {
  name: DATA_EVENTS,
  options: ({newEvents, currentEventsId, updateEvents }) => ({
    notifyOnNetworkStatusChange: true,
    variables: {ids: newEvents || currentEventsId },
    onCompleted: ({appointmentsDetails: todayEvents}) => {
      updateEvents(R.map(x=>R.merge(x, {type: 1}), todayEvents))
    }
  }),
});

// const withExamsDetails = graphql(ExamDetails, {
//   name: "dataExamDetails",
//   options: ({ currentExamsId, updateEvents, newExams }) => ({
//     notifyOnNetworkStatusChange: true,
//     variables: {examIds: currentExamsId || newExams},
//     onCompleted: ({ examsDetails: todayExams }) => {

//       if(todayExams) {
//         updateEvents(todayExams)
//       }
//     }
//   }),
// });

const renderForLoading = branch(({ dataAppointments, allExams }) =>
  // networkStatus: 3 == fetchmore && 4 == refetch
  (dataAppointments&&dataAppointments.loading&&dataAppointments.networkStatus !== 3) ||
  // (dataEvents&&dataEvents.loading&&dataEvents.networkStatus !== 4) ||
  (allExams&&allExams.loading&&allExams.networkStatus !== 3),
  renderComponent(CalendarLoading));

// const setAppointRefetch = withProps(({ dataAppointments }) => ({ appointRefetch : dataAppointments&& dataAppointments.refetch }))

const renderForError = branch(({ dataAppointments }) => dataAppointments && dataAppointments.error, renderComponent(CalendarError));

const lifeCycleMethod = lifecycle({
  componentWillMount() {
    const {navigation, setNavCurrentPage} = this.props;
    setNavCurrentPage(navigation.state);
  },
  componentDidMount() {
    // mark dates in the calendar && set today's event
    const { allExams: { exams }, dataAppointments: { appointmentsDate }, setMarketEvents, setAppoinments, setDate, setEventsId, updateEvents } = this.props;
    const todayEventsId = getEventsId(appointmentsDate, moment().format('YYYY-MM-DD'));
    // const todayExamsId = getExamsId(exams, moment().format('YYYY-MM-DD'));
    
    setDate(moment().format('YYYY-MM-DD'));
    // setExams(exams);
    setAppoinments(appointmentsDate || [])
    setEventsId(todayEventsId || []);
    // setExamsId(todayExamsId || []);
    // get today's exams and updateEvents
    updateEvents(formatExams(R.filter(item => R.equals(moment(R.prop('occurrenceDateTime', item)).format('YYYY-MM-DD'), moment().format('YYYY-MM-DD')), exams)))

    if(appointmentsDate && exams) {
      const examEvents = getMarkedExams({ exams: exams || [], flag: dots.exam })
      const appointmentEvents = getMarkedEvents({ events: appointmentsDate || [], flag: dots.medical })
      const concatValues = (k, l, r) =>  k == 'dots' ? R.concat(l, r) : r
      const allEvents = R.mergeDeepWithKey(concatValues, examEvents, appointmentEvents);

      setMarketEvents(allEvents)
    }
  },
})

const withRefetch = withHandlers({
  refetchAll: ({ dataAppointments: { fetchMore: fetchMoreAppointments }, allExams: { fetchMore: fetchMoreExams }, setAppoinments, setExams, updateMarketEvents, careState }) => () => {
    fetchMoreAppointments({
      updateQuery: (previousResult, { fetchMoreResult }) => {
        if(!fetchMoreResult) return previousResult;
        const diffResultA = R.difference(fetchMoreResult.appointmentsDate, previousResult.appointmentsDate);
        if(diffResultA.length > 0) {
          const newMarkedAppointments = R.differenceWith((x, y)=>x.start == y.start, diffResultA, careState.appoinments);
          const newAppointments = R.unionWith(R.eqBy(R.prop('start')), diffResultA, careState.appoinments);
          updateMarketEvents(getMarkedEvents({ events: newMarkedAppointments, flag: dots.medical}))
          setAppoinments(newAppointments);
        }
      },
    });

    fetchMoreExams({
      updateQuery: (previousResult, { fetchMoreResult }) => {
        if(!fetchMoreResult) return previousResult;
        const diffResultE = R.difference(fetchMoreResult.exams, previousResult.exams);
        if(diffResultE.length > 0) {
          const newMarkedExams = R.differenceWith((x, y)=>x.start == y.start, diffResultE, careState.exams);
          const newExams = R.unionWith(R.eqBy(R.prop('start')), diffResultE, careState.exams);
          updateMarketEvents(getMarkedExams({ events: newMarkedExams, flag: dots.exam}))
          setExams(newExams);
        }
      },
    })
  },
})

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  // State
  withCurrentEventsId,
  withDataLoaded,
  withCurrentExamsId,
  withCurrentAppointsId,
  // Apollo Config
  withApollo,
  // Data
  withAppointmentsData,
  withExamsDate,
  withEventsData,
  // filterAppointments,
  // withExamsDetails,
  // Renders,
  renderForLoading,
  renderForError,
  withRefetch,
  // setAppointRefetch,
  // To get the first date events
  lifeCycleMethod
);

export default enhance(CareScreen);
