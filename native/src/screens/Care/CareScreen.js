/* eslint-disable react/forbid-prop-types */
import React from 'react';
import { RefreshControl } from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';
import EventManager from './EventManager';
import CalendarManager from './CalendarManager';
import { Container, Content, Separator } from './CareStyle';
import { ProfileInfo } from '../../components/Profile';

// const calendarUpdateConfig = {
//   updateQuery: (previousResult, { fetchMoreResult }) => {
//     if(!fetchMoreResult) return previousResult;
//     console.log('prev', previousResult)
//     console.log('fe', fetchMoreResult)
//     return {}
//   }
// }

const CareScreen = props => {
  const {
    updateEvents,
    dataAppointments: {loading: loadCalendar },
    allExams: {loading: loadExamsDate },
    dataEvents: {loading: loadEvents},
    setEventsId,
    refetchAll
  } = props;

  return (
    <Container testID="CARE.CareScreen">
      <ProfileInfo/>
      <Separator/>
      <Content showsVerticalScrollIndicator={false}
        refreshControl={<RefreshControl refreshing={loadCalendar} onRefresh={()=> refetchAll()} />}>
        <CalendarManager {...{
            loading: loadCalendar || loadExamsDate
          }}
          onHandlerDay={({ eventsId, exams }) => { setEventsId(eventsId); updateEvents(exams) }}/>
        <EventManager {...{ loading: loadEvents }}/>
      </Content>
    </Container>
  );
};

CareScreen.propTypes = {
  markedDates: PropTypes.objectOf(PropTypes.any),
  todayEvents: PropTypes.arrayOf(PropTypes.any),
  onSelectDay: PropTypes.func,
  data: PropTypes.objectOf(PropTypes.any),
  currentDay: PropTypes.string
};

CareScreen.defaultProps = {
  markedDates: {},
  todayEvents: [],
  onSelectDay: () => {},
  data: {},
  currentDay: moment().format('YYYY-MM-DD')
};

export default CareScreen;
