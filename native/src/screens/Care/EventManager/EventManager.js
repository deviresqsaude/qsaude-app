import React from 'react';
import R from 'ramda';
import PropTypes from 'prop-types';
import { withNavigation } from 'react-navigation';
import { QText, EventCard } from '../../../components';
import { Colors } from '../../../constants/theme';
import { EventListContainer, EventsPlaceholder, EventsEmpty } from './EventManageStyle';

const EventManager = ({loading, careState: {events}, goToEvent}) => {
  const renderEvents = R.map(item => {
    return (
    <EventCard
      onPress={() => goToEvent(item)}
      {...item}
      key={item.id}
    />
  )});
  return (
    <EventListContainer>
      <QText margin={{ bottom: 16 }} preset="navTitle" color={Colors.TextGray}>
        Eventos
      </QText>
      {loading ? <EventsPlaceholder /> : renderEvents(events)}
      {!loading && events.length === 0 && <EventsEmpty />}
    </EventListContainer>
  );
};

EventManager.propTypes = {
  loading: PropTypes.bool,
};

EventManager.defaultProps = {
  loading: false,
};

export default withNavigation(EventManager);
