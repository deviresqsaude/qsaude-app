import { compose, withHandlers } from 'recompose';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import { CareActions } from '../../../store/actions';
import EventManager from './EventManager';

const mapStateToProps = ({care}) => ({
  careState: {...care}
});

const mapDispatchToProps = dispatch => ({
  setCurrentEvent: event => dispatch(CareActions.actions.setCurrentEvent(event)),
});

const handlers = withHandlers({
  goToEvent: ({setCurrentEvent, navigation: { navigate }}) => event => {
    setCurrentEvent(event);
    navigate('EventDetails', { event });
  }
});

const enchance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  withNavigation,
  handlers,
); // 🧙‍♀️

export default enchance(EventManager);
