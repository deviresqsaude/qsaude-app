
import React from 'react';
import styled from "styled-components/native";
import { Placeholder, PlaceholderMedia, Fade } from "rn-placeholder";
import { Colors } from "../../../constants/theme";
import { QText } from '../../../components';

const EmptyContent = styled.View({});

export const EventListContainer = styled.View({
  marginLeft: 16,
  marginRight: 16,
  paddingBottom: 60,
});

export const EventsPlaceholder = () => (
  <Placeholder Animation={Fade}>
    <PlaceholderMedia style={{width: '100%', height: 48, marginBottom: 12, borderRadius: 12}}/>
    <PlaceholderMedia style={{width: '100%', height: 48, borderRadius: 12}} />
  </Placeholder>
);



export const EventsEmpty = () => (
  <EmptyContent>
    <QText color={Colors.TextGray}>
      Você não possui nenhum evento na data selecionada. Agende agora mesmo!
    </QText>
  </EmptyContent>
);
