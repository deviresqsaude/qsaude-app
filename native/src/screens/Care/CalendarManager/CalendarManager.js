/* eslint-disable react/forbid-prop-types */
import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Calendar } from '../../../components';

const CareScreen = props => {
  const {
    onSelectDay,
    loading,
    currentDay,
    careState: {marketEvents: markedDates}
  } = props;
  return (
    <View testID="CARE.CareScreen">
      <Calendar
        markingType="multi-dot"
        examTag
        {...{
        markedDates: {
          ...markedDates,
          [currentDay]:  {
            dots: !!markedDates[currentDay] && markedDates[currentDay].dots,
            selected: true,
            disableTouchEvent: true,
            marked: !!markedDates[currentDay],
          },
        },
        onSelectDay,
        loading
      }}/>
    </View>
  );
};

CareScreen.propTypes = {
  markedDates: PropTypes.objectOf(PropTypes.any),
  todayEvents: PropTypes.arrayOf(PropTypes.any),
  onSelectDay: PropTypes.func,
  dataAppointments: PropTypes.objectOf(PropTypes.any),
  currentDay: PropTypes.string
};

CareScreen.defaultProps = {
  markedDates: {},
  todayEvents: [],
  onSelectDay: () => {},
  dataAppointments: {},
  currentDay: moment().format('YYYY-MM-DD')
};

export default CareScreen;
