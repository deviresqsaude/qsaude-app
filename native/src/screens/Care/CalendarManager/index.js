import R from 'ramda';
import moment from 'moment';
import { compose, withState, withHandlers, lifecycle } from 'recompose';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import { CareActions } from '../../../store/actions';
import CalendarManager from './CalendarManager';

const getEventsId = (events, date) => {
  return R.prop('events', R.head(R.filter(R.propEq('start', date), events))) || [];
};

const mapStateToProps = ({care}) => ({
  careState: {...care}
});

const mapDispatchToProps = dispatch => ({
  setAppoinments: events => dispatch(CareActions.actions.setAppoinments(events)),
  setMarketEvents: events => dispatch(CareActions.actions.setMarketEvents(events)),
  setEvents: events => dispatch(CareActions.actions.setEvents(events)),
  setDate: date => dispatch(CareActions.actions.setDate(date)),
});

const withCurrentday = withState('currentDay', 'setCurrentDay', moment().format('YYYY-MM-DD'));

const handlers = withHandlers({
  onSelectDay: props => daystring => {
    const {
      setDate,
      careState: { appoinments, exams },
      setCurrentDay,
      setEvents,
      onHandlerDay } = props;
    setDate(daystring);
    setCurrentDay(daystring);
    setEvents([]);
    onHandlerDay({eventsId: getEventsId(appoinments, daystring), exams: R.filter(R.propEq('start', daystring), exams)});
  },
});

const withLifeCycle = lifecycle({
  componentDidMount() {
    const { onSelectDay, currentDay } = this.props;
    onSelectDay(currentDay);
  },
});

const enchance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  withNavigation,
  withCurrentday,
  handlers,
  withLifeCycle,
); // 🧙‍♀️

export default enchance(CalendarManager);
