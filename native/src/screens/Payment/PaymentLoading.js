import React from 'react';
import { Placeholder, PlaceholderLine, Fade } from 'rn-placeholder';
import { PaymentLoadingWrapper, PaymentLoadingShadowBox } from './PaymentStyle';
import { elevationShadow } from '../../helpers';

const PaymentLoading = () => {
  return (
    <PaymentLoadingWrapper>
      <PaymentLoadingShadowBox style={{...elevationShadow("#000000", 8, .5)}}>
        <Placeholder Animation={Fade}>
          <PlaceholderLine height={16} width={40} style={{marginTop: 12, marginBottom:32, borderRadius: 2, alignSelf: 'center'}}/>
          <PlaceholderLine height={44} style={{marginBottom: 18, borderRadius: 2}}/>
          <PlaceholderLine height={44} style={{marginBottom: 18, borderRadius: 2}}/>
          <PlaceholderLine height={44} style={{marginBottom: 18, borderRadius: 2}}/>
          <PlaceholderLine height={16} width={60} style={{marginBottom: 10, borderRadius: 2}}/>
          <PlaceholderLine height={16} width={80} style={{marginBottom: 10, borderRadius: 2}}/>
          <PlaceholderLine height={16} width={60} style={{ borderRadius: 2}}/>
        </Placeholder>
      </PaymentLoadingShadowBox>
    </PaymentLoadingWrapper>
  )
}

export default PaymentLoading;
