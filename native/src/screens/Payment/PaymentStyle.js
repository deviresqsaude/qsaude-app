import styled from 'styled-components/native';
import { ShadowBox } from '../../components/DetailsCard/DetailsCardStyle';
import { EmptyData } from '../../components';
import { SystemImages } from '../../constants/theme';

export const Container = styled.ScrollView.attrs({
  contentContainerStyle: {
    // flex: 1,
  },
})``;

export const ContainerWrapper = styled.View({
  flex: 1
});

export const PaymentLoadingWrapper = styled.View`
  width: 100%;
`;

export const PaymentLoadingShadowBox = styled(ShadowBox)`
`;

export const PaymentError = styled(EmptyData).attrs(props => ({
  title: 'Ops!!',
  description: 'Aconteceu um problema, Tente novamente.',
  buttonText: 'Tentar novamente',
  onPress: props.refetch,
  imageSource: SystemImages.errorImage,
}))``;
