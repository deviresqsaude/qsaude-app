import React from 'react';
import { Clipboard, Linking, Share } from 'react-native';
import Config from 'react-native-config';
import PropTypes from 'prop-types';
import R from 'ramda';

import { DetailsCard, QButton, QText, EmptyData } from '../../components';
import { SystemImages } from '../../constants/theme';
import { Container, ContainerWrapper } from './PaymentStyle';

const validValue = value => {
  return value != undefined && value != null && value ? value : 'valor incorreto';
};

export const getStatus = R.cond([
  [R.equals('EmAberto'), R.always('Em aberto')],
  [R.equals('Pago'), R.always('Pago')],
  [R.equals('Vencido'), R.always('Vencido')],
  [R.T, R.always('Em aberto')],
]);

export const shareBoleto = async ({ boletoLink }) => {
  try {
    const response = await Share.share(
      {
        message: boletoLink,
        title: 'QSaude',
      },
      {
        excludedActivityTypes: [
          'com.apple.UIKit.activity.Print',
          'com.apple.UIKit.activity.MarkupAsPDF',
          'pinterest.ShareExtension',
        ],
      },
    );
    if (response.action === Share.sharedAction) {
      console.log(`Shared Action with: ${response.activityType}`);
    } else if (response.action === Share.dismissedAction) {
      console.log(`Dismissed: ${response.action}`);
    }
  } catch (e) {
    console.log(`Error: ${e}`);
  }
};

const copyBoleto = ({ boleto, onCopySuccess }) => {
  const boletoCodeWithoutSpace = boleto.numberCode.replace(/\s/g, '');
  Clipboard.setString(boletoCodeWithoutSpace);
  // Alert.alert('Código de barras copiado!');
  onCopySuccess({ boleto });
};

const PaymentScreen = ({
  boletos,
  latestBoleto,
  navigation: { navigate },
  onCopySuccess,
  profile,
}) => {
  const sharelinks = latestBoleto.error ? [] : [
    {label: 'Copiar código de barras', onPress: () => copyBoleto({ boleto: latestBoleto, onCopySuccess })},
    {label: 'Compartilhar 2ª via de boleto', onPress: () => shareBoleto({ boletoLink: latestBoleto.url })},
  ];

  const detailConfig = {
    title: latestBoleto.title,
    data: [
      { label: 'Valor', data: `R$ ${validValue(latestBoleto.titleAmount)}` },
      { label: 'Vencimento', data: `${validValue(latestBoleto.dueDate)}` },
      { label: 'Status', data: `${getStatus(latestBoleto.statusBankSlip)}` },
    ],
    links: [...sharelinks,
      {
      label: 'Falar com o Time Q',
      onPress: () =>
        Linking.openURL(
          `https://api.whatsapp.com/send?phone=${Config.WHATSAPP_LINKNUMBER}`,
        ),
    } ],
  };

  return (
    <Container showsVerticalScrollIndicator={false}>
      <ContainerWrapper>
        {boletos.length === 0 ? (
          <EmptyData
            renderImage
            imageSource={SystemImages.emptyBoleto}
            title="Nenhum boleto gerado!"
            description={`${
              profile.completeName.split(' ')[0]
            }, no momento, nenhum boleto foi gerado, aguarde até o dia vigente para realizar o seu pagamento ou fale com a gente.`}
            buttonText="Fale com o TimeQ"
            buttonType="link'"
            onPress={() =>
              Linking.openURL(
                `https://api.whatsapp.com/send?phone=${Config.WHATSAPP_LINKNUMBER}`,
              )
            }
          />
        ) : (
          <>
            <QText
              style={{ marginLeft: 16, marginRight: 16, marginBottom: 16 }}
              preset="body"
            >
              Aqui você visualiza seus boletos passados e futuros, verificando status e
              valores.
            </QText>
            <DetailsCard {...detailConfig} />
            <QButton
              style={{ padding: 16 }}
              text="Meus Boletos"
              onPress={() =>
                navigate('PaymentList', {
                  boletos,
                })
              }
            />
          </>
        )}
      </ContainerWrapper>
    </Container>
  );
};

PaymentScreen.propTypes = {};

PaymentScreen.defaultProps = {};

export default PaymentScreen;
