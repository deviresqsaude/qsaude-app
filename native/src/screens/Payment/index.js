import { compose, withProps, branch, renderComponent } from "recompose";
import { connect } from 'react-redux';
import { graphql } from '@apollo/react-hoc';
import R from 'ramda';
import moment from 'moment';

import PaymentsQuery from "../../service/query/PaymentsQuery";
import PaymentScreen from "./PaymentScreen";
import PaymentLoading from "./PaymentLoading";
import { PaymentError } from './PaymentStyle';
import { AlertActions } from '../../store/actions';
import { SystemImages, Colors } from '../../constants/theme';

const formatBankSlip = (boleto, error=false) => {
  const {competenceMonth, statusBankSlip} = boleto; 
  if(error) {
    return {
      title: 'Entrar em contato conosco',
      status: statusBankSlip,
      ...boleto,
      error: {
        message: 'invalid date',
        type: 'date'
      }
    }
  }
  const month = competenceMonth.split('-')[0].replace('0','');
  const newDate = moment().month(month-1);

  const formatBoleto = {
    title: newDate.format('MMMM'),
    status: statusBankSlip,
  }
  // if(competenceMonth === moment().format('MM-YYYY')) {
  //   latestBoleto = formatBoleto
  // }
  return formatBoleto;
};

const validDate = (date) => {
  const dateForm = /\d+\-\d+/
  return dateForm.test(date);
};

// Redux
const mapStateToProps = ({ auth }) => ({
  profile: auth.userProfile
});

const mapDispatchToProps = dispatch => ({
  onServiceFailed: ({ title, error: description }) =>
  dispatch(
    AlertActions.actions.openAlert({
      title,
      shouldRenderOkButton: true,
      closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
      description,
      isError: true,
    }),
  ),
  onCopySuccess: ({boleto}) =>
    dispatch(
      AlertActions.actions.openAlert({
        shouldRenderOkButton: true,
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        title: 'Código de barras copiado para sua Área de transferência',
        image: SystemImages.copySuccess,
        titleColor: Colors.Purple,
        description: boleto.numberCode,
        goBack: true,
      }),
    ),
});

// Data
const withData = graphql(PaymentsQuery, {options: { notifyOnNetworkStatusChange: true }});

const renderForLoading = branch(({data}) => data && data.loading, renderComponent(PaymentLoading));

// const setApolloRefetch = withProps(({data}) => ({refetch: data && data.refetch}));

const renderForError = branch(({ data }) => data && data.error, renderComponent(PaymentError));

const withPayments = withProps(
  ({
    onServiceFailed,
    navigation: { goBack },
    data : { getBankSlip, error },
    profile
  }) => {
    if (error) {
      onServiceFailed({
        title: 'Erro ao trazer os dados do pagamentos',
        error: error.message,
      });
      goBack();
    }

    if(!getBankSlip || getBankSlip && getBankSlip.length === 0 ) {
      return {
        boletos: [],
        latestBoleto: {}
      }
    }
    // let latestBoleto = null;
    getBankSlip.reverse();
    const boletos = getBankSlip.map(boleto => {
      const {competenceMonth} = boleto;
      // This month's boleto
      const formatBoleto = validDate(competenceMonth) ? formatBankSlip(boleto) : formatBankSlip(boleto, true);

      // if(validDate(competenceMonth)) {
      //   // Format according to error in moment.js
      //   const month = competenceMonth.split('-')[0].replace('0','');
      //   const newDate = moment().month(month-1);

      //   const formatBoleto = {
      //     title: newDate.format('MMMM'),
      //     status: statusBankSlip,
      //     ...boleto
      //   }
      //   if(competenceMonth === moment().format('MM-YYYY')) {
      //     latestBoleto = formatBoleto
      //   }
      //   return formatBoleto;
      // }
      // if(!validDate(competenceMonth)) {
      //   return {
      //     title: 'Entrar em contato conosco',
      //     status: boleto.statusBankSlip,
      //     ...boleto,
      //     error: {
      //       message: 'invalid date',
      //       type: 'date'
      //     }
      //   }
      // }
      return {
        ...formatBoleto,
        ...boleto
      };
    });

    const latestBoleto = R.find(({competenceMonth}) => competenceMonth === moment().format('MM-YYYY'), boletos) || boletos[0];

    return {
      boletos,
      latestBoleto
    }
  }
)

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  // Apollo
  withData,
  renderForLoading,
  renderForError,
  // setApolloRefetch,
  // Handler
  withPayments
);

export default enhance(PaymentScreen);
