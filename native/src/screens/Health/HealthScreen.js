// import React, { useRef } from 'react';
import React from 'react';
import PropTypes from 'prop-types';
import SyncStorage from 'sync-storage';
import { RefreshControl, ScrollView } from 'react-native';
import { Container } from './HealthStyle';
import { ProfileInfo } from '../../components/Profile';
import { MenuList } from '../../components/MenuList';
import { HealthData } from '../../components';
import { HEALTH_MENU } from '../../constants/mock';

const HealthScreen = props => {
  const {legalAge} = SyncStorage.get('profileUser');
  const {
    data: { loading, refetch },
    data,
  } = props;
  return (
    <Container>
      <ProfileInfo/>
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={<RefreshControl onRefresh={refetch} refreshing={loading} />}>
        <HealthData data={data} loading={loading}/>
        <MenuList menuData={HEALTH_MENU.filter((item, key) => (item.legalAge && legalAge) || !item.legalAge)}/>
      </ScrollView>
    </Container>
  );
};

HealthScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

HealthScreen.defaultProps = {};

export default HealthScreen;
