import { compose, withProps, branch, renderComponent, lifecycle } from 'recompose';
import { connect } from 'react-redux';
import { graphql } from '@apollo/react-hoc';
import HealthScreen from './HealthScreen';
import { HealthLoading } from './HealthStyle';
import { AlertActions, NavActions} from '../../store/actions';
import { RecentVitalSigns } from '../../service/query';

const withData = graphql(RecentVitalSigns, {
  options: ({onServiceFailed}) => ({
    notifyOnNetworkStatusChange: true,
    onError: e => onServiceFailed({ title: 'Erro ao trazer dados vitais', error: e.message }),
  }),
});
// Error Handling
const mapDispatchToProps = dispatch => ({
  setNavCurrentPage: state => dispatch(NavActions.actions.setCurrentPage(state)),
  onServiceFailed: ({ title, error: description }) =>
    dispatch(
      AlertActions.actions.openAlert({
        title,
        shouldRenderOkButton: true,
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        description,
        isError: true,
      }),
    ),
  onServiceSuccess: () =>
    dispatch(
      AlertActions.actions.openAlert({
        description: null,
        shouldRenderOkButton: true,
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        title: 'Medida inserida com sucesso!',
        isSuccess: true,
      }),
    ),
});

const withLoading = branch(({ data }) => data && data.loading && data.networkStatus !== 4, renderComponent(HealthLoading));

const withRefetch = withProps(({ data }) => ({refetch: data && data.refetch}));

const life = lifecycle({
  componentDidMount() {
    const {navigation, setNavCurrentPage} = this.props;
    setNavCurrentPage(navigation.state);
  },
});

const enhance = compose(
  connect(
    null,
    mapDispatchToProps,
  ),
  withData,
  withRefetch,
  withLoading,
  withProps(({ navigation: { state: { routeName } } }) => ({
    title: routeName,
  })),
  life
);

export default enhance(HealthScreen);
