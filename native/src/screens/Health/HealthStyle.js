import React from 'react';
import { Placeholder, Fade, PlaceholderLine, PlaceholderMedia } from 'rn-placeholder';
import styled from 'styled-components/native';

const EmptyContent = styled.View({});

export const Container = styled.View`
  flex: 1;
`;

export const HealthLoading = () => (
  <EmptyContent>
    <Placeholder
      Animation={Fade}
      Left={PlaceholderMedia}
      style={{paddingHorizontal: 16}}>
      <PlaceholderLine width={100} />
      <PlaceholderLine width={60} />
    </Placeholder>
    <Placeholder
      style={{marginTop: 10, paddingHorizontal: 16}}
      Left={() => <PlaceholderLine round width={48} height={130} style={{borderRadius: 4, marginBottom: 10}}/>}
      Right={() => <PlaceholderLine round width={48} height={130} style={{borderRadius: 4, marginBottom: 10}}/>}
    />
    <Placeholder Animation={Fade} style={{marginTop: 10, paddingHorizontal: 16}}>
      <PlaceholderLine round width={100} height={50} style={{borderRadius: 0, marginBottom: 10}}/>
      <PlaceholderLine round width={100} height={50} style={{borderRadius: 0, marginBottom: 10}}/>
      <PlaceholderLine round width={100} height={50} style={{borderRadius: 0, marginBottom: 10}}/>
      <PlaceholderLine round width={100} height={50} style={{borderRadius: 0, marginBottom: 10}}/>
      <PlaceholderLine round width={100} height={50} style={{borderRadius: 0, marginBottom: 10}}/>
    </Placeholder>
  </EmptyContent>
);
