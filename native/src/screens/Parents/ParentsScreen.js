import React from "react";
import PropTypes from 'prop-types';
import { Container } from './ParentsStyle';
import { SimpleList } from "../../components/List";

const ParentsScreen = ({dependents}) => {
  return(
    <Container showsVerticalScrollIndicator={false} >
      {
        dependents && dependents.length > 0 ?
        <SimpleList data={dependents}/> :
        null
      }
    </Container>
  )
}

ParentsScreen.propTypes = {
  dependents: PropTypes.arrayOf(PropTypes.any)
}

ParentsScreen.defaultProps = {
  dependents: []
}

export default ParentsScreen;
