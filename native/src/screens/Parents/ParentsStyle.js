import styled from "styled-components/native";

export const Container = styled.ScrollView`
  flex-direction: column;
  flex: 1;
  paddingVertical: 16
`;
