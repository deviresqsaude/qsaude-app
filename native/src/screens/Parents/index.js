import { compose, withHandlers,withProps } from "recompose";
import R from 'ramda';
import { withApollo } from '@apollo/react-hoc';
import { connect } from 'react-redux';
import SyncStorage from 'sync-storage';
import moment from 'moment';
import ParentsScreen from "./ParentsScreen";
import { ChangeUser } from '../../service/mutations';
import { AlertActions, LoadingActions } from '../../store/actions';

const ERROR_MESSAGE = {
  title: "Aconteceu um erro",
  description: "Aconteceu um erro na hora de trocar de usuario, tente novamente ou entre em contato com o Time Q",
  shouldRenderImage: true,
  isError: true,
};

// Error Handling
const mapDispatchToProps = dispatch => ({
  showLoading: options => dispatch(LoadingActions.actions.openLoading({...options})),
  closeLoading: () => dispatch(LoadingActions.actions.closeLoading()),
  showAlert: options => dispatch(AlertActions.actions.openAlert({...options, shouldRenderOkButton: true, closeAlert: () =>  dispatch(AlertActions.actions.closeAlert())}))
});

const parentsHandlers = withHandlers({
  changeUser: ({client, navigation, showAlert, closeLoading, showLoading}) => async parent => {
    showLoading({description: "Estamos trocando de usuario, por favor aguarde", visible: true});
    const currentProfile = SyncStorage.get("profileUser");
    const currentToken = SyncStorage.get("token");
    // SyncStorage.set('profileUser', {...parent});
    // SyncStorage.set('dependents', parent.owner ? [...parent.dependents] : [currentProfile]);
    if(parent.owner) {
      const prevToken = SyncStorage.get("prevToken");
      SyncStorage.set('token', prevToken);
      SyncStorage.set('profileUser', {...parent});
      SyncStorage.set('dependents', [...parent.dependents]);
      SyncStorage.remove('prevToken');
      await client.resetStore();
      closeLoading();
      navigation.goBack();
      return false;
    }
    try {
      const {errors, data} = await client.mutate({
        mutation: ChangeUser,
        variables: {numCard: parent.numCard}
      });
      if (errors) {
        closeLoading();
        showAlert(ERROR_MESSAGE);
        return false;
      };
      const {changeUser} = data;
      SyncStorage.set('prevToken', currentToken);
      SyncStorage.set('token', changeUser.token);
      SyncStorage.set('profileUser', {...parent});
      SyncStorage.set('dependents', [currentProfile]);
      await client.resetStore();
      closeLoading();
      navigation.goBack();
      return data;
    } catch (e) {
      showAlert(ERROR_MESSAGE)
      return e;
    }
  }
});

const parentsProps = withProps(({ navigation: { state: { routeName, params } }, changeUser }) => {
  let dependents = [];
  R.forEach(d => {
    const age = moment().diff(d.birthday, 'years');
    const refactorDependent = {...d, title: d.completeName, subtitle: d.type == 'T' ? `Titular - ${age} ano(s)` : `Dependente - ${age} ano(s)`};
    dependents = R.append({
      ...refactorDependent,
      onPress: () => changeUser(refactorDependent)
    }, dependents);
  }, params.dependents);
  return {
    title: routeName,
    dependents
  }
})

const enhance = compose(
  connect(
    null,
    mapDispatchToProps,
  ),
  withApollo,
  parentsHandlers,
  parentsProps
);

export default enhance(ParentsScreen);
