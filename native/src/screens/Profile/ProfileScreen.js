import React from 'react';
import R from 'ramda';
import { TouchableWithoutFeedback, Keyboard, Platform } from 'react-native';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import { Container, FormSafeAreaView, FormContainer } from './ProfileStyle';
import { QTextInput, QButton, QGeolocation } from '../../components';
import QAlert from '../../components/QAlert';

const MESSAGE = {
  error: "Erro ao alterar dados!",
  success: "Dados alterados com sucesso!"
}

const ProfileScreen = props => {
  const {
    formFields,
    isSubmitting,
    isValid,
    handleSubmit,
    requestAlert,
    setRequestAlert,
    navigation: {goBack}
  } = props;
  return (
    <>
      <FormSafeAreaView>
        <Container behavior={Platform.OS === "ios" ? "padding" : null}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <FormContainer showsVerticalScrollIndicator={false}>
              {
                R.map(item => (
                  <Field
                    type={item.mask ? "custom" : "default"}
                    inputMaskType={item.inputMask}
                    mask={item.mask}
                    options={item.options}
                    key={item.id}
                    label={item.label}
                    testID={item.id}
                    name={item.name}
                    keyboardType={R.includes(item.type, ["date", "select"]) ? "default" : item.type}
                    disabled={item.disabled || isSubmitting}
                    dependent={item.dependent}
                    component={R.cond([
                      [R.equals("geolocation"), () => QGeolocation],
                      [R.T, () => QTextInput]
                    ])(item.type)}/>
                ), formFields)
              }
              <QButton
                testID="sendNewPassword"
                text="Alterar Dados"
                loading={isSubmitting}
                disabled={!isValid || isSubmitting}
                inBlock
                onPress={() => handleSubmit()}
                margin={0}
              />
            </FormContainer>
          </TouchableWithoutFeedback>
        </Container>
      </FormSafeAreaView>
      {/* <QAlert
        visible={requestAlert !== null}
        isSuccess={requestAlert === 'success'}
        isError={requestAlert === 'error'}
        description={MESSAGE[requestAlert === 'error' ? 'error' : 'success']}
        closeAlert={() => requestAlert === 'success' ? goBack() : setRequestAlert(null)}
      /> */}
    </>
  )
}

ProfileScreen.propTypes = {
  formFields: PropTypes.arrayOf(PropTypes.any)
}

ProfileScreen.defaultProps = {
  formFields: []
}

export default ProfileScreen
