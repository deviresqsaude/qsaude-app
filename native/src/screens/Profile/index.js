import { withState, compose, lifecycle } from "recompose";
import { withFormik } from 'formik';
import R from 'ramda';
import { graphql } from '@apollo/react-hoc';
import { connect } from 'react-redux';
import * as yup from 'yup';
import moment from "moment";
import SyncStorage from 'sync-storage';
import ProfileScreen from "./ProfileScreen";
import { UpdateProfile } from "../../service/mutations";
import { AlertActions } from '../../store/actions';

// Error Handling
const mapDispatchToProps = dispatch => ({
  showAlert: options => dispatch(AlertActions.actions.openAlert({...options, shouldRenderOkButton: true, closeAlert: () => {options.close(); dispatch(AlertActions.actions.closeAlert())}})),
  onServiceFailed: ({ title, error: description }) =>
    dispatch(
      AlertActions.actions.openAlert({
        title,
        shouldRenderOkButton: true,
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        description,
        isError: true,
      }),
    ),
  onServiceSuccess: () =>
    dispatch(
      AlertActions.actions.openAlert({
        description: null,
        shouldRenderOkButton: true,
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        title: 'Alteração efetuada com sucesso!',
        isSuccess: true,
      }),
    ),
});

const FORM_FIELDS = [
  {label: "E-mail", id: "fieldEmail", name: "email", type: "email-address"},
  {label: "Telefone celular", id: "fieldPhoneNumber", name: "cellPhoneNumber", type: "phone-pad", mask: "(99) 99999-9999"},
  {label: "CEP", id: "fieldZipCode", name: "zipCode", type: "default"},
  // {label: "CEP", id: "fieldZipCode", name: "zipCode", type: "geolocation", dependent: ["address", "neighborhood", "state", "city"]},
  {label: "Rua", id: "fieldAddress", name: "address", type: "default"},
  {label: "Número", id: "fieldAddressNumber", name: "addressNumber", type: "number-pad"},
  {label: "Complemento (opcional)", id: "addressComplement", name: "addressComplement", type: "default"},
  {label: "Bairro", id: "fieldNeighborhood", name: "neighborhood", type: "default", depend: "zipCode"},
  {label: "Estado", id: "fieldState", name: "state", type: "default", depend: "zipCode"},
  {label: "Cidade", id: "fieldCity", name: "city", type: "default", depend: "zipCode"},
  {label: "Nome", id: "fieldName", name: "completeName", type: "default", disabled: true},
  {label: "CPF", id: "fieldCpf", name: "cpf", type: "default", disabled: true},
  {label: "ID da carterinha", id: "fieldMedicalID", name: "numCard", type: "default", disabled: true},
  {label: "Data de nascimento", id: "fieldBirthday", name: "birthday", type: "date", disabled: true},
];

const unMaskPhone = phone => {
  return R.replace(/ /g, '', R.replace(/\)/g, '', R.replace(/\(/g, '', R.replace(/-/g, '', phone))))
};

const formatPhone = (dd, phone) => {
  return `(${dd}) ${phone.slice(0, 5)}-${phone.slice(5)}`
};

const PasswordScheme = yup.object().shape({
  email: yup.string().email(),
  cellPhoneNumber: yup
    .string()
    .matches(/\(\d{2,}\) \d{5,}-\d{4}/, {message: "O número de celular não é valido"}),
  zipCode: yup
    .number()
    .positive("O CEP não pode ser negativo")
    .min(8, "O CEP deve ter 8 números"),
  address: yup.string(),
  addressNumber: yup.number(),
  addressComplement: yup.string(),
  neighborhood: yup.string(),
  state: yup.string(),
  city: yup.string(),
});

const withForm = withFormik({
  validationSchema: PasswordScheme,
  mapPropsToValues: ({ formFields }) => {
    // const b = R.find(R.propEq('numCard', SyncStorage.get('healthCard')))(beneficiary);
    // if (!b) return {};
    const b = SyncStorage.get("profileUser");
    let formProps = {};
    R.forEach(item => {
      // eslint-disable-next-line no-nested-ternary
      formProps = R.assoc(item.name, item.type === "date" ? moment(b[item.name]).format("DD-MM-YYYY") : item.name === "cellPhoneNumber" ? formatPhone(b["cellPhoneDDD"], b["cellPhoneNumber"]) : b[item.name] || "", formProps);
    }, formFields);
    return formProps;
  },
  handleSubmit: async (values, formikBag) => {
    const {props:{ updateBeneficiary, setRequestAlert, onServiceSuccess, onServiceFailed, navigation: { goBack } }, setSubmitting } = formikBag;
    const phoneUnmasked = unMaskPhone(values.cellPhoneNumber);
    const variables = {
      cpf: values.cpf,
      email: values.email,
      cellPhoneDDD: phoneUnmasked.slice(0, 2),
      cellPhoneNumber: phoneUnmasked.slice(2),
      zipCode: values.zipCode,
      address: values.address,
      addressNumber: values.addressNumber,
      addressComplement: values.addressComplement,
      neighborhood: values.neighborhood,
      city: values.city,
      state: values.state
    }

    try {
      const { data } = await updateBeneficiary({variables});
      if(data) {
        // update profile in async storage
        const profileUser = SyncStorage.get("profileUser");
        const newProfile = R.merge(profileUser, variables);
        SyncStorage.set("profileUser", newProfile);
        onServiceSuccess();
        setSubmitting(false);
        goBack();
        return ;
      }
      // onServiceFailed({ title: "erro ao atualizar seus dados", error : ''});
      // setRequestAlert(!data.updateBeneficiary ? 'error' : 'success');
    } catch (e) { 
      console.log(e);
      // console.error(e); 
      setSubmitting(false);
      onServiceFailed({ title: "erro ao atualizar seus dados", error: e});
      return ;
    }
    setSubmitting(false);
  },
  displayName: 'ProfileForm',
});

const withMutation = graphql(UpdateProfile, {name: "updateBeneficiary"});

const withLifeCycle = lifecycle({
  componentWillMount() {
    const {showAlert, navigation} = this.props;
    const profileUser = SyncStorage.get("profileUser");
    if(!profileUser.legalAge) showAlert({
      isError: true,
      description: "Atenção: seu usuário não tem permissão para alterar seus dados.",
      close: () => navigation.goBack()
    })
  }
});

const enhance = compose(
  connect(
    null,
    mapDispatchToProps
  ),
  withMutation,
  withState('requestAlert', 'setRequestAlert', null),
  withState('messageError', 'setMessageError', ''),
  withState('formFields', 'setFormFields', FORM_FIELDS),
  withForm,
  withLifeCycle
);

export default enhance(ProfileScreen);
