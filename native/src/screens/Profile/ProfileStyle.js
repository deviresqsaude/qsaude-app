import styled from 'styled-components/native';
import { EmptyData } from '../../components';
import { SystemImages } from '../../constants/theme';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';

export const Container= styled(KeyboardAwareScrollView).attrs({
  padding: 16,
  paddingTop: 0,
  paddingBottom: 40,
  flex: 1
})``;

export const FormSafeAreaView = styled.SafeAreaView({
  flex: 1,
})

export const FormContainer = styled.ScrollView({
  flex: 1,
})

export const FormProfile = styled.View({
  backgroundColor: "green",
})

export const ProfileLoadingWrapper = styled.View`
  width: 100%;
  padding-left: 16;
  padding-right: 16;
`;

export const ProfileError = styled(EmptyData).attrs(props => ({
  title: 'Ops!!',
  description: 'Aconteceu um problema. Tente novamente.',
  buttonText: 'Tentar novamente',
  onPress: props.refetch,
  imageSource: SystemImages.errorImage,
}))``;
