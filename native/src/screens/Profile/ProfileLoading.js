import React from 'react';
import { Placeholder, PlaceholderLine, Fade } from 'rn-placeholder';
import { ProfileLoadingWrapper } from './ProfileStyle';

const QuestLoading = () => {
  return (
    <ProfileLoadingWrapper>
      <Placeholder Animation={Fade}>
        <PlaceholderLine height={12} width={40} style={{marginBottom: 8, borderRadius: 2}}/>
        <PlaceholderLine height={56} style={{marginBottom: 18, borderRadius: 2}}/>
        <PlaceholderLine height={12} width={40} style={{marginBottom: 8, borderRadius: 2}}/>
        <PlaceholderLine height={56} style={{marginBottom: 18, borderRadius: 2}}/>
        <PlaceholderLine height={12} width={40} style={{marginBottom: 8, borderRadius: 2}}/>
        <PlaceholderLine height={56} style={{marginBottom: 18, borderRadius: 2}}/>
        <PlaceholderLine height={12} width={40} style={{marginBottom: 8, borderRadius: 2}}/>
        <PlaceholderLine height={56} style={{marginBottom: 18, borderRadius: 2}}/>
        <PlaceholderLine height={12} width={40} style={{marginBottom: 8, borderRadius: 2}}/>
        <PlaceholderLine height={56} style={{marginBottom: 18, borderRadius: 2}}/>
        <PlaceholderLine height={12} width={40} style={{marginBottom: 8, borderRadius: 2}}/>
        <PlaceholderLine height={56} style={{marginBottom: 18, borderRadius: 2}}/>
        <PlaceholderLine height={12} width={40} style={{marginBottom: 8, borderRadius: 2}}/>
        <PlaceholderLine height={56} style={{marginBottom: 18, borderRadius: 2}}/>
        <PlaceholderLine height={12} width={40} style={{marginBottom: 8, borderRadius: 2}}/>
      </Placeholder>
    </ProfileLoadingWrapper>
  )
}

export default QuestLoading;
