import React from 'react';
// import PropTypes from 'prop-types';
import R from 'ramda';
import { Container, RecordItem } from './MedicalRecordStyle';

const MedicalRecordScreen = ({ medicalRecords, data: { loading } }) => {
  return (
    <Container
      showsVerticalScrollIndicator={false}>
      {R.map(
        item => (
          <RecordItem loading={loading} {...item} key={item.label} />
        ),
        medicalRecords,
      )}
    </Container>
  );
};

MedicalRecordScreen.propTypes = {};

export default MedicalRecordScreen;
