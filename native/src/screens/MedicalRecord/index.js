import { compose, withProps, renderComponent, branch } from 'recompose';
import { graphql } from '@apollo/react-hoc';
import { connect } from 'react-redux';
import SyncStorage from 'sync-storage';
import R from 'ramda';
import moment from 'moment';
import MedicalRecordScreen from './MedicalRecordScreen';
import { MedicalRecord } from '../../service/query';
import { AlertActions } from '../../store/actions';
import { MedicalRecordLoading } from './MedicalRecordStyle';

// Redux Handlers
const mapDispatchToProps = dispatch => ({
  onServiceFailed: ({ title, error: description }) =>
    dispatch(
      AlertActions.actions.openAlert({
        title,
        shouldRenderOkButton: true,
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        description,
        isError: true,
      }),
    ),
});

// Redux State
const mapStateToProps = ({ auth }) => ({
  // authState: auth,
});

const withData = graphql(MedicalRecord, {});

// const renderForLoading = branch(({ data, authState: {userProfile}}) => (data && data.loading) || !userProfile, renderComponent(MedicalRecordLoading));
const renderForLoading = branch(({ data }) => data && data.loading, renderComponent(MedicalRecordLoading));

const parseData = R.map(({ display, relationship }) =>
  relationship ? `${display} (${relationship})` : display,
);

const withMedicalRecordsList = withProps(
  ({
    onServiceFailed,
    navigation: {goBack},
    data
  }) => {
    const { procedures, allergies, conditions, familyMemberHistories, error } = data;
    const {completeName, birthday} = SyncStorage.get("profileUser");
    if (error) {
      onServiceFailed({
        title: 'Erro ao trazer os dados do paciente',
        error: error.message,
      });
      goBack();
    }
    return {
      medicalRecords: [
        {
          label: completeName || '---',
          data: `${moment().diff(birthday, 'years')} anos` || '---',
          testID: 'BOTTOMTABBAR.tabHealth.itemMedicalRecord.name',
        },
        {
          label: 'Alergias',
          data: allergies && allergies.length ? parseData(allergies) : 'Não há alergias',
          testID: 'BOTTOMTABBAR.tabHealth.itemMedicalRecord.allergies',
        },
        // {
        //   label: 'Vacinas',
        //   data: 'Não há registros',
        //   testID: 'BOTTOMTABBAR.tabHealth.itemMedicalRecord.vaccines',
        // },
        {
          label: 'Cirugias',
          data: procedures && procedures.length ? parseData(procedures) : 'Não há cirugias',
          testID: 'BOTTOMTABBAR.tabHealth.itemMedicalRecord.procedures',
        },
        {
          label: 'Doenças',
          data: conditions && conditions.length ? parseData(conditions) : 'Não há doenças',
          testID: 'BOTTOMTABBAR.tabHealth.itemMedicalRecord.illnesses',
        },
        {
          label: 'Histórico Familiar',
          data: familyMemberHistories && familyMemberHistories.length
            ? parseData(familyMemberHistories)
            : 'Não há um histórico familiar',
          testID: 'BOTTOMTABBAR.tabHealth.itemMedicalRecord.medicalHistory',
        },
      ],
    };
  },
);

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withData,
  renderForLoading,
  withMedicalRecordsList,
);

export default enhance(MedicalRecordScreen);
