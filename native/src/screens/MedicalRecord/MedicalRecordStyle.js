/* eslint-disable no-nested-ternary */
/* eslint-disable react/prop-types */
import React from 'react';
import { View } from 'react-native';
import styled from 'styled-components/native';
import { Placeholder, Fade, PlaceholderLine } from 'rn-placeholder';
import { QText } from '../../components';
import { Colors } from '../../constants/theme';
import { arrayGenerator } from '../../helpers';

export const Container = styled.ScrollView.attrs({
  contentContainerStyle: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 56,
  },
})``;

const BulletWrapper = styled.View({
  // backgroundColor: '#e5e5e5',
  backgroundColor: Colors.Purple,
  borderRadius: 20,
  paddingLeft: 12,
  paddingBottom: 6,
  paddingTop: 6,
  paddingRight: 12,
  marginRight: 4,
  marginTop: 4,
});

const Bullet = ({ display }) => (
  <BulletWrapper>
    <QText color="#fff" fontSize={14} preset="dataLabel">
      {display}
    </QText>
  </BulletWrapper>
);

const Row = styled.View({
  flexDirection: 'row',
  alignItems: 'center',
  flexWrap: 'wrap',
});

export const RecordItem = ({ testID, label, data, first, loading }) => {
  return (
    <>
      <QText
        testID={testID}
        margin={{ bottom: 4, top: first ? 0 : 16 }}
        preset="dataLabel"
      >
        {label}
      </QText>
      {loading ? (
        <Placeholder Animation={Fade}>
          <PlaceholderLine style={{ height: 32 }} />
        </Placeholder>
      ) : typeof data === 'object' && data.length > 0 ? (
        <Row>
          {data.map(item => (
            <Bullet testID={testID} display={item} key={`${item}`} />
          ))}
        </Row>
      ) : (
        <QText preset="dataText">{String(data)}</QText>
      )}
    </>
  );
};

export const MedicalRecordLoading = () => {
  return (
    <View style={{marginTop: 15}}>
      {
        arrayGenerator(6).map((item) => {
          return (
            <Placeholder key={item} Animation={Fade} style={{paddingHorizontal: 16, paddingVertical: 4}}>
              <PlaceholderLine width={60} />
              <PlaceholderLine width={100} />
            </Placeholder>
          )
        })
      }
    </View>
  )
};
