import { Dimensions } from 'react-native';
import styled from 'styled-components/native';
import { EmptyData } from '../../components';
import { SystemImages } from '../../constants/theme';
const { width } = Dimensions.get('window');

export const Container = styled.View({
  flex: 1,
  alignItems: "center",
  justifyContent: "center",
  padding: 12
});

export const HealthCardWrapper = styled.View({
  width: parseFloat(width) - 32,
  height: 218,
  borderRadius: 15,
  background: "#FFFFFF",
  // overflow: "hidden",
  position: "relative",
  backfaceVisibility: "hidden",
  alignSelf: 'center'
});

export const HealthCardBody = styled.View({
  width: "100%",
  height: "100%",
  paddingHorizontal: 19,
  paddingVertical: 22,
  flexDirection: "column",
  justifyContent: "space-between"
});

export const HealthCardLogo = styled.Image.attrs({
  source: SystemImages.qsaudeMini
})``;

export const CardLoadingWrapper = styled.View`
  left: 0;
  width: 94%;
  height: 40%;
  padding-left: 16;
  padding-right: 16;
  flex: 1;
  align-items: center;
  justify-content: center;
  align-self: center;
`;

export const CardError = styled(EmptyData).attrs(props => ({
  title: 'Ops!!',
  description: 'Aconteceu um problema. Tente novamente, por favor.',
  buttonText: 'Tentar novamente',
  onPress: props.refetch,
  imageSource: SystemImages.errorImage,
}))``;
