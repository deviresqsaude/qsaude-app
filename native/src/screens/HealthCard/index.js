import { compose, withHandlers, withProps, branch, renderComponent } from 'recompose';
import { graphql } from '@apollo/react-hoc';
import { PlanCardQuery } from "../../service/query";
import HealthCard from './HealthCard';
import HealthCardLoading from './HealthCardLoading';
import { CardError } from './HealthCardStyle'

const withData = graphql(PlanCardQuery, {options: { notifyOnNetworkStatusChange: true }});

const renderForLoading = branch(({data}) => data && data.loading, renderComponent(HealthCardLoading));

const setApolloRefetch = withProps(({data}) => ({refetch: data && data.refetch}));

const renderForError = branch(({ data }) => data && data.error, renderComponent(CardError));

const enhance = compose(
  withData,
  renderForLoading,
  setApolloRefetch,
  renderForError,
  withHandlers({})
);

export default enhance(HealthCard);
