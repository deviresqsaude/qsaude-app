/* eslint-disable react/no-this-in-sfc */
/* eslint-disable no-return-assign */
import React from 'react';
import moment from 'moment';
import { View, TouchableWithoutFeedback } from 'react-native';
import CardFlip from 'react-native-card-flip';
import LinearGradient from 'react-native-linear-gradient';
import Config from 'react-native-config';
import R from 'ramda';
import { Container, HealthCardWrapper, HealthCardBody, HealthCardLogo } from './HealthCardStyle'
import QText from '../../components/QText';
import { Colors } from '../../constants/theme';
import { elevationShadow } from '../../helpers';
import {PROFILEUSER} from '../../constants/TestIds'

const site = "www.ans.gov.br";
const flipCard = "Virar carteirinha";
const lackTypes = type => R.cond([
  [R.test(/2/), () => "Consulta"],
  [R.test(/3/), () => "Exame/Terapia"],
  [R.test(/4/), () => "Internação"],
  [R.test(/5/), () => "Parto"],
  [R.T, ()=> "CPT"]
])(type);

const HealthCard = props => {
  const {
    data: {
      getHealthPlanCard : {
        accommodation,
        healthPlanCode,
        birthday,
        clientName,
        cns,
        contract,
        contractor,
        lack=[],
        registration,
        segmentPlan,
        startingDate,
        typeContract,
        coverage,
      }
    }
  } = props;

  return (
    <Container>
      <CardFlip
        style={{width: '100%', height: 218}}
        duration={400}
        perspective={1000}
        ref={card => this.card = card}>
        <HealthCardWrapper
          style={{...elevationShadow("#000000", 8, .5)}}>
          <HealthCardBody>
            <View style={{height: 40, flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
              <HealthCardLogo/>
              <QText testID={PROFILEUSER.checkVcard.registration} color={Colors.Purple} fontSize={14} preset="bold">{registration || '---'}</QText>
            </View>
            <View>
              <View style={{marginBottom: 4, width: '100%'}}>
                <QText testID={PROFILEUSER.checkVcard.clientName} color={Colors.Purple} fontSize={12} preset="bold">{clientName || '---'}</QText>
              </View>
              <View style={{flexDirection: "row", alignItems: "flex-start"}}>
                <View style={{flexDirection: "column", flex: 1}}>
                  <View>
                    <QText color={Colors.Gray} fontSize={10} preset="bold">Meu plano</QText>
                    <QText testID={PROFILEUSER.checkVcard.healthPlanCode}  color={Colors.Purple} fontSize={12} preset="bold">{healthPlanCode || '---'}</QText>
                  </View>
                  <View>
                    <QText color={Colors.Gray} fontSize={12} preset="bold">Inicio vigência</QText>
                    <QText testID={PROFILEUSER.checkVcard.startingDate}  color={Colors.Purple} fontSize={12} preset="bold">{(startingDate ? moment(startingDate).format("DD/MM/YY") : '---')}</QText>
                  </View>
                </View>
                <View style={{flexDirection: "column", flex: 1}}>
                  <View>
                    <QText color={Colors.Gray} fontSize={10} preset="bold">Segmentação</QText>
                    <QText testID={PROFILEUSER.checkVcard.segmentPlan}  color={Colors.Purple} fontSize={12} preset="bold">{segmentPlan || '---'}</QText>
                  </View>
                  <View/>
                </View>
              </View>
            </View>
          </HealthCardBody>
        </HealthCardWrapper>
        <HealthCardWrapper
          style={{...elevationShadow("#000000", 5, .5)}}>
          <LinearGradient
            colors={[Colors.Pink, Colors.Purple]}
            start={{ x: 0, y:0 }}
            end={{ x: 1, y: 0 }}
            style={{
              height: "100%",
              width: "100%",
              borderRadius: 15
            }}>
            <HealthCardBody>
              <View style={{flex: 1, flexDirection: "column", justifyContent: "space-between", position: "relative"}}>
                <View style={{flexDirection: "row", justifyContent: "space-between"}}>
                  <View style={{flexDirection: "column", justifyContent: "flex-start", flexGrow: 1}}>
                    <View style={{marginBottom: 3}}>
                      <QText color={Colors.White} fontSize={10}>Nascimento</QText>
                      <QText  testID={PROFILEUSER.checkVcard.birthday} color={Colors.White} fontSize={11} preset="bold">{birthday ? moment(birthday).format("DD/MM/YY") : '---'}</QText>
                    </View>
                    <View style={{marginBottom: 3}}>
                      <QText color={Colors.White} fontSize={10}>Contratação</QText>
                      <QText testID={PROFILEUSER.checkVcard.typeContract} color={Colors.White} fontSize={11} preset="bold">{typeContract || '---'}</QText>
                    </View>
                    <View>
                      <QText color={Colors.White} fontSize={10}>Abrangência</QText>
                      <QText testID={PROFILEUSER.checkVcard.coverage} color={Colors.White} fontSize={11} preset="bold">{coverage || '---'}</QText>
                    </View>
                  </View>
                  <View style={{flexDirection: "column", justifyContent: "flex-start", flexGrow: 1}}>
                    <View style={{marginBottom: 3}}>
                      <QText color={Colors.White} fontSize={10}>Contrato</QText>
                      <QText testID={PROFILEUSER.checkVcard.contract} color={Colors.White} fontSize={11} preset="bold">{contract || '---'}</QText>
                    </View>
                    <View style={{marginBottom: 3}}>
                      <QText color={Colors.White} fontSize={10}>CNS</QText>
                      <QText testID={PROFILEUSER.checkVcard.cns} color={Colors.White} fontSize={11} preset="bold">{cns || '---'}</QText>
                    </View>
                    <View style={{marginBottom: 3}}>
                      <QText color={Colors.White} fontSize={10}>Contratante</QText>
                      <QText testID={PROFILEUSER.checkVcard.contractor} color={Colors.White} fontSize={11} preset="bold">{contractor || '---'}</QText>
                    </View>
                  </View>
                  <View style={{flexDirection: "column", justifyContent: "center", flexGrow: 1}}>
                    <View style={{marginBottom: 3}}>
                      <QText color={Colors.White} fontSize={10}>Acomodação</QText>
                      <QText testID={PROFILEUSER.checkVcard.accommodation} color={Colors.White} fontSize={11} preset="bold">{accommodation || '---'}</QText>
                    </View>
                  </View>
                </View>
                <View style={{marginBottom: 8}}>
                  <QText color={Colors.White} fontSize={11} preset="bold">Carências</QText>
                  <View style={{flexDirection: "row", justifyContent: "space-between"}}>
                    {
                      lack && lack.slice(1).map((l, i) => (
                      // eslint-disable-next-line react/no-array-index-key
                      <View key={`lack-${i}`} style={{marginRight: 6}}>
                        <QText color={Colors.White} fontSize={10}>{lackTypes(l.lackId)}</QText>
                        <QText color={Colors.White} fontSize={11} preset="bold">{l.lackFinalDate ? moment(l.lackFinalDate).format("DD/MM/YY") : '---'}</QText>
                      </View>))
                    }
                  </View>
                </View>
                <QText style={{marginBottom: 10}} color={Colors.White} fontSize={11} preset="bold">{Config.WHATSAPP_LINKNUMBER} Whatsapp</QText>
                <QText style={{position: "absolute", right: 0, bottom: -7}} color={Colors.White} fontSize={9}>{site}</QText>
              </View>
            </HealthCardBody>
          </LinearGradient>
        </HealthCardWrapper>
      </CardFlip>
      <TouchableWithoutFeedback testID={PROFILEUSER.checkVcard.flipCard} onPress={() => this.card.flip()}>
        <QText color={Colors.Purple} fontSize={14} preset="bold" style={{marginTop: 20}}>{flipCard}</QText>
      </TouchableWithoutFeedback>
    </Container>
  )
}

HealthCard.propTypes = {}

HealthCard.defaultProps = {}

export default HealthCard
