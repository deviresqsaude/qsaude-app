import React from 'react';
import { Placeholder, PlaceholderLine, Fade } from 'rn-placeholder';
import { View } from 'react-native';
import { CardLoadingWrapper } from './HealthCardStyle';

const QuestLoading = () => {
  return (
    <CardLoadingWrapper>
      <View style={{height: 218, width: "100%"}}>
        <Placeholder Animation={Fade}>
          <PlaceholderLine height={218} style={{borderRadius: 5}}/>
        </Placeholder>
      </View>
    </CardLoadingWrapper>
  )
}

export default QuestLoading;
