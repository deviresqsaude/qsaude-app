import styled from 'styled-components/native';

export const Container = styled.View`
  align-items: center;
  justify-content: center;
  flex: 1;
`
export const ImageContainer = styled.View`
  align-items: center;
`
export const MessageText = styled.Text`
  font-size: 18;
  font-family: 'Montserrat-Regular';
`
