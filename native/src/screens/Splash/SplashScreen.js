import React from "react";
import PropTypes from "prop-types";
import { Image } from "react-native";
import { Container, ImageContainer, MessageText } from "./SplashStyle";

const SplashScreen = props => {
  const { env, isProd } = props;
  return (
    <Container>
      <ImageContainer>
        <Image source={require(`./../../../assets/images/img_logo.png`)} />
        {isProd !== "true" && <MessageText>{env}</MessageText>}
      </ImageContainer>
    </Container>
  );
};

SplashScreen.propTypes = {
  env: PropTypes.string,
  isProd: PropTypes.string
};

SplashScreen.defaultProps = {
  env: null,
  isProd: "false"
};

export default SplashScreen;
