import { compose, lifecycle, withProps } from 'recompose';
import AsyncStorage from '@react-native-community/async-storage';
import Config from 'react-native-config';
import SplashScreen from './SplashScreen';

const checkAuthentication = lifecycle({
  async componentDidMount() {
    const {
      navigation: { navigate },
    } = this.props;
    try {
      const token = await AsyncStorage.getItem('token');
      const healthCard = await AsyncStorage.getItem('healthCard');
      const callId = await AsyncStorage.getItem('callId');
      if (!token || !healthCard) { 
        navigate('Auth');
        return;
      } 
      if (callId) {
        navigate('TelemedicineError');
        return;
      }
      navigate('App');
    } catch (e) {
      console.error('Failed to fetch token');
    }
  },
});

const enhance = compose(
  checkAuthentication,
  withProps(() => ({
    env: Config.ENV_NAME,
    isProd: Config.IS_PRODUCTION
  }))
);

export default enhance(SplashScreen);
