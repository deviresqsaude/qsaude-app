import styled from 'styled-components/native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';

export const Container= styled(KeyboardAwareScrollView).attrs({
  padding: 16,
  paddingTop: 0,
  flex: 1
})``;

export const FormSafeAreaView = styled.SafeAreaView({
  flex: 1,
})

export const FormContainer = styled.ScrollView({
  flex: 1,
  // justifyContent: "flex-end",
  // paddingBottom: 100,
  // marginBottom: 20
})

export const FormPassword = styled.View({
  backgroundColor: "green",
  // flex: 1
})
