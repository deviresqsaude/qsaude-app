import React from 'react';
import { Platform, Keyboard, TouchableWithoutFeedback } from 'react-native';
import { Field } from 'formik';
import { Container, FormSafeAreaView, FormContainer } from './PasswordStyle';
import { QTextInput, QButton, QText } from '../../components';
import QAlert from '../../components/QAlert';
import { Colors, SystemImages } from '../../constants/theme'

const MESSAGE = {
  error: 'Ops! Tente novamente',
  success: 'Senha alterada com sucesso!',
};

const PasswordScreen = props => {
  const {
    isValid,
    isSubmitting,
    handleSubmit,
    requestAlert,
    setRequestAlert,
    messageError,
    navigation: { goBack },
  } = props;
  return (
    <>
      <FormSafeAreaView>
        <Container behavior={Platform.OS === 'ios' ? 'padding' : null}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <FormContainer>
              <Container>
                <QText color={Colors.TextGray} style={{ fontSize:12}}>
                  A nova senha deve ter 8 ou mais caracteres e ter pelo menos 3 dos 4
                  itens abaixo:
                </QText>
                <QText color={Colors.TextGray} imageSource= {SystemImages.passwordCheck} style={{ marginTop: 8, fontSize:12}}> - Pelo menos 1 caractere maiúscula</QText>
                <QText color={Colors.TextGray} style={{ marginTop: 1, fontSize:12}}> - Pelo menos 1 caractere minúsculo</QText>
                <QText color={Colors.TextGray} style={{ marginTop: 1, fontSize:12}}> - Pelo menos 1 caractere especial</QText>
                <QText color={Colors.TextGray} style={{ marginTop: 1, fontSize:12}}> - Pelo menos 1 número</QText>
              </Container>

              <Field
                testID="currentPassword"
                secureTextEntry
                name="currentPassword"
                label="Senha atual"
                disabled={isSubmitting}
                component={QTextInput}
              />
              <Field
                testID="password"
                secureTextEntry
                name="password"
                label="Nova senha"
                disabled={isSubmitting}
                component={QTextInput}
              />
              <Field
                testID="confirmPassword"
                secureTextEntry
                name="confirmPassword"
                label="Confirme a senha"
                disabled={isSubmitting}
                component={QTextInput}
              />
              <QButton
                testID="sendNewPassword"
                text="Alterar senha"
                loading={isSubmitting}
                disabled={!isValid || isSubmitting}
                inBlock
                onPress={() => handleSubmit()}
                margin={0}
              />
            </FormContainer>
          </TouchableWithoutFeedback>
        </Container>
      </FormSafeAreaView>
      <QAlert
        visible={requestAlert !== null}
        isSuccess={requestAlert === 'success'}
        isError={requestAlert === 'error'}
        description={
          messageError || MESSAGE[requestAlert === 'error' ? 'error' : 'success']
        }
        closeAlert={() => (requestAlert === 'success' ? goBack() : setRequestAlert(null))}
      />
    </>
  );
};

PasswordScreen.propTypes = {};

PasswordScreen.defaultProps = {};

export default PasswordScreen;
