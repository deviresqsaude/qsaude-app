import { withState, compose, withProps, lifecycle } from "recompose";
import { withFormik } from 'formik';
import { graphql } from '@apollo/react-hoc';
import { connect } from 'react-redux';
import * as yup from 'yup';
import R from 'ramda';
import SyncStorage from 'sync-storage';
import PasswordScreen from "./PasswordScreen";
import { UpdatePassword } from "../../service/mutations";
import { AlertActions } from '../../store/actions';

// Error Handling
const mapDispatchToProps = dispatch => ({
  showAlert: options => dispatch(AlertActions.actions.openAlert({...options, shouldRenderOkButton: true, closeAlert: () => {options.close(); dispatch(AlertActions.actions.closeAlert())}}))
});

const getErrorMessage = errors =>{
  if(!errors) return false;
  const lastError = R.findLast(R.T)(errors);
  const { extensions: { response: {body : {critics}}}} = lastError;
  const obj = R.findLast(R.T)(critics);
  return obj.message;
}

const PasswordScheme = yup.object().shape({
  currentPassword: yup
    .string()
    .required('Campo obrigatorio'),
  password: yup
    .string()
    .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,100}$/, { message: "Senha não segue os padrões acima." })
    .required('Campo obrigatorio')
    .min(8, 'A senha deve ter no minimo 8 caracteres'),
  confirmPassword: yup
    .string()
    // ^(?=.*[a-zA-Z])(?=.*\d).{6,9999}$
    .matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,100}$/, { message: "Senha não segue os padrões acima." })
    .required('Campo obrigatorio')
    .oneOf([yup.ref('password'), null], 'As senhas são diferentes')
});

const withForm = withFormik({
  validationSchema: PasswordScheme,
  mapPropsToValues: () => ({currentPassword: '', password: '', confirmPassword: ''}),
  handleSubmit: async (values, formikBag) => {
    const {props:{changePassword, setRequestAlert, setMessageError}, setSubmitting} = formikBag;
    const variables = {
      password: values.currentPassword,
      newPassword: values.password
    }
    const {data, errors} = await changePassword({variables});
    if(errors) setMessageError(getErrorMessage(errors));
    setSubmitting(false);
    setRequestAlert(errors ? 'error' : 'success');
  },
  displayName: 'PasswordForm',
});

const witMutation = graphql(UpdatePassword, {name: "changePassword"});

const withLifeCycle = lifecycle({
  componentWillMount() {
    const {showAlert, navigation} = this.props;
    const profileUser = SyncStorage.get("profileUser");
    if(!profileUser.legalAge) showAlert({
      isError: true,
      description: "Atenção: seu usuário não tem permissão para alterar senha.",
      close: () => navigation.goBack()
    })
  }
});

const enhance = compose(
  connect(
    null,
    mapDispatchToProps,
  ),
  withState('requestAlert', 'setRequestAlert', null),
  withState('messageError', 'setMessageError', null),
  withProps(({ navigation: { state: { routeName } } }) =>({
    title: routeName
  })),
  witMutation,
  withForm,
  withLifeCycle
);

export default enhance(PasswordScreen);
