import React from 'react';
import styled from 'styled-components/native';
import { Placeholder, Fade, PlaceholderLine, PlaceholderMedia } from 'rn-placeholder';
import { Sizing, Colors, SystemFonts, SystemImages } from './../../constants/theme';
import { EmptyData, QText } from '../../components';

export const Container = styled.ScrollView({
  flexDirection: 'column',
  paddingLeft: 16,
  paddingRight: 16,
  paddingBottom: 16,
  width: '100%'
});

export const LaboratoryWrapper = styled.View({
  flexDirection: "column",
  paddingBottom: 10
});

export const LaboratoryDescription = styled.Text({
  color: Colors.Gray,
  marginBottom: 10
});

export const LaboratoryPhone = styled(QText).attrs({
  color: Colors.Purple,
  marginLeft: 10,
  marginRight: 10,
  marginBottom: 6
})``;

export const LaboratorySite = styled(QText).attrs({
  color: Colors.Purple,
  marginLeft: 10,
  marginRight: 10
})``;

export const LaboratorySeparator = styled.View({
  height: 12,
  backgroundColor: Colors.White,
  width: "100%"
});

export const LaboratoryRow = styled.View({
  flexDirection: 'row',
  alignItems: 'center',
})

const EmptyContent = styled.View({
  paddingHorizontal: 16
});

export const LaboratoryLoading = () => (
  <EmptyContent>
    <Placeholder Animation={Fade}>
      <PlaceholderMedia style={{width: '100%', height: 50, marginBottom: 12, borderRadius: 12 }} />
    </Placeholder>
    <Placeholder Animation={Fade}>
      <PlaceholderMedia style={{width: '100%', height: 50, marginBottom: 12, borderRadius: 12 }} />
    </Placeholder>
    <Placeholder Animation={Fade}>
      <PlaceholderMedia style={{width: '100%', height: 50, marginBottom: 12, borderRadius: 12 }} />
    </Placeholder>
  </EmptyContent>
);

export const LaboratoryError = styled(EmptyData).attrs(props => {
  return {
    title: 'Ops!!',
    description: 'Sem resultados',
    buttonText: 'Tentar novamente',
    onPress: props.refetch,
    imageSource: SystemImages.imgRecord,
  }
})``;
