import React from 'react';
import { Linking } from 'react-native';
import PropTypes from 'prop-types';
import { responsiveWidth } from 'react-native-responsive-dimensions';

import {
  LaboratoryWrapper,
  LaboratoryDescription,
  LaboratoryPhone,
  LaboratorySite,
  LaboratoryRow
} from './LaboratoryStyle';
import { SystemIcon } from '../../components/Icons';
import  { SystemIcons, Colors } from '../../constants/theme';

const LaboratoryInfo = props => {
  const {description, phone, site} = props;
  console.log('s')
  return (
    <LaboratoryWrapper>
      <LaboratoryDescription>{description || "Este parceiro ainda não possui agendamento online, contate pelo telefone e acesse o site para mais informações:"}</LaboratoryDescription>
      <LaboratoryRow style={{ marginBottom: 12 }}>
        <SystemIcon
          source={SystemIcons.phone}
          width={responsiveWidth(6)}
          tintColor={Colors.Purple}
          style={{ marginRight: 4 }}
        />
        <LaboratoryPhone preset="link" onPress={() => Linking.openURL(`tel:${phone}`)}>{phone}</LaboratoryPhone>
      </LaboratoryRow>
      <LaboratoryRow>
        <SystemIcon
          source={SystemIcons.web}
          width={responsiveWidth(6)}
          tintColor={Colors.Purple}
          style={{ marginRight: 4 }}
        />
        <LaboratorySite preset="link" onPress={() => Linking.openURL(`https://${site}`)}>{site}</LaboratorySite>
      </LaboratoryRow>
    </LaboratoryWrapper>
  )
};

LaboratoryInfo.propTypes = {
  description: PropTypes.string,
  phone: PropTypes.string,
  site: PropTypes.string
};

LaboratoryInfo.defaultProps = {
  description: "",
  phone: "",
  site: ""
};

export default LaboratoryInfo;
