import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';
import { Acordion } from '../../components';
import LaboratoryInfo from './LaboratoryInfo';
import { Container, LaboratorySeparator } from './LaboratoryStyle';
// import { LABORATORY_DATA } from '../../constants/mock';

const LaboratoryScreen = ({ laboratory  }) => {
  return (
    <Container>
      <Text style={{ marginBottom: 20, fontSize: 14, color: '#525252' }}>
        Selecione o laboratório que deseja agendar seus exames:
      </Text>
      <Acordion
        data={laboratory}
        RenderSeparator={LaboratorySeparator}
        RenderComponent={LaboratoryInfo}
      />
    </Container>
  );
};

// RenderComponent={({description, phone, site}) => (
//   <LaboratoryWrapper>
//     <LaboratoryDescription>{description}</LaboratoryDescription>
//     <LaboratoryPhone>{phone}</LaboratoryPhone>
//     <LaboratorySite>{site}</LaboratorySite>
//   </LaboratoryWrapper>
// )}
LaboratoryScreen.propTypes = {
  title: PropTypes.string,
};

LaboratoryScreen.defaultProps = {
  title: '',
};

export default LaboratoryScreen;
