import { compose, withProps, branch, renderComponent } from "recompose";
import React from 'react';
import R from 'ramda';
import { graphql } from '@apollo/react-hoc';
import { HospitalQuery } from '../../service/query';
import LaboratoryScreen from "./LaboratoryScreen";
import { LaboratoryError, LaboratoryLoading } from './LaboratoryStyle';


const withLaboratoryData = graphql(HospitalQuery, {
  name: "dataHospitals",
  options: ({ navigation: { state : { params : { tussCode }}}}) => ({
    notifyOnNetworkStatusChange: true,
    variables: {
      serviceType: "5",
      procedureTUSSCode: tussCode,
    }
  })
})

const laboratoryDataTreated = withProps(props => {

  const { dataHospitals: { getAccreditedNetwork } } = props;

  const reformatData = R.map(item => ({
    title: item.name,
    phone: item.units[0].phone,
    site: item.units[0].site
  }), getAccreditedNetwork);

  return {
    laboratory: [
      ...reformatData
    ]
  };
});

const renderForLoading = branch(({ dataHospitals }) => dataHospitals && dataHospitals.loading && dataHospitals.networkStatus !== 4, renderComponent(LaboratoryLoading))
const withLaboratoryRefetch = withProps(({ dataHospitals }) => ({refetch: dataHospitals && dataHospitals.refetch}));
const renderForError = branch(({ dataHospitals }) => dataHospitals && dataHospitals.error, renderComponent(LaboratoryError));

const enhance = compose(
  withProps(({ navigation: { state: { routeName } } }) => ({
    title: routeName
  })),
  withLaboratoryData,
  renderForLoading,
  withLaboratoryRefetch,
  renderForError,
  laboratoryDataTreated,
);

export default enhance(LaboratoryScreen);
