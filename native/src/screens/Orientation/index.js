import React from 'react';
import { Text } from 'react-native';
import { Placeholder, PlaceholderMedia, Shine } from 'rn-placeholder';
import PDFView from 'react-native-view-pdf/lib';
import { compose, renderComponent, branch, withState } from 'recompose';

const resources = {
  url: 'https://image.qsaude.net/lib/fe3c157175640478761c71/m/1/d87e2fe1-a841-4f08-8dbc-0748453b6f37.pdf',
};

// eslint-disable-next-line react/prop-types
const HealthGuidelineScreen = ({ toggleLoad }) => {
  const resourceType = 'url';

  return (
    <>
      <PDFView
        resource={resources[resourceType]}
        onLoad={() => {
          toggleLoad(true);
        }}
        style={{ flex: 1 }}
      />
    </>
  );
};

const withLoadingState = withState('pdfLoaded', 'toggleLoad', false);

const renderWhileLoading = component =>
  branch(props => props.pdfLoaded, renderComponent(component));

const PdfPlaceholder = () => (
  <Placeholder Animation={Shine}>
    <PlaceholderMedia style={{ width: '100%', height: '100%' }} />
  </Placeholder>
);

export default compose(
  withLoadingState,
  // renderWhileLoading(PdfPlaceholder),
)(HealthGuidelineScreen);
