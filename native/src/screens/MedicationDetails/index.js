import { connect } from 'react-redux';
import moment from 'moment';
import R from 'ramda';
import { Alert } from 'react-native';
import { compose, withState, withHandlers } from 'recompose';
import RNCalendarEvents from 'react-native-calendar-events';
import MedicationDetailsScreen from './MedicationDetails';
import { Colors } from '../../constants/theme';
import { AlertActions } from '../../store/actions';
import SyncStorage from 'sync-storage';

// Error Handling
const mapDispatchToProps = dispatch => ({
  onServiceFailed: ({ title, error: description }) =>
    dispatch(
      AlertActions.actions.openAlert({
        title,
        shouldRenderOkButton: true,
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        description,
        isError: true,
      }),
    ),
  onServiceSuccess: () =>
    dispatch(
      AlertActions.actions.openAlert({
        description: null,
        shouldRenderOkButton: true,
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        title: 'Salvar ao calendário foi efetuada com sucesso!',
        isSuccess: true,
      }),
    ),
});

const withModalState = withState('visible', 'isVisible', false);
const withPickerState = withState('showDatePicker', 'toggleDatePicker', false);
const withSelectedDateState = withState('selectedDate', 'saveDate', new Date());
const withHourPickerState = withState('showHourPicker', 'toggleHourPicker', false);
const withSelectedHourState = withState('selectedHour', 'saveHour', new Date());
const withIntervalPickerState = withState('showIntervalPicker', 'toggleIntervalPicker', false);
const withSelectedIntervalState = withState('selectedInterval', 'saveInterval', 4);
const withCalendar = withState('calendar', 'setCalendar', '');

const withCalendarHandler = withHandlers({
  closeAllPicker: ({ toggleDatePicker, toggleHourPicker, toggleIntervalPicker }) => () => {
    toggleDatePicker(false);
    toggleHourPicker(false);
    toggleIntervalPicker(false);
  },
  setPermission: ({ isVisible, onServiceFailed, setCalendar }) => () => {
    
    RNCalendarEvents.authorizationStatus()
    .then((status) => {
      if(status == 'authorized') {
        RNCalendarEvents.findCalendars().then(calendars => {
          if(calendars.length == 0) {
            onServiceFailed({
              title: 'O lembrete precisa o aplicativo de calendário para criar eventos',
              description: ''
            })
          } else {
            const primaryCalendar = R.find(R.propEq('isPrimary', true))(calendars);
            setCalendar(primaryCalendar.id);
            isVisible(true)
          }
        })
      } else if (status === 'undetermined') {
        RNCalendarEvents.authorizeEventStore().then((res) => {
          if (res == 'authorized') {
            isVisible(true)
          }
        });
      } else if (status == 'denied' || status == 'restriced') {
        Alert.alert('Permite acesso a calendário no configuração.');
      }
    })
    .catch(error => {
      console.log('Auth Error: ', error)
      onServiceFailed({ title : 'Erro ao salvar permissão ao calendário', description: error})
    });
  },
  saveCalendarEvent: ({ calendar, onServiceFailed, onServiceSuccess, isVisible, navigation : { state: { params : { medication} }}, selectedDate, selectedHour, selectedInterval }) => ({ endDate }) => {
    /** 
     * TODO
     * in case of 'uso continuo' when would the end date be ? 
    */
    const user = SyncStorage.get('profileUser');

    const startDate = moment(selectedDate).set({ hour: moment(selectedHour).get('hour'), minute: moment(selectedHour).get('minute')});
    const hours = endDate.diff(startDate, 'hours');
    const maximumOcurrence = Math.floor(parseInt(hours) / parseInt(selectedInterval));
    try {
      var i;
      for (i = 0; i <= maximumOcurrence; i++) {
        var details = {
          calendarId: calendar,
          startDate: moment(startDate).add(parseInt(selectedInterval)*i, 'h').toISOString(),
          endDate: moment(startDate).add(parseInt(selectedInterval)*i, 'h').toISOString(),
          alarms: [
            {date: -10}
          ],
          notes: user.completeName,
          description: user.completeName
        }

        RNCalendarEvents.saveEvent(medication['display'], details).then(res=> 
          // console.log('saved', res)
          { onServiceSuccess() }
          )
        .catch(e => {
          console.log('error saving calendar', e);
          onServiceFailed({ title : 'Erro ao salvar lembretes ao calendário', description: e});
        });
      }      
    } catch(e) {
      console.log('saving in the calendar error', e)
      onServiceFailed({ title : 'Erro ao salvar lembretes ao calendário', description: e})
    }
    
    isVisible(false);
  }
})

const enhance = compose(
  connect(
    null,
    mapDispatchToProps
  ),
  withCalendar,
  withModalState,
  withPickerState,
  withHourPickerState,
  withIntervalPickerState,
  withSelectedDateState,
  withSelectedHourState,
  withSelectedIntervalState,
  withCalendarHandler,
);
export default enhance(MedicationDetailsScreen);