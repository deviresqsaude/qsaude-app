import React from 'react';
import  { Platform, TouchableWithoutFeedback } from 'react-native';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';
import R from 'ramda';
import { DetailCard, DetailRow, Container, ReminderWrapper, ReminderRowContainer, DateTimePickerWrapper, IntervalPicker, DisabledDataInput, InputContaier, IconWrapper, MedicationDateInput } from '../Medications/MedicationStyles';
import { QButton, QText, SystemIcon } from '../../components';
import { QSModal } from '../../components/Modal';
import { Colors } from '../../constants/theme';


const toLocaleString = {
  display: 'Nome',
  dosageInstruction: 'Período',
  frequency: 'Frequência',
  method: 'Método',
  doseQuantity: 'Dosagem',
  when: 'Quando',
};

const formatTime = (string) => {
  if (R.contains('dia', string)) {
    return 'd';
  } else if (R.contains('semana', string)) {
    return 'w';
  } else if (R.contains('mês', string)) {
    return 'M';
  }
};

const MedicationDetailsScreen = ({
  navigation: {
    state: {
      params: { medication },
    },
  },
  isVisible,
  visible,
  showDatePicker,
  toggleDatePicker,
  saveDate,
  selectedDate,
  setPermission,
  showHourPicker,
  toggleHourPicker,
  selectedHour,
  saveHour,
  closeAllPicker,
  showIntervalPicker,
  toggleIntervalPicker,
  saveInterval,
  selectedInterval,
  saveCalendarEvent
}) => {
  const startDate = moment(selectedDate).set({ hour: moment(selectedHour).get('hour'), minute: moment(selectedHour).get('minute')});
  const number = medication.dosageInstruction.split(' ')[0];
  const count = medication.dosageInstruction.split(' ')[1];
  const endDate = R.contains('(s)', medication.dosageInstruction) ? moment(startDate).add(number, formatTime(count)) : moment().set({ 'month': 11, 'date': 31 });
  const closePicker= () => closeAllPicker();
  return (
    <Container showsVerticalScrollIndicator={false}>
      <DetailCard >
        <>
          {/* render modal title */}
          <DetailRow center label="Configurações do medicamento" noBorder />
          {/* render obj itens */}
          {Object.keys(R.dissoc('note', medication)).map(key => (
            <DetailRow data={medication[key]} label={toLocaleString[key]} key={medication[key]}/>
          ))}
          {/* render note */}
          {medication.note && (
            <DetailRow data={R.prop('note', medication)} label="Observação" />
          )}
        </>
      </DetailCard>
      <QButton text="Adicionar lembrete" style={{margin: 16}} onPress={() => setPermission()} />
      <QSModal
        visible={visible}
        closeModal={() => isVisible(false)}
        title="Adicionar lembrete"
      >
        <TouchableWithoutFeedback onPress={()=>{closeAllPicker()}} >
          <ReminderWrapper showsVerticalScrollIndicator={false}>
              <DetailRow label="Nome" data={medication.display} onPress={()=>{closeAllPicker()}} />
              <DetailRow label="Período" data={medication.dosageInstruction} onPress={()=>{closeAllPicker()}} />
              <DetailRow label="Frequência" data={medication.frequency} onPress={()=>{closeAllPicker()}} />
              <MedicationDateInput
                closePicker={closePicker}
                title="Data de Início"
                style={{marginTop: 0}}
                onPress={() => {closeAllPicker(); toggleDatePicker(!showDatePicker)}}
                value={String(moment(selectedDate).format('DD MMMM YYYY'))}
              />
              <MedicationDateInput
                  closePicker={closePicker}
                  title="Horário de Início"
                  style={{marginTop: 0}}
                onPress={() => {closeAllPicker(); toggleHourPicker(!showHourPicker)}}
                value={String(moment(selectedHour).format('LT'))}
                />
              <ReminderRowContainer  onPress={()=>{closeAllPicker()}}>
                {
                  Platform.OS === 'ios' ? 
                  <MedicationDateInput
                    closePicker={closePicker}
                    title="Intervalo de horários"
                    onPress={() => {closeAllPicker(); toggleIntervalPicker(!showIntervalPicker)}}
                    value={String(selectedInterval)}
                  />
                  : 
                  <>
                    <QText color={Colors.TextGray} onPress={()=>{closeAllPicker()}} style={{paddingTop: 16}}>Intervalo de horários</QText>
                    <InputContaier style={{position: 'relative', activeOpacity: 1}}>
                      <IntervalPicker
                        onPress={(itemValue) => saveInterval(itemValue)}
                        value={selectedInterval}
                      />
                      <IconWrapper>
                        <SystemIcon
                          tintColor={Colors.Purple}
                          name="chevromDown"
                        />
                      </IconWrapper>
                    </InputContaier>
                  </>
                }

              </ReminderRowContainer>
              <ReminderRowContainer  onPress={()=>{closeAllPicker()}}>
                <QText color={Colors.TextGray} onPress={()=>{closeAllPicker()}} style={{paddingTop: 16}}>Data de termino do lembrete</QText>
                <DisabledDataInput 
                  value={String(moment(endDate).format('DD MMMM YYYY'))}
                />
              </ReminderRowContainer>
              <QButton text="Salvar lembrete" style={{marginVertical: 16}} onPress={() => saveCalendarEvent({ endDate })} />
            
          </ReminderWrapper>
        </TouchableWithoutFeedback>
        <DateTimePickerWrapper>
          {showDatePicker && (
            <DateTimePicker
              onChange={(e, date) => {
                if(Platform.OS === 'android') {
                  toggleDatePicker(false)
                }
                saveDate(date);
              }}
              style={{
                backgroundColor: '#fff',
                width: '100%',
              }}
              mode="date"
              value={selectedDate}
            />
          )}
          {showHourPicker && (
            <DateTimePicker
              minuteInterval={30}
              mode="time"
              is24Hour
              onChange={(e, hour) => {
                if(Platform.OS === 'android') {
                  toggleHourPicker(false)
                }
                saveHour(hour);
              }}
              style={{
                backgroundColor: '#fff',
                width: '100%',
              }}
              value={selectedHour}
            />
          )}
          {
            showIntervalPicker && Platform.OS === 'ios' && (
              <IntervalPicker
                onPress={(itemValue) => saveInterval(itemValue)}
                value={selectedInterval}
              />
            )
          }
        </DateTimePickerWrapper>
      </QSModal>
    </Container>
  );
}

export default MedicationDetailsScreen;
