import { compose, withProps, lifecycle } from 'recompose';
import { connect } from 'react-redux';
import SyncStorage from 'sync-storage';
import AccountScreen from './AccountScreen';
import { AuthActions, NavActions } from '../../store/actions';

const mapDispatchToProps = dispatch => ({
  setNavCurrentPage: state => dispatch(NavActions.actions.setCurrentPage(state)),
  clearAuth: () => dispatch(AuthActions.actions.clearUserProfile()),
});

const life = lifecycle({
  componentDidMount() {
    const {navigation, setNavCurrentPage} = this.props;
    setNavCurrentPage(navigation.state);
  },
});

const enhance = compose(
  connect(
    null,
    mapDispatchToProps
  ),
  withProps(({ navigation: { state: { routeName }, navigate }, clearAuth }) => ({
    title: routeName,
    logout: () => {
      SyncStorage.remove('token');
      SyncStorage.remove('prevToken');
      SyncStorage.remove('healthCard');
      SyncStorage.remove('profileUser');
      SyncStorage.remove('dependents');
      clearAuth();
      navigate('Login', {loginType: "IS_LOGOUT_SESSION"});
    },
  })),
  life
);

export default enhance(AccountScreen);
