import styled from "styled-components/native";

export const Container = styled.ScrollView({
  flexDirection: 'column',
  flex: 1
});


export const ActionWrapper = styled.View({
  padding: 16,
});
