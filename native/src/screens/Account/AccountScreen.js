/* eslint-disable eqeqeq */
import React from 'react';
import PropTypes from 'prop-types';
import SyncStorage from 'sync-storage';
import { Container, ActionWrapper } from './AccountStyle';
import { MenuList } from '../../components/MenuList';
import QButton from '../../components/Button/Button';
import {PROFILEUSER} from '../../constants/TestIds';
import { SystemIcons } from '../../constants/theme';
import { companyGenerator } from '../../constants/mock';

const AccountScreen = ({ logout }) => {

  const profile = SyncStorage.get("profileUser");

  // const PAYMENT_ = {
  //   icon: SystemIcons.payment,
  //   title: 'Pagamentos',
  //   link: 'PaymentList',
  //   testID: 'PROFILEUSER.itemsList.BankSlip',
  //   // size: { height: 36, width: 29}
  // }

  const PROFILE_MENU = [
    {
      icon: SystemIcons.lock,
      title: 'Alterar minha senha',
      link: 'Password',
      testID: PROFILEUSER.itemsList.changePassword
    },
    {
      icon: SystemIcons.dataPaper,
      title: 'Alterar meus dados',
      link: 'UpdateInfo',
    },
    {
      icon: SystemIcons.identification,
      title: 'Ver carterinha virtual',
      link: 'HealthCard',
      testID: 'PROFILEUSER.itemsList.checkVcard',
    },
    {
      icon: SystemIcons.manual2,
      title: 'Manual do beneficiário',
      link: 'BenificiaryManual',
      testID: 'PROFILEUSER.itemsList.BenificiaryManual',
    },
    {
      icon: SystemIcons.payment,
      title: 'Pagamentos',
      link: 'PaymentList',
      testID: 'PROFILEUSER.itemsList.BankSlip',
      visible: !!(profile && profile.segment === "1"
        && ["T", "CO"].includes(profile.relationshipDegree)
        && companyGenerator(profile.groupCode) !== "QUALICORP")
        // && companyGenerator(profile.groupCode) !== "QUALICORP"
    }
  ];

  // const showMenuData = () => {
  //   if(profile && profile.segment == 1) {
  //     if(profile.type === 'T' || profile.type === 'D' && profile.relationshipDegree === 'CO') {
  //       return true;
  //     }
  //   }
  //   return false
  // }

  return (
    <Container>
      <MenuList
        menuData={PROFILE_MENU}
        // menuData={showMenuData() ? [...PROFILE_MENU, PAYMENT_] : PROFILE_MENU}
      />
      <ActionWrapper>
        <QButton testID={PROFILEUSER.itemsList.logout}
          error
          text="Sair"
          onPress={logout} />
      </ActionWrapper>
    </Container>
  );
};

AccountScreen.propTypes = {
  logout: PropTypes.func.isRequired,
};

AccountScreen.defaultProps = {};

export default AccountScreen;
