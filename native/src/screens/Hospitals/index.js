import { compose, withState, withProps, withHandlers, lifecycle, branch, renderComponent } from 'recompose';
import R from 'ramda';
import { graphql } from '@apollo/react-hoc';
import { connect } from 'react-redux';
import getDirections from 'react-native-google-maps-directions';
import Geolocation from '@react-native-community/geolocation';
import { Linking, Animated } from 'react-native';
import HospitalsScreen from './Hospitals';
import { SystemIcons } from '../../constants/theme';
import { HospitalQuery } from '../../service/query';
import { HospitalActions } from '../../store/actions';
import { HospitalsLoading, HospitalsError } from './HospitalStyle';

const DATA_HOSPITAL = "dataHospitals";

const withTabState = withState('selectedTabIndex', 'setSelectedTabIndex', 0);
const withUserLocationState = withState('source', 'setSource', {});
const withHelpState = withState('helpVisible', 'setHelpVisible', false);
const stateAnimateCard = withState('animateDoctors', 'setAnimateDoctors', new Animated.Value(150));
const stateCollapse = withState('collapse', 'setCollapse', false);
const withSelectedUnidade = withState('selectedUnidade', 'setUnidade', { title: '?', description: '?'});

// Redux State
const mapStateToProps = ({hospital}) => ({
  hospitalState: hospital
});

// Redux Handlers
const mapDispatchToProps = dispatch => ({
  setStoreHospitals: hospitals =>
    dispatch(HospitalActions.actions.setHospitals(hospitals))
});

// get user position
const withUserLocation = lifecycle({
  componentDidMount() {
    const { setSource, setStoreHospitals, dataHospitals } = this.props;
    setStoreHospitals(dataHospitals);
    Geolocation.getCurrentPosition(
      ({ coords }) => setSource({ ...coords }),
      e => console.log(`failed to get user position \n ${JSON.stringify(e, null, 2)}`),
      {
        timeout: 5000,
        enableHighAccuracy: true,
      },
    );
    
    if(this.props.type == 2) {
      this.props.setHelpVisible(true)
    }
  },
})

const withMarkersProps = withProps(props => {
  const { openNavigation, dataHospitals : { getAccreditedNetwork } } = props;

  const reformatData = R.map(item => ({
    title: item.unitName,
    description: `${item.address || ''} - ${item.neighborhood || ''} - ${item.city || ''},${item.state || ''}, ${item.cep || ''} ` || "-",
    links: [
      {
        icon: SystemIcons.phone,
        label: item.phone || "-",
        onPress: () => Linking.openURL(`tel:${item.phone}`),
        visible: true
      },
      { 
        icon: SystemIcons.navigation,
        label: 'Ver como chegar',
        onPress: () => 
          openNavigation({
            destination: {
              latitude: parseFloat(item.latitude),
              longitude: parseFloat(item.longitude)
            }
          }),
        visible: item.latitude && item.longitude
      }
    ],
    coordinate: {
      latitude: parseFloat(item.latitude),
      longitude: parseFloat(item.longitude),
    },
  }), R.reduce((acc, next) => [...R.union(acc, next.units)], [], getAccreditedNetwork));

  return {
    markers: [
      ...reformatData
    ]
};
});

const withNavigationController = withHandlers({
  openNavigation: ({ source }) => ({ destination }) => {
    const data = {
      source,
      destination,
      params: [
        {
          key: 'travelmode',
          value: 'driving', // may be "walking", "bicycling" or "transit" as well
        },
        {
          key: 'dir_action',
          value: 'navigate', // this instantly initializes navigation using the given travel mode
        },
      ],
    };
    getDirections(data);
  },
  toggleDoctors: ({animateDoctors, collapse, setCollapse}) => () => {
    Animated.spring(animateDoctors, {
      toValue: !collapse ? 0 : 150,
    }).start();
    setCollapse(!collapse);
  },
});

const withParams = withProps(({ navigation: { state: { params }} }) => {
  return {
    type: params.type
  }
})

const withHospitalsData = graphql(HospitalQuery, {
  name: DATA_HOSPITAL,
  options: ({ type }) => ({
    notifyOnNetworkStatusChange: true,
    variables: {
      serviceType: type
    }
  })
})
const renderForLoading = branch(({ dataHospitals }) => dataHospitals && dataHospitals.loading && dataHospitals.networkStatus !== 4, renderComponent(HospitalsLoading))
const withHospitalsRefetch = withProps(({ dataHospitals }) => ({refetch: dataHospitals && dataHospitals.refetch}));
const renderForError = branch(({ dataHospitals }) => dataHospitals && dataHospitals.error, renderComponent(HospitalsError));

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withParams,
  withTabState,
  withUserLocationState,
  withHelpState,
  stateAnimateCard,
  stateCollapse,
  withSelectedUnidade,
  // Apollo
  withHospitalsData,
  renderForLoading,
  withHospitalsRefetch,
  renderForError,
  // Props
  withNavigationController,
  withMarkersProps,
  // Lifecycle
  withUserLocation,
)(HospitalsScreen);
