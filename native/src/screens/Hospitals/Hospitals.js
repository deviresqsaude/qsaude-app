import React from 'react';
import R from 'ramda';
import { FlatList, RefreshControl, View, Animated, TouchableWithoutFeedback } from 'react-native';
import { MapView, DoctorCard, TabView, SystemIcon, QText } from '../../components';
import QAlert from '../../components/QAlert';
import { Colors } from '../../constants/theme';
import { DoctorsContainer, DoctorItem } from '../CustomerCare/Schedule/Map/MapStyle';

const MESSAGES = {
  loading: "Carregando médicos, aguarde um momento",
  empty: "Não foram localizados médicos nessa região"
};


const Doctor = ({ hospital }) => (
  <TouchableWithoutFeedback>
    <DoctorItem>
      <QText preset="link">{hospital.title}</QText>
      <View style={{paddingRight: 70, position: "relative"}}>
        <QText preset="body">{hospital.description}</QText>
        {/* <QText preset="link" style={{position: "absolute", top: 0, right: 0}}>{doctor.distance || "0km"}</QText> */}
      </View>
    </DoctorItem>
  </TouchableWithoutFeedback>
);

const Hospitals = props => {
  const {
    selectedTabIndex,
    setSelectedTabIndex,
    markers,
    helpVisible,
    setHelpVisible,
    refetch,
    dataHospitals: {loading},
    selectedUnidade,
    setUnidade,
  } = props;
  const alertDescription = `Lembre-se: antes de ir diretamente ao Pronto-Socorro (PS), acione o Botão de Emergência, que orientamos você. Utilizar o PS sem nossa indicação poderá gerar cobrança de coparticipação.`;

  const filterMarkers = markers => {
    return R.filter(item => !!item.coordinate.latitude&&!!item.coordinate.longitude, markers)
  }

  return (
    <>
      <TabView
        tabList={['Lista', 'Mapa']}
        selectedIndex={selectedTabIndex}
        onChange={setSelectedTabIndex}
        sceneList={[
          () => (
            <FlatList
              refreshControl={<RefreshControl refreshing={loading} onRefresh={refetch}/>}
              renderItem={({ item }) => <DoctorCard {...item} key ={item.title}/>}
              keyExtractor={({ title }) => title}
              data={markers}
            />
          ),
          () => (
            <>
              <MapView markers={markers} onSelectMarker={setUnidade} noDescription={true} showUnity={true} />
            </>
          )
          ,
        ]}
      />
      <QAlert
        description={alertDescription}
        visible={helpVisible}
        shouldRenderImage={false}
        shouldRenderOkButton
        isSuccess
        closeAlert={() => setHelpVisible(!helpVisible)}
      />
    </>
  );
};

export default Hospitals;

// [title]
// [description]
// [links]
