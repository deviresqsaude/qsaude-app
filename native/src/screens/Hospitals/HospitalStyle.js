import React from 'react';
import styled from 'styled-components/native';
import { Placeholder, Fade, PlaceholderLine, PlaceholderMedia } from 'rn-placeholder';
import { SystemImages } from '../../constants/theme';
import { EmptyData } from '../../components';

const EmptyContent = styled.View({
  padding: 16
});

export const HospitalsLoading = () => (
  <EmptyContent>
    <Placeholder Animation={Fade}>
      <PlaceholderMedia style={{width: '60%', height: 24, marginBottom: 4 }} />
      <PlaceholderMedia style={{width: '100%', height: 24, marginBottom: 12 }} />
      <PlaceholderMedia style={{width: '80%', height: 24, marginBottom: 4 }}  />
      <PlaceholderMedia style={{width: '40%', height: 24, marginBottom: 40 }}  />
    </Placeholder>
    <Placeholder Animation={Fade}>
      <PlaceholderMedia style={{width: '80%', height: 24, marginBottom: 4 }} />
      <PlaceholderMedia style={{width: '100%', height: 24, marginBottom: 12 }} />
      <PlaceholderMedia style={{width: '30%', height: 24, marginBottom: 4 }}  />
      <PlaceholderMedia style={{width: '40%', height: 24, marginBottom: 40 }}  />
    </Placeholder>
    <Placeholder Animation={Fade}>
      <PlaceholderMedia style={{width: '50%', height: 24, marginBottom: 4 }} />
      <PlaceholderMedia style={{width: '100%', height: 24, marginBottom: 12 }} />
      <PlaceholderMedia style={{width: '20%', height: 24, marginBottom: 4 }}  />
      <PlaceholderMedia style={{width: '40%', height: 24, marginBottom: 40 }}  />
    </Placeholder>
  </EmptyContent>
);

export const HospitalsError = styled(EmptyData).attrs(props => {
  return {
    title: 'Ops!!',
    description: 'Sem resultados',
    buttonText: 'Tentar novamente',
    onPress: props.refetch,
    imageSource: SystemImages.imgRecord,
  }
})``;
