import { compose, withProps } from "recompose";
import HomeScreen from "./HomeScreen";

const enhance = compose(
  withProps(({ navigation: { state: { routeName } } }) => ({
    title: routeName
  }))
);

export default enhance(HomeScreen);
