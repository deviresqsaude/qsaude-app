import React from "react";
import PropTypes from 'prop-types';
import { Text } from 'react-native';
import env from 'react-native-config';
import { Container } from './HomeStyle';

const HomeScreen = props => {
  const {title} = props;
  return(
    <Container>
      <Text>{title}</Text>
      <Text>ENV: {env.ENV_NAME}</Text>
    </Container>
  )
}

HomeScreen.propTypes = {
  title: PropTypes.string
}

HomeScreen.defaultProps = {
  title: `Welcome to Home`
}

export default HomeScreen;
