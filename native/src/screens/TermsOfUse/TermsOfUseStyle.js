import styled from 'styled-components/native';
import React from 'react';
import { Placeholder, Fade, PlaceholderMedia } from 'rn-placeholder';

export const Container = styled.View({
  // paddingTop: hasDeviceNotch(),
  flex: 1,
  // paddingTop: 20,
  paddingBottom: 38,
  paddingLeft: 20,
  paddingRight: 20,
  backgroundColor: 'rgba(0, 0, 0, .3)'
});


export const TermWrapper = styled.View`
  flex: 1;
  border-radius: 15;
  background-color: white;
  overflow: hidden;
`;

export const TermBody = styled.ScrollView.attrs({
  showsVerticalScrollIndicator: false
})`
  flex: 1;
  padding-left: 20;
  padding-right: 20;
`;

export const TermHeader = styled.View`
  height: 44;
  align-items: center;
  justify-content: center;
`;

export const TermFooter = styled.View`
  height: 112;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.View({

})

export const Loading = () => (
  <Wrapper>
    <Placeholder Animation={Fade}>
      <PlaceholderMedia style={{ height: '100%', width: '100%'}} />
    </Placeholder>
  </Wrapper>
);
