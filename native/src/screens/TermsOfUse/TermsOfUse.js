import React from 'react';
import R from 'ramda';
import { TouchableWithoutFeedback, Dimensions } from 'react-native';
import WebView from 'react-native-webview';
import { Container, TermWrapper, TermFooter, TermHeader, TermBody, Loading } from './TermsOfUseStyle';
import { SystemIcons, Colors } from '../../constants/theme';
import { elevationShadow } from '../../helpers';
import { ButtomIconWrapper, ButtomIcon, BackButtonContainer } from '../../components/Icons';
import { QText, QCheckbox, QButton } from '../../components';

const screenHeight = Dimensions.get('window').height;

const TermsOfUse = props => {
  const {
    setTermAccepted,
    accepted,
    loading,
    logOut,
    sendTermOfUse,
    termData
  } = props;
  if(R.isEmpty(termData)) return null;
  const {term} = termData;
  return (
    <Container>
      <TouchableWithoutFeedback onPress={() => loading ? () => {} : logOut()}>
        <BackButtonContainer>
          <ButtomIconWrapper style={{ paddingBottom: 20 }}>
            <ButtomIcon
              resizeMode="contain"
              width={13}
              source={SystemIcons.close}
              tintColor="#FFFFFF"
            />
          </ButtomIconWrapper>
        </BackButtonContainer>
      </TouchableWithoutFeedback>
      <TermWrapper style={{...elevationShadow(Colors.Black, 5, .5)}}>
        <TermHeader>
          <QText preset="navTitle" fontSize={17}>Termo de Uso</QText>
        </TermHeader>
        <TermBody>
          <WebView
            source={{ html: `${term}` }}
            containerStyle={{}}
            style={{ height: screenHeight - 280, flex: 1 }}
            startInLoadingState
            renderLoading={() => <Loading />}
          />
        </TermBody>
        <TermFooter>
          <QCheckbox
            disabled={loading}
            preset="primary"
            onFieldChange={val => setTermAccepted(val)}
            label="Li e concordo com os termos de uso"/>
          <QButton
            loading={loading}
            preset="primary"
            inBlock={false} text="Entrar"
            disabled={!accepted || loading}
            style={{width: 156, marginTop: 5}}
            onPress={() => sendTermOfUse()}/>
        </TermFooter>
      </TermWrapper>
    </Container>
  )
};

export default TermsOfUse;
