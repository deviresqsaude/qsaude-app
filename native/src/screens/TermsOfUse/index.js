import React from 'react';
import { compose, withState, withHandlers, branch, renderComponent } from "recompose";
import { graphql, withApollo } from '@apollo/react-hoc';
import { Alert } from 'react-native';
import SyncStorage from 'sync-storage';
import { AcceptTerm } from "../../service/mutations";
import { TermOfUseQuery } from "../../service/query";
import { setAsyncStorage } from '../../helpers';
import { QLoading } from '../../components';
import TermsOfUse from './TermsOfUse';

const termState = withState('accepted', 'setTermAccepted', false);
const loadingState = withState('loading', 'setLoading', false);
const termDataState = withState('termData', 'setTerm', {});

const handlers = withHandlers({
  logOut: props => () => {
    SyncStorage.remove('token');
    SyncStorage.remove('prevToken');
    SyncStorage.remove('healthCard');
    SyncStorage.remove('profileUser');
    SyncStorage.remove('dependents');
    SyncStorage.remove('@form[login]:user');
    props.setLoading(false);
    props.navigation.goBack();
  },
  sendTermOfUse: props => async () => {
    const { termData: {termId}, navigation: {navigate , state: {params: {token, personalId, profileUser, healthCardNumber, beneficiary, healthCard}}}, accepTerm, setLoading } = props;
    setLoading(true);
    const variables = {termId, personalId};
    const response = await accepTerm({
      variables,
      context: {headers: { authorization: token }}
    });
    if (response.data.accepTerm) {
      setAsyncStorage({profileUser, healthCardNumber, token, user: personalId, beneficiary, healthCard});
      navigate('App');
    } else {
      Alert.alert(
        "Termo de Uso",
        "Aconteceu um problema com o aceite de termo, tente novamente ou entre em contato com a Qsaude",
        [{text: 'OK', onPress: () => {}}],
      );
      setLoading(false);
    };
  }
});
const withAccepTermMutation = graphql(AcceptTerm, {name: "accepTerm"});
const withTermDataMutation = graphql(TermOfUseQuery, {
  name: "termOfUse",
  options: ({
    setTerm,
    navigation: {
      navigate,
      state: {params: {personalId, token, profileUser, healthCardNumber, beneficiary}},
    }
  }) => ({
    notifyOnNetworkStatusChange: true,
    variables: {id: personalId},
    context: {headers: { authorization: token }},
    onCompleted: ({getTerm: {accepted, text: term, id: termId}}) => {
      setTerm({accepted, term, termId});
      if (accepted) {
        setAsyncStorage({profileUser, healthCardNumber, token, user: personalId, beneficiary});
        navigate("App");
      };
    }
  })
});

const withLoading = branch(({termOfUse}) => {
  return !termOfUse || termOfUse.loading || termOfUse.getTerm.accepTerm;
}, renderComponent(() => <QLoading visible simple/>))

const enhance = compose(
  termState,
  termDataState,
  withAccepTermMutation,
  withTermDataMutation,
  withLoading,
  loadingState,
  handlers,
  withApollo
);

export default enhance(TermsOfUse);
