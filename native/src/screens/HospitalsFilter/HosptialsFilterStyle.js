import styled from 'styled-components/native';

export const Container = styled.ScrollView({
  contentContainerStyle: {
    flex: 1
  }
})