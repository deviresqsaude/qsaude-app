import React from 'react';
import { Container } from './HosptialsFilterStyle';
import { SystemIcons, Colors } from '../../constants/theme';
import { MenuList, QText } from '../../components';

const hospitalFilter = [
  {
    icon: SystemIcons.hospitalize,
    title: 'Internação',
    titleColor: Colors.Purple,
    link: 'Hospitals',
    params: {type: "1"},
  },
  {
    icon: SystemIcons.drugBox,
    title: 'Pronto-Socorro',
    titleColor: Colors.Purple,
    link: 'Hospitals',
    params: {type: "2"},
  },
  {
    icon: SystemIcons.maternity,
    title: 'Maternidade',
    titleColor: Colors.Purple,
    link: 'Hospitals',
    params: {type: "3"},
  },
  {
    icon: SystemIcons.chatEmergency,
    title: 'Consulta ambulatorial',
    titleColor: Colors.Purple,
    link: 'Hospitals',
    params: {type: "4"},
  },
  {
    icon: SystemIcons.lab,
    title: 'Exames',
    titleColor: Colors.Purple,
    link: 'Hospitals',
    params: {type: "5"},
  },
]

const HospitalsFilterScreen = () => {
  return (
    <Container showsHorizontalScrollIndicator={false} >
      <QText preset="dataText" margin={{ horizontal: 16 }} >Selecione um serviço para pesquisa de nossos parceiros:</QText>
      <MenuList menuData={hospitalFilter} />
    </Container>
  )
}

export default HospitalsFilterScreen;