// auth
export { default as HomeScreen } from './Home';
export { default as SplashScreen } from './Splash';
export { default as LoginScreen } from './Login';
export { default as ForgotPassword } from './ForgotPassword';
export { default as TermsOfUse } from './TermsOfUse';
export { default as MultiHealthCard } from './MultiHealthCard';

// care (now is AGENDA)
export { default as CareScreen } from './Care';
export { default as EventDetails } from './EventDetails';
export { default as ParentsScreen } from './Parents';

// customer care
export { default as CustomerCare } from './CustomerCare';
export { default as ScheduleFilter } from './CustomerCare/Filter';
export { default as Schedule } from './CustomerCare/Schedule';
export { default as ScheduleExpert } from './CustomerCare/ExpertList';
export { default as CredentialNetwork } from './CustomerCare/CredentialNetwork';
export { default as CredentialUnitNetwork } from './CustomerCare/CredentialUnitNetwork';
export { default as Hospitals } from './Hospitals';
export { default as HospitalsFilter } from './HospitalsFilter';
export { default as ExamList } from './ExamList';
export { default as ExamDetails } from './ExamDetails';
export { default as LaboratoryScreen } from './Laboratory';

// health
export { default as VitalSignForm } from './VitalSignForm';
export { default as HealthScreen } from './Health';
export { default as VitalSignChart } from './VitalSignCharts';
export { default as QuestionnaireScreen } from './Questionnaire';
export { default as QuestScreen } from './Questionnaire/Quest';
export { default as QuestionsScreen } from './Questionnaire/Questions';
export { default as MedicalRecordScreen } from './MedicalRecord';
export { default as MedicalConsultScreen } from './MedicalConsult';
export { default as Medications } from './Medications';
export { default as HealthGuidelineScreen } from './Orientation';
export { default as MedicationDetails } from './MedicationDetails';

// account
export { default as AccountScreen } from './Account';
export { default as PasswordScreen } from './Password';
export { default as ProfileScreen } from './Profile';
export { default as BenificiaryManual } from './BenificiaryManual';
export { default as HealthCard } from './HealthCard';
export { default as Payment } from './Payment';
export { default as PaymentList } from './PaymentList';

// Offline
export { default as OfflineScreen } from './Offline';
export { OfflineCardScreen } from './OfflineCard'

// Telemedicine
export { default as TelemedicineScreen } from './Telemedicine';
export { default as TelemedicineErrorScreen } from './TelemedicineError';
export { default as MeetingScreen } from './Telemedicine/Meeting'

// Token
export { default as SecurityTokenScreen } from './SecurityToken';
