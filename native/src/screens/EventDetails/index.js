
import { Linking, } from 'react-native';
import { withMutation } from '@apollo/react-hoc';
import { compose, withState } from 'recompose';
import { connect } from 'react-redux';
import R from 'ramda';
import Config from 'react-native-config';
import EventDetails from './EventDetails';
import { CancelAppointment } from '../../service/mutations';
import { CareActions, AlertActions } from '../../store/actions';
import { SystemImages, Colors } from '../../constants/theme';

const mapStateToProps = ({care}) => ({
  careState: {...care}
});

const mapDispatchToProps = dispatch => ({
  setEvents: event => dispatch(CareActions.actions.setEvents(event)),
  confirmAction: ({ onPress }) =>
    dispatch(
      AlertActions.actions.openAlert({
        title: 'Deseja cancelar esta consulta?',
        image: SystemImages.question,
        titleColor: Colors.Purple,
        description: 'Uma vez que cancelar esta consulta, o horário agendado irá ser liberado.',
        doubleButton: true,
        firstOnPress: () => dispatch(AlertActions.actions.closeAlert()),
        secondOnPress: onPress,
        firstButtonTitle: 'Voltar',
        secondButtonTitle: 'Confirmar',
      }),
    ),
  closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
  onCancelFailed: () =>
    dispatch(
      AlertActions.actions.openAlert({
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        title: 'Erro ao cancelar consulta!',
        isError: true,
        titleColor: Colors.Purple,
        description: 'Ocorreu algum erro ao tentar cancelar sua consulta, tente novamente, ou fale com a gente',
        isSuccess: true,
        shouldRenderOkButton:true,
        link: true,
        linkTitle: 'Falar com o Time Q',
        linkOnPress: () =>
          Linking.openURL(
            `https://api.whatsapp.com/send?phone=${Config.WHATSAPP_LINKNUMBER}`,
          ),
      }),
    ),
  onCancelSuccess: () =>
    dispatch(
      AlertActions.actions.openAlert({
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        title: 'Consulta cancelada',
        titleColor: Colors.Purple,
        description: 'Sua consulta foi cancelada com sucesso',
        isSuccess: true,
        shouldRenderOkButton:true,
      }),
    ),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withState('loading', 'toggleLoading', false),
  withMutation(CancelAppointment, {
    options: ({onCancelFailed, onCancelSuccess, navigation, setEvents, toggleLoading, careState: {events, currentEvent}}) => {
      return {
        variables: {
          appointmentId: navigation.state.params.event.id,
        },
        onError: () => {
          toggleLoading(false);
          onCancelFailed();
        },
        onCompleted: () => {
          const clone = {...currentEvent};
          const cloneEvents = [...events];
          const i = R.findIndex(R.propEq('id', clone.id))(cloneEvents);
          cloneEvents.splice(i,1,clone);
          clone.status = "cancelled";
          toggleLoading(false);
          setEvents(cloneEvents);
          // alert('Consulta cancelada com sucesso!');
          onCancelSuccess();
          navigation.goBack();
        },
      };
    },
  }),
)(EventDetails);
