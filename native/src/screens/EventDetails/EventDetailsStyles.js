import styled from 'styled-components/native';

export const Container = styled.View({
  flex: 1,
});

export const ButtonWrapper = styled.View({
  padding: 16,
});