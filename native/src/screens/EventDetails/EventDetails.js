import React from 'react';
import moment from 'moment';
import { Container, ButtonWrapper } from './EventDetailsStyles';
import { MenuList, QButton } from '../../components';

const EventDetails = props => {
  const {
    mutate,
    toggleLoading,
    loading,
    navigation: {
      state: {
        params: {
          event: { name, address, telecom, start, status, type, doctorName, location },
        },
      },
    },
    confirmAction,
    closeAlert,
  } = props;

  const addressIcon = location === 'Atendimento Virtual' ? 'teleconsulta' : 'map';
  const addressTitle = location === 'Atendimento Virtual' ? location : address;

  const menuData = [
    {
      iconName: type === 1 ? 'doctor' : 'lab',
      visible: !!name,
      title: name,
      disabled: true,
      // size: { height: 40, width: 28},
    },
    {
      iconName: type === 1 ? addressIcon : 'doctor',
      visible: type === 1 ? !!address : !!doctorName,
      title: type === 1 ? addressTitle : doctorName,
      disabled: true,
      // size: { height: 31, width: 29},
    },
    {
      iconName: 'phone_ring',
      visible: !!telecom && !(location === 'Atendimento Virtual'),
      title: telecom || '––',
      disabled: true,
      // size: { height: 27, width: 27},
    },
    {
      iconName:  ["fulfilled", "arrived", "completed"].includes(status) ? 'calendar' : 'schedule',
      visible: !!start,
      title: type === 1 ? moment(start).format('LLL') : moment(start).format('LL'),
      disabled: true,
      // size: { height: 27, width: 26},
    },
  ];

  return (
    <Container>
      <MenuList menuData={menuData} />
      {
      !["cancelled", "fulfilled", "arrived", "completed"].includes(status) &&
        <ButtonWrapper>
          <QButton
            onPress={() => {
              confirmAction({ onPress: ()=> {
                closeAlert(); toggleLoading(true); mutate();
              }})
            }}
            loading={loading}
            error
            text={`Cancelar ${type === 1 ? 'Consulta' : 'Exame'}`}
          />
        </ButtonWrapper>
      }
    </Container>
  );
};

export default EventDetails;
