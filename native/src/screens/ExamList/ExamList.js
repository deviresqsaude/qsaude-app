import React from 'react';
import { ExamCard } from '../../components';
import { Container as ExamListComponent } from './ExamListStyles';

export default function ExamList(props) {
  const { labexam, navigation: { navigate }, examListRefetch } = props;
  return (
    <ExamListComponent
      data={labexam}
      showsVerticalScrollIndicator={false}
      keyExtractor={((item) => `exam-${item.id}`)}
      renderItem={({item}) => {
        return <ExamCard onPress={() => navigate('ExamDetails', {labExam: item, refetch: examListRefetch})} {...item} />;
      }}
    />
  );
}
