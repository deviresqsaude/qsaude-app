import React from 'react';
import styled from 'styled-components/native';
import { FlatList, Linking } from 'react-native';
import { EmptyData } from '../../components';
import { SystemImages } from '../../constants/theme';
import Config from 'react-native-config';

export const EmptyExam = ({description}) => (
  <EmptyData
    imageSource={SystemImages.calendarEmpty}
    title="Nenhum encaminhamento solicitado!"
    description="No momento, não é possível fazer esse agendamento.
    Antes de agendar um exame, você precisa do encaminhamento de um médico, ok?
    Dúvidas?"
    linkText="Fale com o Time Q"
    linkOnPress={() => Linking.openURL(`https://api.whatsapp.com/send?phone=${Config.WHATSAPP_LINKNUMBER}`)}
    />
    
);
// cons

export const Container = styled(FlatList)({
  paddingHorizontal: 16,
});
