import R from 'ramda';
import { connect } from 'react-redux';
import { graphql } from '@apollo/react-hoc';
import { compose, withProps, branch, renderComponent } from 'recompose';
import { AllExams } from '../../service/query';
import ExamList from './ExamList';
import { EmptyExam } from './ExamListStyles';
import { renderEmptyData } from '../../components';
import { MedicationPlaceholder } from '../Medications/MedicationStyles';

// Redux
const mapStateToProps = ({auth}) => ({
  scheduleState: {...auth}
});

const withData = graphql(AllExams, {
  options: {
    variables: {
      params: {
        status: "active",
        category: "108252007,363679005,71388002"
      },
      resultType: null
    },
  }
});

const withDataProps = withProps(({ data : { exams }}) => {
  if (!exams) return null;
  if(exams) {
    const labexam = R.map(item => ({
      name: R.prop('code', item)[0].display, 
      status: R.prop('status', item), 
      id: R.prop('id', item),
      doctorName: R.prop('requester', item).display,
      tussCode: R.prop('tussCode', item)[0],
    }), exams);
    return { labexam }
  }
})

const renderForLoading = branch(({data}) => data && data.loading, renderComponent(MedicationPlaceholder));

const setApolloRefetch = withProps(({data}) => ({examListRefetch: data && data.refetch}));

export default compose(
  connect(mapStateToProps, null),
  withData,
  withDataProps,
  renderForLoading,
  setApolloRefetch,
  renderEmptyData(EmptyExam, 'exams'),
)(ExamList);
