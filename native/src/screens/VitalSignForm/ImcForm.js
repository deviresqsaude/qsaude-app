/* eslint-disable react/prop-types */
import React from 'react';
import moment from 'moment';
import { Field } from 'formik';
import { KeyboardAwareScrollView as Content } from 'react-native-keyboard-aware-scrollview';
import {
  QTextInput,
  QButton,
  FakeTextInput,
  DateTimePicker,
  Container,
} from '../../components';

import VSCons from '../../constants/VitalSignForm';
// import { BOTTOMTABBAR } from '../../constants/TestIds';

const ImcForm = ({
  calculateImc,
  onDateChange,
  values: { measuredAt },
  isValid,
  handleSubmit,
  isSubmitting,
}) => {
  const scrollEl = React.useRef(null);
  const [pickerOpen, togglePicker] = React.useState(false);

  const openDatePicker = () => {
    togglePicker(true);
    if (scrollEl.current) {
      setTimeout(() => scrollEl.current.scrollToBottom(), 200);
    }
  };

  const handleDateChange = (e, date) => onDateChange(e, date, togglePicker)


  return (
    <>
      <>
        <Content
          ref={scrollEl}
          contentContainerStyle={{
            paddingHorizontal: 16,
          }}
          showsVerticalScrollIndicator={false}
        >
          <Field
            keyboardType="numeric"
            validateOnBlur={calculateImc}
            name="weight"
            returnKeyType="done"
            label={VSCons.imc.Labels.weight}
            component={QTextInput}
          />
          <Field
            returnKeyType="done"
            keyboardType="numeric"
            validateOnBlur={calculateImc}
            name="height"
            label={VSCons.imc.Labels.height}
            component={QTextInput}
          />

          <Field
            disabled
            name="imc"
            label={VSCons.imc.Labels.imc}
            component={QTextInput}
          />

          <Field
            isInputFocused={pickerOpen}
            disabled
            value={String(moment(measuredAt).format('DD/MM/YYYY'))}
            name="measuredAt"
            label="Quando"
            onPress={openDatePicker}
            component={FakeTextInput}
          />

          <QButton
            onPress={handleSubmit}
            loading={isSubmitting}
            disabled={!isValid}
            text="Enviar"
          />
        </Content>
      </>
      {pickerOpen && (
        <DateTimePicker
          value={measuredAt}
          onChange={handleDateChange}
          closePicker={() => togglePicker(false)}
        />
      )}
    </>
  );
};

export default ImcForm;
