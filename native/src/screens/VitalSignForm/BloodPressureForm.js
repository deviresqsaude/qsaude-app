/* eslint-disable react/prop-types */
import React from 'react';
import moment from 'moment';

import { Field } from 'formik';
import { KeyboardAwareScrollView as Content } from 'react-native-keyboard-aware-scrollview';
import {
  QTextInput,
  QButton,
  FakeTextInput,
  DateTimePicker,
  Container,
} from '../../components';
import VSCons from '../../constants/VitalSignForm';
import { BOTTOMTABBAR } from '../../constants/TestIds';

const GlycemiaForm = ({
  isValid,
  handleSubmit,
  isSubmitting,
  onDateChange,
  values: { measuredAt },
}) => {
  const [pickerOpen, togglePicker] = React.useState(false);
  const scrollEl = React.useRef(null);

  const handleDateChange = (e, date) => onDateChange(e, date, togglePicker);

  const openDatePicker = () => {
    togglePicker(true);
    if (scrollEl.current) {
      setTimeout(() => scrollEl.current.scrollToBottom(), 200);
    }
  };
  return (
    <Container>
      <Container>
        <Content ref={scrollEl} contentContainerStyle={{ paddingHorizontal: 16 }}>
          <Field
            testID={BOTTOMTABBAR.tabHealth.dadosVitaisPressaoArterial.maxSistolica}
            keyboardType="numeric"
            name="systolic"
            returnKeyType="done"
            label={VSCons.bloodPressure.Labels.systolic}
            component={QTextInput}
          />
          <Field
            testID={BOTTOMTABBAR.tabHealth.dadosVitaisPressaoArterial.minDiastolica}
            keyboardType="numeric"
            name="diastolic"
            returnKeyType="done"
            label={VSCons.bloodPressure.Labels.diastolic}
            component={QTextInput}
          />

          <Field
            isInputFocused={pickerOpen}
            disabled
            value={String(moment(measuredAt).format('DD/MM/YYYY'))}
            name="measuredAt"
            label="Quando"
            onPress={openDatePicker}
            component={FakeTextInput}
          />

          <QButton
            testID={BOTTOMTABBAR.tabHealth.dadosVitaisPressaoArterial.submit}
            onPress={handleSubmit}
            loading={isSubmitting}
            disabled={!isValid}
            text="Enviar"
          />
        </Content>
      </Container>
      {pickerOpen && (
        <DateTimePicker
          value={measuredAt}
          onChange={handleDateChange}
          closePicker={() => togglePicker(false)}
        />
      )}
    </Container>
  );
};

export default GlycemiaForm;
