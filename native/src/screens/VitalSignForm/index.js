import { Platform } from 'react-native';
import moment from 'moment';
import { withFormik } from 'formik';
import { compose, withHandlers } from 'recompose';
import { connect } from 'react-redux';
import { withApollo } from '@apollo/react-hoc';
import { validateInsertVitalSignForm } from '../../validations';
import VitalSignForm from './VitalSignForm';
import InsertVitalSignStrings from '../../constants/VitalSignForm';
import { InsertVitalSign } from '../../service/mutations';
import { AlertActions } from '../../store/actions';

// Error Handling
const mapDispatchToProps = dispatch => ({
  onServiceFailed: ({ error: description }) =>
    dispatch(
      AlertActions.actions.openAlert({
        title: 'Falha ao Inserir medida',
        shouldRenderOkButton: true,
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        description,
        isError: true,
      }),
    ),
  onServiceSuccess: () =>
    dispatch(
      AlertActions.actions.openAlert({
        description: null,
        shouldRenderOkButton: true,
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        title: 'Medida inserida com sucesso!',
        isSuccess: true,
      }),
    ),
});

export const VitalSignType = {
  Height: 'height',
  Weight: 'weight',
  Imc: 'imc',
  BloodPressure: 'bloodPressure',
  BloodType: 'bloodType',
  Glycemia: 'glycemia',
  DiastolicPressure: 'diastolicPressure',
  MeanPressure: 'meanPressure',
  SystolicPressure: 'systolicPressure',
};

const createVitalSignMapper = type => values => {
  if (type === VitalSignType.Imc) {
    return [
      {
        type: VitalSignType.Height,
        measuredAt: moment(values.measuredAt).toISOString(),
        measureUnit: InsertVitalSignStrings.imc.Units.height,
        measure: {
          amount: Number(values.height),
          unit: InsertVitalSignStrings.imc.Units.height,
        },
      },
      {
        type: VitalSignType.Weight,
        measureUnit: InsertVitalSignStrings.imc.Units.weight,
        measuredAt: moment(values.measuredAt).toISOString(),
        measure: {
          amount: Number(values.weight),
          unit: InsertVitalSignStrings.imc.Units.weight,
        },
      },
      {
        type: VitalSignType.Imc,
        measureUnit: InsertVitalSignStrings.imc.Units.imc,
        measuredAt: moment(values.measuredAt).toISOString(),
        measure: {
          amount: Number(values.imc),
          unit: InsertVitalSignStrings.imc.Units.imc,
        },
      },
    ];
  }
  if (type === VitalSignType.Glycemia) {
    return [
      {
        type: VitalSignType.Glycemia,
        measuredAt: moment(values.measuredAt).toISOString(),
        measure: {
          amount: Number(values.glycemia),
          unit: InsertVitalSignStrings.glycemia.Unit,
        },
      },
    ];
  }
  if (type === VitalSignType.BloodPressure) {
    return [
      {
        type: VitalSignType.BloodPressure,
        measuredAt: moment(values.measuredAt).toISOString(),
        subMeasures: [
          {
            type: VitalSignType.DiastolicPressure,
            measuredAt: moment(values.measuredAt).toISOString(),
            measure: {
              amount: Number(values.diastolic),
              unit: InsertVitalSignStrings.bloodPressure.Unit,
            },
          },
          {
            type: VitalSignType.SystolicPressure,
            measuredAt: moment(values.measuredAt).toISOString(),
            measure: {
              amount: Number(values.systolic),
              unit: InsertVitalSignStrings.bloodPressure.Unit,
            },
          },
        ],
      },
    ];
  }
  return {};
};

/**
 * LoginForm Logic
 */
const withForm = withFormik({
  enableReinitialize: true,
  validate: (values, props) => validateInsertVitalSignForm(values, props),
  mapPropsToValues: () => ({ measuredAt: new Date() }),
  handleSubmit: async (values, formik) => {
    const {
      setSubmitting,
      props: {
        onServiceFailed,
        onServiceSuccess,
        client,
        // testID,
        navigation: {
          goBack,
          state: {
            params: { vitalSign: type },
          },
        },
      },
    } = formik;

    const vitalSignMapper = createVitalSignMapper(type);
    const dataReadyForMutation = vitalSignMapper(values);
    try {
      await client.mutate({
        mutation: InsertVitalSign,
        refetchQueries: ['RequestVitalSigns', 'RecentVitalSigns'],
        variables: {
          data: dataReadyForMutation,
        },
      });

      onServiceSuccess();
      // alert('Medida cadastrada com sucesso');
      // if (response === '') {
      // } else {
      // }
    } catch (e) {
      onServiceFailed({ error: e.message });
    }

    goBack();
    setSubmitting(false);
  },
});

const withCalculateImc = withHandlers({
  calculateImc: props => () => {
    const {
      handleChange,
      values: { weight, height },
    } = props;

    let imc = 0;
    if (weight && height) {
      imc = Number(weight) / (Number(height) / 100) ** 2;
    }
    const formattedImc = Number(imc)
      .toFixed(2)
      .toString();

    handleChange(VitalSignType.Imc)(formattedImc);
  },
});

const withSelectedDate = withHandlers({
  onDateChange: props => (e, date, togglePicker) => {
    const { handleChange } = props;
    if (Platform.OS === 'android') {
      // android uses a calendar modal
      togglePicker(false);
    }

    if (date) {
      handleChange('measuredAt')(date);
    }
  },
});

export default compose(
  connect(
    null,
    mapDispatchToProps,
  ),
  // graphql(InsertVitalSign),
  withApollo,
  withForm,
  withCalculateImc,
  withSelectedDate,
)(VitalSignForm);
