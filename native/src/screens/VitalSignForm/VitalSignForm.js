import React from 'react';
import ImcForm from './ImcForm';
import GlycemiaForm from './GlycemiaForm';
import BloodPressureForm from './BloodPressureForm';

const VitalSignForm = props => {
  const {
    navigation: {
      state: {
        params: { vitalSign },
      },
    },
  } = props;

  if (vitalSign === 'imc') {
    return <ImcForm {...props} />;
  }
  if (vitalSign === 'glycemia') {
    return <GlycemiaForm {...props} />;
  }

  if (vitalSign === 'bloodPressure') {
    return <BloodPressureForm {...props} />;
  }

  return null;
};

export default VitalSignForm;
