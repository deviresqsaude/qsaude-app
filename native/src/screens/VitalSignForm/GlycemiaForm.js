/* eslint-disable react/prop-types */
import React from 'react';
import { Field } from 'formik';
import moment from 'moment';
import { KeyboardAwareScrollView as Content } from 'react-native-keyboard-aware-scrollview';
import {
  QTextInput,
  QButton,
  FakeTextInput,
  DateTimePicker,
  Container,
} from '../../components';
import VSCons from '../../constants/VitalSignForm';
import { BOTTOMTABBAR } from '../../constants/TestIds';

const GlycemiaForm = ({
  isValid,
  handleSubmit,
  isSubmitting,
  onDateChange,
  values: { measuredAt },
}) => {
  const scrollEl = React.useRef(null);
  const [pickerOpen, togglePicker] = React.useState(false);

  const openDatePicker = () => {
    if (pickerOpen) return;
    togglePicker(true);
    if (scrollEl.current) {
      setTimeout(() => scrollEl.current.scrollToBottom(), 200);
    }
  };

  const handleDateChange = (e, date) => onDateChange(e, date, togglePicker)

  return (
    <Container>
      <Container>
        <Content ref={scrollEl} contentContainerStyle={{ paddingHorizontal: 16 }}>
          <Field
            testID={BOTTOMTABBAR.tabHealth.dadosVitaisGlicemia.glicemia}
            keyboardType="numeric"
            name="glycemia"
            returnKeyType="done"
            label={VSCons.glycemia.Labels.glycemia}
            component={QTextInput}
          />
          <Field
            isInputFocused={pickerOpen}
            disabled
            value={String(moment(measuredAt).format('DD/MM/YYYY'))}
            name="measuredAt"
            label="Quando"
            onPress={openDatePicker}
            component={FakeTextInput}
          />

          <QButton
            testID={BOTTOMTABBAR.tabHealth.dadosVitaisGlicemia.submit}
            onPress={handleSubmit}
            loading={isSubmitting}
            disabled={!isValid}
            text="Enviar"
          />
        </Content>
      </Container>
      {pickerOpen && (
        <DateTimePicker
          value={measuredAt}
          onChange={handleDateChange}
          closePicker={() => togglePicker(false)}
        />
      )}
    </Container>
  );
};

export default GlycemiaForm;
