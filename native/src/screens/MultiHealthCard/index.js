import React from 'react';
import R from 'ramda';
import { compose, withState, withHandlers, branch, renderComponent } from "recompose";
import { graphql, withApollo } from '@apollo/react-hoc';
import { Alert } from 'react-native';
import SyncStorage from 'sync-storage';
import { AllHealthPlanCardQuery, LoginV2Query, LoginQuery as OldLogin } from "../../service/query";
import MultiHealthCard from './MultiHealthCard';
import { QLoading } from '../../components';

const healthCardsState = withState('healthCards', 'setHealthCards', false);
const submittingState = withState('isSubmitting', 'setSubmitting', false);
const currentCardState = withState('healthCardNumber', 'setHealthCardNumber', {});

const handlers = withHandlers({
  logOut: props => () => {
    SyncStorage.remove('token');
    SyncStorage.remove('@form[login]:user');
    props.setLoading(false);
    props.navigation.goBack();
  },
  changeToken: ({ client, setSubmitting, healthCardNumber, navigation: { navigate, state: { params: {profileUser, password, beneficiary, deviceId}} } }) => async () => {
    setSubmitting(true);
    const request = await client.query({
      // query: OldLogin,
      query: LoginV2Query,
      variables: {
        user: healthCardNumber.numCard,
        password,
        deviceId
      }
    });
    const { errors, data } = request;
    const hasError = errors ? errors.length > 0 : false;
    if (hasError) {
      setSubmitting(false);
      Alert.alert(
        "Erro ao selecionar carteirinha",
        errors[0].message,
        [{text: 'OK', onPress: () => {}}],
      );
      return;
    };
    // const { login: { token, personalId } } = data;
    const { loginV2: { token, personalId } } = data;
    navigate(`TermsOfUse`, { token, personalId, profileUser, healthCardNumber, beneficiary});
  }
});
// Queries
const withPlanCardQuery = graphql(AllHealthPlanCardQuery, {
  name: "planCardData",
  options: ({
    setHealthCards,
    setHealthCardNumber,
    navigation: {
      state: {params: {personalId, token}},
    }
  }) => ({
    notifyOnNetworkStatusChange: true,
    variables: {id: personalId},
    context: {headers: { authorization: token }},
    onCompleted: ({getAllHealthPlanCardUser: cardUser}) => {
      setHealthCardNumber(cardUser[0]);
      setHealthCards(R.filter(R.propEq('status', true), cardUser));
    }
  })
});

const withLoading = branch(({planCardData}) => {
  return !planCardData || planCardData.loading;
}, renderComponent(() => <QLoading visible simple/>));

const enhance = compose(
  withApollo,
  submittingState,
  currentCardState,
  healthCardsState,
  withPlanCardQuery,
  handlers,
  withLoading,
);

export default enhance(MultiHealthCard);
