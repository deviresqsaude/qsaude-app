import styled from 'styled-components/native';
import { Colors } from '../../constants/theme';

export const SwiperItem = styled.View({
  justifyContent: 'center',
  alignItems: 'center',
  paddingTop: 8,
  paddingBottom: 16
});

export const CardsScreenWrapper = styled.View({
  flex: 1,
  backgroundColor: Colors.White,
  justifyContent: "space-between",
  width: "100%",
  height: "100%"
});

export const SelectCardContainer = styled.ScrollView({
  width: '100%',
  // flex: 1,
  paddingBottom: 16,
});

export const PaddingWrapper = styled.View({
  paddingHorizontal: 16
});

export const CardStatusText = styled.Text(({status}) => ({
  color: status ? Colors.GreenSuccess : Colors.Red,
  fontWeight: 'bold',
  fontSize: 12,
}));

export const CardStatusImage = styled.Image({
  width: 22,
  height: 22,
  marginRight: 4
});

export const CardStatusWrapper = styled.View({
  alignSelf: 'flex-end',
  flexDirection: 'row',
  alignItems: 'center'
});

export const LinkContainer = styled.TouchableOpacity({
  width: '100%',
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'row'
});
