import React from 'react';
import R from 'ramda';
import moment from 'moment';
import { View, Linking, Image } from 'react-native';
import Swiper from 'react-native-swiper';
import Config from 'react-native-config';

import { QText, QButton } from '../../components';
import {
  CardsScreenWrapper,
  SelectCardContainer,
  SwiperItem,
  PaddingWrapper,
  CardStatusText,
  CardStatusImage,
  CardStatusWrapper,
  LinkContainer
} from './MultiHealthCardStyle';
import {
  HealthCardWrapper,
  HealthCardBody,
  HealthCardLogo,
} from '../HealthCard/HealthCardStyle';
import {
  BottomWaves,
  QLogo
} from '../Login/LoginStyle'
import { PROFILEUSER } from '../../constants/TestIds';
import { elevationShadow } from '../../helpers';
import { Colors, SystemIcons, SystemImages, Dots } from '../../constants/theme';
import HealthCardLoading from '../HealthCard/HealthCardLoading';

const SelectCard = ({ planCardData,  navigation: {goBack, state: {params: {profileUser}}}, healthCards, healthCardNumber, setHealthCardNumber, changeToken, isSubmitting }) => {
  const swiperConfig = {
    onIndexChanged: (index) => setHealthCardNumber(healthCards[index]),
    style: {height: 290},
    index: 0,
    loop: false,
    dot: <View style={{...Dots.styleDot}}/>,
    activeDot: <View style={{...Dots.styleActiveDot}}/>,
    scrollEnabled: !isSubmitting
  };
  return (
    <CardsScreenWrapper>
      <QLogo />
      <SelectCardContainer showsVerticalScrollIndicator={false}>
        <PaddingWrapper>
        <QText color={Colors.Purple} fontSize={12} preset="bold">Dependentes: {healthCards.length}</QText>
        </PaddingWrapper>
        {
          !planCardData || planCardData.loading || !planCardData.getAllHealthPlanCardUser ? 
          <View style={{paddingVertical: 22}}><HealthCardLoading /></View>
          : 
          <Swiper {...swiperConfig} >
          {
            planCardData.getAllHealthPlanCardUser &&
            planCardData.getAllHealthPlanCardUser.length > 0 &&
            planCardData.getAllHealthPlanCardUser.map((card, index) => {
              const newCard = {...card, segmentPlan: card.segmentPlan || profileUser.segmentPlan};
              const { numCard, name, healthPlanCode, startingDate, segmentPlan, status } = newCard;
              return (
                // eslint-disable-next-line react/no-array-index-key
                <SwiperItem key={index}>
                  <HealthCardWrapper style={{ ...elevationShadow('#000000', 8, 0.5) }}>
                    <HealthCardBody>
                      <View
                        style={{
                          height: 40,
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                        }}>
                        <HealthCardLogo />
                        <QText
                          testID={PROFILEUSER.checkVcard.registration}
                          color={Colors.Purple}
                          fontSize={14}
                          preset="bold">
                          {numCard || '---'}
                        </QText>
                      </View>
                      <View>
                        <View style={{ marginBottom: 4, width: '100%' }}>
                          <QText
                            testID={PROFILEUSER.checkVcard.clientName}
                            color={Colors.Purple}
                            fontSize={12}
                            preset="bold">
                            {name || '---'}
                          </QText>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                          <View style={{ flexDirection: 'column', flex: 1 }}>
                            <View>
                              <QText color={Colors.Gray} fontSize={10} preset="bold">
                                Meu plano
                              </QText>
                              <QText
                                testID={PROFILEUSER.checkVcard.healthPlanCode}
                                color={Colors.Purple}
                                fontSize={12}
                                preset="bold">
                                {healthPlanCode || '---'}
                              </QText>
                            </View>
                            <View>
                              <QText color={Colors.Gray} fontSize={12} preset="bold">
                                Inicio vigência
                              </QText>
                              <QText
                                testID={PROFILEUSER.checkVcard.startingDate}
                                color={Colors.Purple}
                                fontSize={12}
                                preset="bold">
                                {startingDate ? moment(startingDate).format('DD/MM/YY') : '---'}
                              </QText>
                            </View>
                          </View>
                          <View style={{ flexDirection: 'column', flex: 1 }}>
                            <View>
                              <QText color={Colors.Gray} fontSize={10} preset="bold">Segmentação</QText>
                              <QText
                                testID={PROFILEUSER.checkVcard.segmentPlan}
                                color={Colors.Purple}
                                fontSize={12}
                                preset="bold">
                                {segmentPlan || '---'}
                              </QText>
                            </View>
                            <View />
                          </View>
                        </View>
                        <CardStatusWrapper>
                        <CardStatusImage source={ status ? SystemImages.successImage:  SystemImages.errorImage } />
                        <CardStatusText status={status}>{ status ? "Ativo" : "Inativo" } </CardStatusText>
                      </CardStatusWrapper>
                      </View>
                    </HealthCardBody>
                  </HealthCardWrapper>
                </SwiperItem>
              );
            })
          }
        </Swiper>
        }
        <PaddingWrapper>
          <QButton text="Continuar" onPress={() => changeToken()} loading={isSubmitting} disabled={R.isEmpty(healthCardNumber) || isSubmitting}/>
          <QButton error text="Cancelar" onPress={() => goBack()} disabled={isSubmitting}/>
          <LinkContainer>
            <Image source={SystemIcons.whatsapp} style={{width: 16, height: 16, marginRight: 4}}/>
            <QText
              preset="link"
              onPress={() => isSubmitting ? () => {} : Linking.openURL(
                `https://api.whatsapp.com/send?phone=${Config.WHATSAPP_LINKNUMBER}`,
              )}
              style={{ textDecorationLine: 'underline' }}>
              Carteirinha inativa?
            </QText>
          </LinkContainer>
        </PaddingWrapper>
      </SelectCardContainer>
      <BottomWaves />
    </CardsScreenWrapper>
  );
};

export default SelectCard;
