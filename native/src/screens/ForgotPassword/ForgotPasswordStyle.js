import styled from 'styled-components/native';
import LOGO from '../../../assets/images/img_logo.png';

// Container
export const Container = styled.ScrollView`
  padding-left: 16px;
  padding-right: 16px;
  padding-top: 40px;
  background-color: white;
`;

export const QsaudeLogo = styled.Image.attrs({
  source: LOGO,
  resizeMode: 'contain',
})`
  width: 200px;
  margin-top: 5px;
  margin-bottom: 22px;
  align-self: center;
`

Container.defaultProps = {
  contentContainerStyle: {
    alignItems: 'center',
    flex: 1,
  },
};
