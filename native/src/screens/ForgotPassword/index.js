import { withFormik } from 'formik';
import { compose, withState, withHandlers } from 'recompose';
import { withNavigation } from 'react-navigation';
import { graphql } from '@apollo/react-hoc';
import * as yup from 'yup';
import { ResetPassword } from '../../service/mutations';
import ForgotPasswordScreen from './ForgotPasswordScreen';

const ResetPasswordScheme = yup.object().shape({
  user: yup.string()
  .min(11, 'Mínimo de 11 dígitos')
  .required('Campo obrigatório')
})

const withForm = withFormik({
  mapPropsToValues: () => ({
    user: ''
  }),
  validationSchema: ResetPasswordScheme,
  handleSubmit: async (values, formikBag) => {
    const {props:{forgotPassword, setRequestAlert}, setSubmitting} = formikBag;
    const variables = {
      user: values.user
    }
    const {data} = await forgotPassword({variables});
    setSubmitting(false);
    setRequestAlert(!data.forgotPassword ? 'error' : 'success');
  },
});

const withMutation = graphql(ResetPassword, {name: "forgotPassword"});

export default compose(
  withState('requestAlert', 'setRequestAlert', null),
  withHandlers({
    goToLogin: ({setRequestAlert, navigation: {goBack}}) => () => {
      setRequestAlert(null);
      goBack();
    }
  }),
  withNavigation,
  withMutation,
  withForm,
)(ForgotPasswordScreen);
