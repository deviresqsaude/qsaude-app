/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'formik';
import { Container, QsaudeLogo } from './ForgotPasswordStyle';
import { QTextInput, QButton } from '../../components';
import { FORGOTPASSWORD } from '../../constants/TestIds';
import QAlert from '../../components/QAlert';

const MESSAGE = {
  error: "Ops!! Aconteceu um erro. Tente novamente, por favor.",
  success: "Muito obrigado! Em breve você receberá um e-mail com as instruções para sua nova senha!"
}

const ForgotPasswordScreen = props => {
  const {
    isSubmitting,
    isValid,
    handleSubmit,
    requestAlert,
    setRequestAlert,
    goToLogin,
    navigation: {goBack}
  } = props;
  return (
    <>
      <Container>
        <QsaudeLogo/>
        <Field
          testID={FORGOTPASSWORD.form.forgotPasswordEmailField}
          label="CPF ou carterinha"
          name="user"
          keyboardType="numeric"
          disabled={isSubmitting}
          component={QTextInput}
        />
        <QButton
          testID={FORGOTPASSWORD.form.forgotPasswordSubmitButton}
          loading={isSubmitting}
          disabled={!isValid || isSubmitting}
          text="Envie-me uma nova senha"
          onPress={handleSubmit}
        />
        <QButton
          style={{marginTop: 15}}
          error
          testID={FORGOTPASSWORD.form.forgotPasswordSubmitButton}
          disabled={isSubmitting}
          text="Cancelar"
          onPress={() => goBack()}
        />
      </Container>
      <QAlert
        visible={requestAlert !== null}
        isSuccess={requestAlert === 'success'}
        isError={requestAlert === 'error'}
        description={MESSAGE[requestAlert === 'error' ? 'error' : 'success']}
        closeAlert={() => requestAlert === 'error' ?
          setRequestAlert(null) :
          goToLogin()
        }
      />
    </>
  );
};

ForgotPasswordScreen.propTypes = {
  isSubmitting: PropTypes.bool.isRequired,
  isValid: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

ForgotPasswordScreen.defaultProps = {};

export default ForgotPasswordScreen;
