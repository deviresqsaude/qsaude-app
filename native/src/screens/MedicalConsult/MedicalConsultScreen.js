import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';
import { Container } from './MedicalConsultStyle';

const MedicalConsultScreen = props => {
  const {title} = props;
  return (
    <Container>
      <Text>{title}</Text>
    </Container>
  )
};

MedicalConsultScreen.propTypes = {
  title: PropTypes.string
};

export default MedicalConsultScreen;
