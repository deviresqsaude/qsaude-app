import { compose, withProps } from 'recompose';
import MedicalConsultScreen from './MedicalConsultScreen';

const enhance = compose(
  withProps((props) => ({}))
);

export default enhance(MedicalConsultScreen);
