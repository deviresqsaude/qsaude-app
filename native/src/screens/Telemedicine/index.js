// import { Platform } from 'react-native';
import { Platform } from 'react-native';
import { withApollo } from '@apollo/react-hoc';
import SyncStorage from 'sync-storage';
import { compose, withProps, withState, withHandlers, lifecycle } from "recompose";
import { withNavigation, withNavigationFocus } from 'react-navigation';
import { connect } from 'react-redux';
import { AlertActions } from '../../store/actions';
import TelemedicineScreen from "./TelemedicineScreen";
import { SystemImages, Colors } from '../../constants/theme';

import ConexaAndroid from '../../modules/conexa/conexa.android';
import ConexaIos from '../../modules/conexa/conexa.ios';

import ObserveAttendanceEmergency from '../../service/subscription/observeEmergency';

let subscription = null;
let telemedicineSubscription = null;

const mapDispatchToProps = dispatch => ({
  confirmAction: ({ navigate }) =>
    dispatch(
      AlertActions.actions.openAlert({
        buttonTitle: 'Sim, quero cancelar',
        shouldRenderOkButton: true,
        buttonOnPress: () => {dispatch(AlertActions.actions.closeAlert()); SyncStorage.remove('callId'); navigate('App')},
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        title: 'Cancelar consulta',
        image: SystemImages.question,
        titleColor: Colors.Purple,
        description: 'Você gostaria de cancelar a consulta',
        link: true,
        linkTitle: 'voltar',
        linkOnPress: () => dispatch(AlertActions.actions.closeAlert()),
      }),
    ),
});

const lifeCycle = lifecycle({
  componentDidMount() {
    const { params, setInitCall, setMeetingStatus, client, initConnection } = this.props;
    const conexa = Platform.OS === 'ios' ? ConexaIos : ConexaAndroid;
    const profile = SyncStorage.get('profileUser');

    if(params && params.meetingNumber) {
      const { meetingNumber } = params;
      this.props.initConnection({ meetingNumber });
      SyncStorage.set('callId', meetingNumber)
    }
    subscription = conexa.addListener(
      'endMeeting',
      (meeting) => {
        console.log(meeting)
        if(meeting.status =='finished') {
          setInitCall(false)
          setMeetingStatus('finished');
        }
      }
    );

    telemedicineSubscription = client.subscribe({
      query: ObserveAttendanceEmergency,
      variables: { cpf: profile.cpf },
    }).subscribe({
      next: (event) => {
        const { data } = event;
        if(data) {
          const { observerAttendanceEmergency: { meetingId } } = data;
          initConnection({ meetingNumber : meetingId });
          SyncStorage.set('callId', meetingId);
        }
       },
      error: (err) => {
        console.error("err", err);
      }
    })
  },
  componentWillUnmount() {
    subscription.remove()
    telemedicineSubscription.unsubscribe();

  }
});

const enhance = compose(
  connect(
    null,
    mapDispatchToProps
  ),
  withApollo,
  withState('loading', 'setLoading', false),
  withState('initCall', 'setInitCall', false),
  withState('meetingStatus', 'setMeetingStatus', 'loading'),
  withNavigation,
  withProps(({ navigation: { state: { params } } }) => ({
    params
  })),
  withNavigationFocus,
  withHandlers({
    initConnection: props => async ({ meetingNumber }) => {
      const { setLoading, setInitCall } = props;
      const profile = SyncStorage.get('profileUser');
      const conexa = Platform.OS === 'ios' ? ConexaIos : ConexaAndroid;

      if (!meetingNumber) return false;
      setLoading(true);
      // eslint-disable-next-line radix
      // await conexa.getEndingConnection();
      const res = await conexa.getConexaConnection(parseInt(meetingNumber), profile.completeName || "Paciente QSaude");
      setInitCall(true)
      return res;
    }
  }),
  lifeCycle,
  // withSubscriptionMeeting,
);

export default enhance(TelemedicineScreen);
