import styled from 'styled-components/native';
import { hasDeviceNotch } from '../../components/HeaderNav/HeaderNavStyle';

export const Container = styled.View({
  flex: 1,
  alignItems: "center",
  justifyContent: "center",
  padding: 16,
  paddingTop: hasDeviceNotch(),
})

export const WaitImage = styled.Image({
  marginBottom: 23
});

export const QuestContainer = styled.View({
  backgroundColor: "white",
  borderRadius: 14,
  marginTop: 18,
  paddingHorizontal: 14,
  paddingTop: 12,
  marginHorizontal: 20
});

export const HeaderTitle = styled.View({
  position: "absolute",
  left: 20,
  top: 32
});

export const TipContainer = styled.View({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
  height: '15%',
  marginBottom: 18
})

export const Tip = styled.View({
  width: '80%',
  height: '100%',
  textAlign: 'center',
})
