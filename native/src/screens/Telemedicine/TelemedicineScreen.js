import React, { useEffect } from 'react';
import { Text } from 'react-native';
import SyncStorage from 'sync-storage';
import { useSubscription } from '@apollo/react-hooks';
import Swiper from 'react-native-swiper';
import ObserveAttendanceEmergency from '../../service/subscription/observeEmergency';
import { Container, WaitImage, HeaderTitle, TipContainer, Tip } from './TelemedicineStyle';
import { QButton } from '../../components/Button';
import QText from '../../components/QText';
import { SystemImages, Colors, SystemIcons } from '../../constants/theme';
import { ErrorScreen } from '../../components/Error';

const swiperConfig = {
  loop: true,
  autoplay: true,
  showsPagination: false,
  autoplayTimeout: 4,
  showsHorizontalScrollIndicator: false,
}

const TelemedicineScreen = ({ confirmAction, navigation, initCall, meetingStatus }) => {

  // useSubscription(ObserveAttendanceEmergency, {
  //   variables: { cpf: "98967284020" },
  //   onSubscriptionData: (props) => {
  //     console.log('onsubscriptiondata', props)
  //     alert(props.subscriptionData.data.observerAttendanceEmergency.meetingId)
  //     if(!params || params && !params.meetingNumber) {      
  //       const { meetingId } = props.subscriptionData.data.observerAttendanceEmergency;
  //       initConnection({ meetingNumber : meetingId });
  //       SyncStorage.set('callId', meetingId);
  //     }
  //   },
  //   // shouldResubscribe: false
  // });

  const error = null;
  
  return (
    <Container>
      {
        error ? <ErrorScreen image={SystemIcons.close} title={error}/> 
        : 
        <>
        {
          meetingStatus != "finished" ? 
          <>
            <HeaderTitle>
              <QText preset="navTitle">{'Conectando'} </QText>
            </HeaderTitle>
            <WaitImage source={SystemImages.waitingCall}/>
            <Text style={{color: Colors.Purple, fontSize: 16, textAlign: "center", marginBottom: 20, marginHorizontal: 20}}>{initCall ? 'Obrigado por usar os nossos serviços!' : 'Estamos selecionando o melhor médico para você!'}</Text>
            <TipContainer>
              <QText preset="dataLabel" style={{fontWeight: 'bold', marginRight: 4 }}>Dica:</QText>
              <Tip>
                <Swiper {...swiperConfig}>
                  <QText preset="body" style={{textAlign: 'center'}} >Fique atento! É importante estar em um local reservado e iluminado para sua consulta.</QText>
                  <QText preset="body" style={{textAlign: 'center'}} >Lembre-se de autorizar o acesso ao aúdio e vídeo do seu aparelho.</QText>
                  <QText preset="body" style={{textAlign: 'center'}} >Wi-Fi é melhor para a transmissão, mas evite redes públicas.</QText>
                  <QText preset="body" style={{textAlign: 'center'}} >Veja se a bateria do seu aparelho vai aguentar até o final da consulta.</QText>
                  <QText preset="body" style={{textAlign: 'center'}} >Verifique se o perfil do usuário selecionado é o mesmo de quem fará a consulta. E deixe um documento com foto por perto. A gente vai precisar.</QText>
                  <QText preset="body" style={{textAlign: 'center'}} >Se depois você ainda precisar de uma consulta presencial, é só agendar uma visita com seu Médico Pessoal.</QText>
                  <QText preset="body" style={{textAlign: 'center'}} >Ah! E fique tranquilo, se a ligação cair em até dois minutos nossa equipe entrará em contato novamente</QText>
                </Swiper>
              </Tip>
            </TipContainer>
            <QButton text="Cancelar" error onPress={() => confirmAction({ navigate: navigation.navigate })}/>
          </>
          :
          <>
            <HeaderTitle>
              <QText preset="navTitle">{'Consulta finalizada'} </QText>
            </HeaderTitle>
            <WaitImage source={SystemImages.finishCall}/>
            <Text style={{color: Colors.Purple, fontSize: 16, textAlign: "center", marginBottom: 20, marginHorizontal: 20}}>{initCall ? 'Obrigado por usar os nossos serviços!' : 'Estamos selecionando o melhor médico para você!'}</Text>
            <QText preset="body" style={{textAlign: 'center', marginBottom: 20}}>Sempre que precisar conte com a gente, estamos aqui pra sua saúde!</QText>
            <QButton text="Voltar para Tela inicial" error onPress={() => { navigation.navigate('App'); SyncStorage.remove('callId'); }}/>
          </>
        }
        </>
      }
    </Container>
  )
}

TelemedicineScreen.propTypes = {}

export default TelemedicineScreen;








