import React from "react";
// import PropTypes from 'prop-types';
import { Text } from 'react-native';
import { Container } from './MeetingStyle';

const MeetingScreen = () => {
  return(
    <Container>
      <Text>Welcome to Meeting</Text>
    </Container>
  )
}

MeetingScreen.propTypes = {}

MeetingScreen.defaultProps = {}

export default MeetingScreen;
