import { compose, withHandlers, lifecycle} from "recompose";
import { Platform } from 'react-native';
import MeetingScreen from "./Meeting";
import ConexaAndroid from '../../../modules/conexa/conexa.android';
import ConexaIos from '../../../modules/conexa/conexa.ios';

const enhance = compose(
  withHandlers({
    initConnection: props => async () => {
      const { params: {meetingNumber, participantName }} = props.navigation.state;
      if (!meetingNumber) return false;
      const conexa = Platform.OS === 'ios' ? ConexaIos : ConexaAndroid;
      setLoading(true);
      // eslint-disable-next-line radix
      const res = await conexa.getConexaConnection(parseInt(meetingNumber), participantName || "Paciente QSaude");
      setLoading(false);
      return res;
    }
  }),
  lifecycle({
    componentDidMount() {
      const {initConnection} = this.props;
      initConnection();
    }
  })
);

export default enhance(MeetingScreen);
