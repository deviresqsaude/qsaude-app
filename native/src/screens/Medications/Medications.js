import React from 'react';
import R from 'ramda';
import { Text } from 'react-native';
import { MedicineCard } from '../../components';
import { MedicationList } from './MedicationStyles';

const indexedMapper = R.addIndex(R.map);

// eslint-disable-next-line react/prop-types
const Medications = ({ medicationList=[], navigation: { navigate } }) => (
  <MedicationList showsVerticalScrollIndicator={false}>
    {
      medicationList.length && medicationList.map((item, index) => {
        return (
          <MedicineCard
            onPress={() => navigate('MedicationDetails', { medication: item })}
            key={`medicamento_${index}`}
            id={`medicamento_${index}`}
            {...item}
          />
        )
      })
    }

  </MedicationList>
);

export default Medications;
