import React from 'react';
import { Picker, TouchableWithoutFeedback } from 'react-native';
import { Placeholder, PlaceholderMedia, Fade } from 'rn-placeholder';
import styled from 'styled-components/native';
import { QText, EmptyData, SystemIcon } from '../../components';
import { Colors, SystemImages, SystemFonts, Sizing } from '../../constants/theme';

export const MedicationList = styled.ScrollView({
  padding: 16,
  flex: 1,
  backgroundColor: '#fff',
});
export const placeholderstyle = {
  width: '100%',
  height: 80,
  marginBottom: 12,
  borderRadius: 4,
};

export const Container = styled.ScrollView.attrs({
  contentContainerStyle: {
    paddingTop: 15,
  },
})``;

export const DetailCard = styled.View({
  shadowColor: '#000',
  backgroundColor: '#fff',
  shadowOffset: { width: 10, height: 10 },
  shadowOpacity: 0.3,
  elevation: 3,
  shadowRadius: 16,
  borderColor: Colors.LightGray,
  borderWidth: 1,
  borderRadius: 18,
  padding: 14,
  margin: 16,
});

const ItemContainer = styled.View(({ center, noBorder }) => ({
  paddingBottom: 8,
  paddingTop: 8,
  borderTopWidth: noBorder ? 0 : 1,
  borderTopColor: 'rgba(0, 0, 0, .2)',
  flexDirection: 'row',
  alignItems: 'center',
  flexWrap: 'wrap',
  justifyContent: center ? 'center' : 'space-between',
}));

// eslint-disable-next-line react/prop-types
export const DetailRow = ({ label, data, center, onPress, noBorder }) => (
  <TouchableWithoutFeedback  onPress={onPress}>
    <ItemContainer center={center} noBorder={noBorder}>
      {label && data ? (
        <>
          <QText margin={{ right: 32 }} color={Colors.TextGray}>{label}</QText>
          <QText style={{ maxWidth: '75%' }} preset="link">
            {data}
          </QText>
        </>
      ) : (
        <QText color="rgba(0,0,0,0.5)" preset="bold">
          {label}
        </QText>
      )}
    </ItemContainer>
  </TouchableWithoutFeedback>
);

export const MedicationPlaceholder = () => (
  <Placeholder style={{ padding: 16 }} Animation={Fade}>
    <PlaceholderMedia style={placeholderstyle} />
    <PlaceholderMedia style={placeholderstyle} />
    <PlaceholderMedia style={placeholderstyle} />
    <PlaceholderMedia style={placeholderstyle} />
  </Placeholder>
);

export const EmptyMedicationList = styled(EmptyData).attrs(props => ({
  title: 'Não há medicamentos cadastrados',
  description: 'Aguarde o cadastro de medicamentos pelo teu médico ou tente novamente.',
  buttonText: 'Tentar novamente',
  onPress: props.refetch,
}))``;

export const MedicationError = styled(EmptyData).attrs(props => ({
  title: 'Ops!!',
  description: 'Aconteceu um problema, Tente novamente.',
  buttonText: 'Tentar novamente',
  onPress: props.refetch,
  imageSource: SystemImages.errorImage,
}))``;

export const ReminderWrapper = styled.ScrollView.attrs({
  contentContainerStyle: {
    padding: 20,
  },
})``;

export const ReminderRowContainer = styled.ScrollView({
  // paddingVertical: 8,
})

export const DateTimePickerWrapper = styled.View({
});

export const IntervalPicker = ({ onPress, value }) => (
  <Picker 
    onValueChange={onPress} 
    style={{width: '100%', color: Colors.TextGray,}} 
    selectedValue={value}
    mode='dialog'
    itemStyle={{
      color: Colors.TextGray,
      fontFamily: SystemFonts.PlexSansRegular,
      fontSize: Sizing.small
    }}
  >
    <Picker.Item label="4 horas" value={4} />
    <Picker.Item label="8 horas" value={8} />
    <Picker.Item label="12 horas" value={12} />
    <Picker.Item label="24 horas" value={24} />
    <Picker.Item label="48 horas" value={48} />
  </Picker>
)

export const InputContaier = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9,
})({
  flexDirection: 'row',
  backgroundColor: '#fff',
  alignItems: 'center',
  justifyContent: 'space-between',
  borderRadius: 16,
  marginTop: 16,
  borderColor: Colors.Gray,
  width: '100%',
  height: 48,
  borderWidth: 1,
  padding: 8,
});

export const DisabledDataInput = ({ value, onPress }) => (
  <InputContaier onPress={onPress} style={{opacity: 0.75}} disabled>
    <QText preset="body">{value}</QText>
  </InputContaier>
);

export const MedicationDateInput = ({ title, value, onPress, closePicker }) => {
  return (
    <TouchableWithoutFeedback onPress={closePicker}>
      <>
        <QText color={Colors.TextGray} style={{paddingVertical: 16}} onPress={closePicker} >{title}</QText>
        <InputContaier onPress={onPress} style={{marginTop: 0}}>
          <QText preset="body">{value}</QText>
          <SystemIcon
            tintColor={Colors.Purple}
            name="chevromDown"
          />
        </InputContaier>
      </>
    </TouchableWithoutFeedback>
  );
};

export const IconWrapper = styled.View({
  position: 'absolute', 
  zIndex: 1, 
  backgroundColor: '#fff', 
  right: 8,
  width: 60,
  height: 44,
  display: 'flex',
  alignItems: 'flex-end',
  justifyContent: 'center',
})