import { graphql } from '@apollo/react-hoc';
import { compose, withProps, branch, renderComponent } from 'recompose';
import R from 'ramda';
// import { connect } from 'react-redux';
import { Medications as MedicationsQuery } from '../../service/query';
import { renderWhileLoading, renderEmptyData } from '../../components';
import { MedicationPlaceholder, EmptyMedicationList, MedicationError } from './MedicationStyles';
import MedicationsScreen from './Medications';

const withData = graphql(MedicationsQuery, {options: { notifyOnNetworkStatusChange: true }});

const setApolloRefetch = withProps(({data}) => ({refetch: data && data.refetch}));

const renderForError = branch(({ data }) => data && data.error, renderComponent(MedicationError));

const translateFrequency = when => {
  if (!when) return 'Não há dados cadastrados';
  const testHHMMFormat = new RegExp('\d*:\d*');
  const isTimeFormat = testHHMMFormat.test(R.head(when));
  const toTimeString = R.join(' - ');
  const toPeriodString = R.join(', ');

  const parsePeriods = R.map(item =>
    R.cond([
      [R.equals('sun'), R.always('Dom')],
      [R.equals('sat'), R.always('Sáb')],
      [R.equals('fri'), R.always('Sex')],
      [R.equals('thu'), R.always('Qui')],
      [R.equals('wed'), R.always('Qua')],
      [R.equals('tue'), R.always('Terç')],
      [R.equals('mon'), R.always('Seg')],
      [R.T, R.always('Indefinido')],
    ])(item),
  );

  const parseTime = R.map(item => 
    R.splitAt(5, item)[0],
  )

  const formattedData = isTimeFormat
    ? toTimeString(parseTime(when))
    : toPeriodString(parsePeriods(when));

  return formattedData;
};

const withFrequencyTranslated = withProps(
  ({
    onServiceFailed,
    navigation: { goBack },
    data: { medications, loading, error },
  }) => {
    if (error) {
      onServiceFailed({
        title: 'Erro ao consultar os medicamentos',
        error: error.message,
      });
      goBack();
    }

    if (loading) return [];

    const removeGraphMeta = R.dissoc('__typename');

    const mergeItemWithoutGraphMeta = ({ when, ...rest }) =>
      R.merge(removeGraphMeta(rest), { when: translateFrequency(when) });

    return {
      medicationList: R.map(mergeItemWithoutGraphMeta, medications || []),
    };
  },
);

const enhance = compose(
  withData,
  setApolloRefetch,
  renderWhileLoading(MedicationPlaceholder, 'medications'),
  renderEmptyData(EmptyMedicationList, 'medications'),
  renderForError,
  withFrequencyTranslated,
);

export default enhance(MedicationsScreen);
