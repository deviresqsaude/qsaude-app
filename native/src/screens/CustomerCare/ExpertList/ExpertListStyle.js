import styled from 'styled-components/native';
import { EmptyData } from '../../../components';
import { SystemImages } from '../../../constants/theme';

export const Wrapper = styled.View({
  flex: 1,
  marginHorizontal: 16,
  paddingVertical: 16,
});

export const ExpertError = styled(EmptyData).attrs(props => ({
  title: 'Ops!!',
  description: 'Aconteceu um problema, Tente novamente.',
  buttonText: 'Tentar novamente',
  onPress: props.refetch,
  imageSource: SystemImages.errorImage,
}))``;

export const ExpertEmpty = styled(EmptyData).attrs(props => ({
  title: 'Nenhum encaminhamento solicitado!',
  description: '@Nome, no momento, não é possível fazer esse agendamento. Antes de agendar com um especialista, você precisa do encaminhamento do seu médico pessoal, ok? Dúvidas? Fale com uma GuiaQ.',
  imageSource: SystemImages.calendarEmpty,
}))``;

export const ExpertListContainer = styled.View({
  paddingVertical: 16
})
