import { compose, withProps, withHandlers, branch, renderComponent } from 'recompose';
import { graphql } from '@apollo/react-hoc';
import R from 'ramda';
import moment from 'moment';
import ExpertList from './ExpertList';
import ExpertLoading from './ExpertLoading';
import { ExpertError, ExpertEmpty } from './ExpertListStyle';
import { AllExams } from "../../../service/query";

const withData = graphql(AllExams, {
  options: { 
    notifyOnNetworkStatusChange: true,
    variables: {
      params: {
        status: "active",
        category: "3457005,308273005"
      },
      resultType: null
    },
  }
});

const renderForLoading = branch(({data}) => data && data.loading, renderComponent(ExpertLoading));

const setApolloRefetch = withProps(({data}) => ({refetch: data && data.refetch}));

const renderForError = branch(({ data }) => data && data.error, renderComponent(ExpertError));

const renderWithNull = branch(({ data }) => !data.error && !data.loading && data.exams.length == 0, renderComponent(ExpertEmpty))

// const experts = withProps(({navigation}) => ({
//   experts: navigation.getParam('specialities')
// }));

const getDescription = ({category, data}) => {
  const today = moment();
  return category == '3457005' ? `Limite de agendamento - Restam ${moment(data).diff(today, 'days')} dias` : `Retorno em consulta - Restam ${moment(data).diff(moment(today), 'days')} dias`
}

const withDataProps = withProps(({ data: { exams }}) => {
  if(exams)
  return {
    experts: R.map(item => ({
      name: R.prop('performerType', item)[0].display,
      code: R.prop('performerType', item)[0].code,
      description: getDescription({category: R.prop('category', item), data: R.prop('occurrencePeriod', item).end})
    }), R.filter(R.propEq('priority', 'routine'), exams))
  }
})

const handlers = withHandlers({
  showNetwork: ({navigation: {navigate}}) => type => {
    const params = {
      specialities: type
    };
    navigate("CredentialNetwork", {...params});
  }
})

const enchance = compose(
  withData,
  // experts,
  withDataProps,
  renderForLoading,
  setApolloRefetch,
  renderForError,
  renderWithNull,
  handlers
);

export default enchance(ExpertList)
