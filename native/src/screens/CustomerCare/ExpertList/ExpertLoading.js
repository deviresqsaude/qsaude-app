import React from 'react';
import { Placeholder, PlaceholderMedia, Fade } from 'rn-placeholder';
import { Wrapper } from './ExpertListStyle';

export const placeholderstyle = {
  width: '100%',
  height: 56,
  marginBottom: 12,
  borderRadius: 4,
};

const ExpertLoading = () => (
  <Wrapper>
    <Placeholder Animation={Fade}>
      <PlaceholderMedia style={placeholderstyle} />
      <PlaceholderMedia style={placeholderstyle} />
      <PlaceholderMedia style={placeholderstyle} />
      <PlaceholderMedia style={placeholderstyle} />
      <PlaceholderMedia style={placeholderstyle} />
    </Placeholder>
  </Wrapper>
)

export default ExpertLoading;
