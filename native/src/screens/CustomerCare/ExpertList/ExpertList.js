import React from 'react';
import moment from 'moment';
import momentBR from 'moment/locale/pt-br';
import { FlatList } from 'react-native';
import { QText, ExamCard } from '../../../components';
import { Wrapper, ExpertListContainer } from './ExpertListStyle'

moment.locale('pt-br', momentBR);

const SELECT_SPECIALITY = "Selecione a especialidade";

const ExpertList = ({experts, showNetwork}) => {
  return (
    <Wrapper><QText preset="dataText">{SELECT_SPECIALITY}</QText>
      <ExpertListContainer>
        <FlatList
          data={experts}
          showsVerticalScrollIndicator={false}
          keyExtractor={item => item.code }
          renderItem={({item}) => {
            // const remainDate = moment(item.expireDate || new Date(), 'DD/MM/YYYY', true).diff(moment(), 'days');
            return <ExamCard
              name={item.name}
              description={item.description}
              // description={`Limite de agendamento - Restam ${remainDate} dias`}
              onPress={() => showNetwork(item.cbo)}
              {...item}/>
          }}/>
      </ExpertListContainer>
    </Wrapper>
  )
}

ExpertList.propTypes = {};

export default ExpertList;

