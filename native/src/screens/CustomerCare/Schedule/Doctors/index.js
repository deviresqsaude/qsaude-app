import { compose, withProps, withState, branch, renderComponent, withHandlers } from 'recompose';
import { connect } from 'react-redux';
import { graphql } from '@apollo/react-hoc';
import { MyDoctorsQuery } from '../../../../service/query';
import { LoadingData, ErrorData, EmptyData } from './DoctorsStyle';
import { ScheduleActions } from '../../../../store/actions';
import Doctors from './Doctors';

const mapStateToProps = ({schedule}) => ({
  scheduleState: {...schedule}
});

const mapDispatchToProps = dispatch => ({
  // setStep: step => dispatch(ScheduleActions.actions.setScheduleStep(step)),
  setLoading: loading => dispatch(ScheduleActions.actions.setLoading(loading)),
  setCurrentDoctor: doctor => dispatch(ScheduleActions.actions.setDoctor(doctor)),
  setFetch: fetch => dispatch(ScheduleActions.actions.setFetching(fetch)),
  addMonths: month => dispatch(ScheduleActions.actions.addMonths(month)),
  setAvailableDays: days => dispatch(ScheduleActions.actions.setAvailableDays(days)),
  setCurrentDay: day => dispatch(ScheduleActions.actions.setCurrentDay(day)),
  setAvailableSlots: slots => dispatch(ScheduleActions.actions.setAvailableSlots(slots)),
  setCurrentSlot: slot => dispatch(ScheduleActions.actions.setCurrentSlot(slot)),
  clearCalendar: () => dispatch(ScheduleActions.actions.clearCalendar()),
});

const stateFilter = withState('filter', 'setFilter', "");

const withData = graphql(
  MyDoctorsQuery, {
    options: { notifyOnNetworkStatusChange: true }
  });

const renderForLoading = branch(({data}) => data && data.loading, renderComponent(LoadingData));

const setApolloRefetch = withProps(({data}) => ({refetch: data && data.refetch}));

const renderForError = branch(({ data }) => data && data.error, renderComponent(ErrorData));

const renderWithNull = branch(({ data, myDoctors }) => !data.error && !data.loading && (myDoctors && !myDoctors.length), renderComponent(EmptyData));

const doctorsProps = withProps(({data: {myDoctors}}) => ({
  myDoctors
}));

const handlers = withHandlers({
  setDoctor: ({setCurrentDoctor, clearCalendar, onCallBack, scheduleState: {doctor, step, mode}}) => doctorSelected => {
    const clearOptions = {
      1: () => clearCalendar(),
      2: () => {},
      3: () => {},
      4: () => {},
      5: () => clearCalendar()
    };
    if(doctor && (doctor.practitionerId !== doctorSelected.practitionerId)) clearOptions[mode]();
    setCurrentDoctor(doctorSelected);
    onCallBack(step + 1);
  }
})

const enchance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  stateFilter,
  withData,
  renderForLoading,
  setApolloRefetch,
  renderForError,
  renderWithNull,
  doctorsProps,
  handlers
);

export default enchance(Doctors);
