
import React from 'react';
import R from 'ramda';
import { Text, FlatList } from 'react-native';
import { Container } from './DoctorsStyle';
import { Colors } from '../../../../constants/theme';
import { MyDoctorCard, QSearch } from '../../../../components';

// Textos
const MESSAGE_DOCTORS = "Selecione o médico de preferência: ";

const Doctors = ({myDoctors, setDoctor, setFilter, filter}) => {
  return (
    <Container>
      <Text style={{ color: Colors.TextGray, marginBottom: 0 }}>{MESSAGE_DOCTORS}</Text>
      <QSearch style={{marginTop: 15}} placeholder="Pesquise por nome ou CRM" onSearch={s => setFilter(s)}/>
      <FlatList
        data={R.filter(item => R.indexOf(filter, item.name) !== -1, myDoctors)}
        keyExtractor={({practitionerId}) => practitionerId}
        showsVerticalScrollIndicator={false}
        renderItem={({ item }) => {
          return <MyDoctorCard {...item} onPress={() => setDoctor(item)} />;
        }}
      />
    </Container>
  )
}

Doctors.propTypes = {};

export default Doctors;

