import React from 'react';
import { View } from 'react-native';
import styled from 'styled-components/native';
import { Placeholder, PlaceholderMedia, Fade } from 'rn-placeholder';
import { QText } from '../../../../components';

export const Container = styled.View({
  backgroundColor: "#FFFFFF",
  paddingHorizontal: 15
});

export const DataList = styled.FlatList({
  marginTop: 16,
});

export const EmptyContainer = styled.View({
  alignItems: 'center',
  justifyContent: 'center',
});

export const ErrorData = () => (
  <EmptyContainer>
    <QText>Error ao carregar</QText>
  </EmptyContainer>
)

const PLACEHOLDER_STYLE = {
  width: '100%',
  height: 80,
  marginBottom: 12,
  borderRadius: 4
};

export const LoadingData = () => (
  <View style={{marginHorizontal: 15}}>
    <Placeholder Animation={Fade}>
      <PlaceholderMedia style={PLACEHOLDER_STYLE} />
      <PlaceholderMedia style={PLACEHOLDER_STYLE} />
      <PlaceholderMedia style={PLACEHOLDER_STYLE} />
      <PlaceholderMedia style={PLACEHOLDER_STYLE} />
      <PlaceholderMedia style={PLACEHOLDER_STYLE} />
    </Placeholder>
  </View>
);

export const EmptyData = () => (
  <EmptyContainer>
    <QText>Sem medicos.</QText>
  </EmptyContainer>
);
