import React from 'react';
import { View } from 'react-native';
import { QText, EmptyData } from '../../../../components';
import { Placeholder, PlaceholderMedia, Fade } from 'rn-placeholder';
import styled from 'styled-components/native';

export const SlotsContainer = styled.View({
  flexDirection: 'row',
  flex: 1,
  marginBottom: 16,
  flexWrap: 'wrap',
});

export const PillContainer = styled.View({
  width: '25%',
  alignItems: 'center',
});

export const DoctorContainer = styled.View({
  // margin: 16,
  marginBottom: 16,
});

export const Container = styled.ScrollView({
  paddingHorizontal: 16,
  flex: 1,
});

export const LoadingData = () => (
  <View style={{ marginHorizontal: 15 }}>
    <QText margin={{ bottom: 15}} preset="dataText">Horários disponíveis</QText>
    <Placeholder Animation={Fade} >
      <View style={{ flexDirection: 'row', width: '100%', flexWrap: 'wrap'}}>
        <View style={{width: '25%', justifyContent: 'center', alignItems: 'center', paddingVertical: 5}}>
          <PlaceholderMedia style={{ width: 62, height: 34}} />
        </View>
        <View style={{width: '25%', justifyContent: 'center', alignItems: 'center', paddingVertical: 5}}>
          <PlaceholderMedia style={{ width: 62, height: 34}} />
        </View>
        <View style={{width: '25%', justifyContent: 'center', alignItems: 'center', paddingVertical: 5}}>
          <PlaceholderMedia style={{ width: 62, height: 34}} />
        </View>
        <View style={{width: '25%', justifyContent: 'center', alignItems: 'center', paddingVertical: 5}}>
          <PlaceholderMedia style={{ width: 62, height: 34}} />
        </View>
        <View style={{width: '25%', justifyContent: 'center', alignItems: 'center', paddingVertical: 5}}>
          <PlaceholderMedia style={{ width: 62, height: 34}} />
        </View>
        <View style={{width: '25%', justifyContent: 'center', alignItems: 'center', paddingVertical: 5}}>
          <PlaceholderMedia style={{ width: 62, height: 34}} />
        </View>
        <View style={{width: '25%', justifyContent: 'center', alignItems: 'center', paddingVertical: 5}}>
          <PlaceholderMedia style={{ width: 62, height: 34}} />
        </View>
      </View>
    </Placeholder>
  </View>
);


export const SlotError = styled(EmptyData).attrs(props => ({
  title: 'Ops!!',
  description: 'Aconteceu um problema, Tente novamente.',
  // buttonText: 'Tentar novamente',
  // onPress: props.refetch,
  imageSource: SystemImages.errorImage,
}))``;
