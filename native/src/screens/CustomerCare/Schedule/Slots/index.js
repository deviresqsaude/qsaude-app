import { compose } from 'recompose';
import { connect } from 'react-redux';
import Slots from './Slots';
import { ScheduleActions } from '../../../../store/actions';

const mapStateToProps = ({schedule}) => ({
  scheduleState: {...schedule}
});

const mapDispatchToProps = dispatch => ({
  setLoading: loading => dispatch(ScheduleActions.actions.setLoading(loading)),
  setCurrentSlot: slot => dispatch(ScheduleActions.actions.setCurrentSlot(slot))
});

const enchance = compose(
  connect(mapStateToProps, mapDispatchToProps)
);

export default enchance(Slots);

