import React from 'react';
import moment from 'moment';
import R from 'ramda';
import { Pill, QText, QButton, MyDoctorCard } from '../../../../components';
import { SlotsContainer, PillContainer, DoctorContainer, Container , LoadingData} from './SlotsStyle'

const MESSAGES = {
  slots_not_available: "Sem horários disponíveis",
  loading: "Carregando horários disponíveis",
  availableSlots: "Horários disponíveis",
  listDays: "Dias disponíveis"
}

const Slots = ({scheduleState: { mode, step, currentSlot, availableDays, availableSlots, doctor, currentDay, loading }, setCurrentSlot, onCallBack, filterSlots}) => {
  if(R.isEmpty(availableSlots)) return <QText margin={{ bottom: 15}} preset="dataText">{MESSAGES.slots_not_available}</QText>;
  if( !availableSlots || loading ) return <LoadingData />;
  
  // const selectedDoctor = {
  //   name: doctor && doctor.practitioner.display,
  //   address: doctor && doctor.location.reference,
  //   speciality: doctor && doctor.specialty && doctor.specialty[0].coding[0].display || 'CLINÍCO GERAL',
  //   crm: doctor && doctor.practitioner.identifier.value
  // }

  return (
    <Container showsHorizontalScrollIndicator={false}>
      {
        doctor && mode == 2 && 
        <DoctorContainer>
          <MyDoctorCard name={doctor.practitioner.display} address={doctor.location.reference} speciality={ doctor.specialty && doctor.specialty[0].coding[0].display || 'CLINÍCO GERAL'} practitionerId={doctor.practitioner.identifier.value} />
          <QText margin={{bottom: 16, top: 16}} preset="dataText">{MESSAGES.listDays}</QText>
          <SlotsContainer>
            {
              R.map(day => (
                <PillContainer key={`pill-${day}`}>
                  <Pill
                    selected={currentDay ? currentDay === day : false}
                    onPress={() => filterSlots(day)}
                    text={moment(day).date()}
                  />
                </PillContainer>
              ), availableDays || [])
            }
          </SlotsContainer>
        </DoctorContainer>
      }
      <QText margin={{bottom: 16}} preset="dataText">{MESSAGES.availableSlots}</QText>
      <SlotsContainer>
        {
          R.map(slot => (
            <PillContainer key={`pill-${slot.id}`}>
              <Pill
                selected={currentSlot ? slot.id === currentSlot.id : false}
                onPress={() => setCurrentSlot(slot)}
                text={slot.start.slice(11, 16)}
              />
            </PillContainer>
          ), availableSlots || [])
        }
      </SlotsContainer>
      {mode === 2 &&
      <QButton text="Próximo"
        disabled={!currentSlot}
        onPress={() => onCallBack(step + 1)}
      />}
    </Container>
  )
}

Slots.propTypes = {};

export default Slots;
