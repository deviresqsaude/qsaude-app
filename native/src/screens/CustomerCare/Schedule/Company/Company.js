import React from 'react';
import { FlatList, Text, View } from 'react-native';
import { ExamCard } from '../../../../components';
import { Colors } from '../../../../constants/theme';

const MESSAGE_COMPANY = "Insira um referenciado de sua preferência:";

const Company = ({companies, setCompany}) => {
  return (
    <View style={{marginHorizontal: 15}}>
      <Text style={{ color: Colors.TextGray, marginBottom: 15 }}>{MESSAGE_COMPANY}</Text>
      <FlatList
        data={companies}
        showsVerticalScrollIndicator={false}
        keyExtractor={({id}) => id}
        renderItem={({item}) => <ExamCard onPress={() => setCompany(item)} name={item.name}/>}/>
    </View>
  )
};

Company.propTypes = {};

export default Company;

