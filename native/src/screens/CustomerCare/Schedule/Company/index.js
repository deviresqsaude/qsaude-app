import { compose, withProps, withHandlers } from 'recompose';
import { connect } from 'react-redux';
import Company from './Company';
import { ScheduleActions } from '../../../../store/actions';
import { COMPANY_UNITS } from '../../../../constants/mock';

const mapStateToProps = ({schedule}) => ({
  scheduleState: {...schedule}
});

const mapDispatchToProps = dispatch => ({
  setCurrentCompany: company => dispatch(ScheduleActions.actions.setCurrentCompany(company)),
  setUnits: units => dispatch(ScheduleActions.actions.setUnits(units)),
});

const propsCompany = withProps({
  companies: COMPANY_UNITS
});

const handlers = withHandlers({
  setCompany: ({setCurrentCompany, setUnits, onCallBack, scheduleState: {step}}) => company => {
    setCurrentCompany(company);
    setUnits(company.units);
    onCallBack(step + 1);
  }
});

const enchance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  propsCompany,
  handlers
);

export default enchance(Company);
