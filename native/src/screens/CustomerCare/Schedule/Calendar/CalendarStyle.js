import React from 'react';
import styled from 'styled-components/native';
import { Placeholder, PlaceholderMedia, Fade } from 'rn-placeholder';

export const Container = styled.ScrollView({
  flex: 1,
  flexDirection: "column",
  paddingHorizontal: 15
});
export const ButtonContainer = styled.View({
  width: '100%',
  paddingRight: 16,
  justifyContent: 'flex-end',
  flexDirection: 'row',
});
export const PillContainer = styled.View({
  width: '25%',
  minWidth: 78,
  alignItems: 'center',
});
export const SlotsContainer = styled.View({
  flexDirection: 'row',
  flex: 1,
  paddingHorizontal: 24,
  marginBottom: 16,
  flexWrap: 'wrap',
});

const EmptyContainer = styled.View({
  alignItems: 'center',
  justifyContent: 'center',
});

const EmptyText = styled.Text({});

export const ErrorData = () => (
  <EmptyContainer>
    <EmptyText>Error ao carregar</EmptyText>
  </EmptyContainer>
)

const PLACEHOLDER_STYLE = {
  width: '100%',
  height: 40,
  marginBottom: 12,
  borderRadius: 4,
};

export const LoadingData = () => (
  <Placeholder Animation={Fade}>
    <PlaceholderMedia style={PLACEHOLDER_STYLE} />
    <PlaceholderMedia style={{
      ...PLACEHOLDER_STYLE,
      height: 400
    }} />
  </Placeholder>
);
