import React from 'react';
import R from 'ramda';
import moment from 'moment';
import { Container } from './CalendarStyle';
import { Calendar, QButton } from '../../../../components';
import Slots from '../Slots';

const CalendarSchedule = props => {
  const {
    onSelectDay,
    onMonthChange,
    onPeriodSelect,
    scheduleState: {currentSlot, availableDays, currentDay, loading, step, mode, schedulePeriod},
    loadingCalendar,
    onCallBack,
    periods,
    getAvailableDoctors
  } = props;
  return (
    <Container showsVerticalScrollIndicator={false}>
      <Calendar {...{
        disabledByDefault: !(mode === 2),
        markedDates: mode !== 2 ? {
          ...availableDays,
          [currentDay]:  { selected: true }
        } : { ...periods },
        onSelectDay: mode !== 2 ? onSelectDay : onPeriodSelect,
        footerTag: "Dias disponiveis",
        markingType: mode === 2 ? "period": "simple",
        onMonthChange,
        loading: loading || loadingCalendar,
        minDate: moment().format('YYYY-MM-DD')
      }}/>
      {mode !== 2 && <Slots/>}
      <QButton text="Próximo"
        disabled={mode !== 2 ?
          (loadingCalendar || !currentDay || !currentSlot) :
          (R.isNil(schedulePeriod.start) || R.isNil(schedulePeriod.end))
        }
        onPress={
          () => onCallBack(step + 1)
          // () => getAvailableDoctors()
        }
        style={{marginVertical: 15}}
      />
    </Container>
  );
};
CalendarSchedule.propTypes = {};

export default CalendarSchedule;
