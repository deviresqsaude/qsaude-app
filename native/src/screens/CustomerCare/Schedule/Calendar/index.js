import { compose, branch, withState, withProps, withHandlers, renderComponent, pure, lifecycle } from 'recompose';
import { connect } from 'react-redux';
import { graphql } from '@apollo/react-hoc';
import R from 'ramda';
import moment from 'moment';
import Calendar from './Calendar';
import { MyDoctorSlotsQuery } from '../../../../service/query';
import { LoadingData, ErrorData } from './CalendarStyle';
import { ScheduleActions } from '../../../../store/actions';
import { Colors } from '../../../../constants/theme';
import { getBetweenDates } from '../../../../helpers';
import { slotsMock } from '../../../../constants/mock';

// Helpers
const refactorSlots = slots => {
  const reducer = R.reduce((acc, cur) => ({
    ...acc,
    [cur.date]: {
      marked: true,
      disabled: false,
      disableTouchEvent: false,
    }
  }), {}, slots);
  return reducer;
};
const refactorPeriod = periods => {
  const reducer = R.reduce((acc, item) => ({
    ...acc,
    [item] : {
      selected: true,
      color: Colors.Purple,
      textColor: Colors.White
    }
  }), {}, periods);
  return reducer;
};
const filteredSlots = (date, slots) => R.filter(R.propEq('date', date), slots || []);
const todaySlots = slots => slots.length ? R.prop('slots', slots[0]) : [];
// End - Helpers

const withPeriod = withState('startPeriod', 'toggleStartPeriod', true);
const withPeriods = withState('periods', 'setPeriods', {});

// Redux
const mapStateToProps = ({schedule}) => ({
  scheduleState: {...schedule}
});

const mapDispatchToProps = dispatch => ({
  setAllData: response => dispatch(ScheduleActions.actions.setAllData(response)),
  setLoading: loading => dispatch(ScheduleActions.actions.setLoading(loading)),
  setAvailableDays: days => dispatch(ScheduleActions.actions.setAvailableDays(days)),
  setCurrentDay: day => dispatch(ScheduleActions.actions.setCurrentDay(day)),
  setAvailableSlots: slots => dispatch(ScheduleActions.actions.setAvailableSlots(slots)),
  setCurrentSlot: slot => dispatch(ScheduleActions.actions.setCurrentSlot(slot)),
  setFetch: fetch => dispatch(ScheduleActions.actions.setFetching(fetch)),
  addMonths: month => dispatch(ScheduleActions.actions.addMonths(month)),
  updatePeriod: period => dispatch(ScheduleActions.actions.updatePeriod(period)),
});

const handlers = withHandlers({
  onPeriodSelect: ({setAvailableSlots, updatePeriod, toggleStartPeriod, startPeriod, periods, setPeriods, scheduleState: {schedulePeriod}}) => period => {
    // const keys = R.keys(periods);
    // const exits = R.includes(period, keys);
    // if(exits) return;
    const dates = !startPeriod ? getBetweenDates(schedulePeriod.start, period) : [];
    const inverted = !startPeriod && moment(schedulePeriod.start) > moment(period);
    const config = {
      startingDay: startPeriod || (inverted && !startPeriod),
      endingDay: startPeriod || (!inverted && !startPeriod),
      color: Colors.Purple,
      textColor: Colors.White
    };
    let clonePeriod = {};
    if (!startPeriod) {
      clonePeriod = periods[schedulePeriod.start];
      clonePeriod.startingDay = !inverted;
      clonePeriod.endingDay = inverted;
    }
    const newPeriods = R.assoc(period, config, startPeriod ? {} : {...refactorPeriod(dates), ...{...periods, ...clonePeriod}});
    const sp = R.assoc(startPeriod ? 'start': 'end', period, startPeriod ? {end: period} : schedulePeriod);
    setAvailableSlots(!startPeriod ? slotsMock : null);
    setPeriods(newPeriods);
    updatePeriod(sp);
    toggleStartPeriod(!startPeriod);
  },
  onSelectDay: ({
    setCurrentDay,
    setAvailableSlots,
    setCurrentSlot,
    scheduleState: {allData, mode}
  }) => daystring => {
    if(mode === 2) return;
    setCurrentDay(daystring);
    setCurrentSlot(null);
    setAvailableSlots(todaySlots(filteredSlots(daystring, allData)));
  },
  onMonthChange: ({
    loadMoreDays,
    setAvailableSlots,
    setCurrentSlot,
    scheduleState: {mode}
  }) => async ({ dateString }) => {
    if(mode === 2) return;
    const formatDate = moment(dateString).toISOString();
    // setAvailableSlots([]); // TODO: Verificar se é necessario limpar os slots de horarios disponi veis cada vez que o trocamos de mês
    setCurrentSlot(null);
    loadMoreDays(formatDate);
  }
});

const withData = graphql(MyDoctorSlotsQuery, {
  skip: ({scheduleState: {doctor, mode}}) => mode === 2 || !doctor,
  options: ({
    scheduleState: { doctor: {practitionerId, scheduleId, nextSlot}, isFeching},
    setAvailableDays,
    setAllData,
    setAvailableSlots,
    addMonths,
  }) => ({
    variables: { practitionerId, scheduleId, date: nextSlot},
    notifyOnNetworkStatusChange: true,
    awaitRefetchQueries: true,
    onCompleted: data => {
      if (isFeching || !data || (data && R.isEmpty(data.myDoctorSlots))) return;
      setAllData(data.myDoctorSlots);
      addMonths([moment(nextSlot).month()]);
      setAvailableDays(refactorSlots(data.myDoctorSlots));
      setAvailableSlots(todaySlots(filteredSlots(moment().format('YYYY-MM-DD'), data.myDoctorSlots)));
    },
  }),
  props: ({
    data: {loading, fetchMore},
    ownProps: {
      setAvailableDays, setFetch, addMonths, setCurrentDay, setAllData,
      scheduleState: { doctor: {practitionerId,scheduleId}, availableDays, months }
    }}) => ({
    loadingCalendar: loading,
    loadMoreDays: dayString => {
      const month = moment(dayString).month();
      if(R.includes(month, months)) return;
      setFetch(true);
      fetchMore({
        variables: { practitionerId, scheduleId, date: dayString},
        updateQuery: (prev, {fetchMoreResult: newData}) => {
          addMonths([...months, month]);
          if (!newData) return prev;
          setAllData(newData.myDoctorSlots);
          setAvailableDays(R.merge(availableDays, refactorSlots(newData.myDoctorSlots)));
          setCurrentDay(dayString);
          return {};
        }
      })
    }
  }),
});

const renderForLoading = branch(({data}) => data && data.loading, renderComponent(LoadingData));

const setApolloRefetch = withProps(({data}) => ({refetch: data && data.refetch}));

const renderForError = branch(({ data }) => data && data.error, renderComponent(ErrorData));

const lifeCycleMethod = lifecycle({
  componentDidMount() {
    //to add today's available slots
  }
})

const enchance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  // Data
  withData,
  renderForLoading,
  setApolloRefetch,
  renderForError,
  // State
  withPeriod,
  withPeriods,
  // handlers
  handlers,
  lifeCycleMethod
);

export default enchance(pure(Calendar));
