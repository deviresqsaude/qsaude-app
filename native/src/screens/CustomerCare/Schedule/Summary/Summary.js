import React from 'react';
import moment from 'moment';
import { View } from 'react-native';
import { DetailsCard, QText, QButton } from '../../../../components';

const MESSAGES = {
  summary: "Confirme o seu agendamento:"
};

const Summary = props=> {
  const {
    insertAppointment,
    isSubmitting,
    result,
    scheduleState: {
      doctor: {name, address, locationId, practitionerId},
      currentSlot: {start, end}
    }
  } = props;
  const scheduleDate = moment.parseZone(start);
  const scheduleDateFormat = scheduleDate.format("DD/MM HH:mm");
  const detailConfig = {
    title: `Meu médico - ${name}`,
    description: address,
    data: [{ label: 'Consulta', data: scheduleDateFormat }],
  };

  const handlesubmit = async () => {
    await insertAppointment({
      variables: {
        locationId,
        practitionerId,
        start,
        end
      }
    })
    .then(res => console.log('res', res))
    .catch(e => console.log('error', e))
  }
  return (
    <View style={{marginHorizontal: 15}}>
      <QText margin={{ bottom: 15}} preset="dataText">{MESSAGES.summary}</QText>
      <View style={{marginHorizontal: -16}}>
        <DetailsCard {...detailConfig}/>
      </View>
      <QButton text="Agendar"
        disabled={isSubmitting}
        loading={isSubmitting}
        onPress={handlesubmit}
        style={{marginVertical: 15}}
      />
    </View>
  )
}

Summary.propTypes = {};

export default Summary;
