import styled from 'styled-components/native';
import { EmptyData } from '../../../../components';

export const SummaryEmpty = styled(EmptyData).attrs(props => ({
  title: 'Aguarde',
  description: 'Selecione um medico e uma data para realizar o agendamento',
  renderImage: false
}))``;
