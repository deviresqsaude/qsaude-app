import R from 'ramda';
import moment from 'moment';
import { compose, withState, branch, renderComponent } from 'recompose';
import { graphql } from '@apollo/react-hoc'
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import { ScheduleActions, AlertActions, CareActions } from '../../../../store/actions';
import { InsertAppointment } from '../../../../service/mutations';
import { SummaryEmpty } from './SummaryStyle';
import Summary from './Summary';
import { dots } from '../../../../helpers';

const MESSAGE = {
  success: {
    title: "Consulta agendada!",
    description: "Agora é só comparecer no local e horário agendados."
  },
  error: {
    title: "Erro ao agendar consulta",
    description: "Por favor, tente novamente ou entre em contato com a gente."
  },
};

const mapStateToProps = ({schedule, care}) => ({
  scheduleState: {...schedule},
  careState: {...care}
});

const mapDispatchToProps = dispatch => ({
  resetState: () => dispatch(ScheduleActions.actions.resetState()),
  showAlert: options => dispatch(AlertActions.actions.openAlert(options)),
  closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
  setMarketEvents: event => dispatch(CareActions.actions.setMarketEvents(event)),
  upEvents: event => dispatch(CareActions.actions.updateEvents(event)),
  upAppoinments: event => dispatch(CareActions.actions.setAppoinments(event)),
});

const stateSubmit = withState('isSubmitting', 'setSubmit', false);
const stateError = withState('currentError', 'setCurrentError', false);

const withSubmit = graphql(InsertAppointment, {
  skip: ({scheduleState: {doctor, currentSlot}}) => !currentSlot || !doctor,
  options: ({setSubmit, showAlert, closeAlert, setMarketEvents, upEvents, upAppoinments, careState: {appoinments, marketEvents, date}}) => ({
    errorPolicy: "all",
    onCompleted: data => {
      setSubmit(false);
      if (!R.isNil(data)) {
        const {insertAppointment: appoinment } = data;
        const startFormat = moment(appoinment.start).format("YYYY-MM-DD");

        const cloneList = [...appoinments];
        const index = R.findIndex(R.propEq('start', startFormat))(cloneList);
        const events = index !== -1 ? [...cloneList[index].events, parseInt(appoinment.id)] : [parseInt(appoinment.id)];
        cloneList.splice(index, index !== -1 ? 1 : 0, {start: startFormat, events});
        upAppoinments(cloneList);

        const newMarkedEvents = marketEvents;
        newMarkedEvents[startFormat] = { marked: true, dots: newMarkedEvents[startFormat] ? R.uniq([...newMarkedEvents[startFormat].dots, dots.medical ]) : [dots.medical]};
        setMarketEvents(newMarkedEvents)

        if(startFormat == date) {
          upEvents([{id: parseInt(appoinment.id), ...appoinment, type: 1}])
        }
      }
    },
    onError: err => {
      showAlert({
        title: MESSAGE.error.title,
        shouldRenderOkButton: true,
        closeAlert,
        description: err.message || MESSAGE.onError.description,
        isSuccess: true
      })
    },
  }),
  props: ({mutate, ownProps: {showAlert, setSubmit, closeAlert, resetState, navigation}, result, result: {loading}}) => ({
    insertAppointment: async vars => {
      setSubmit(true);
      const {data, errors} = await mutate({...vars});
      const dismissAlert = R.isNil(data) ?
      closeAlert :
      () => {
        closeAlert();
        resetState();
        navigation.navigate("CustomerCare");
      };
      showAlert({
        title: MESSAGE[data ? "success" : "error"].title,
        shouldRenderOkButton: true,
        closeAlert: dismissAlert,
        description: (!R.isNil(errors) && R.last(errors).message) || MESSAGE[data ? "success" : "error"].description,
        isError: R.isNil(data),
        isSuccess: !R.isNil(data)
      });
    },
    loading,
    result,
    dataMutate: mutate
  }),
});

const withEmptyData = branch(({scheduleState: {doctor, currentSlot}}) => !doctor || !currentSlot, renderComponent(SummaryEmpty))

const enchance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  withNavigation,
  stateSubmit,
  stateError,
  withSubmit,
  withEmptyData
);

export default enchance(Summary);

