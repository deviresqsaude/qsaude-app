import React from 'react';
import { connect } from 'react-redux';
import { compose, withProps, withHandlers, lifecycle } from 'recompose';
import { ScheduleActions } from '../../../store/actions';
import Schedule from './Schedule';

const mapStateToProps = ({schedule}) => ({
  scheduleState: {...schedule}
});

const mapDispatchToProps = dispatch => ({
  resetState: () => dispatch(ScheduleActions.actions.resetState()),
  setScheduleMode: mode => dispatch(ScheduleActions.actions.setScheduleMode(mode)),
  setScheduleStep: step => dispatch(ScheduleActions.actions.setScheduleStep(step)),
});

const scheduleMode = {
  1: {name: "Médico", steps: 3},
  2: {name: "Data", steps: 4},
  3: {name: "Endereco", steps: 4},
  4: {name: "CRM", steps: 3},
  5: {name: "Unidade", steps: 5},
};

const scheduleRefs = withProps(() => ({
  refList: React.createRef()
}));

const propsSchedule = withProps(props => {
  const {navigation: {state: {params: {mode}}}, scheduleState: {step}} = props;
  return {
    title: `Agendar consulta`,
    steps: scheduleMode[mode].steps,
    step,
    mode
  }
});

const handlers = withHandlers({
  goToStep: ({setScheduleStep, refList}) => step => {
    refList.current.scrollToIndex({animated: true, index: step});
    setScheduleStep(step);
  }
});

const life = lifecycle({
  componentDidMount() {
    const {setScheduleMode, resetState, navigation: {state: {params: {mode}}}} = this.props;
    resetState();
    setScheduleMode(mode);
  }
})

const enchance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  scheduleRefs,
  propsSchedule,
  handlers,
  life
);

export default enchance(Schedule);
