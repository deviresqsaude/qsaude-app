import { compose, withHandlers } from 'recompose';
import { connect } from 'react-redux';
import { ScheduleActions } from '../../../../store/actions';
import Units from './Units';

const mapStateToProps = ({schedule}) => ({
  scheduleState: {...schedule}
});

const mapDispatchToProps = dispatch => ({
  setCurrentUnit: unit => dispatch(ScheduleActions.actions.setCurrentUnit(unit))
});

const handlers = withHandlers({
  setUnit: ({setCurrentUnit, onCallBack, scheduleState: {step}}) => unit => {
    setCurrentUnit(unit);
    onCallBack(step + 1);
  }
});

const enchance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  handlers
);

export default enchance(Units);

