import React from 'react';
import { Text, FlatList, View } from 'react-native';
import { MyDoctorCard } from '../../../../components';
import { Colors } from '../../../../constants/theme';

const MESSAGE_UNIT = "Insira a unidade de sua preferência:";

const Units = ({scheduleState: {unitsList}, setUnit}) => {
  return (
    <View style={{marginHorizontal: 15}}>
      <Text style={{ color: Colors.TextGray, marginBottom: 0}}>{MESSAGE_UNIT}</Text>
      <FlatList
        data={unitsList}
        showsVerticalScrollIndicator={false}
        keyExtractor={({id}) => id}
        renderItem={({item}) => (
          <MyDoctorCard onPress={() => setUnit(item)} {...item}/>
        )}/>
    </View>
  )
}

Units.propTypes = {};

export default Units;
