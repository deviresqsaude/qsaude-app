import React from 'react';
import { FlatList } from 'react-native';
import R from 'ramda';
import { Container, ScheduleItem } from './ScheduleStyle';
import { QText, Stepper } from '../../../components';
// Process Import
import Calendar from './Calendar';
import Company from './Company';
import Doctors from './Doctors';
import Slots from './Slots';
import Summary from './Summary';
import Units from './Units';
import Address from './Address';
import Map from './Map';

const components = {Calendar, Company, Doctors, Slots, Summary, Units, Address, Map};

const scheduleMode = mode => {
  return R.cond([
    [R.equals(3), () => ['Address', 'Map', 'Calendar', 'Summary']],
    [R.equals(2), () => ['Calendar', 'Doctors', 'Slots', 'Summary']],
    [R.equals(5), () => ['Company', 'Units', 'Doctors', 'Calendar', 'Summary']],
    [R.T, () => ['Doctors', 'Calendar', 'Summary']]
  ])(mode)
}

const scheduleProcess = ({mode, refList, goToStep}) => {
  const process = scheduleMode(mode);
  return (
    <FlatList
      showsHorizontalScrollIndicator={false}
      ref={refList}
      style={{width: "100%", flexGrow: 1, marginBottom: 15, marginTop: 10}}
      data={process}
      scrollEnabled={false}
      horizontal
      keyExtractor={item => item}
      renderItem={({item}) => {
        const ItemComponent = components[item];
        return (
          <ScheduleItem>
            <ItemComponent onCallBack={step => goToStep(step)}/>
          </ScheduleItem>
        )
      }}
    />
  )
}

const Schedule = props => {
  const {title, steps, scheduleState: {step}, setScheduleStep, goToStep, mode} = props;
  // const isLastStep = R.equals((steps - 1), step);
  return (
    <Container>
      {/* <QText preset="navTitle" style={{marginBottom: 15, paddingHorizontal: 15}}>{title}</QText> */}
      <Stepper
        style={{marginHorizontal: 15}}
        numberSteps={steps}
        currentStep={step}
        onStepHandler={st => {
          if(st <= step) {
            setScheduleStep(st);
            goToStep(st);
          };
        }}/>
      {scheduleProcess({...props})}
      {/* <QButton text={isLastStep ? "Agendar" : "Próximo"}
        disabled={loading}
        onPress={() => isLastStep ? sendSchedule() : goToStep(step + 1)}
        style={{marginBottom: 15}}
      /> */}
    </Container>
  )
}

Schedule.propTypes = {};

export default Schedule;

