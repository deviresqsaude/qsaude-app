import { graphql } from '@apollo/react-hoc';
import { Animated } from 'react-native';
import { connect } from 'react-redux';
import { compose, withHandlers, withState } from 'recompose';
import { MyDoctorsQuery } from '../../../../service/query';
import { geolocation as geoHelper } from '../../../../helpers';
import { ScheduleActions } from '../../../../store/actions';
import Map from './Map';

// Redux
const mapStateToProps = ({schedule}) => ({
  scheduleState: {...schedule}
});
const mapDispatchToProps = dispatch => ({
  setCurrentDoctor: doctor => dispatch(ScheduleActions.actions.setDoctor(doctor)),
  clearCalendar: () => dispatch(ScheduleActions.actions.clearCalendar()),
});
// End Redux

const stateLoading = withState('loading', 'setLoading', false);
const stateCollapse = withState('collapse', 'setCollapse', false);
const stateSelected = withState('selectedDoctor', 'setSelected', {});
const stateDoctors = withState('allDoctors', 'setDoctors', []);
const stateMarkers = withState('allMarkers', 'setMarkers', []);
const stateAnimateCard = withState('animateDoctors', 'setAnimateDoctors', new Animated.Value(150));

const handlers = withHandlers({
  onAddresSearch: ({setLoading}) => async address => {
    // TODO: Adicionar o appKey no google maps da conta da Qsaude para ver funcionando o GelocationAPI
    setLoading(true);
    const geo = await geoHelper(address);
    setLoading(false);
  },
  toggleDoctors: ({animateDoctors, collapse, setCollapse}) => () => {
    Animated.spring(animateDoctors, {
      toValue: !collapse ? 0 : 150,
    }).start();
    setCollapse(!collapse);
  },
  selectDoctor: ({setSelected}) => doctor => {
    setSelected(doctor);
  },
  scheduleDoctor: ({setCurrentDoctor, clearCalendar, selectedDoctor, onCallBack, scheduleState: {step}}) => () => {
    setCurrentDoctor(selectedDoctor);
    clearCalendar();
    onCallBack(step + 1);
  }
});

const withData = graphql(
  MyDoctorsQuery, {
    skip: ({scheduleState: {address}}) => !address,
    options: () => ({
      notifyOnNetworkStatusChange: true,
    }),
    props: ({data}) => {
      const {loading, myDoctors} = data;
      return {
        loadingDoctors: loading,
        myDoctors: myDoctors || []
      }
    },
  }
);

const enchance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  stateLoading,
  stateCollapse,
  stateSelected,
  stateDoctors,
  stateMarkers,
  stateAnimateCard,
  handlers,
  withData,
);

export default enchance(Map);
