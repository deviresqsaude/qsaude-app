import styled from 'styled-components/native';
import { Colors } from '../../../../constants/theme';

export const DoctorsContainer = {
  backgroundColor: Colors.White,
  borderTopRightRadius: 12,
  borderTopLeftRadius: 12,
  bottom: 0,
  height: 335,
  left: 0,
  position: "absolute",
  flexDirection: "column",
  width: "100%",
  zIndex: 9998
};

export const MapContainer = styled.View({
  position: "relative",
  // height: 370,
  flex: 1
});

export const FloatSearch = styled.View({
  position: "absolute",
  left: 0,
  height: 45,
  top: 30,
  zIndex: 9999,
  flex: 1,
  paddingHorizontal: 15,
  width: "100%"
});

export const DoctorItem = styled.View(({selected}) => ({
  borderRadius: 5,
  backgroundColor: Colors[selected ? "WhiteSmoke" : "White"],
  borderBottomWidth: 1,
  borderColor: Colors.WhiteSmoke,
  paddingVertical: 7,
  paddingHorizontal: 10,
  position: "relative",
  marginBottom: 4
}));

export const ButtonContainer = styled.View({
  position: "absolute",
  paddingHorizontal: 15,
  bottom: 0,
  left: 0,
  height: 62,
  justifyContent: "center",
  alignItems: "center",
  zIndex: 9999,
  width: "100%",
})
