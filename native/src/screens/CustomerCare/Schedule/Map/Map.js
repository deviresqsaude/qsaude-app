import React from 'react';
import R from 'ramda';
import { FlatList, TouchableWithoutFeedback, View, Animated } from 'react-native';
import { MapView, QSearch, QText, QButton, SystemIcon } from '../../../../components';
import { MapContainer, FloatSearch, DoctorsContainer, DoctorItem, ButtonContainer } from './MapStyle';
import { elevationShadow } from '../../../../helpers';
import { Colors } from '../../../../constants/theme';

const MESSAGES = {
  loading: "Carregando médicos, aguarde um momento",
  empty: "Não foram localizados médicos nessa região"
}

const Doctor = ({doctor, selectDoctor, selected}) => (
  <TouchableWithoutFeedback onPress={() => selectDoctor(doctor)}>
    <DoctorItem selected={selected}>
      <QText preset="link">{doctor.name}</QText>
      <View style={{paddingRight: 70, position: "relative"}}>
        <QText preset="body">{doctor.address}</QText>
        <QText preset="link" style={{position: "absolute", top: 0, right: 0}}>{doctor.distance || "0km"}</QText>
      </View>
    </DoctorItem>
  </TouchableWithoutFeedback>
)

const Map = ({loadingDoctors, markers, myDoctors, loading, onAddresSearch, scheduleDoctor, collapse, toggleDoctors, animateDoctors, selectDoctor, selectedDoctor}) => {
  return (
    <MapContainer>
      <FloatSearch>
        <QSearch
          noBorder
          clear
          disabled={loading}
          placeholder="Digite aquí o endereço ou CEP"
          onEndEditing={s => onAddresSearch(s)}
          style={{marginBottom: 15, ...elevationShadow(null, 5, null)}}/>
      </FloatSearch>
      <MapView {...{markers}}/>
      <Animated.View style={{...DoctorsContainer, paddingBottom: 80, transform: [{translateY: animateDoctors}] }}>
        <TouchableWithoutFeedback onPress={() => toggleDoctors()}>
          <View style={{width: "100%", height: 40, justifyContent: "center", alignItems: "center"}}>
            <View style={{height: 20, width: 20, justifyContent: "center", alignItems: "center", transform: [{rotate: `${collapse ? 180 : 0}deg`}]}}>
              <SystemIcon name="chevromDown" tintColor={Colors.Purple}/>
            </View>
          </View>
        </TouchableWithoutFeedback>
        <FlatList
          style={{marginHorizontal: 10}}
          data={myDoctors}
          ListEmptyComponent={
            <QText preset="body">{MESSAGES[loadingDoctors ? "loading" : "empty"]}</QText>
          }
          keyExtractor={({practitionerId}) => practitionerId.toString()}
          renderItem={({item}) => <Doctor {...{doctor: item, selectDoctor, selected: item.practitionerId === selectedDoctor.practitionerId}}/>}/>
      </Animated.View>
      <ButtonContainer>
        <QButton text="Agendar"
          disabled={R.isEmpty(selectedDoctor)}
          onPress={() => scheduleDoctor()}
          style={{marginVertical: 15}}
        />
      </ButtonContainer>
    </MapContainer>
  )
}

Map.propTypes = {};

Map.defaultProps = {
  markers: [],
  doctors: [
    {practitionerId: 1001, name: "Kelvin Arnold", crm: "2391923", address: "Alameda Apetupas 154 - Planalto Paulista ZS Moema", distance: "12km"},
    {practitionerId: 1002, name: "Kelvin Arnold", crm: "2391923", address: "Alameda Apetupas 154 - Planalto Paulista ZS Moema", distance: "12km"},
    {practitionerId: 1003, name: "Kelvin Arnold", crm: "2391923", address: "Alameda Apetupas 154 - Planalto Paulista ZS Moema", distance: "12km"},
    {practitionerId: 1004, name: "Kelvin Arnold", crm: "2391923", address: "Alameda Apetupas 154 - Planalto Paulista ZS Moema", distance: "12km"},
    {practitionerId: 1005, name: "Kelvin Arnold", crm: "2391923", address: "Alameda Apetupas 154 - Planalto Paulista ZS Moema", distance: "12km"},
  ]
}

export default Map;

