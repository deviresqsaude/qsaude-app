import { Dimensions } from 'react-native';
import styled from 'styled-components/native';

export const Container = styled.View({
  flex: 1,
  // paddingHorizontal: 16,
});

export const ScheduleItem = styled.View({
  width: Dimensions.get('screen').width,
  // width: Dimensions.get('screen').width - 32,
  flex: 1
});
