import Geolocation from 'react-native-geolocation-service';
import { Platform, Alert } from 'react-native';
import { connect } from 'react-redux';
import { request, PERMISSIONS, RESULTS, openSettings } from 'react-native-permissions';
import { compose, withHandlers, withState } from 'recompose';
import { geolocation as geoHelper } from '../../../../helpers';
import { ScheduleActions } from '../../../../store/actions';
import Address from './Address';

const permissionsAlert = () => {
  Alert.alert(
    'Permissões negadas',
    'Acesse as configurações do aplicativo e conceda as permissões necessárias',
    [
      {
        text: 'Configurações',
        onPress: () => openSettings().catch(e => console.log('failed to open settings', e)),
      },
      {
        text: 'Cancelar',
        style: 'destructive',
      },
    ],
  );
}

// Redux
const mapStateToProps = ({schedule}) => ({
  scheduleState: {...schedule}
});
const mapDispatchToProps = dispatch => ({
  setAddress: response => dispatch(ScheduleActions.actions.setAddress(response)),
});
// End Redux

const stateLoading = withState('loading', 'setLoading', false);
const stateError = withState('error', 'setError', false);

const handlers = withHandlers({
  onAddresSearch: ({setLoading, setError}) => async address => {
    if(!address) return;
    setLoading(true);
    const geo = await geoHelper(address);
    setLoading(false);
    if(geo.error) { setError(true); }
  },
  onSelectAddress: ({onCallBack, setAddress, scheduleState: {step}}) => address => {
    // TODO: Adicionar logica para pegar a latitude e longitude do endereço selecionado "address"
    setAddress({latitude: -23.5950707, longitude: -46.6237895,});
    onCallBack(step + 1);
  },
  onCurrentlocation: ({setAddress, onCallBack, scheduleState: {step}}) => () => {
    const permissionString = Platform.OS === 'ios' ? PERMISSIONS.IOS.LOCATION_WHEN_IN_USE : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION;
    request(permissionString).then(result => {
      switch (result) {
        case RESULTS.GRANTED:
          Geolocation.getCurrentPosition(
            ({coords: { latitude, longitude }}) => {
              setAddress({latitude, longitude});
              onCallBack(step + 1);
            },
            e => e.PERMISSION_DENIED === 1 ? permissionsAlert() : alert('Aconteceu um erro, tente novamente ou entre en contato com a QSaude'),
            { timeout: 5000 }
          );
          break;
        case RESULTS.DENIED:
        case RESULTS.BLOCKED:
        case RESULTS.UNAVAILABLE:
          permissionsAlert();
          break;
        default:
          break;
      }
    });
  }
});

const enchance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  stateLoading,
  stateError,
  handlers,
);

export default enchance(Address);
