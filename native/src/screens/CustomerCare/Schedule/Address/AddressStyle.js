import styled from 'styled-components/native';
import { Colors, SystemIcons} from '../../../../constants/theme';

export const AddressTouch = styled.TouchableWithoutFeedback({
  position: "relative"
});

export const AddressWrapper = styled.View({
  paddingTop: 15,
  paddingBottom: 15,
  borderBottomWidth: 1,
  borderColor: Colors.WhiteSmoke,
  paddingLeft: 34,
})

export const AddressText = styled.Text({
  color: Colors.TextGray,
  fontSize: 12
})

export const ImageItem = styled.Image.attrs(({name}) => ({
  source: SystemIcons[name]
}))({
  tintColor: Colors.Purple,
  position: "absolute",
  left: 4,
  top: 8,
  height: 24,
  width: 24,
});

// export const AddressItem = styled.TouchableWithoutFeedback
