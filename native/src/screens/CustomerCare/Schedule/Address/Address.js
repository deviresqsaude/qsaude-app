import React from 'react';
import { View, FlatList } from 'react-native';
import { QSearch } from '../../../../components';
import { AddressTouch, AddressWrapper, AddressText, ImageItem } from './AddressStyle'

const MESSAGE = {
  addressEmpty: "Local não encontrado pelo endereço informado...",
  currentLocation: "Usar localização atual"
}

const EmptyAddress = () => (
  <AddressWrapper style={{alignItems: "center"}}>
    <AddressText>{MESSAGE.addressEmpty}</AddressText>
  </AddressWrapper>
)

const AddressItem = ({image, address, onPress}) => (
  <AddressTouch {...{onPress}}>
    <AddressWrapper>
      <ImageItem name={image}/>
      <AddressText>{address}</AddressText>
    </AddressWrapper>
  </AddressTouch>
)

const Address = ({error, onAddresSearch, onSelectAddress, onCurrentlocation, loading}) => {
  return (
    <View style={{marginHorizontal: 15}}>
      <QSearch
        disabled={loading}
        clear
        placeholder="Digite aquí o endereço ou CEP"
        onEndEditing={s => onAddresSearch(s)}
        style={{marginBottom: 15}}/>
      <AddressItem image="map" address={MESSAGE.currentLocation} onPress={() => onCurrentlocation()}/>
      <FlatList data={[]}
        ListEmptyComponent={error && EmptyAddress}
        renderItem={({item}) => <AddressItem image="map" address={item} onPress={() => onSelectAddress(item)}/>}/>
    </View>
  )
}

Address.propTypes = {};

export default Address;

