import { connect } from 'react-redux';
import { compose, withProps, withState, withHandlers, renderComponent, branch, lifecycle } from 'recompose';
import { withNavigation } from 'react-navigation';
import { graphql } from '@apollo/react-hoc';
import CustomerCareScreen from './CustomerCareScreen';
import { SpecialityQuery } from '../../service/query';
import { PROFILECUSTOMERCARE } from '../../constants/TestIds';
import { AlertActions, NavActions } from '../../store/actions';

const APPOINTMENTS = [
  {
    text: 'Seu médico pessoal',
    testID: PROFILECUSTOMERCARE.AppointmentCategory.personalDoctor,
    key: 'PersonalDoctor',
    link: 'ScheduleFilter',
    modalityType: 1
  },
  {
    text: 'Agendar com um especialista',
    testID: PROFILECUSTOMERCARE.AppointmentCategory.scheduleSpecialist,
    key: 'ExpertList',
    link: 'ScheduleExpert',
    modalityType: 3
  },
];

const mapDispatchToProps = dispatch => ({
  setNavCurrentPage: state => dispatch(NavActions.actions.setCurrentPage(state)),
  onServiceFailed: ({ title, error: description }) =>
    dispatch(
      AlertActions.actions.openAlert({
        title: "Ocorreu um erro ao trazer lista de médicos para agendamento, tente novamente",
        shouldRenderOkButton: true,
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        description,
        isError: true,
      }),
    ),
});


const propsCare = withProps(({ navigation: { state: { routeName } } }) => ({
  title: routeName,
  appointments: APPOINTMENTS,
}));

const stateModal = withState('modal', 'setModal', false);
const stateSpecialities = withState('specialities', 'showSpeciality', false);

const handlers = withHandlers({
  goTo: ({navigation: {navigate}, setModal}) => (type, specialities) => {
    const TYPES = {
      1: {link: "Schedule", mode: 1, params: {type, mode: 1}},
      2: {link: "ScheduleExpert", mode: 2, params: {type, specialities}},
      3: {link: "CredentialNetwork", mode: 3, params: {type, specialities}},
    };
    // const params = type !== 2 ? {type, mode: 1} : { type, specialities };
    const {link, params} = TYPES[type];
    navigate(link, params);
    setModal(false);
  },
  toggleModal: ({setModal, modal}) => () => {
    setModal(!modal);
  }
});

const withSpecialtyData = graphql(SpecialityQuery, {
  name: "dataSpeciality",
  skip: ({modal}) => !modal,
});

const withRefetch = withProps(({ dataSpeciality }) => ({refetch: dataSpeciality && dataSpeciality.refetch}));

const life = lifecycle({
  componentDidMount() {
    const {navigation, setNavCurrentPage} = this.props;
    setNavCurrentPage(navigation.state);
  },
});

const enhance = compose(
  connect(
    null,
    mapDispatchToProps
  ),
  propsCare,
  withNavigation,
  stateModal,
  stateSpecialities,
  withSpecialtyData,
  withRefetch,
  handlers,
  life
);

export default enhance(CustomerCareScreen);
