import React from 'react';
import { MenuList, QText } from '../../../components';
import { Container, DescriptionWrapper } from './FilterStyle';

const Filter = ({filterList}) => (
  <Container>
    <DescriptionWrapper><QText preset="dataText">Selecione como gostaria de pesquisar o seu especialista:</QText></DescriptionWrapper>
    <MenuList menuData={filterList}/>
  </Container>
);

export default Filter;
