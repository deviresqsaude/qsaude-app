import { compose, withProps } from 'recompose';
import { SystemIcons } from '../../../constants/theme';
import Filter from './Filter';

const FILTER_LINKS = [
  {
    icon: SystemIcons.doctor,
    title: 'Por Médico',
    link: "Schedule",
    params: {mode: 1},
    testID: "SCHEDULE.doctor",
    // size: { height: 40, width: 28}
  },
  {
    icon: SystemIcons.calendar,
    title: 'Por Data',
    link: "Schedule",
    params: {mode: 2},
    testID: "SCHEDULE.date",
    // size: { height: 40, width: 28}
  },
  {
    icon: SystemIcons.map,
    title: 'Por Endereço',
    link: "Schedule",
    params: {mode: 3},
    testID: "SCHEDULE.address",
    // size: { height: 40, width: 28}
  },
  {
    icon: SystemIcons.crm,
    title: "Por CRM",
    link: "Schedule",
    params: {mode: 4},
    testID: "SCHEDULE.crm",
    // size: { height: 40, width: 28}
  },
  {
    icon: SystemIcons.unidade,
    title: "Por Marca",
    link: "Schedule",
    params: {mode: 5},
    testID: "SCHEDULE.brand",
    // size: { height: 40, width: 28}
  },
]

const propsFilter = withProps(() => ({
  filterList: FILTER_LINKS
}));

const enchance = compose(
  propsFilter
);

export default enchance(Filter);
