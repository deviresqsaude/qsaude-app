import styled from "styled-components/native";

export const Container = styled.View({
  flexDirection: 'column'
});

export const Separator = styled.View({
  height: 16,
  width: "100%"
});

export const ModalWrapper = styled.View({
  backgroundColor: 'rgba(0, 0, 0, .6)',
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  padding: 16,
});
