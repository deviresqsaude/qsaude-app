import React from 'react';
import R from 'ramda';
import { Linking, Modal, ActivityIndicator } from 'react-native';
import Config from 'react-native-config';
import PropTypes from 'prop-types'
import { Container, ModalWrapper } from './CustomerCareStyle';
import { MenuList } from '../../components/MenuList';
import { SystemIcons } from '../../constants/theme';
import { QButton } from '../../components';
// import ScheduleAppointment from './ScheduleAppointment';
import { PROFILECUSTOMERCARE } from '../../constants/TestIds';

// eslint-disable-next-line no-unused-vars
const CustomerCare = ({ dataSpeciality: { allSpecialities, loading }, toggleModal, modal, goTo }) => {
  const ATTENDANCE_MENU = [
    {
      icon: SystemIcons.doctor,
      title: 'Agendar uma consulta',
      link: () => toggleModal(),
      testID: PROFILECUSTOMERCARE.itemsList.ScheduleAppointment,
      // size: { height: 40, width: 28},
    },
    {
      icon: SystemIcons.lab,
      title: 'Agendar um exame',
      link: 'ExamList',
      testID: PROFILECUSTOMERCARE.itemsList.ScheduleExam,
      // size: { height: 37, width: 31}
    },
    {
      icon: SystemIcons.whatsapp,
      title: 'Tirar suas dúvidas',
      link: () => Linking.openURL(`http://api.whatsapp.com/send?phone=${Config.WHATSAPP_LINKNUMBER}`),
      testID: PROFILECUSTOMERCARE.itemsList.AskQuestions,
      // size: { height: 27, width: 27}
    },
    {
      icon: SystemIcons.search,
      title: 'Buscar parceiros na saúde',
      link: 'HospitalsFilter',
      testID: PROFILECUSTOMERCARE.itemsList.HealthPartners,
      // size: { height: 23, width: 21}
    },
  ];
  const specialitiesFilter = !allSpecialities ? [] : R.filter(i => i.modalityType === 2, allSpecialities);
  return (
    <Container>
      <MenuList menuData={ATTENDANCE_MENU}/>
      <Modal
        onRequestClose={() => toggleModal()}
        transparent
        animated
        animationType="fade"
        visible={modal}>
        <ModalWrapper>
          {
            // eslint-disable-next-line no-nested-ternary
            loading ?
            <ActivityIndicator size="large" color="#FFFFFF"/> :
            !allSpecialities ?
            <QButton
              onPress={() => toggleModal(!modal)}
              preset="secondary"
              style={{ marginBottom: 10 }}
              text="Não há encaminhamentos"
            /> :
            <>
              {
                R.map(({display: text, code: testID, cbo, modalityType}) =>
                  <QButton
                    key={testID}
                    onPress={() => goTo(modalityType, cbo)}
                    preset="secondary"
                    style={{ marginBottom: 10 }}
                    {...{ text: modalityType === 1 ? "MÉDICO PESSOAL" : text.toUpperCase(), testID }}
                  />,
                R.filter(item => item.modalityType !== 2, allSpecialities || []))
              }
              {
                specialitiesFilter.length > 0 &&
                <QButton
                  testID={PROFILECUSTOMERCARE.AppointmentCategory.butttonCancel}
                  onPress={() => goTo(2, specialitiesFilter)}
                  preset="secondary"
                  style={{ marginBottom: 10 }}
                  badge={specialitiesFilter.length}
                  text="AGENDAR COM ESPECIALISTAS"
                />
              }
              <QButton
                testID={PROFILECUSTOMERCARE.AppointmentCategory.butttonCancel}
                onPress={() => toggleModal(!modal)}
                preset="cancel"
                text="CANCELAR"
              />
            </>
          }
        </ModalWrapper>
      </Modal>
    </Container>
  );
};

CustomerCare.propTypes = {
  dataSpeciality: PropTypes.shape({
    allSpecialities: PropTypes.array,
    loading: PropTypes.bool,
  }),
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

CustomerCare.defaultProps = {
  dataSpeciality: {
    allSpecialities: [],
    loading: false
  },
};

export default CustomerCare;
