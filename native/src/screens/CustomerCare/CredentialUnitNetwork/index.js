import { compose, withProps } from 'recompose';
import CredentialUnitNetwork from './CredentialUnitNetwork';

const withUnits = withProps(({navigation}) => ({
  units: navigation.getParam('units')
}));

const enchance = compose(
  withUnits
);

export default enchance(CredentialUnitNetwork);
