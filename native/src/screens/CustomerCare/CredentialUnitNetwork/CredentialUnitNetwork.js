import React from 'react';
import R from 'ramda';
import PropTypes from 'prop-types';
import { View, FlatList} from 'react-native';
import { DoctorCard } from '../../../components';

const CredentialNetwork = ({units}) => {
  return (
    <View style={{
      flex: 1,
      marginHorizontal: 0,
      paddingVertical: 16,
    }}>
      <FlatList
        renderItem={({ item }) => <DoctorCard {...item} />}
        keyExtractor={({ title }) => title}
        data={units}
      />
    </View>
  );
};

CredentialNetwork.propTypes = {
  units: PropTypes.arrayOf(PropTypes.any)
};

CredentialNetwork.defaultProps = {
  units: []
};

export default CredentialNetwork;
