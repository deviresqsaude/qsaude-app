import React from 'react';
import styled from 'styled-components/native';
import { Placeholder, Fade, PlaceholderMedia } from 'rn-placeholder';
import { EmptyData } from '../../../components';
import { SystemImages } from '../../../constants/theme';

export const Wrapper = styled.View({
  flex: 1,
  marginHorizontal: 0,
  paddingVertical: 16,
  paddingHorizontal: 16
});

export const CredentialError = styled(EmptyData).attrs(props => ({
  title: 'Ops!!',
  description: 'Aconteceu um problema, Tente novamente.',
  buttonText: 'Tentar novamente',
  onPress: () => props.refetch,
  imageSource: SystemImages.errorImage,
}))``;

export const CredentialEmpty = styled(EmptyData).attrs(() => ({
  title: 'Ops!!',
  description: 'Não existem parceiros atendendo neste momento!',
  imageSource: SystemImages.calendarEmpty,
}))``;

export const EmptyContent = styled.View({});

export const placeholderstyle = {
  width: '100%',
  height: 56,
  marginBottom: 12,
  borderRadius: 4,
};

export const CredentialLoading = () => (
  <Wrapper style={{paddingHorizontal: 16,}}>
    <Placeholder Animation={Fade}>
      <PlaceholderMedia style={placeholderstyle} />
      <PlaceholderMedia style={placeholderstyle} />
      <PlaceholderMedia style={placeholderstyle} />
      <PlaceholderMedia style={placeholderstyle} />
      <PlaceholderMedia style={placeholderstyle} />
    </Placeholder>
  </Wrapper>
);
