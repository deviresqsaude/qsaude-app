import { compose, withProps, withHandlers, branch, renderComponent } from 'recompose';
import R from 'ramda';
import { withNavigation } from 'react-navigation'
import { graphql } from '@apollo/react-hoc';
import getDirections from 'react-native-google-maps-directions';
import CredentialNetworkList from './CredentialNetwork';
import { CredentialError, CredentialEmpty, CredentialLoading } from './CredentialNetworkStyle';
import { HospitalQuery } from "../../../service/query";

const VARIABLES_HEALT = {
  specialty: "",
  serviceType: "",
  healthProvider: "",
  healthProviderUnit: "",
  procedureTUSSCode: "",
}

const withData = graphql(HospitalQuery, {
  options: ({navigation}) => ({
    notifyOnNetworkStatusChange: true,
    variables: {...VARIABLES_HEALT, ...{specialty: navigation.getParam('specialities')}}
  }),
});

const renderForLoading = branch(({data}) => data && data.loading, renderComponent(CredentialLoading));

const setApolloRefetch = withProps(({data}) => ({refetch: data && data.refetch}));

const renderForError = branch(({ data }) => data && data.error, renderComponent(CredentialError));

const renderWithNull = branch(({ data }) => !data.error && !data.loading && R.isEmpty(data.getAccreditedNetwork), renderComponent(CredentialEmpty))

const experts = withProps(({navigation}) => ({
  speciality: navigation.getParam('specialities')
}));

const handlers = withHandlers({
  openNavigation: ({ source }) => ({ destination }) => {
    const data = {
      source,
      destination,
      params: [
        {
          key: 'travelmode',
          value: 'driving', // may be "walking", "bicycling" or "transit" as well
        },
        {
          key: 'dir_action',
          value: 'navigate', // this instantly initializes navigation using the given travel mode
        },
      ],
    };
    getDirections(data);
  },
});

const enchance = compose(
  withNavigation,
  withData,
  experts,
  renderForLoading,
  setApolloRefetch,
  renderForError,
  renderWithNull,
  handlers
);

export default enchance(CredentialNetworkList)
