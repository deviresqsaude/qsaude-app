import React from 'react';
import R from 'ramda';
import PropTypes from 'prop-types';
import Config from 'react-native-config';
import { FlatList, Linking } from 'react-native';
import { SystemIcons } from '../../../constants/theme';
import { Wrapper } from './CredentialNetworkStyle';
import { ExamCard } from '../../../components';

const CredentialNetwork = props => {
  const {data: {getAccreditedNetwork}, navigation} = props;

  const refactorUnits = units => R.map(item => ({
    title: item.unitName,
    description: item.address || "-",
    links: [
      {
        icon: SystemIcons.whatsapp,
        label: "Agende com o TimeQ",
        onPress: () => Linking.openURL(`https://api.whatsapp.com/send?phone=${Config.WHATSAPP_LINKNUMBER}`),
      },
      {
        icon: SystemIcons.phone,
        label: item.phone || "-",
        visible: !!item.phone,
        onPress: () => Linking.openURL(`tel:${item.phone}`),
      },
      {
        icon: SystemIcons.webLink,
        label: item.site,
        visible: !!item.site,
        onPress: () => Linking.openURL(item.site),
      },
      {
        icon: SystemIcons.navigation,
        label: 'Ver como chegar',
        visible: item.latitude && item.longitude,
        onPress: () =>
          props.openNavigation({
            destination: {
              latitude: parseFloat(item.latitude),
              longitude: parseFloat(item.longitude)
            }
          }),
        disabled: !(!!item.latitude && !!item.longitude)
      }
    ],
    coordinate: {
      latitude: parseFloat(item.latitude),
      longitude: parseFloat(item.longitude),
    },
  }), units);

  const refactor = list => R.map(({name, units}) => ({
    name,
    units: refactorUnits(units)
  }), list);

  return (
    <Wrapper>
      <FlatList
        renderItem={({ item }) => <ExamCard name={item.name} onPress={() => navigation.navigate("UnitsNetwork", {units: item.units})}/>}
        keyExtractor={({ code }) => code}
        data={refactor(getAccreditedNetwork)}
        showsVerticalScrollIndicator={false}
      />
    </Wrapper>
  );
};

CredentialNetwork.propTypes = {
  data: PropTypes.shape({
    getAccreditedNetwork: PropTypes.array,
    loading: PropTypes.bool,
  })
};

CredentialNetwork.defaultProps = {
  data: {
    getAccreditedNetwork: [],
    loading: PropTypes.bool
  },
};

export default CredentialNetwork;
