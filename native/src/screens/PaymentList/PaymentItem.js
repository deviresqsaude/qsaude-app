import React from 'react';
import { Linking, Clipboard } from 'react-native';
import PropTypes from 'prop-types';
import Config from 'react-native-config';

import { QButton, QText } from '../../components';
import { Colors } from '../../constants/theme';
import { PaymentItemWrapper, PaymentItemRow, PaymentItemText } from './PaymentListStyle';
import { shareBoleto, getStatus } from '../Payment/PaymentScreen';

const copyBoleto = ({ boleto, onCopySuccess }) => {
  const boletoCodeWithoutSpace = boleto.numberCode.replace(/\s/g, '');
  Clipboard.setString(boletoCodeWithoutSpace);
  // Alert.alert('Código de barras copiado!');
  onCopySuccess({boleto})
};


const PaymentItem = props => {
  const { titleAmount, dueDate, statusBankSlip, url, error } = props;
  return (
    <PaymentItemWrapper>
      <PaymentItemRow>
        <PaymentItemText>R$ {titleAmount}</PaymentItemText>
      </PaymentItemRow>
      <PaymentItemRow>
        <PaymentItemText>Vencimento: {dueDate}</PaymentItemText>
      </PaymentItemRow>
      <PaymentItemRow>
        <PaymentItemText>Status: </PaymentItemText>
        <PaymentItemText tintColor={statusBankSlip == 'Vencido' && Colors.Magenta} bold>
          {getStatus(statusBankSlip)}
        </PaymentItemText>
      </PaymentItemRow>
      {statusBankSlip == 'Vencido' || error ? (
        <QButton
          style={{ marginTop: 12 }}
          text='Falar com o TimeQ'
          onPress={() =>
            Linking.openURL(
              `https://api.whatsapp.com/send?phone=${Config.WHATSAPP_LINKNUMBER}`,
            )
          }
        />
      ) : (
        <>
          <PaymentItemRow>
            <QText
              preset='link'
              onPress={() => copyBoleto({boleto: props, onCopySuccess: props.onCopySuccess})}
              style={{ paddingBottom: 5 }}
            >
              Copiar código de barras
            </QText>
          </PaymentItemRow>
          <PaymentItemRow>
            <QText
              style={{ paddingBottom: 5 }}
              preset='link'
              onPress={() =>
                Linking.openURL(
                  `https://api.whatsapp.com/send?phone=${Config.WHATSAPP_LINKNUMBER}`,
                )
              }
            >
              Falar com o TimeQ
            </QText>
          </PaymentItemRow>
          <QButton
            style={{ marginTop: 6 }}
            preset={'invert'}
            text='Compartilhar 2ª via de boleto'
            onPress={() => shareBoleto({ boletoLink: url })}
          />
        </>
      )}
    </PaymentItemWrapper>
  );
};

PaymentItem.propTypes = {
  titleAmount: PropTypes.string,
  dueDate: PropTypes.string,
  statusBankSlip: PropTypes.string,
};

PaymentItem.defaultProps = {
  titleAmount: '',
  dueDate: '',
  statusBankSlip: '',
};

export default PaymentItem;
