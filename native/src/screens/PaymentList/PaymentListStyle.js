import styled from 'styled-components/native';
import { LaboratorySeparator } from '../Laboratory/LaboratoryStyle';

export const PaymentListLoadingWrapper = styled.View({
  padding: 16
})

export const PaymentListContainer = styled.ScrollView.attrs({
  contentContainerStyle: {
    flex: 1
  }
})({
  paddingHorizontal: 16,
  paddingBottom: 16,
  flex: 1
})

export const PaymentItemSeperator = styled(LaboratorySeparator)``;


export const PaymentItemWrapper = styled.View({
})

export const PaymentItemRow = styled.View({
  marginBottom: 5,
  alignItems: 'center',
  flexDirection: 'row',
  paddingHorizontal: 10
})

export const PaymentItemText = styled.Text(({ tintColor, bold }) => ({
  color: tintColor || '#777',
  fontSize: 13,
  lineHeight: 17,
  fontWeight: bold && 'bold',
}))

export const EmptyContainer = styled.View({
  flex: 1
})