import React from 'react';
import { Placeholder, Fade, PlaceholderMedia } from 'rn-placeholder';
import { PaymentListLoadingWrapper } from './PaymentListStyle';

const PaymentListLoading = () => {
  return (
    <PaymentListLoadingWrapper>
      <Placeholder Animation={Fade}>
        <PlaceholderMedia
          style={{ width: '100%', height: 48, marginBottom: 12, borderRadius: 12 }} />
        <PlaceholderMedia style={{ width: '100%', height: 48, borderRadius: 12 }} />
      </Placeholder>
    </PaymentListLoadingWrapper>
  );
};

export default PaymentListLoading;
