/* eslint-disable consistent-return */
/* eslint-disable no-else-return */
import { compose, withProps, branch, renderComponent } from "recompose";
import { connect } from 'react-redux';
import { graphql } from '@apollo/react-hoc';
import moment from 'moment';

import PaymentsQuery from "../../service/query/PaymentsQuery";
import { AlertActions } from '../../store/actions';
import PaymentListLoading from "./PaymentListLoading";
// import { PaymentError } from '../Payment/PaymentStyle';
import PaymentListScreen from "./PaymentListScreen";
import { SystemImages, Colors } from '../../constants/theme';

// Helpers

const formatBankSlip = (boleto, error=false) => {
  const {competenceMonth, statusBankSlip} = boleto; 
  const formatBoleto = error ? {
    title: 'Entrar em contato conosco',
    error: {
      message: 'invalid date',
      type: 'date'
    },
  } : {
    ...(() => {
      const month = competenceMonth.split('-')[0].replace('0','');
      const newDate = moment().month(month-1);
      return {
        title: newDate.format('MMMM'),
      }
    })()
  };
  return {
    ...formatBoleto,
    status: statusBankSlip
  }
};

const validDate = (date) => {
  const dateForm = /\d+\-\d+/
  return dateForm.test(date);
}

// Redux

const mapDispatchToProps = dispatch => ({
  onServiceFailed: ({ title, error: description }) =>
  dispatch(
    AlertActions.actions.openAlert({
      title,
      shouldRenderOkButton: true,
      closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
      description,
      isError: true,
    }),
  ),
  onCopySuccess: ({boleto}) =>
    dispatch(
      AlertActions.actions.openAlert({
        shouldRenderOkButton: true,
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        title: 'Código de barras copiado para sua Área de transferência',
        image: SystemImages.copySuccess,
        titleColor: Colors.Purple,
        description: boleto.numberCode,
        goBack: true,
      }),
    ),
});

const withData = graphql(PaymentsQuery, {options: { notifyOnNetworkStatusChange: true }});

const renderForLoading = branch(({data}) => data && data.loading && data.networkStatus !== 4, renderComponent(PaymentListLoading));

const setApolloRefetch = withProps(({data}) => ({refetch: data && data.refetch}));

// const renderForError = branch(({data}) => data && data.error , renderComponent(PaymentError));

const withPayments = withProps(
  ({
    // onServiceFailed,
    // navigation: { goBack },
    // data: { error, getBankSlip },
    data: { getBankSlip },
  }) => {
    /**
     * TODO: Adicionar filtro de erro
     * Quando o usuario nao tem boletos o barramento retorna como erro, ao inves de retornar um array vazio []
     * Dentro desta tela, qualquer outro erro que aconteca na consulta de boletos será mostrado com a mensagem "Boletos nao encontrados"
     * Ate que este problema seja solucionado
     */

    // if (error) {
    //   onServiceFailed({
    //     title: 'Erro ao trazer os dados do pagamentos',
    //     error: error.message,
    //   });
    //   goBack();
    //   return ;
    // };

    if(!getBankSlip || (getBankSlip && getBankSlip.length) === 0 ) {
      return {
        boletos: [],
      }
    };

    getBankSlip.reverse();
    const boletos = getBankSlip.map(boleto => {
      const {competenceMonth} = boleto;
      const formatBoleto = validDate(competenceMonth) ? formatBankSlip(boleto) : formatBankSlip(boleto, true);
      return {
        ...formatBoleto,
        ...boleto
      };
    });
    
    return {
      boletos
    }
  }
);

const enhance = compose(
  connect(
    null,
    mapDispatchToProps
  ),
  withData,
  renderForLoading,
  setApolloRefetch,
  // renderForError,
  withPayments,
);

export default enhance(PaymentListScreen);
