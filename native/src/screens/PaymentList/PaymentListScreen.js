import React from 'react';
import { Linking, RefreshControl } from 'react-native';
import Config from 'react-native-config';
import { QText, Acordion, EmptyData } from '../../components';
import { PaymentListContainer, PaymentItemSeperator, EmptyContainer } from './PaymentListStyle';
import PaymentItem from './PaymentItem';
import { SystemImages } from '../../constants/theme';

const PaymentListScreen = (props) => {
  const { boletos, onCopySuccess, refetch, data: {loading} } = props;

  return (
    <PaymentListContainer
      refreshControl={<RefreshControl refreshing={loading} onRefresh={()=> refetch()} />}>
      <QText style={{ marginBottom: 16 }} preset='body'>
        Aqui você visualiza seus boletos passados e futuros, verificando status e valores.
      </QText>
      {
        boletos.length > 0 ? 
          <Acordion
            data={[...boletos]}
            RenderSeparator={PaymentItemSeperator}
            RenderComponent={PaymentItem}
            // {...props}
            onCopySuccess={onCopySuccess}
            boletos={boletos}
          />
        : 
        <EmptyContainer>
          <EmptyData
            renderImage
            imageSource={SystemImages.emptyBoleto}
            title='Nenhum boleto gerado!'
            description='você no momento, nenhum boleto foi gerado, aguarde até o dia vigente para realizar o seu pagamento ou fale com a gente.'
            buttonText='Fale com o TimeQ'
            buttonType='link'
            onPress={() =>
              Linking.openURL(
                `https://api.whatsapp.com/send?phone=${Config.WHATSAPP_LINKNUMBER}`,
              )
            }
          />
        </EmptyContainer>
      }
      
    </PaymentListContainer>
  );
};
export default PaymentListScreen;
