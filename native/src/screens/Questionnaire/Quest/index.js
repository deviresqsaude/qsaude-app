import { compose, withHandlers } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Quest from './Quest';
import { QuestActions } from '../../../store/actions';

// Map State Props
const mapStateToProps = ({quest, auth}) => ({
  questState: quest,
  authState: auth
});
// Map Dispatch Props
const mapDispatchToProps = dispatch =>  bindActionCreators(QuestActions.actions, dispatch);
// Handlers
const withQuestHandlers = withHandlers({
  setQuest: ({setCurrentQuestionsList, setCurrentGuide, navigation: {navigate}}) => quest => {
    setCurrentGuide(quest);
    setCurrentQuestionsList(quest.groups.questions);
    navigate(`Questions`);
  },
});
// Enchance
const enchance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withQuestHandlers
);

export default enchance(Quest);
