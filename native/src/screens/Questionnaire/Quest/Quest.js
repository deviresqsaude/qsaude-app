import React from 'react';
import { Text } from 'react-native';
import R from 'ramda';
// import SyncStorage from 'sync-storage';
import { MenuList } from '../../../components';
import { Container } from '../QuestionnaireStyle';
import { SystemIcons } from '../../../constants/theme';

// const refactorQuestion = ({visible: questionVisible, answerId, required, questionText, questionFormat, sexType, answers}, conditions, gender) => {
//   const options = R.filter(item => !!item.visible, R.map(opt => {
//     const {visible, value, label, selected, responseContent, condicionalAnswer, conditionalResponsesObject, sex} = opt
//     return {
//       visible: (visible && (sex === gender || !sex)) || (!visible && R.includes(value, conditions)),
//       id: value,
//       value,
//       label,
//       selected,
//       responseContent,
//       conditionalRange: conditionalResponsesObject,
//       conditional: [...condicionalAnswer]
//     }
//   }, answers));
//   return {
//     id: answerId,
//     required,
//     label: questionText,
//     type: questionFormat,
//     visible: (questionVisible && (sexType === gender || sexType === "3" || !sexType)) || (!questionVisible && options.length ),
//     options
//   }
// };

const Questions = ({questState: {allGuides, answers}, setQuest}) => {
  // const {gender} = SyncStorage.get("profileUser");
  // const conditions = Object.keys(answers).reduce((acc, next) => [...R.union(acc, answers[next].conditional || [])], []);
  return (
    <Container>
      {
        allGuides && allGuides.length ?
        <MenuList
          customIcon={SystemIcons.check}
          multiple
          menuData={[
            ...R.map(
              (item, index) => ({
                testID: `test-quest-${index}`,
                id: `id-quest-${index}`,
                iconName: item.finished ? 'successMenuIcon' : 'emptyMenuIcon',
                title: item.name,
                subtitle: item.finished ? 'Completado' : 'Pendente',
                progress: 0,
                noTintColor: true,
                visible: item.visible,
                link: () => setQuest(item),
              }), allGuides)
          ]}
        /> :
        <Text>Sem Questionarios</Text>
      }
    </Container>
  )
};

Questions.propTypes = {};

export default Questions;
