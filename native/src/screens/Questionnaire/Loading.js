import React from 'react';
import { Placeholder, Fade, PlaceholderLine, PlaceholderMedia } from 'rn-placeholder';
import { QuestLoadingWrapper } from './QuestionnaireStyle';

const QuestLoading = () => {
  return (
    <QuestLoadingWrapper>
      <Placeholder Animation={Fade} style={{marginTop: 20}}>
        <PlaceholderLine width={40} style={{marginBottom: 10}}/>
        <PlaceholderLine/>
        <PlaceholderLine width={80} style={{marginBottom: 20}}/>
        <Placeholder Left={PlaceholderMedia}>
          <PlaceholderLine/>
          <PlaceholderLine width={60}/>
        </Placeholder>
        <Placeholder Left={PlaceholderMedia}>
          <PlaceholderLine/>
          <PlaceholderLine width={60}/>
        </Placeholder>
        <Placeholder Left={PlaceholderMedia}>
          <PlaceholderLine/>
          <PlaceholderLine width={60}/>
        </Placeholder>
      </Placeholder>
    </QuestLoadingWrapper>
  )
}

export default QuestLoading;
