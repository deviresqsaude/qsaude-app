import React from 'react';
import R from 'ramda';
import { Container } from './QuestionnaireStyle';
import { MenuList } from '../../components';
import { SystemIcons } from '../../constants/theme';

const Questionnaire = ({questState: {list}, setCurrentQuest}) => {
  return (
    <Container>
      <MenuList
        customIcon={SystemIcons.check}
        multiple
        menuData={[
          ...R.map(
            item => ({
              testID: `test-${item.questionareId}`,
              id: item.questionareId,
              iconName: item.processing ? 'successMenuIcon' : 'emptyMenuIcon',
              title: item.name,
              subtitle: item.processing ? 'Preenchido' : 'Pendente',
              progress: 0,
              noTintColor: true,
              link: () => setCurrentQuest(item),
            }), list)
        ]}
      />
    </Container>
  )
};

Questionnaire.propTypes = {};

export default Questionnaire;
