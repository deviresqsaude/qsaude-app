import styled from 'styled-components/native';
import { EmptyData } from '../../components';
import { SystemImages } from '../../constants/theme';

export const Container = styled.ScrollView({
  flex: 1,
  marginTop: 20
});

export const QuestContainer = styled.View({
  backgroundColor: "white",
  borderRadius: 14,
  marginTop: 18,
  paddingHorizontal: 14,
  paddingTop: 12
});

export const QuestActions = styled.View({
  flexDirection: "row",
  flex: 1,
  justifyContent: "space-between",
  marginTop: 16,
});

export const QuestLoadingWrapper = styled.View({
  flex: 1,
  padding: 16
});

export const QuestErrorWrapper = styled.View({
  flex: 1,
  padding: 16
});

export const QuestError = styled(EmptyData).attrs(props => ({
  title: 'Ops!!',
  description: 'Aconteceu um problema, Tente novamente.',
  buttonText: 'Tentar novamente',
  onPress: props.refetch,
  imageSource: SystemImages.errorImage,
}))``;

export const QuestEmpty = styled(EmptyData).attrs(props => ({
  title: 'Questionario!!',
  description: 'Não existem questionarios disponíveis.',
  buttonText: 'Tentar novamente',
  onPress: props.refetch,
  imageSource: SystemImages.imgRecord,
}))``;
