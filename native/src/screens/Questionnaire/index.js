import { compose, branch, renderComponent, withProps, withHandlers, lifecycle } from 'recompose';
import { graphql } from '@apollo/react-hoc';
import SyncStorage from 'sync-storage';
import R from 'ramda';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Questionnaire from './Questionnaire';
import Loading from './Loading';
import { QuestError, QuestEmpty } from './QuestionnaireStyle';
import { QuestQuery } from "../../service/query";
import { QuestActions } from '../../store/actions';

// Helpers
// const reduceGuideSex = (acc, next, gender) =>
//   [...R.union(acc, next.sexType || "3")];

const guideIsVisible = (guide, gender) => {
  const guideClone = Object.assign({}, guide);
  const questionsFilterSex = R.filter(item => [gender, "3"].includes(item.sexType), R.map(item => ({...item, guide: guide.id, sexType: item.sexType || "3"}), guideClone.groups.questions));
  // const sextypes = R.reduce((acc, next) => reduceGuideSex(acc, next, gender), [], guideClone.groups.questions);
  // const sextypes = R.reduce((acc, next) => reduceGuideSex(acc, next, gender), [], guide.groups.questions);
  // const visible = !sextypes.length || sextypes.includes("3") || sextypes.includes(gender);
  return {
    ...{...guideClone, groups: {questions: [...questionsFilterSex]}},
    visible: !!questionsFilterSex.length
  }
};

const filterGuidesByGender = (guides, gender) =>
  R.filter(R.propEq("visible", true), R.map(item => guideIsVisible(item, gender), guides));

const conditionalByRange = (objCond, answer) => {
  // eslint-disable-next-line radix
  let cond = [];
  // eslint-disable-next-line no-restricted-syntax
  for (const key in objCond) {
    // eslint-disable-next-line radix
    if (parseInt(answer) >= key) cond = [...objCond[key]];
  }
  return cond;
};
const typeAnswer = { "0401": "01", "0301": "04" };
const getAnswerdsQuestions = questions => {
  let answers = {};
  R.forEach(question => {
    // const conditions = Object.keys(answers).reduce((acc, next) => [...R.union(acc, answers[next].conditional || [])], []);
    const format = question.answers[0];
    const type = typeAnswer[R.concat(question.answerFormat, format.type)] || format.type;
    const answersSelected = !R.includes(type, ["02", "03"]) ?
    R.filter(item => item.selected ,question.answers) :
    question.answers
    // R.filter(item => !!item.responseContent ,question.answers);
    const answer = !R.includes(type, ["02", "03"]) ? {
      answer: R.reduce((acc, next) => type === "04" ? next.value : [...R.union(acc, [next.value])], [], answersSelected),
      conditional: R.reduce((acc, next) => [...R.union(acc, next.condicionalAnswer || [])], [], answersSelected)
    } : (() => {
      const textAnswer = answersSelected[0];
      const {conditionalResponsesObject, responseContent, value, condicionalAnswer} = textAnswer;
      const condRange = JSON.parse(`${conditionalResponsesObject || "{}"}` || `{}`);
      return {
        answer: {value, text: responseContent},
        // answer: R.reduce((acc, next) => ({value: next.value, text: next.responseContent}), "", answersSelected),
        conditional: R.isEmpty(condRange) ? [...condicionalAnswer] : [...conditionalByRange(condRange, responseContent)]
      }
    })();
    answers = {...answers, [question.answerId]: { type, ...answer}};
  }, R.filter(R.propEq("questionAnswered", true), questions));
  return answers;
};
// End Helpers
// Map State Props
const mapStateToProps = ({quest}) => ({
  questState: quest
});
// Map Dispatch Props
const mapDispatchToProps = dispatch =>  bindActionCreators(QuestActions.actions, dispatch);
// Data
const withData = graphql(QuestQuery, {options: { notifyOnNetworkStatusChange: true }});
const renderForLoading = branch(({ data }) => data && data.loading, renderComponent(Loading));
const renderEmptyData = branch(({ data }) => data && !data.loading && R.isEmpty(data.getQuestionare), renderComponent(QuestEmpty));
const setApolloRefetch = withProps(({data}) => ({refetch: data && data.refetch}));
const renderForError = branch(({ data }) => data && data.error, renderComponent(QuestError));
// Handlers
const withQuestHandlers = withHandlers({
  setCurrentQuest: ({questState: {answers: answersState}, setCurrentQuestionsList, setCurrentGuide, setCurrentQuest, setAllGuides, updateAnswers, navigation: {navigate}}) => quest => {
    /**
     * TODO: Update questionary
     * * Get last answer
     * * Continue from the last answer
     * * Get all Questions
     * * Define page number
     * * Set current guide from the last question
     */
    const {gender, numCard} = SyncStorage.get("profileUser");
    const synclastQuestion = SyncStorage.get("Questionary.LastQuestion") || {};
    const lastQuestion = synclastQuestion[numCard];
    const guides = filterGuidesByGender(quest.guides.map((item, i) => ({order: i+1, id: `guide-${i+1}`, ...item})), gender);
    const allQuestions = R.reduce((acc, next) => [...acc, ...next.groups.questions], [], guides);
    const page = lastQuestion? R.indexOf(lastQuestion, R.map(item => item.answerId, allQuestions)) : 0;
    // const page = !R.isEmpty(lastQuestion) ? R.indexOf(lastQuestion, R.map(item => item.answerId, allQuestions)) : 0;
    const answers = {...getAnswerdsQuestions(allQuestions)};
    // const answers = R.reduce((acc, next) => ({...acc, ...getAnswerdsQuestions(next.groups.questions)}), {}, quest.guides) || {};
    updateAnswers({...answers, ...answersState});
    setCurrentQuest({...quest, guides});
    setAllGuides(guides || []);
    setCurrentGuide((guides && guides[0]) || {});
    setCurrentQuestionsList(allQuestions);
    navigate(`Questions`, {page});
    // setCurrentQuestionsList(quest.groups.questions);
    // navigate(`Quest`);
  }
});
// LifeCycle
const withLifecycle = lifecycle({
  componentDidMount() {
    const {data: {getQuestionare}, setQuestsList} = this.props;
    const {numCard} = SyncStorage.get("profileUser");
    const questCache = SyncStorage.get("Questionary.Finished") || {};
    const questFinished = questCache[numCard] || [];
    const questsList = getQuestionare.map(item => ({...item, processing: questFinished.includes(item.questionareId)}));
    setQuestsList(questsList);
  },
  componentWillUnmount() {
    const {clearQuestState} = this.props;
    clearQuestState();
  }
});

// Enchance
const enchance = compose(
  // Redux
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  // Data
  withData,
  renderForLoading,
  setApolloRefetch,
  renderForError,
  renderEmptyData,
  // // Props
  // withQuestListProps,
  // Handlers
  withQuestHandlers,
  // Lifecycle
  withLifecycle
)

export default enchance(Questionnaire);
