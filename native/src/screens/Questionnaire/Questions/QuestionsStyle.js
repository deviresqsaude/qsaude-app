import styled from 'styled-components/native';

export const Cotainer = styled.ScrollView.attrs({
  showsVerticalScrollIndicator: false
})({
  paddingHorizontal: 20,
})

export const QuestContainer = styled.View({
  backgroundColor: "white",
  borderRadius: 14,
  marginTop: 18,
  paddingHorizontal: 14,
  paddingTop: 16,
  paddingBottom: 0
});

export const QuestActions = styled.View({
  flexDirection: "row",
  // flex: 1,
  justifyContent: "space-between",
  marginTop: 16,
});

export const QuestAlert = styled.View({
  alignItems: "center",
  flexDirection: "column",
  flex: 1,
  height: "100%",
  paddingHorizontal: 50,
  justifyContent: "center",
});

export const QuestAlertImage = styled.Image(() => ({
  height: 92,
  width: 92,
}));
