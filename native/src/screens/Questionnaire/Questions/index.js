import { compose, withHandlers, withState, lifecycle} from 'recompose';
import R from 'ramda';
import SyncStorage from 'sync-storage';
import { graphql } from '@apollo/react-hoc';
import { withFormik } from 'formik'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Questions from './Questions';
import { QuestActions } from '../../../store/actions';
import { InsertQuest } from '../../../service/mutations';

// Map State Props
const mapStateToProps = ({quest, auth}) => ({
  questState: quest,
  authState: auth
});
// Map Dispatch Props
const mapDispatchToProps = dispatch => bindActionCreators(QuestActions.actions, dispatch);
// Helpers
const conditionalByRange = (objCond, answer) => {
  // eslint-disable-next-line radix
  let cond = [];
  // eslint-disable-next-line no-restricted-syntax
  for (const key in objCond) {
    // eslint-disable-next-line radix
    if (parseInt(answer) >= key) cond = [...objCond[key]];
  }
  return cond;
};

const refactorQuestion = ({visible: questionVisible, answerId, required, questionText, questionFormat, sexType, answers, guide}, conditions, gender) => {
  const options = R.filter(item => !!item.visible, R.map(opt => {
    const {visible, value, label, selected, responseContent, condicionalAnswer, conditionalResponsesObject, sex} = opt
    return {
      visible: (visible && (sex === gender || !sex)) || (!visible && R.includes(value, conditions)),
      id: value,
      value,
      label,
      selected,
      responseContent,
      conditionalRange: conditionalResponsesObject,
      conditional: [...condicionalAnswer]
    }
  }, answers));
  return {
    id: answerId,
    required,
    label: questionText,
    type: questionFormat,
    visible: (questionVisible && [gender, "3"].includes(sexType) && !!options.length) || (!questionVisible && !!options.length),
    options,
    guide
  }
};
const typeAnswer = { "0401": "01", "0301": "04" };
// State
const withPageState = withState('page', 'setPage', 0);
const withPageLoading = withState('loading', 'setLoading', false);
const withAnswers = withState('isEnded', 'questIsEmded', false);
const withPendent = withState('isPendent', 'questIsPendent', false);
const withNoQuest = withState('isNoQuests', 'setNoQuests', false);
const withErrorQuest = withState('isErrorQuests', 'setErrorQuest', false);
const withSuccessQuest = withState('isSuccessQuests', 'setSuccessQuest', false);
const withQuestHistory = withState('questHistory', 'addQuestHistory', {});
const withQuestUpdated = withState('questUpdated', 'isQuestUpdated', false);
const witMyAnswers = withState('myAnswers', 'setMyAnswers', {});
// Lifecycle
const withLifeCycle = lifecycle({
  componentDidMount() {
    const {setValues, getQuestion, setNoQuests, setPage, setCurrentQuestion, questState: {answers, questions}, navigation: {state: {params: {page}}}} = this.props;
    let p=page-1; let q={};
    do {
      p+=1; q = getQuestion(answers, p);
    } while (!q.visible && p<questions.length-1);
    if (p === questions.length-1 && !q.visible) setNoQuests(true);
    else {
      const values = R.reduce((acc, next) => {
        const {type, answer} = answers[next];
        return {...acc, ...{[next]: R.includes(type, ["02", "03"]) ? answer.text : answer}}
      }, {}, Object.keys(answers));
      setValues({...values});
      setPage(p);
      setCurrentQuestion(q);
    };
  },
  componentWillUnmount() {
    const {isQuestUpdated, setMyAnswers} = this.props;
    isQuestUpdated(false);
    setMyAnswers({});
  }
});
// Handlers
const withQuestionUpdate = withHandlers({
  getQuestion: ({questState: {questions}}) => (answers, page) => {
    const {gender} = SyncStorage.get("profileUser");
    const question = questions[page]
    const conditions = Object.keys(answers).reduce((acc, next) => [...R.union(acc, answers[next].conditional || [])], []);
    const format = question.answers[0];
    const type = typeAnswer[R.concat(question.answerFormat, format.type)] || format.type;
    return refactorQuestion({...question, questionFormat: type}, conditions, gender);
  },
});
/**
 * @QuestionFormat
 * *01: Checkbox
 * *02: Text
 * *03: Decimal
 * *04: RadioButtom
*/
const withQuestionHandlers = withHandlers({
  next: ({setMyAnswers, myAnswers, setPage, page, questIsEmded, addQuestHistory, questHistory, getQuestion, updateAnswers, setCurrentQuestion, questState: {answers, questions}}) => () => {
    let p=page; let q={};
    do {
      p+=1; q = getQuestion({...answers}, p);
      // p+=1; q = getQuestion({...answers, ...myAnswers}, p);
    } while (!q.visible && p<questions.length-1);
    if (p === questions.length-1 && !q.visible) {
      questIsEmded(true);
    } else {
      const qh = {...questHistory};
      delete qh[q.id];
      updateAnswers({...answers, ...(questHistory[q.id] ? {[q.id]: questHistory[q.id]} : {})});
      // setMyAnswers({...myAnswers, ...(questHistory[q.id] ? {[q.id]: questHistory[q.id]} : {})});
      // updateAnswers({...myAnswers, ...(questHistory[q.id] ? {[q.id]: questHistory[q.id]} : {})});
      addQuestHistory({...qh});
      setPage(p);
      setCurrentQuestion(q);
    }
  },
  prev: ({setMyAnswers, myAnswers, addQuestHistory, questHistory, updateAnswers, setPage, page, getQuestion, setCurrentQuestion, questState: {answers, currentQuestion}}) => () => {
    let p=page; let q={};
    const {id: questionId} = currentQuestion;
    const a = {...answers};
    // const a = {...answers, ...myAnswers};
    const h = {...questHistory, [questionId]: a[questionId]};
    addQuestHistory({...h});
    delete a[questionId];
    // setMyAnswers({...a});
    updateAnswers({...a});
    do {
      p-=1; q = getQuestion(a, p);
    } while (!q.visible && p>0);
    setPage(p);
    setCurrentQuestion(q);
  },
  change: ({setMyAnswers, myAnswers, updateAnswers, isQuestUpdated, questState: {answers, currentQuestion: {id, type, options}}}) => a => {
    isQuestUpdated(true);
    const {numCard} = SyncStorage.get("profileUser");
    const lastQuestion = SyncStorage.get("Questionary.LastQuestion") || {};
    SyncStorage.set('Questionary.LastQuestion', {...lastQuestion, [numCard]: id});
    let conditional = [];
    if(!R.includes(type, ["02", "03"]) ) {
      conditional = type === "01" ?
      options.reduce((acc, next) => [...acc, ...(() => a.includes(next.id) ? next.conditional : [])()], []) :
      options.reduce((acc, next) => [...acc, ...(() => a === next.id ? next.conditional : [])()], [])
    } else {
      const condRange = JSON.parse(`${options[0].conditionalRange || "{}"}` || `{}`);
      conditional = R.isEmpty(condRange) ? [...options[0].conditional] : [...conditionalByRange(condRange, a)];
    };
    const question = {[id]: { type, answer: R.includes(type, ["02", "03"]) ? {value: options[0].value, text: a} : a, conditional}};
    // setMyAnswers({...myAnswers, ...question});
    updateAnswers({...answers, ...question});
  },
  sendQuest: ({setQuestsList, clearQuestState, myAnswers, questHistory, updateAnswers, setErrorQuest, setSuccessQuest, setLoading, sendQuestionnaire, updateCurrentGuide, questState: {list: questList, currentQuestion: {id: currentQuestionId}, allGuides, quest: questToSend, answers, guide}, navigation}) => async (finished=true) => {
    setLoading(true);
    const {numCard} = SyncStorage.get("profileUser");
    const {questionareId, externalId, name, answerVersion} = questToSend;
    const formatAnswerVersion = parseInt(answerVersion, 0) || 0;
    const allAnswers = {...answers, ...myAnswers};
    // const allAnswers = questUpdated && !R.isEmpty(questHistory) ? {...R.omit(R.keys(questHistory), mergeAnswers)} : {...mergeAnswers};
    const guidesUpdated = R.map(item => ({...item, ...(item.id === guide.id ? {finished: finished || guide.finished} : {})}), allGuides);
    // const allGuidesCompleted = false;
    updateCurrentGuide([...guidesUpdated]);
    // updateCurrentQuest({...questToSend, guides: [...guidesUpdated]});
    const responseAnswers = R.reduce((acc, next) => [...acc, ...(!R.includes(allAnswers[next].type, ["02", "03"]) ? R.map(i => ({value: i}), allAnswers[next].type === "04" ? [allAnswers[next].answer] : allAnswers[next].answer) : [allAnswers[next].answer])], [], Object.keys(allAnswers));
    const data = {
      answerVersion: (formatAnswerVersion + 1).toString(),
      questionId: questionareId,
      status: finished ? "Completo" : "Incompleto",
      name,
      answers: responseAnswers,
      externalId: externalId || `ext-id-${numCard}`,
    };
    const response = await sendQuestionnaire({variables: {...data}});
    if (!response.data || response.data.type === "error") setErrorQuest({message: (response.data && response.data.message) || "Acontecei um erro, tenten novamente mais tarde"});
    else if(finished){
      const questCache = SyncStorage.get("Questionary.Finished") || {};
      const questFinished = questCache[numCard] || [];
      const questsListFactory = questList.map(item => ({...item, processing: finished}));
      setQuestsList(questsListFactory);
      setSuccessQuest({message: response.data.message || "As suas respostas foram enviados, espere enquanto voce é direcionado"});
      SyncStorage.set('Questionary.Finished', {...questCache, [numCard]: [...R.union(questFinished, [questionareId])]});
      setTimeout(() => {setLoading(false); navigation.goBack();}, 500);
    } else {
      // TODO: Setar a ultima resposta correta
      // if(finished) SyncStorage.remove('Questionary.LastQuestion');
      // else SyncStorage.set('Questionary.LastQuestion', currentQuestionId);
      // updateAnswers({...answers, ...myAnswers});
      setSuccessQuest({message: response.data.message || "As suas respostas foram enviados, espere enquanto voce é direcionado"});
      setTimeout(() => {setLoading(false); navigation.goBack();}, 500);
      // setTimeout(() => {clearQuestState(); setLoading(false); navigation.goBack();}, 500);
    };
  },
});

const withModalHandlers = withHandlers({
  closeModal: ({navigation, sendQuest}) => mode => {
    if (mode === "withsave") sendQuest(false);
    else navigation.navigate('Health');
  },
});

const witMutation = graphql(InsertQuest, {name: "sendQuestionnaire"});

const withForm = withFormik({
  mapPropsToValues: () => {},
  displayName: 'QuestForm',
  enableReinitialize: true
});

const enchance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  // Mutation
  witMutation,
  // Form
  withForm,
  // State
  withQuestUpdated,
  withPageState,
  withPageLoading,
  withAnswers,
  withPendent,
  withNoQuest,
  withErrorQuest,
  withSuccessQuest,
  withQuestHistory,
  witMyAnswers,
  // Handlers
  withQuestionUpdate,
  withQuestionHandlers,
  withModalHandlers,
  // lifecycle
  withLifeCycle
);

export default enchance(Questions);
