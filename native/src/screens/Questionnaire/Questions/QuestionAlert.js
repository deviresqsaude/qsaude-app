import React from 'react';
import { Colors, SystemImages } from '../../../constants/theme';
import { QuestAlert, QuestAlertImage } from './QuestionsStyle';
import { QText, QButton } from '../../../components';

const MESSAGE = {
  empty: {
    image: SystemImages.imgRecord,
    title: "Questionário indisponível!",
    description: "No momento, seu questionário está indisponível. Dúvidas?",
    action: null,
    secondAction: null,
    link: "Falar com TimeQ"
  },
  success: {
    image: SystemImages.successImage,
    title: "Questionário finalizado!",
    description: "Você finalizou o questionário.Clique em salvar para enviar as suas informações.",
    action: "Enviar questionário",
    link: "Voltar para o questionário"
  },
  warning: {
    image: SystemImages.question,
    title: "Progresso do questionário",
    description: "Deseja salvar o progresso atual do questionário?",
    action: "Sair sem salvar",
    secondAction: "Salvar e sair",
    link: "Cancelar"
  },
}

const QuestionAlert = ({loading, onClose, onCloseSecond, onLink, type}) => (
  <QuestAlert>
    <QuestAlertImage source={MESSAGE[type].image}/>
    <QText style={{ marginTop: 16 }} textCenter preset="title" fontSize={17} color={Colors.Purple}>
      {MESSAGE[type].title}
    </QText>
    <QText
      textCenter
      color={Colors.TextGray}
      fontSize={15}
      style={{ marginTop: 12}}>
      {MESSAGE[type].description}
    </QText>
    {
      // eslint-disable-next-line no-nested-ternary
      type === "success" ?
      <QButton
        loading={loading}
        text={MESSAGE[type].action}
        inBlock={false}
        onPress={onClose}
        style={{marginTop: 16}}
      /> :
      type === "warning" ?
      <>
        <QButton
          loading={loading}
          text={MESSAGE[type].action}
          inBlock={false}
          onPress={onClose}
          style={{marginTop: 16, width: 174}}
        />
        <QButton
          loading={loading}
          text={MESSAGE[type].secondAction}
          inBlock={false}
          onPress={onCloseSecond}
          style={{marginTop: 0, width: 174}}
        />
      </> : null
    }
    <QText
      preset="link"
      onPress={onLink}
      inBlock={false}
      style={{textDecorationLine: 'underline'}}>
      {MESSAGE[type].link}
    </QText>
  </QuestAlert>
)

export default QuestionAlert;
