import React from 'react';
import R from 'ramda';
import { Text, Linking } from 'react-native';
import { Field } from 'formik';
import Config from 'react-native-config';
import { QSModal } from '../../../components/Modal';
import { elevationShadow } from '../../../helpers';
import { Cotainer, QuestContainer, QuestActions } from './QuestionsStyle';
import QuestionAlert from './QuestionAlert';
import { Colors } from '../../../constants/theme';
import { QText, QTextInput, QButton, QRadioGroup, QCheckGroup } from '../../../components';

/**
 * @QuestionFormat
 * *01: Checkbox
 * *02: Text
 * *03: Decimal
 * *04: RadioButtom
 *
 * @Gender
 * *01: Masculino
 * *02: Feminino
 */

const getGuideName = (guides, currentGuideId = "") => {
  const [guide={}] = guides.filter(item => item.id === currentGuideId);
  return guide.name || "";
}

const Questions = props => {
  const {questState: {currentQuestion, allGuides, questions, answers}, myAnswers, questUpdated, questIsPendent, closeModal, isNoQuests, isPendent, isEnded, questIsEmded, sendQuest, loading, page, navigation, change, next, prev} = props;
  if(R.isEmpty(currentQuestion)) return null;
  return (
    <QSModal
      visible
      closeModal={loading ? () => {} : () => questUpdated ? questIsPendent(!isPendent) : navigation.goBack()}
      // closeModal={loading ? () => {} : () => (!R.isEmpty(myAnswers) && questUpdated) ? questIsPendent(!isPendent) : navigation.goBack()}
      title={getGuideName(allGuides, currentQuestion.guide)}
      showCloseAction={!isPendent}>
      {
        // eslint-disable-next-line no-nested-ternary
        isPendent ?
        <QuestionAlert
          loading={loading}
          onLink={() => questIsPendent(!isPendent)}
          onClose={() => closeModal("nosave")}
          onCloseSecond={() => closeModal("withsave")}
          type="warning"/> :
        // eslint-disable-next-line no-nested-ternary
        isNoQuests ?
        <QuestionAlert
          onLink={() => Linking.openURL(`https://api.whatsapp.com/send?phone=${Config.WHATSAPP_LINKNUMBER}`)}
          // onLink={() => {navigation.goBack()}}
          type="empty"/> :
        // eslint-disable-next-line no-nested-ternary
        !R.isNil(currentQuestion) && !isEnded ?
        <Cotainer>
          <QuestContainer style={{ ...elevationShadow(Colors.Black, 5, 0.2) }}>
            {loading && <Text>Loading...</Text>}
            <QText
              preset="title"
              color={Colors.Purple}
              style={{ marginBottom: 20 }}>{currentQuestion.label}
            </QText>
            <Field
              testID={currentQuestion.id}
              name={currentQuestion.id}
              label={null}
              options={currentQuestion.options}
              onFieldChange={val => change(val)}
              keyboardType={R.cond([
                [R.equals("03"), () => 'numeric'],
                [R.T, () => 'default'],
              ])(currentQuestion.type)}
              component={R.cond([
                [R.equals("01"), () => QCheckGroup],
                [R.equals("04"), () => QRadioGroup],
                [R.T, () => QTextInput],
              ])(currentQuestion.type)}
            />
          </QuestContainer>
          <QuestActions>
            <QButton
              text="Anterior"
              preset="link"
              disabled={page === 0}
              inBlock={false}
              onPress={prev}
              margin={0}/>
            <QButton
              disabled={currentQuestion.required && !answers[currentQuestion.id]}
              text={questions.length === page + 1 ? "Finalizar" : "Próximo"}
              inBlock={false}
              onPress={questions.length === page + 1 ? () => questIsEmded(!isEnded) : next}
              margin={0}/>
          </QuestActions>
        </Cotainer> :
        isEnded ?
        <QuestionAlert
          loading={loading}
          onClose={() => sendQuest()}
          onLink={() => questIsEmded(!isEnded)}
          type="success"/> :
        <Text>Loading...</Text>
      }
    </QSModal>
  )
};

Questions.propTypes = {};

export default Questions;
