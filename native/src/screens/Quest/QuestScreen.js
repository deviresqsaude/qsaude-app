import React from 'react';
import { RefreshControl } from 'react-native'
import R from 'ramda';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import { elevationShadow } from '../../helpers';
import QText from '../../components/QText';
import { MenuList } from '../../components/MenuList';
import { QSModal } from '../../components/Modal';
import { ErrorScreen } from '../../components/Error';
import { Container, QuestContainer, QuestActions } from './QuesStyle';
import { QTextInput, QButton, QRadioGroup, QCheckGroup, QOpenChoice } from '../../components';
import { Colors, SystemIcons } from '../../constants/theme';
import QAlert from '../../components/QAlert';
import { QUEST } from '../../constants/TestIds';
import { ScrollView } from 'react-native-gesture-handler';

const MESSAGE = {
  error: 'Aconteceu um problema com o envio do questionário! Tente novamente, por favor',
  success: 'Seu questionário foi enviado com sucesso. Muito obrigado!',
};

/**
 * Tela de questionarios
 * @param {string} title - Titulo da tela
 */
const QuestScreen = props => {
  const {
    title,
    navigation,
    questModal,
    closeQuestModal,
    questList,
    currentQuest,
    prevQuestion,
    nextQuestion,
    currentQuestion,
    error,
    updateAnswers,
    pageTitle,
    validateField,
    requestAlert,
    setRequestAlert,
    requestHasError,
    refetch,
    data: { loading }
  } = props;
  return (
    <Container refreshControl={<RefreshControl refreshing={loading} onRefresh={refetch}/>} >
      <QText
        preset="title"
        color={Colors.Empress}
        margin={{ left: 16, right: 16, top: 0, bottom: 8 }}
      >
        {pageTitle || title}
      </QText>
      <MenuList
        customIcon={SystemIcons.check}
        isProgressive
        menuData={questList}
        navigation={navigation}
        multiple
      />
      {questModal && (
        <QSModal
          testID="modalQuest"
          visible={questModal}
          closeModal={() => closeQuestModal()}
          title={currentQuest.title}>
          {!error ? (
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{paddingHorizontal: 20}}>
              <QuestContainer style={{ ...elevationShadow(Colors.Black, 5, 0.2) }}>
                <Field
                  testID={currentQuestion.id}
                  name={R.replace(/\./g, '__', currentQuestion.id)}
                  label={currentQuestion.label}
                  options={currentQuestion.options}
                  validate={currentQuestion.parameters ? validateField : () => {}}
                  onFieldChange={val => updateAnswers(val)}
                  keyboardType={R.cond([
                    [R.equals('decimal'), () => 'numeric'],
                    [R.equals('integer'), () => 'numeric'],
                    [R.T, () => 'default'],
                  ])(currentQuestion.type)}
                  component={R.cond([
                    [R.equals('choice'), () => QRadioGroup],
                    [R.equals('check'), () => QCheckGroup],
                    [R.equals('openChoice'), () => QOpenChoice],
                    [R.T, () => QTextInput],
                  ])(currentQuestion.type)}
                />
              </QuestContainer>
              <QuestActions>
                <QButton
                  testID={QUEST.prevQuestion}
                  text="Anterior"
                  preset="link"
                  inBlock={false}
                  onPress={prevQuestion}
                  margin={0}
                />
                <QButton
                  testID={QUEST.nextQuestion}
                  text="Próximo"
                  inBlock={false}
                  onPress={nextQuestion}
                  margin={0}
                />
              </QuestActions>
            </ScrollView>
          ) : (
            <ErrorScreen image={SystemIcons.close} title={error} />
          )}
        </QSModal>
      )}
      <QAlert
        visible={requestAlert}
        isSuccess={!requestHasError}
        isError={requestHasError}
        description={MESSAGE[requestHasError ? 'error' : 'success']}
        closeAlert={() => setRequestAlert(!requestAlert)}
      />
    </Container>
  );
};

QuestScreen.propTypes = {
  title: PropTypes.string,
  pageTitle: PropTypes.string,
  navigation: PropTypes.objectOf(PropTypes.any),
  questModal: PropTypes.bool,
  closeQuestModal: PropTypes.func,
  questList: PropTypes.arrayOf(PropTypes.any),
  currentQuest: PropTypes.objectOf(PropTypes.any),
  prevQuestion: PropTypes.func,
  nextQuestion: PropTypes.func,
  currentQuestion: PropTypes.objectOf(PropTypes.any),
  error: PropTypes.string,
  updateAnswers: PropTypes.func,
  requestAlert: PropTypes.bool,
  setRequestAlert: PropTypes.func,
};

QuestScreen.defaultProps = {
  title: 'Questionários',
  pageTitle: '',
  navigation: {},
  questModal: false,
  closeQuestModal: () => {},
  questList: [],
  currentQuest: {},
  prevQuestion: () => {},
  nextQuestion: () => {},
  currentQuestion: {},
  error: '',
  updateAnswers: () => {},
  requestAlert: false,
  setRequestAlert: () => {},
};

export default QuestScreen;
