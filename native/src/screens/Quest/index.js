import { compose, withState, withHandlers, branch, lifecycle, renderComponent, withProps } from 'recompose';
import { withFormik } from 'formik';
import { graphql } from '@apollo/react-hoc';
import { Alert } from 'react-native';
import R from 'ramda';
import QuestScreen from './QuestScreen';
import QuestLoading from './QuestLoading';
import { QuestQuery } from "../../service/query";
import { InsertQuest } from "../../service/mutations";
import { parseQuestQuestions } from '../../helpers';
import { QuestError } from './QuesStyle';

/**
 * TODO: Check en todos os TODO do arquivo
 * Remover todos os console log e TODO do arquivo
 */

const refactorAnswer = q =>{
  return q.answer ? R.cond([
    [R.equals("choice"), () => (q.repeats ? [...R.map(i => i.valueCoding.code, q.answer)] : q.answer[0].valueCoding.code)],
    [R.equals("openChoice"), () => (q.repeats ? [...R.map(i => i.valueCoding.code, q.answer)] : q.answer[0].valueCoding.code)],
    [R.equals("decimal"), () => (q.answer[0].valueDecimal)],
    [R.T, () => (q.answer[0].valueInteger)]
  ])(q.type) : null
}

const refactorData = data => {
  if (data.error) return [];
  const {questionnaire: {groups}} = data;
  const Q = R.map(item => ({
    id: item.code || `quest_${Math.floor(Math.random() * (9999 - 1000) + 1000)}`,
    title: item.label,
    subtitle: `min: ${item.numRequired}    respondido: ${item.numAnswered}`,
    min: item.numRequired,
    respond: item.numAnswered,
    total: item.numTotal,
    questions: R.map(q => ({
      ...q,
      id: R.replace(/\./g,'__', q.code),
      answer: refactorAnswer(q),
      type: R.equals(q.type, "choice") && q.repeats ? 'check' : q.type,
      options: R.map(option => ({
        id: R.replace(/\./g,'__', option.code),
        value: option.code,
        label: option.label
      }), q.options || [])
    }), item.questions)
  }), groups);
  return Q;
};

const mapQuestAnswers = data => {
  if (data.loading || data.error) return {};
  const list = R.reduce((prev, current) => {
    const next = R.map(q => {
      const qa = refactorAnswer(q);
      return {
        [R.replace(/\./g, '__', q.code)]: qa
      }
    }, current.questions);
    return R.concat(prev, next);
  }, [], data.questionnaire.groups);
  const answers = R.reduce((prev, current) => R.merge(prev, current), {}, list);
  return answers;
};

const showPrevQuestion = ({
  currentPage,
  currentQuest: { questions },
  pages,
  setPages,
  answers,
  setAnswers,
}) => {
  let countPage = currentPage;
  let currentQuestion = null;
  let close = false;
  while (!close) {
    countPage = countPage <= 0 ? 0 : countPage - 1;
    currentQuestion = questions[countPage];
    setAnswers(R.remove(R.indexOf(R.replace(/\./g,'__', currentQuestion.id), answers), 1, answers));
    if (R.includes(countPage, pages)) {
      setPages(R.remove(R.indexOf(currentPage, pages), 1, pages));
      close = !close;
    }
  }
  return {
    page: countPage,
    currentQuestion,
  };
};

const showNextQuestion = ({
  values,
  currentPage,
  currentQuest: { questions },
  pages,
  setPages,
  answers,
  sendAnswers
}) => {
  console.log(values);
  let countPage = currentPage;
  let currentQuestion = questions[countPage];
  let close = false;
  if (countPage + 1 > (questions.length - 1)) {
    Alert.alert(
      'Questionário finalizado!',
      'Você finalizou o questionário. Clique em salvar para enviar as suas informações',
      [
        {
          text: 'Enviar questionário',
          onPress: () => {
            const qr = parseQuestQuestions(answers);
            sendAnswers(qr);
          }
        },
        {
          text: 'Voltar para o questionário',
          onPress: () => {},
          style: 'destructive'
        },
      ],
      {cancelable: false}
    );
  }
  while (!close && countPage < (questions.length - 1)) {
    countPage+=1;
    currentQuestion = questions[countPage];
    const enable = currentQuestion.enableWhen;
    const value = enable ? R.product(R.map(item => {
      const ir = {
        qc: R.replace(/\./g,'__', item.questionCode),
        qa: R.replace(/\./g,'__', item.questionAnswer)
      }
      const val = values[ir.qc];
      const ex = val ? val === ir.qa : false;
      return ex;
    }, enable)) : null;
    if (!enable || value) {
      close = !close;
      if(!R.includes(countPage, pages)) setPages(R.concat(pages || [], [countPage]));
    }
  }
  return {
    page: countPage,
    currentQuestion,
  };
};

const withForm = withFormik({
    mapPropsToValues: ({data}) => mapQuestAnswers(data),
    handleSubmit: async () => {},
    displayName: 'QuestForm',
    enableReinitialize: true
});

const withData = graphql(QuestQuery, {options: { notifyOnNetworkStatusChange: true }});

const witMutation = graphql(InsertQuest, {name: "inserQuest", options: { notifyOnNetworkStatusChange: true }});

const renderForLoading = branch(({ data }) => data && data.loading, renderComponent(QuestLoading));

const setApolloRefetch = withProps(({data}) => ({refetch: data && data.refetch}));

const renderForError = branch(({ data }) => data && data.error, renderComponent(QuestError));

const enhance = compose(
  withData,
  witMutation,
  withForm,
  renderForLoading,
  setApolloRefetch,
  renderForError,
  withState('questList', 'setQuestList', []),
  withState('pageTitle', 'setPageTitle', ''),
  withState('questModal', 'toggleQuestModal', false),
  withState('currentQuest', 'setCurrentQuest', {}),
  withState('currentQuestion', 'setCurrentQuestion', {}),
  withState('currentPage', 'setCurrentPage', 0),
  withState('pages', 'setPages', [0]),
  withState('answers', 'setAnswers', []),
  withState('error', 'setError', null),
  withState('requestAlert', 'setRequestAlert', false),
  withState('requestHasError', 'setRequestError', false),
  withHandlers({
    cleanAll: props => () => {
      const {
        toggleQuestModal,
        setCurrentPage,
        setCurrentQuest,
        setCurrentQuestion,
        // setAnswers,
        setPages,
        // setValues
      } = props;
      toggleQuestModal(false);
      setCurrentPage(0);
      setCurrentQuest({});
      setCurrentQuestion({});
      // setAnswers([]);
      setPages([0]);
      // setValues({});
    },
  }),
  withHandlers({
    setQuest: ({
      setCurrentQuest,
      toggleQuestModal,
      questModal,
      setCurrentQuestion,
      setError,
    }) => quest => {
      /**
       * TODO: Add --answeres-- to state answer
       * Quando enviamos as respostas, ele nao pegas as respostas enviadas pelo back
       * e esta sustituinod as antigas respostas com *null*
       * Seguir o formato de perguntas utilizadas no handler *updateAnswer*
       * Limpar as respostas apos finalizar o envio do questionario
       */
      setError(null);
      setCurrentQuest(quest);
      R.cond([
        [R.equals(0),() => setError('Questionario não disponivel. Por favor, tente com outro questionário')],
        [R.T, () => setCurrentQuestion(quest.questions[0])],
      ])(quest.questions.length);
      toggleQuestModal(!questModal);
    },
    sendAnswers: ({
      currentQuest,
      inserQuest,
      cleanAll,
      setRequestAlert,
      requestHasError,
      refetch
    }) => async (answers) => {
      /**
       * TODO: Adicioanr um Loading apos clicar em Enviar as respostas
       */
      const variables = {
        group: currentQuest.id,
        answers
      }
      const {errors, data} = await inserQuest({variables})
      setRequestAlert(true);
      // eslint-disable-next-line no-unused-expressions
      if(errors || data.answerQuestionnaire === "error") {
        requestHasError(true)
      } else {
        cleanAll();
        refetch();
      };
    }
  }),
  withHandlers({
    validateField: ({currentQuestion}) => val => {
      if (!currentQuestion.parameters) return null;
      const { min, max } = currentQuestion.parameters;
      let error = null;
      if (min && val < min) {
        error = `Deve ser no mínimo ${min}`
      } else if (max && val > max) {
        error = `Deve ser no máximo ${max}`
      }
      return error;
    },
    toggleModal: ({ questModal, toggleQuestModal }) => () => {
      toggleQuestModal(!questModal);
    },
    setCurrentQuestion: ({ setCurrentQuestion }) => question => {
      setCurrentQuestion(question);
    },
    prevQuestion: props => () => {
      const { setCurrentQuestion, setCurrentPage } = props;
      const q = showPrevQuestion(props);
      setCurrentQuestion(q.currentQuestion);
      setCurrentPage(q.page);
    },
    nextQuestion: props => () => {
      const { setCurrentQuestion, setCurrentPage } = props;
      const q = showNextQuestion(props);
      setCurrentQuestion(q.currentQuestion);
      setCurrentPage(q.page);
    },
    closeQuestModal: ({
      cleanAll,
      answers,
      sendAnswers
    }) => () => {
      // eslint-disable-next-line no-unused-expressions
      answers.length ?
      Alert.alert(
        'Progresso do questionário',
        'Deseja salvar o progresso atual do questionário?',
        [
          {
            text: 'Salvar e sair',
            testID: 'SaveAndExit',
            onPress: () => {
              const qr = parseQuestQuestions(answers);
              sendAnswers(qr);
            }
          },
          {
            text: 'Sair sem salvar',
            testID: 'NoSaveAndExit',
            onPress: () => cleanAll()
          },
          {
            text: 'Cancelar',
            testID: 'Cancel',
            onPress: () => {},
            style: 'destructive'
          },
        ],
        {cancelable: false}
      ) : cleanAll();

    },
    updateAnswers: ({
      setAnswers,
      answers,
      currentQuestion,
    }) => (answer) => {
      const newAnswer = {
        name: R.replace(/\./g,'__', currentQuestion.id),
        type: currentQuestion.type,
        val: typeof answer === "object" ? answer : [R.includes(currentQuestion.type, ['decimal', 'integer']) ? parseInt(answer, 0) : answer]
      }
      const exists = R.find(R.propEq('name', newAnswer.name))(answers);
      if (!exists && newAnswer.val) {
        const add = R.concat(answers, [newAnswer]);
        setAnswers(add);
      } else if (exists && newAnswer.val !== exists.val) {
        const upt = R.update(R.indexOf(newAnswer.name, answers), newAnswer, answers);
        setAnswers(upt);
      } else if (exists && !newAnswer.val) {
        const rmv = R.remove(R.indexOf(newAnswer.name, answers), 1, answers);
        setAnswers(rmv);
      } else {
        // eslint-disable-next-line no-console
      }
    },
  }),
  lifecycle({
    componentDidMount() {
      const {setQuest, setQuestList, setPageTitle, data, setError} = this.props;
      if (!data.loading && !data.error) {
        setQuestList(R.map(
          item => ({
            testID: item.id,
            ...item,
            subtitle: `Min: ${item.min}     Respondido: ${item.respond}`,
            progress: (item.respond * 150) / item.min,
            link: () => setQuest(item),
          }), refactorData(data)));
        setPageTitle(data.questionnaire.name);
      } else if (data.error) {
        setError("Aconteceu um erro, tente novamente")
      }
    }
  })
);

export default enhance(QuestScreen);
