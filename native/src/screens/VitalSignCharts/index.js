import { graphql } from '@apollo/react-hoc';
import { compose, withProps } from 'recompose';
import R from 'ramda';
import { connect } from 'react-redux';
import moment from 'moment';
import { Colors } from '../../constants/theme';
import { VitalSignsQuery } from '../../service/query';
import VitalSignChart from './VitalSignChart';
import { AlertActions } from '../../store/actions';

const VitalSignType = {
  DiastolicPressure: 'diastolicPressure',
  SystolicPressure: 'systolicPressure',
};

// Error Handling
const mapDispatchToProps = dispatch => ({
  onServiceFailed: ({ title, error: description }) =>
    dispatch(
      AlertActions.actions.openAlert({
        title,
        shouldRenderOkButton: true,
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        description,
        isError: true,
      }),
    ),
});

const mapNormalVitalSigns = vitalSigns => {
  const data = !vitalSigns.length ?
  [{y: 0, x: moment(new Date()).format('DD-MMM-YY')}] :
  vitalSigns.map(vitalSign => {
    const {measure, measuredAt} = vitalSign;
    return {
      y: measure.amount || 0,
      x: moment(measuredAt || new Date()).format('DD-MMM-YY'),
    };
  });
  return [{ seriesName: 'signs', data: R.reverse(data), color: Colors.Purple }];
};

const mapBloodPressure = vitalSigns => {
  if(!vitalSigns.length) {
    return [
      { seriesName: 'systolic', data: [{
        y: 0,
        x: moment(new Date()).format('DD-MMM-YY'),
      }], color: Colors.Purple },
      { seriesName: 'diastolic', data: [{
        y: 0,
        x: moment(new Date()).format('DD-MMM-YY'),
      }], color: Colors.PurpleDisabled },
    ]
  }
  return vitalSigns
    .map(bloodPressure => {
      const {subMeasures, measuredAt} = bloodPressure;
      const systolic = subMeasures ? subMeasures.find(it => it.type === VitalSignType.SystolicPressure) : null;
      const diastolic = subMeasures ? subMeasures.find(it => it.type === VitalSignType.DiastolicPressure) : null;
      return {
        ys: systolic ? systolic.measure.amount : 0,
        yd: diastolic ? diastolic.measure.amount : 0,
        x: moment(measuredAt || new Date()).format('DD-MMM-YY'),
      };
    })
    .reduce(
      (acc, cur) => {
        // "Push" to systolic accumulator
        acc[0].data.unshift({ x: cur.x, y: cur.ys });
        // "Push" to diastolic accumulator
        acc[1].data.unshift({ x: cur.x, y: cur.yd });
        return acc;
      },
      [
        { seriesName: 'systolic', data: [], color: Colors.Purple },
        { seriesName: 'diastolic', data: [], color: Colors.PurpleDisabled },
      ],
    );
};

const withData = graphql(VitalSignsQuery, {
  options: ({
    onServiceFailed,
    navigation: {
      state: {
        params: { chartVitalSign: vitalSign },
      },
    },
  }) => {
    return {
      onError: e =>
        onServiceFailed({ title: 'Erro ao trazer dados vitais', error: e.message }),
      variables: { vitalSign },
    };
  },
});

const createDataMapper = vitalSign =>
  R.equals(vitalSign, 'bloodPressure') ? mapBloodPressure : mapNormalVitalSigns;

const withRefinedData = withProps(props => {
  const {
    data: { vitalSign, loading },
    navigation: {
      state: {params: { chartVitalSign }},
    },
  } = props;

  if (loading || !vitalSign) {
    return {
      vitalSign: chartVitalSign,
      chartData: [],
    };
  }

  const dataMapper = createDataMapper(chartVitalSign);
  const chartData = dataMapper(vitalSign);
  return {
    vitalSign: chartVitalSign,
    chartData,
  };
});

const enhance = compose(
  connect(
    null,
    mapDispatchToProps,
  ),
  withData,
  withRefinedData,
);

export default enhance(VitalSignChart);
