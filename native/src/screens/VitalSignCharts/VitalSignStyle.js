import React from 'react';
import styled from 'styled-components/native';
import { compose, branch, renderComponent } from 'recompose';
import { Dimensions } from 'react-native';
import { Placeholder, PlaceholderMedia, Fade } from 'rn-placeholder';
import { QText, QButton } from '../../components';
import PureChart from '../../components/VitalSignChart';
import EMPTY_STATE from '../../../assets/images/img_record.png';
import { Colors, Sizing } from '../../constants/theme';

const EmptyWrapper = styled.View({
  marginLeft: 16,
  marginRight: 16,
  alignItems: 'center',
  justifyContent: 'center',
  height: Dimensions.get('window').height * 0.6,
});

const EmptyImage = styled.Image({
  marginBottom: 38,
  width: 300,
});

const ChartPlaceHolder = () => (
  <Placeholder
    style={{ marginBottom: 21, paddingLeft: 16, paddingRight: 16 }}
    Animation={Fade}
  >
    <PlaceholderMedia style={{ width: '100%', height: 300 }} />
  </Placeholder>
);

const ChartEmpty = ({ refetch }) => (
  <EmptyWrapper>
    <EmptyImage source={EMPTY_STATE} resizeMode="contain" />
    <QText textCenter fontSize={Sizing.xlarge} preset="link">
      Nenhum dado cadastrado
    </QText>
    <QText style={{ marginTop: 16, marginBottom: 16 }} textCenter preset="caption">
      Pressione o botão abaixo para inserir uma nova medida.
    </QText>
    <QButton
      preset="secondary"
      onPress={refetch}
      text="Tentar Novamente"
      inBlock={false}
    />
  </EmptyWrapper>
);

const StyledLineChart = styled(PureChart).attrs({
  selectedColor: Colors.SelectedGreen,
  type: 'line',
  height: 300,
})``;

export const LineChart = compose(
  branch(props => props.loading, renderComponent(ChartPlaceHolder)),
  branch(props => !props.loading && !props.data.length, renderComponent(ChartEmpty)),
)(StyledLineChart);
