/* eslint-disable react/prop-types */
import React from 'react';
import R from 'ramda';
import { RefreshControl } from 'react-native';
import { KeyboardAwareScrollView as Content } from 'react-native-keyboard-aware-scrollview';
import { QButton } from '../../components';
import { LineChart } from './VitalSignStyle';
import { BOTTOMTABBAR } from '../../constants/TestIds';

const VitalSignChart = props => {
  const {
    data: { loading, refetch },
    navigation: { navigate },
    chartData,
    vitalSign,
  } = props;

  const selectedIndex = chartData[0] ? chartData[0].data.length - 1 : 0;
  const signs = R.includes(vitalSign, ['height', 'weight', 'imc']);
  const signsNotAccepted = R.includes(vitalSign, ['heartRate', 'bodySurface']);
  return (
    <>
      <Content  showsHorizontalScrollIndicator={false}
        refreshControl={<RefreshControl refreshing={loading} onRefresh={refetch} />}
        contentContainerStyle={{ paddingHorizontal: 16 }}
      >
        <LineChart
          selectedIndex={selectedIndex}
          xAxisGridLineColor="#e0e0e0"
          data={chartData}
          refetch={refetch}
          loading={loading}
          goToEnd
        />
        <QButton
          disabled={signsNotAccepted}
          style={{ marginTop: 16 }}
          text="Adicionar nova medida"
          testID={BOTTOMTABBAR.tabHealth.dadosVitaisIMC.addNew}
          onPress={() =>
            navigate('VitalSignForm', {
              chartVitalSign: vitalSign,
              vitalSign: signs ? 'imc' : vitalSign,
            })
          }
        />
      </Content>
    </>
  );
};

export default VitalSignChart;
