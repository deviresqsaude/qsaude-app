import React from 'react';
import styled from 'styled-components/native';
import { Sizing, Colors } from '../../constants/theme';
import { QText, SystemIcon } from '../../components';
import QUEST_SIGN from '../../../assets/images/question.png';

export const Description = styled(QText).attrs({
  preset: 'link',
  fontSize: Sizing.medium,
})``;

export const Body = styled.View({
  flex: 0.7,
  paddingTop: 24
});

export const Footer = styled.View({
  flex: 0.3,
  alignItems: 'center',
  justifyContent: 'flex-end',
  paddingBottom: 16,
});

export const Status = styled(QText).attrs({
  preset: 'body',
  fontSize: Sizing.medium,
})`
  ${() => ({
    marginBottom: 16,
  })}
`;

export const ModalContainer = styled.View({
  backgroundColor: 'rgba(0, 0, 0, .35)',
  flex: 1,
  justifyContent: 'center',
});

export const AlertContainer = styled.View({
  width: '100%',
  padding: 16,
  marginBottom: 16,
  backgroundColor: '#fff',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 16,
});

export const InputContaier = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9,
})({
  flexDirection: 'row',
  backgroundColor: '#fff',
  alignItems: 'center',
  justifyContent: 'space-between',
  borderRadius: 16,
  marginTop: 16,
  borderColor: Colors.Gray,
  width: '100%',
  height: 48,
  borderWidth: 1,
  padding: 8,
});

export const CloseButton = styled(SystemIcon).attrs({
  name: 'close',
  tintColor: Colors.Purple,
})``;

export const DateInput = ({ value, onPress, style }) => {
  return (
    <InputContaier onPress={onPress} style={style}>
      <QText preset="body">{value}</QText>
      <SystemIcon
        tintColor={Colors.Purple}
        name="chevromDown"
      />
    </InputContaier>
  );
};

export const QuestionSign = styled.Image.attrs({
  source: QUEST_SIGN,
  resizeMode: 'contain',
})({
  marginBottom: 16,
});
