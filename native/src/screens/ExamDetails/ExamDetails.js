import React from 'react';
import DateTimePicker from '@react-native-community/datetimepicker';
import { Modal, View, TouchableOpacity, Platform } from 'react-native';
import moment from 'moment';
import { Container, QButton, QText, SystemIcon } from '../../components';
import {
  Status,
  Description,
  Footer,
  Body,
  ModalContainer,
  AlertContainer,
  DateInput,
  QuestionSign,
} from './ExamDetailsStyles';
import { Colors } from '../../constants/theme';
// import { Container } from './styles';

export default function ExamDetails({
  handleSubmit,
  selectedDate,
  saveDate,
  loading,
  toggleModal,
  isModalOpen,
  showPicker,
  togglePicker,
  navigation: {
    navigate,
    state: {
      params: {
        labExam: { name, status, tussCode },
      },
    },
  },
}) {
  const message =
    status === 'active'
      ? 'Este exame ainda não foi marcado como realizado'
      : 'Exame realizado';

  return (
    <Container style={{ paddingHorizontal: 16}}>
      <QText preset="navTitle">{name}</QText>
      <Body>
        <Status>{message}</Status>
        <Description>Descrição</Description>
        <Status>{name}</Status>
      </Body>
      <Footer>
        <QButton onPress={() => navigate('Laboratories', { tussCode })} text="Laboratórios" />
        <QButton
          style={{ width: '100%' }}
          preset="secondary"
          loading={loading}
          // onPress={handleSubmit}
          onPress={() => toggleModal(true)}
          text="Marcar como realizado"
        />
      </Footer>

      <Modal visible={isModalOpen} transparent animationType="fade" animated>
        <ModalContainer>
          <View style={{ paddingHorizontal: 16, flex: 1, justifyContent: 'center' }}>
            <AlertContainer>
              <View style={{ position: 'absolute', right: 16, top: 16 }}>
                <TouchableOpacity activeOpacity={0.9} onPress={() => toggleModal(false)}>
                  <SystemIcon tintColor={Colors.Purple} name="close" />
                </TouchableOpacity>
              </View>
              <QuestionSign />
              <QText color={Colors.TextGray} preset="bold">
                {/* <CloseButton /> */}
                Quando você realizou este exame?
              </QText>
              <QText textCenter preset="body">
                {name}
              </QText>
              <DateInput
                onPress={() => togglePicker(!showPicker)}
                value={String(moment(selectedDate).format('DD MMMM YYYY'))}
              />
              <QButton
                loading={loading}
                onPress={handleSubmit}
                style={{ marginTop: 16 }}
                text="salvar"
                inBlock={false}
              />
            </AlertContainer>
          </View>
          {showPicker && (
            <DateTimePicker
              onChange={(e, date) => {
                // eslint-disable-next-line eqeqeq
                if(Platform.OS == 'android') {
                  togglePicker(false)
                }
                saveDate(date);
              }}
              style={{
                backgroundColor: '#fff',
                width: '100%',
              }}
              mode="date"
              value={selectedDate}
            />
          )}
        </ModalContainer>
      </Modal>
    </Container>
  );
}
