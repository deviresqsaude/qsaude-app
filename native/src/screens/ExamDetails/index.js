import { withApollo } from '@apollo/react-hoc';
import { compose, withState, withHandlers } from 'recompose';
import moment from 'moment';
import R from 'ramda';
import { connect } from 'react-redux';
import ExamDetails from './ExamDetails';
import { MarkExamCompleted } from '../../service/mutations';
import { AlertActions, CareActions } from '../../store/actions';
import { dots } from '../../helpers';

const mapStateToProps = ({care}) => ({
  careState: {...care}
});

// Error Handling
const mapDispatchToProps = dispatch => ({
  onServiceFailed: ({ title, error: description }) =>
    dispatch(
      AlertActions.actions.openAlert({
        title,
        shouldRenderOkButton: true,
        closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
        description,
        isError: true,
      }),
    ),
  onServiceSuccess: ({ navigate }) =>
    dispatch(
      AlertActions.actions.openAlert({
        description: 'Seu exame foi marcado como realizado na data informada',
        shouldRenderOkButton: true,
        closeAlert: () => { dispatch(AlertActions.actions.closeAlert()); navigate('ExamList');},
        title: 'Exame realizado com sucesso!',
        isSuccess: true,
      }),
    ),
  setExams: exams => dispatch(CareActions.actions.setExams(exams)),
  setMarketEvents: events => dispatch(CareActions.actions.setMarketEvents(events)),
  upEvents: event => dispatch(CareActions.actions.updateEvents(event)),
});

const withMarkAsDone = withHandlers({
  handleSubmit: props => async () => {
    const {
      careState,
      client,
      toggleModal,
      selectedDate,
      toggleLoading,
      togglePicker,
      onServiceFailed,
      onServiceSuccess,
      setExams,
      setMarketEvents,
      upEvents,
      navigation: {
        navigate,
        state: {
          params: {
            labExam,
            refetch
          },
        },
      },
    } = props;
    togglePicker(false);
    toggleLoading(true);
    try {
      const mutate = await client.mutate({
        mutation: MarkExamCompleted,
        variables: {
          occurrenceDateTime: moment(selectedDate).toISOString(),
          examId: labExam.id,
        },
      });

      // if(mutate != "success") {
      //   onServiceFailed({
      //     title: 'Erro ao marcar exâme como realizado'
      //   })
      //   return ;
      // }
      // const index = R.findIndex(R.propEq('start', startFormat))(cloneList);
      // const events = index !== -1 ? [...cloneList[index].events, labExam.id] : [labExam.id];
      // cloneList.splice(index, index !== -1 ? 1 : 0, {start: startFormat, events});
      
      const startFormat = moment(selectedDate).format('YYYY-MM-DD');
      const cloneList = [...careState.exams, {...labExam, start: startFormat, status: 'completed'}];
      setExams(cloneList);


      const newMarkedEvents = careState.marketEvents;
      newMarkedEvents[startFormat] = { marked: true, dots: newMarkedEvents[startFormat] ? R.uniq([...newMarkedEvents[startFormat].dots, dots.exam ]) : [dots.exam]};
      setMarketEvents(newMarkedEvents);

      if(startFormat == careState.date) {
        upEvents([{id: labExam.id, ...labExam, status: 'completed', start: startFormat}])
      }

      toggleModal(false)
      onServiceSuccess({ navigate });
      refetch();
      // alert(markExamCompleted);
    } catch (error) {
      toggleModal(false)
      onServiceFailed({
        title: 'Erro ao marcar exâme como realizado',
        error: error.message,
      });
    }
    toggleModal(false)
    toggleLoading(false);
  },
});

const withModalState = withState('isModalOpen', 'toggleModal', false);
const withDatePickerState = withState('showPicker', 'togglePicker', false);
const withSelectedDateState = withState('selectedDate', 'saveDate', new Date());
const withLoadingState = withState('loading', 'toggleLoading', false);

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withApollo,
  withLoadingState,
  withModalState,
  withSelectedDateState,
  withDatePickerState,
  withMarkAsDone,
)(ExamDetails);
