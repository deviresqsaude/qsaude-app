import { withNavigation } from 'react-navigation';
import OfflineScreen from './OfflineScreen';

export default withNavigation(OfflineScreen);