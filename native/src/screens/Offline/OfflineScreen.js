import React from 'react';
import { Linking } from 'react-native';
import SyncStorage from 'sync-storage';
import { QButton, EmptyData } from '../../components';
import { FooterImage, Container, ButtonContainer } from './OfflineStyle';
import { BottomWaves } from '../Login/LoginStyle';
import { SystemImages } from '../../constants/theme';

const OfflineScreen = props => {
  const { screenProps: { refetch }, navigation } = props;
  const cards = SyncStorage.get('allHelathCards');
  return (
    <>
      <Container>
        <FooterImage/>
        <EmptyData
          title='Sem conexão!'
          description='Ops, verificamos que você esta sem uma conexão de internet disponível no momento, mas você ainda pode tem acesso as suas carteirinhas.'
          imageSource={SystemImages.offline}
          linkText='Tentar novamente'
          linkOnPress={refetch}
        >
          <ButtonContainer>
            {
              cards && cards.length > 0 ? 
                <QButton text="Visualizar carteirinhas" onPress={() => navigation.navigate('OfflineCard')} style={{marginBottom: 8}}/>
                :
                null
            }
            <QButton text="Ligue para Central Clínica 24hrs" onPress={() => Linking.openURL('tel://08005229090')}/>
          </ButtonContainer>
        </EmptyData>
      </Container>
    <BottomWaves />
    </>
  )
};

export default OfflineScreen

