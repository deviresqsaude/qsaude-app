import styled from 'styled-components/native';
import { EmptyData } from '../../components';
import { SystemImages, Colors } from '../../constants/theme';
import { hasDeviceNotch } from '../../components/HeaderNav/HeaderNavStyle';

export const OfflineMessage = styled(EmptyData).attrs(props => ({
  title: 'Sem conexão!',
  description: 'Ops, verificamos que você esta sem uma conexão de internet disponível no momento, mas você ainda pode tem acesso as suas carteirinhas.',
  // buttonText: 'Tentar novamente',
  // buttonType: 'primary',
  // buttonBlock: true,
  // onPress: props.refetch,
  imageSource: SystemImages.offline,
  linkText: 'Tentar novamente',
  linkOnPress: props.refetch,
}))``;

export const Container = styled.View({
  flex: 1,
  paddingTop: hasDeviceNotch(),
  // paddingVertical: 16,
  // paddingHorizontal: 16,
  marginBottom: 77,
  justifyContent: 'center',
  alignItems: 'center',
  width: '100%',
})

export const FooterWrapper = styled.View`
  position: absolute;
  left: 0;
  bottom: 30;
  display: flex;
  align-items: center;
  flex-direction: row;
  width: 100%;
`;

export const FooterStroke = styled.View`
  flex-grow: 1;
  border-bottom-width: 1;
  border-bottom-color: ${Colors.Purple};
  height: 2px;
`;

export const FooterImage = styled.Image.attrs({
  source: SystemImages.qsaudeMini
})`
  width: 78;
  height: 25;
  margin-bottom: 16;
`;

export const ButtonContainer = styled.View({
  paddingHorizontal: 16,
  width: '100%'
})
