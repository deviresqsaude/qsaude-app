import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { onError } from 'apollo-link-error';
import { ApolloLink, split } from 'apollo-link';
import { getMainDefinition } from 'apollo-utilities';
import SyncStorage from 'sync-storage';
import apolloLogger from 'apollo-link-logger';
import Config from 'react-native-config';
import NavigationService from '../navigation/NavigationService';

const BASE_URL = Config.API_URL;

const SUBSCRIPTION_URL = Config.WS_URL;

const httpLink = new HttpLink({ uri: BASE_URL });

const wsLink = new WebSocketLink({
  uri: SUBSCRIPTION_URL,
  options: {
    reconnect: true,
    connectionParams: {
      headers: {
        authorization: SyncStorage.get('token'),
      },
    }
  }
});

const authMiddleware = new ApolloLink((operation, forward) => {
  // add the authorization to the header
  const token = SyncStorage.get('token');
  if (token) {
    operation.setContext({
      headers: {
        authorization: token,
      },
    });
  }
  return forward(operation);
});

const errorLink = onError(({ graphQLErrors, networkError } ) => {
  if (graphQLErrors) {
    // eslint-disable-next-line no-restricted-syntax
    for (const error of graphQLErrors) {
      switch (error.extensions.code) {
        case "401":
        case "UNAUTHENTICATED":
          NavigationService.navigate('Login', {loginType: "IS_SESSION_ENDED"});
          SyncStorage.remove('token');
          SyncStorage.remove('prevToken');
          SyncStorage.remove('healthCard');
          SyncStorage.remove('@form[login]:user');
          SyncStorage.remove('profileUser');
          SyncStorage.remove('dependents');
          return null;
        default:
          console.log('[🛑 GraphQL Error]:', JSON.stringify(error, null, 2));
          break;
      }
    }
  }
  if (networkError) console.log(`[🔗 Network error]: ${networkError}`); // eslint-disable-line no-console
  return null;
});

const cache = new InMemoryCache();

const httpLinkTreated = __DEV__ ?
  ApolloLink.from([errorLink, authMiddleware, apolloLogger, httpLink]) :
  ApolloLink.from([errorLink, authMiddleware, httpLink]);

const wsLinkTreated =
  ApolloLink.from([errorLink, authMiddleware, wsLink]);

const link = split(
  // split based on operation type
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    );
  },
  wsLinkTreated,
  httpLinkTreated
);

const defaultOptions = {
  watchQuery: {
    fetchPolicy: 'network-only',
    errorPolicy: 'all',
  },
  query: {
    fetchPolicy: 'network-only',
    errorPolicy: 'all',
  },
  mutate: {
    errorPolicy: 'all',
  }
};

const client = new ApolloClient({
  cache,
  link,
  queryDeduplication: true,
  connectToDevTools: __DEV__,
  defaultOptions,
});

export default client;
