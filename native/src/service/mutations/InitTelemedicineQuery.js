import gql from 'graphql-tag';

export default gql`
  mutation InitTelemedicine($symptoms: [String!]!) {
    initTelemedicine(symptoms: $symptoms)
  }
`;
