import gql from 'graphql-tag';

export default gql`
  mutation AccepTerm($termId: String!, $personalId: String!) {
    accepTerm(idTerm: $termId, cpf: $personalId)
  }
`;
