import gql from 'graphql-tag';

export default gql`
  mutation($password: String!, $newPassword: String!) {
    changePassword(password: $password, newPassword: $newPassword)
  }
`;
