import gql from 'graphql-tag';

export default gql`
  mutation MarkExamCompleted($examId: ID!, $occurrenceDateTime: String!) {
    markExamCompleted(examId: $examId, occurrenceDateTime: $occurrenceDateTime)
  }
`;
