import gql from 'graphql-tag';

export default gql`
  mutation(
    $cpf: String!,
    $email: String,
    $cellPhoneDDD: String,
    $cellPhoneNumber: String,
    $zipCode: String,
    $address: String,
    $addressNumber: String,
    $addressComplement: String,
    $neighborhood: String,
    $city: String,
    $state: String) {
    updateBeneficiary(
      cpf: $cpf,
      email: $email,
      cellPhoneDDD: $cellPhoneDDD,
      cellPhoneNumber: $cellPhoneNumber,
      zipCode: $zipCode,
      address: $address,
      addressNumber: $addressNumber,
      addressComplement: $addressComplement,
      neighborhood: $neighborhood,
      city: $city,
      state: $state
    ) {
      payLoad,
      critics
    }
  }
`;
