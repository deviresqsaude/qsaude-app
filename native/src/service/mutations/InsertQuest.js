import gql from 'graphql-tag';

export default gql`
  mutation SendQuestions($answerVersion: String, $questionId: String, $name: String, $status: StatusInput, $answers: [AnswersInput!], $externalId: String) {
    sendQuestions(
      question: {
        answerVersion: $answerVersion,
        questionId: $questionId,
        name: $name,
        status: $status,
        answers: $answers,
        externalId: $externalId,
      }
    ) {
      message
      type
    }
  }
`;