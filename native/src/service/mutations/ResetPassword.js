import gql from 'graphql-tag';

export default gql`
  mutation($user: String!) {
    forgotPassword(user: $user)
  }
`;
