import gql from 'graphql-tag';

export default gql`
  mutation InsertAppointment(
    $locationId: ID!,
    $practitionerId: ID!,
    $start: Date!,
    $end: Date!
  ) {
    insertAppointment(
      locationId: $locationId,
      practitionerId: $practitionerId,
      start: $start,
      end: $end,
    ) {
      id
      name
      location
      telecom
      address
      start
      status
    }
  }
`;
