import gql from 'graphql-tag';

export default gql`
  mutation changeUser($numCard: ID!) {
    changeUser(numCard: $numCard) {
      token
      prontLifePatientId
      personalId
      numCard
    }
  }
`
