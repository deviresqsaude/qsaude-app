import gql from 'graphql-tag';

export default gql`
  mutation CancelAppointment($appointmentId: ID!) {
    removeAppointment(appointmentId: $appointmentId)
  }
`;
