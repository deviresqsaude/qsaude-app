import gql from 'graphql-tag';

export default gql`
  mutation InserVitalSign($data: [VitalSignInput]) {
    insertVitalSign(data: $data)
  }
`;
