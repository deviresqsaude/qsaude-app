import gql from 'graphql-tag';

export default gql`query ExamsDetails($examIds: [ID!]) {
  examsDetails(examIds: $examIds) {
    id
    name
    location
    telecom
    address
    start
    status
    doctorName
  }
}`;
