import gql from 'graphql-tag';

export default gql`
  query Payments {
    getBankSlip {
      url
      numberCode
      paidAmount
      fineAmount
      dueDate
      cpfCnpj
      numberCode
      titleAmount
      paymentDate
      interestAmount
      createdDate
      controlNumber
      statusBankSlip
      competenceMonth
    }
  }
`;
