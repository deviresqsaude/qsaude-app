import gql from 'graphql-tag';

export default gql`
  query login($user: String!, $password: String!) {
    login(user: $user, password: $password) {
      token
      personalId
      numCard
    }
  }
`;
