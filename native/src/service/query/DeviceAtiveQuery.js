import gql from 'graphql-tag';

export default gql`
  query activeDevice($deviceID: String!, $cpf: String, $cardNumber: String, $code: String!) {
    activeDevice(deviceID: $deviceID, cpf: $cpf, cardNumber: $cardNumber, code: $code)
  }
`
