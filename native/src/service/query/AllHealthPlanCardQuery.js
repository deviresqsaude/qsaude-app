import gql from 'graphql-tag';

export default gql`
  query AllHealthPlanCard{
    getAllHealthPlanCardUser {
      name
      segment
      startingDate
      dependents
      numCard
      status
      healthPlanCode
    }
  }
`
