import gql from 'graphql-tag';

export default gql`
  query codeResend($twoFactorCodeId: String!, $receiveType: Int!) {
    codeResend(twoFactorCodeId: $twoFactorCodeId, receiveType: $receiveType)
  }
`
