import gql from 'graphql-tag';

export default gql`
  query MyDoctors {
    myDoctors {
      name
      nextSlot
      address
      practitionerId
      locationId
      scheduleId
    }
  }
`;
