import gql from 'graphql-tag';

export default gql`
  query AppointmentsDate {
    appointmentsDate {
      start
      events
    }
  }
`;
