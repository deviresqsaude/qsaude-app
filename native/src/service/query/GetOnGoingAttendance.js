import gql from 'graphql-tag';

export default gql`
  query GetOnGoingAttendance {
    getOngoingAttendance {
      doctorId
      appointmentId
      hasAttendance
      zoomMeetingId
    }
  }
  `;
  
  // numCard
  // procedureTUSSCode
  // procedureDescription
  // specialty
  // healthProvider
  // healthProviderUnit
  // serviceType