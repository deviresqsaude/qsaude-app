import gql from 'graphql-tag';

export default gql`
  query {
    doctors {
      name
      professionalBoard
      position {
        latitude
        longitude
      }
      address
      postalCode
    }
  }
`;
