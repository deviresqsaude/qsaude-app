import gql from 'graphql-tag';

export default gql`
  query myDoctorSlots($practitionerId: ID!, $scheduleId: ID!, $date: String!) {
    myDoctorSlots(practitionerId: $practitionerId, scheduleId: $scheduleId, date: $date) {
      date
      slots {
        start
        end
        id
      }
    }
  }
`;
