import gql from 'graphql-tag';

export default gql`query exams($params: ExamsParams, $resultType: ResultTypesEnum) {
  exams(params: $params, resultType: $resultType) {
    ... on Exams {
      tussCode
      resultType
      resourceType
      id
      meta {
        extension {
          reference
          display
        }
        lastUpdated
        versionId
        security {
          system
          code
        }
      }
      extension {
        url
        valueBoolean
      }
      status
      intent
      category
      code {
        system
        code
        display
      }
      quantityQuantity
      subject {
        reference
        display
      }
      encounter
      occurrencePeriod {
        start
        end
      }
      authoredOn
      requester {
        reference
        display
      }
      performerType {
        system
        code
        display
      }
      versionId
      lastUpdated
      contained {
        resourceType
        id
        target {
          reference
        }
        recorded
      }
      priority
      reasonCode {
        text
      }
      insurance {
        reference
      }
      relevantHistory {
        reference
      }
      occurrenceDateTime
      patientInstruction
    }
  }
}`