import gql from 'graphql-tag';

export default gql`
  query HealthPlanCard {
    getHealthPlanCard {
      coverage
      clientName
      segmentPlan
      registration
      startingDate
      birthday
      network
      contract
      typeContract
      cns
      accommodation
      scopePlan
      contractor
      lackRemaining
      cpf
      whatsup
      healthPlanCode
      companyID
      lack {
        lackType
        lackFinalDate
        lackRemaining
        lackId
      }
      dependents {
        completeName
        type
        nationalHealthCard
        birthday
        cpf
        relationshipDegree
        dependentID
        numCard
        familyCode
        accommodation
        coverage
        lack {
          lackType
          lackFinalDate
          lackRemaining
        }
      }
    }
  }
`
