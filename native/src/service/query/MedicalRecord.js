import gql from 'graphql-tag';

export default gql`
  query {
    allergies {
      display
    }
    conditions {
      display
    }
    familyMemberHistories {
      display
      relationship
    }
    procedures {
      display
    }
  }
`;
