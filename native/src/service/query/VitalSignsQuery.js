import gql from 'graphql-tag';

export default gql`
  query RequestVitalSigns($vitalSign: VitalSignType!) {
    vitalSign(vitalSign: $vitalSign) {
      type
      measuredAt
      measure {
        amount
        unit
      }
      subMeasures {
        type
        measuredAt
        measure {
          amount
          unit
        }
      }
    }
  }
`;
