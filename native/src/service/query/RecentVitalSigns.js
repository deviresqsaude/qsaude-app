import gql from 'graphql-tag';

export default gql`
  query RecentVitalSigns {
    recentVitalSigns(
      vitalSigns: [bloodType, weight, height, imc, bloodPressure, glycemia]
    ) {
      type
      measureUnit
      measuredAt
      measure {
        unit
        amount
        amountString
      }
      subMeasures {
        type
        measuredAt
        measure {
          amount
          unit
        }
      }
    }
  }
`;
