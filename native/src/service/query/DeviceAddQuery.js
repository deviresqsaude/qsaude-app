import gql from 'graphql-tag';

export default gql`
  query addNewDevice($deviceID: String!, $cpf: String, $cardNumber: string, $receiveType: Int!) {
    addNewDevice(deviceID: $deviceID, cpf: $cpf, cardNumber: $cardNumber, receiveType: $receiveType)
  }
`
