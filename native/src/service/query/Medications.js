import gql from 'graphql-tag';

export default gql`
  query Medications {
    medications {
      display
      dosageInstruction
      frequency
      when
      method
      doseQuantity
      note
    }
  }
`;
