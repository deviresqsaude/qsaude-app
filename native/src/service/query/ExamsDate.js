import gql from 'graphql-tag';

export default gql`
  query ExamsDate {
    examsDate {
      id
      start
      events
    }
  }
`;
