import gql from 'graphql-tag';

export default gql`
  query Hospitals(
    $procedureTUSSCode: String,
    $procedureDescription: String,
    $specialty: String,
    $healthProvider: String,
    $healthProviderUnit: String,
    $serviceType: String ) {
    getAccreditedNetwork (
      procedureTUSSCode: $procedureTUSSCode,
      procedureDescription: $procedureDescription,
      specialty: $specialty,
      healthProvider: $healthProvider,
      healthProviderUnit: $healthProviderUnit,
      serviceType: $serviceType
    ) {
      address
      code
      state
      phone
      services
      neighborhood
      name
      city
      specialty
      units {
        whatsApp
        address
        state
        phone
        neighborhood
        unitName
        city
        cep
        email
        site
        latitude
        longitude
        speciality {
          services {
            code
            type
          }
          esp
          clinicalStaff {
            number
            initials
            name
          }
        }
      }
      procedure {
        rolCoverage
        lackFinalDate
      }
    }
  }
`;

  // numCard
  // procedureTUSSCode
  // procedureDescription
  // specialty
  // healthProvider
  // healthProviderUnit
  // serviceType
