import gql from 'graphql-tag';

export default gql`
  query AllSpecialities {
    allSpecialities {
      code
      display
      modalityType
      followUp
      system
      cbo
    }
  }
`
