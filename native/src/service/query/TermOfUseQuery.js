import gql from 'graphql-tag';

export default gql`
  query Termofuse($id: String!) {
    getTerm(cpf: $id) {
      id
      text
      accepted
    }
  }
`
