import gql from 'graphql-tag';

export default gql`
  query loginV2($user: String!, $password: String!, $deviceId: String!) {
    loginV2(
      user: $user
      password: $password
      deviceID: $deviceId
    ) {
      token
      prontLifePatientId
      personalId
      numCard
    }
  }
`;
