import gql from 'graphql-tag';

export default gql`
  query {
    beneficiary {
      groupCode
      segment
      type
      completeName
      socialName
      zipCode
      address
      addressNumber
      addressComplement
      city
      neighborhood
      state
      birthday
      motherName
      gender
      maritalStatus
      generalRegistration
      shippingDate
      emittingOrgan
      pisNumber
      cpf
      cellPhoneDDD
      cellPhoneNumber
      email
      nationalHealthCard
      profession
      lackCode
      healthPlanAmount
      receiveEmail
      receiveSMS
      familyCode
      userSequence
      relationshipDegree
      dependentID
      numCard
    }
    myDependents {
      segment
      type
      completeName
      socialName
      zipCode
      address
      addressNumber
      addressComplement
      city
      neighborhood
      state
      birthday
      motherName
      gender
      maritalStatus
      generalRegistration
      shippingDate
      emittingOrgan
      pisNumber
      cpf
      cellPhoneDDD
      cellPhoneNumber
      email
      nationalHealthCard
      profession
      lackCode
      healthPlanAmount
      receiveEmail
      receiveSMS
      familyCode
      userSequence
      relationshipDegree
      dependentID
      numCard
      legalResponsible {
        name
        cpf
        birthday
        gender
        relationshipDegree
        generalRegistration
        emittingOrgan
      }
      dps {
        height
        weight
        questionnaire {
          questionCode
          answerCode
          answerDetail
          medicalOpinion
        }
      }
      bankSlipSendType
      accommodation
      coverage
      preExistingDiseases {
        cid
        cpt
        cptFinalDate
      }
      healthPlan {
        code
      }
    }
  }
`
