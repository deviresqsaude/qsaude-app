export { default as LoginQuery } from './LoginQuery';
export { default as LoginV2Query } from './LoginV2Query';
export { default as VitalSignsQuery } from './VitalSignsQuery';
export { default as QuestQuery } from './QuestQuery';
export { default as ListAppointments } from './ListAppointments';
export { default as AppointmentDetails } from './AppointmentDetails';
export { default as MedicalRecord } from './MedicalRecord';
export { default as Medications } from './Medications';
export { default as RecentVitalSigns } from './RecentVitalSigns';
export { default as DoctorListQuery } from './DoctorListQuery';
export { default as MyDoctorsQuery } from './MyDoctorsQuery';
export { default as MyDoctorSlotsQuery } from './MyDoctorSlotsQuery';
export { default as LabExams } from './LabExams';
export { default as ProfileQuery } from './ProfileQuery';
export { default as PlanCardQuery } from './PlanCard';
export { default as TermOfUseQuery } from './TermOfUseQuery';
export { default as HospitalQuery } from './HospitalQuery';
export { default as ExpertsQuery } from './ExpertsQuery';
export { default as GetOnGoingAttendance } from './GetOnGoingAttendance';
export { default as SpecialityQuery } from './SpecialityQuery';
export { default as TelemedicinaQuery  } from './TelemedicinaQuery';
export { default as AllHealthPlanCardQuery } from './AllHealthPlanCardQuery';
export { default as MyDependents } from './Dependents';
export { default as BeneficiaryAndDependents } from './BeneficiaryAndDependents';
export { default as ExamsDate } from './ExamsDate';
export { default as ExamDetails } from './ExamDetails';
export { default as DeviceAddQuery } from './DeviceAddQuery';
export { default as DeviceAtiveQuery } from './DeviceAtiveQuery';
export { default as DeviceResendQuery } from './DeviceResendQuery';
export { default as AllExams } from './AllExams';
