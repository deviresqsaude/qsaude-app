import gql from 'graphql-tag';

export default gql`
  query Symptoms {
    symptoms {
      value
      description
    }
  }
`;



