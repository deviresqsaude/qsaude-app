import gql from 'graphql-tag';

export default gql`
  query Questionnaire {
    getQuestionare {
      name
      questionareId
      externalId
      active
      answerVersion
      guides {
        name
        finished
        groups {
          questions {
            visible
            questionText
            condicionalAnswer
            required
            Informative
            answerId
            answerFormat
            sexType
            questionAnswered
            answers {
              selected
              responseContent
              visible
              value
              minValue
              maxValue
              type
              sex
              condicionalAnswer
              conditionalResponsesObject
              negative
              label
              disabled
              housesDecimal
            }
          }
        }
      }
    }
  }
`;
