import gql from 'graphql-tag';

export default gql`
  query Profile {
    beneficiary{
      segment
      type
      completeName
      email
      cellPhoneDDD
      cellPhoneNumber
      zipCode
      address
      addressNumber
      addressComplement
      neighborhood
      city
      state
      birthday
      cpf
      numCard
      relationshipDegree
      gender
    }
  }
`
