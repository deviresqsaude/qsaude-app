import gql from 'graphql-tag';

export default gql`
  query($ids: [Int!]!) {
    appointmentsDetails(id: $ids) {
      id
      name
      location
      telecom
      start
      address
      status
    }
  }
`;
