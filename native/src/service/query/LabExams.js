import gql from 'graphql-tag';

export default gql`
  query LabExams {
    labexam {
      id
      name
      status
    }
  }
`;
