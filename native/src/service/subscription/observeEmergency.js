import gql from 'graphql-tag';

export default gql`
  subscription ObserveAttendanceEmergency (
    $cpf: String!
  ) {
    observerAttendanceEmergency (
      cpf: $cpf
    ) {
      cpf
      meetingId
    }
  }
`
