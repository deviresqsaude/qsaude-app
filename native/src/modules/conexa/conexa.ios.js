import { NativeModules, NativeEventEmitter } from 'react-native';

class Conexa extends NativeEventEmitter{
  constructor(nativeModule) {
    super(nativeModule);
    this.getConexaConnection = async (mettingNumber, participantName) => {
      const connection = nativeModule.getConexaConnection(
        mettingNumber,
        participantName,
        (val, ...others) => {
          console.log(`success: `, val);
          console.log(`success: `, others);
        },
      );
      return connection;
    };
  }
}

export default new Conexa(NativeModules.ConexaIntegration);
