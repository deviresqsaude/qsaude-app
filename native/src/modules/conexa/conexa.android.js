import { NativeModules, NativeEventEmitter } from 'react-native';

class Conexa extends NativeEventEmitter {
  constructor(nativeModule) {
    super(nativeModule)
    this.getConexaConnection = async (mettingNumber, participantName) => {
      const connection = await nativeModule.getConexaConnection(
        mettingNumber,
        participantName,
        success => console.log(`success: `, success),
        err => console.log(err)
      );
      return connection;
    };
  }
}

export default new Conexa(NativeModules.ConexaIntegration);
