/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import { ApolloProvider } from '@apollo/react-hooks';
import SplashScreen from 'react-native-splash-screen';
import moment from 'moment';
import SyncStorage from 'sync-storage';
import momentBR from 'moment/locale/pt-br';
import Analytics from 'appcenter-analytics';
import initStore from './store';
import AppNavigation, { OfflineNavigator } from './navigation';
import client from './service/apolloClient';

moment.locale('pt-br', momentBR);

class App extends Component {
  state = {
    loading: true,
    isConnected: false,
    unsubscribe: () => {}
  }

  async componentDidMount() {
    await SyncStorage.init();
    this.setState({
      loading: false,
      unsubscribe: NetInfo.addEventListener(this.handlerConnectionStatus)
    });
    SplashScreen.hide();
    this.getConnection();

    await Analytics.setEnabled(true);
  }

  componentWillUnmount() {
    const {unsubscribe} = this.state;
    unsubscribe();
  }

  getConnection = () => { NetInfo.fetch().then(this.handlerConnectionStatus)}

  handlerConnectionStatus = ({isConnected}) => {this.setState({ isConnected: isConnected ? 'online' : 'offline'})}

  render() {
    const { loading, isConnected } = this.state;

    if (loading) return null;
    if (isConnected === 'offline') return <OfflineNavigator screenProps={{refetch: () => this.getConnection()}} />;
    return (
      <Provider store={initStore()}>
        <ApolloProvider client={client}>
          <AppNavigation />
        </ApolloProvider>
      </Provider>
    );
  }
}

export default App;
