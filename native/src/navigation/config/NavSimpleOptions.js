import React from "react";
import { HeaderNavSimple } from '../../components/HeaderNav';
import { HeaderSimpleStyle } from '../../constants/theme';

const NavSimpleOptions = (title, navigation) => ({
  headerTitle: <HeaderNavSimple title={title} navigation={navigation}/>,
  headerStyle: { ...HeaderSimpleStyle },
  headerRight: null,
  headerLeft: null,
})

export default NavSimpleOptions;
