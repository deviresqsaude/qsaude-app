import { Colors } from '../../constants/theme';

const Config = () => ({
  activeTintColor: Colors.Purple,
  style: {
    borderTopWidth: 2,
    borderTopColor: Colors.Purple
  }
});

export default Config;