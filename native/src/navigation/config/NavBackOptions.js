import React from "react";
import { HeaderNavBack } from '../../components/HeaderNav';
import { HeaderBackStyle } from '../../constants/theme';

const NavBackOptions = () => ({
  headerTitle: <HeaderNavBack/>,
  headerStyle: {...HeaderBackStyle},
  headerRight: null,
  headerLeft: null,
});

export default NavBackOptions;
