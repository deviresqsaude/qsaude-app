import React from 'react';
import { HeaderNavFull } from '../../components/HeaderNav';
import { HeaderStyle } from '../../constants/theme';

const NavFullOptions = (title, navigation) => ({
  header: <HeaderNavFull title={title} navigation={navigation} />,
  headerStyle: { ...HeaderStyle },
  // headerLeft: () => null,
});

export default NavFullOptions;
