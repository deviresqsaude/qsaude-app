import { createStackNavigator } from 'react-navigation';
import * as Screens from '../../screens';
import { NavFullOptions, NavSimpleOptions, NavBackOptions, NavStackOptions } from '../config';

const CustomerCareStackNavigation = createStackNavigator(
  {
    CustomerCare: {
      screen: Screens.CustomerCare,
      navigationOptions: ({ navigation }) => NavFullOptions('Atendimento', navigation),
    },
    // Start Schedule Navigation
    ScheduleFilter: {
      screen: Screens.ScheduleFilter,
      navigationOptions: NavSimpleOptions('Filtro de Pesquisa'),
    },
    ScheduleExpert: {
      screen: Screens.ScheduleExpert,
      navigationOptions: NavSimpleOptions('Especialistas'),
    },
    CredentialNetwork: {
      screen: Screens.CredentialNetwork,
      navigationOptions: NavSimpleOptions('Agendar Consulta'),
    },
    UnitsNetwork: {
      screen: Screens.CredentialUnitNetwork,
      navigationOptions: NavSimpleOptions('Agendar Consulta'),
    },
    Schedule: {
      screen: Screens.Schedule,
      navigationOptions: NavSimpleOptions('Agendar Consulta'),
    },
    // End Schedule Navigation
    Laboratories: {
      screen: Screens.LaboratoryScreen,
      navigationOptions: NavSimpleOptions('Selecione o laboratório'),
    },
    ExamList: {
      screen: Screens.ExamList,
      navigationOptions: NavSimpleOptions('Agendamento de exames'),
    },
    ExamDetails: {
      screen: Screens.ExamDetails,
      navigationOptions: ({
        navigation
      }) => NavBackOptions(navigation),
    },
    ScheduleAppointment: {
      screen: Screens.MedicalConsultScreen,
      navigationOptions: NavSimpleOptions('Agendar Consulta'),
    },
    Hospitals: {
      screen: Screens.Hospitals,
      navigationOptions: NavSimpleOptions('Parceiros na saúde'),
    },
    HospitalsFilter: {
      screen: Screens.HospitalsFilter,
      navigationOptions: NavSimpleOptions('Busca de parceiros'),
    },
    // telemedicine: {
    //   screen: Screens.TelemedicineScreen,
    //   path: "telemedicine/:mettingNumber/:participantName",
    //   navigationOptions: ({ navigation }) =>
    //     NavFullOptions('Telemedicine', navigation),
    // }
  },
  {
    ...NavStackOptions,
  },
);

export default CustomerCareStackNavigation;
