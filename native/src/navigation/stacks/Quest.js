import { createStackNavigator } from 'react-navigation';
import { QuestionnaireScreen, QuestionsScreen } from '../../screens';

const QuestStack = createStackNavigator(
  {
    // Quest: QuestScreen,
    Questionnaire: QuestionnaireScreen,
    Questions: QuestionsScreen,
  },
  {
    headerMode: 'none',
    mode: 'modal',
    defaultNavigationOptions: {
      gesturesEnabled: false,
    },
    cardStyle: {
      backgroundColor: 'rgba(0,0,0,0)',
      opacity: 1
    },
    transitionConfig: () => ({
      transitionSpec: {
        duration: 0
      },
      containerStyle: {
        backgroundColor: 'rgba(0,0,0,0)',
      },
    })
  }
)

const QuestionnaireStack = createStackNavigator(
  {
    // Questionnaire: QuestionnaireScreen,
    Quest: QuestStack
  },
  {
    headerMode: 'none',
  }
);

export default QuestionnaireStack;
