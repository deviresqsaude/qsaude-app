import { createStackNavigator } from 'react-navigation';
import * as Screens from '../../screens';
import { NavFullOptions, NavSimpleOptions, NavStackOptions } from '../config';

const CareStackNavigation = createStackNavigator(
  {
    Care: {
      screen: Screens.CareScreen,
      navigationOptions: ({ navigation }) =>
        NavFullOptions('Agenda', navigation),
    },
    EventDetails: {
      screen: Screens.EventDetails,
      navigationOptions: ({ navigation }) =>
      NavSimpleOptions('Detalhes do evento', navigation),
    },
    Parents: {
      screen: Screens.ParentsScreen,
      path: 'Parents/:dependents',
      navigationOptions: ({ navigation }) =>
        NavSimpleOptions('Beneficiários', navigation),
    }
  },
  {
    ...NavStackOptions,
  },
);

export default CareStackNavigation;
