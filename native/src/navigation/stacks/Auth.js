import { createStackNavigator } from 'react-navigation';
import { LoginScreen, ForgotPassword, TermsOfUse, SecurityTokenScreen, MultiHealthCard } from '../../screens';

const AuthStackNavigation = createStackNavigator(
  {
    Login: {
      screen: LoginScreen,
      navigationOptions: {
        title: `Login`,
        header: null,
      },
    },
    ForgotPassword: {
      screen: ForgotPassword,
      navigationOptions: {
        header: null
      },
    },
    TermsOfUse: {
      screen: TermsOfUse,
      path: "TermsOfUse/:token/:id"
    },
    MultiHealthCard: {
      screen: MultiHealthCard,
      path: "MultiHealthCard"
    },
    SecurityToken: {
      screen: SecurityTokenScreen,
      navigationOptions: {
        title: "Olá, Bem-víndo",
        header: null
      }
    }
  },
  {
    mode: 'modal',
    headerMode: 'none',
    cardStyle: {
      backgroundColor: 'rgba(0, 0, 0, 0)',
      opacity: 1
    },
    transitionConfig: () => ({
      containerStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0)',
      },
    })
  },
);

export default AuthStackNavigation;
