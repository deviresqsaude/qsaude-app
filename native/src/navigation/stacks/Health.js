import { createStackNavigator } from 'react-navigation';
import * as Screens from '../../screens';
import { NavFullOptions, NavSimpleOptions, NavStackOptions } from '../config';
import Constants from '../../constants/VitalSignForm';
import QuestStack from './Quest';

const HealthStack = createStackNavigator(
  {
    Health: {
      screen: Screens.HealthScreen,
      navigationOptions: ({ navigation }) => NavFullOptions('Saúde', navigation),
    },
    MedicalRecord: {
      screen: Screens.MedicalRecordScreen,
      navigationOptions: ({ navigation }) => NavSimpleOptions('Ficha Médica', navigation),
    },
    Medications: {
      screen: Screens.Medications,
      navigationOptions: ({ navigation }) => NavSimpleOptions('Medicamentos', navigation),
    },
    Orientation: {
      screen: Screens.HealthGuidelineScreen,
      navigationOptions: ({ navigation }) => NavSimpleOptions('Orientações de saúde', navigation),
    },
    MedicationDetails: {
      screen: Screens.MedicationDetails,
      navigationOptions: ({
        navigation,
        navigation: {
          state: {
            params: { medication },
          },
        },
      }) => NavSimpleOptions(String(medication.display).split('(')[0], navigation),
    },
    Parents: {
      screen: Screens.ParentsScreen,
      navigationOptions: ({ navigation }) =>
        NavSimpleOptions('Troca Usuário', navigation),
    },
    Quest: {
      screen: QuestStack,
      navigationOptions: ({ navigation }) =>
      NavSimpleOptions('Questionário', navigation),
    },
    VitalSignChart: {
      screen: Screens.VitalSignChart,
      navigationOptions: ({
        navigation,
        navigation: {
          state: {
            params: { chartVitalSign, vitalSign },
          },
        },
      }) =>
      NavSimpleOptions(
          Constants[vitalSign].Labels[chartVitalSign || vitalSign],
          navigation,
        ),
    },
    VitalSignForm: {
      screen: Screens.VitalSignForm,
      navigationOptions: ({
        navigation,
        navigation: {
          state: {
            params: { chartVitalSign, vitalSign },
          },
        },
      }) =>
      NavSimpleOptions(
          `${Constants[vitalSign].Labels[chartVitalSign || vitalSign]}`,
          navigation,
        ),
    },
  },
  {
    ...NavStackOptions,
  },
);

export default HealthStack;
