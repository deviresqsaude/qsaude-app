import { createStackNavigator } from 'react-navigation';
import { AccountScreen, BenificiaryManual, HealthCard, PasswordScreen, ProfileScreen, Payment, PaymentList } from '../../screens';
import { NavFullOptions, NavStackOptions, NavSimpleOptions } from '../config';

const AccountStackNavigation = createStackNavigator(
  {
    Account: {
      screen: AccountScreen,
      navigationOptions: ({ navigation }) => NavFullOptions('Conta', navigation),
    },
    Password: {
      screen: PasswordScreen,
      navigationOptions: ({ navigation }) => NavSimpleOptions('Alterar Senha', navigation),
    },
    UpdateInfo: {
      screen: ProfileScreen,
      navigationOptions: ({ navigation }) => NavSimpleOptions('Alterar Dados', navigation),
    },
    BenificiaryManual: {
      screen: BenificiaryManual,
      navigationOptions: ({ navigation }) => NavSimpleOptions('Manual do beneficiário', navigation),
    },
    HealthCard: {
      screen: HealthCard,
      navigationOptions: ({ navigation }) => NavSimpleOptions('Carteirinha virtual', navigation),
    },
    // Payment: {
    //   screen: Payment,
    //   navigationOptions: ({ navigation }) => NavFullOptions('Boleto', navigation),
    // },
    PaymentList: {
      screen: PaymentList,
      navigationOptions: ({ navigation }) => NavSimpleOptions('Meus Boletos', navigation),
    }
  },
  {
    ...NavStackOptions,
  },
);

export default AccountStackNavigation;
