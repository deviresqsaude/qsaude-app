import { createStackNavigator } from 'react-navigation';
import * as Screens from '../../screens';
import { NavFullOptions } from '../config';

const TeleStackNavigation = createStackNavigator(
  {
    Telemedicine: {
      screen: Screens.TelemedicineScreen,
      path: 'telemedicine/:meetingNumber/:participantName',
      navigationOptions: ({ navigation }) => {
        NavFullOptions('Telemedicine', navigation);
      }
    },
    TelemedicineError: {
      screen: Screens.TelemedicineErrorScreen
    }
  },
  {
    headerMode: 'none',
  },
);

export default TeleStackNavigation;
