/* eslint-disable react/prop-types */
import React from 'react';
import {
  createBottomTabNavigator,
  createAppContainer,
  createSwitchNavigator,
  createStackNavigator,
} from 'react-navigation';
import { connect } from 'react-redux';
import CareStack from './stacks/Care';
import HealthStack from './stacks/Health';
import AuthStackNavigation from './stacks/Auth';
import CustomerCareStack from './stacks/CustomerCare';
import AccountStackNavigation from './stacks/Account';
import TelemedicineStack from './stacks/Telemedicine';
import { SplashScreen, OfflineScreen, OfflineCardScreen } from '../screens';
import { TabBarOptions } from './config';
import { QEmergency } from '../components/QEmergency';
import QTutorial from '../components/QTutorial';
import NavigationService from './NavigationService';

import BottomTabbar from '../components/BottomTabbar';
import { AlertActions } from '../store/actions';
import { QAlert, QButton, QLoading } from '../components';

const TabBar = () => (
  <>
    <BottomTabbar/>
    <QEmergency/>
    <QTutorial/>
  </>
);

const AppStack = createBottomTabNavigator(
  {
    Care: CareStack,
    Health: HealthStack,
    CustomerCare: CustomerCareStack,
    Account: AccountStackNavigation
  },
  {
    tabBarComponent: TabBar,
    tabBarOptions: TabBarOptions,
    swipeEnabled: true,
  },
);

const switchContainer = createSwitchNavigator(
  {
    App:  {
      screen: AppStack,
      path: ''
    },
    Auth: AuthStackNavigation,
    AuthLoading: SplashScreen,
    TelemedicineStack: {
      screen: TelemedicineStack,
      path: 'meeting'
    },
  },
  {
    initialRouteName: 'AuthLoading',
  },
);

const offLineNavigator = createStackNavigator({
  Offline: {
    screen: props => <OfflineScreen {...props} />
  },
  OfflineCard: {
    screen: props => <OfflineCardScreen {...props} />
  },
},
{
  initialRouteName: 'Offline',
  headerMode: 'none'
}
);

export const OfflineNavigator = createAppContainer(offLineNavigator);

const AppContainer = createAppContainer(switchContainer);

class Navigation extends React.Component {
  render() {
    // const {showRating} = this.state;
    const { alertConfig, closeAlert, loadingConfig } = this.props;
    return (
      <>
        <AppContainer
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}/>
        <QLoading {...loadingConfig}/>
        <QAlert closeAlert={closeAlert} {...alertConfig}>
          {alertConfig.actionsToTake &&
          alertConfig.actionsToTake.length &&
          alertConfig.actionsToTake.map(action => {
            const { onPress, label, preset, block } = action;
            return (
              <QButton
                preset={preset || 'primary'}
                text={label}
                inBlock={block || false}
                onPress={onPress}
              />
            );
          })}
        </QAlert>
      </>
    );
  }
}

const mapStateToProps = ({ alert, loadSystem }) => ({
  alertConfig: alert,
  loadingConfig: loadSystem
});

const mapDispatchToProps = dispatch => ({
  closeAlert: () => dispatch(AlertActions.actions.closeAlert()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Navigation);
