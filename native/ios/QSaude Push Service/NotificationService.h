//
//  NotificationService.h
//  QSaude Push Service
//
//  Created by Alan  Marcell on 07/10/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
