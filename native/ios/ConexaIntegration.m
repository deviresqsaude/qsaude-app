//
//  ConexaIntegration.m
//  qsaudeapp
//
//  Created by Kelvin Arnold on 29/10/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "React/RCTBridgeModule.h"
#import "React/RCTEventEmitter.h"

@interface RCT_EXTERN_MODULE(ConexaIntegration, RCTEventEmitter)
 RCT_EXTERN_METHOD(getConexaConnection:(NSInteger)meeting withParticipan:(NSString)participant withCallback:(RCTResponseSenderBlock)callback);

 RCT_EXTERN_METHOD(getEndingConnection);
// RCT_EXTERN_METHOD(endMeeting);
// RCT_EXTERN_METHOD(successStartMeeting);
// RCT_EXTERN_METHOD(errorAuthentication);
// RCT_EXTERN_METHOD(errorStartMeeting);
@end

