//
//  ConexaIntegration.swift
//  qsaudeapp
//
//  Created by Kelvin Arnold on 29/10/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation
import ConexaFramework

@objc(ConexaIntegration)
class ConexaIntegration: RCTEventEmitter {
  fileprivate let conexaSdk = ConexaSDK.sharedInstance;
  
  private var status = "loading";
  
  @objc
  func getConexaConnection(
    _ meeting:NSInteger,
    withParticipan participant:NSString,
    withCallback callback:@escaping RCTResponseSenderBlock
  ) -> Void {
    conexaSdk.delegate = self as? ConexaSdkDelegate;
    DispatchQueue.main.async {
      self.conexaSdk.startMeeting(meetingNumber: meeting, nameParticipant: participant as String); 
      callback([meeting, participant]);
    }
  }
  
  @objc
  func getEndingConnection(){
    print("Hola que tal end meeting");
    
  }
  
  override func supportedEvents() -> [String]! {
    return ["successStartMeeting", "errorAuthentication", "errorStartMeeting", "endMeeting"]
  }
  
  override static func requiresMainQueueSetup() -> Bool {
    return true
  }
}

extension ConexaIntegration: ConexaSdkDelegate {

  func endMeeting() {
//    getEndingConnection();
    sendEvent(withName: "endMeeting", body: ["status": "finished"]);
    print("End meeting");
  }
  
  func errorStartMeeting(message: String) {
    print("Conexa Error startMeeting");
    
  }
  
  @objc
  func successStartMeeting() {
    print("Conexa startMeeting success");
  }
  
  @objc
  func errorAuthentication() {
    print("Conexa Error Authentication");
  }
}

