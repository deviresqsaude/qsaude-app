import { PROFILECUSTOMERCARE, LOGIN } from '../src/constants/TestIds';

// OK
/* eslint-disable no-undef */

export const customerCare = () => {
  it("Check 'Agendar uma consulta' button", async () => {
    await element(by.id('BOTTOMTABBAR.tab.CustomerCare')).tap();
    await expect(
      element(by.id(PROFILECUSTOMERCARE.itemsList.ScheduleAppointment)),
    ).toExist();
    await element(by.id(PROFILECUSTOMERCARE.itemsList.ScheduleAppointment)).tap();
    await element(by.id(PROFILECUSTOMERCARE.AppointmentCategory.personalDoctor)).tap();
    await element(by.id('voltarBreadcrumb')).tap();
    await waitFor(element(by.id(PROFILECUSTOMERCARE.AppointmentCategory.butttonCancel)))
      .toBeVisible()
      .withTimeout(1000);
    await element(by.id(PROFILECUSTOMERCARE.AppointmentCategory.butttonCancel)).tap();
  });
};
