/* eslint-disable no-undef */

// FEITO
describe.skip('Login tests', () => {
  beforeEach(async () => {
    await device.launchApp({ newInstance: false });
    // await device.reloadReactNative({ newInstance: false });
    // await device.launchApp({delete:true, newInstance: true});
  });

  // OK
  it("Valid login using 'ID Carteirinha'", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).clearText();
    await element(by.id(LOGIN.form.userInput)).typeText('000010000127008');
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).clearText();
    await element(by.id(LOGIN.form.passwordInput)).typeText('Qsaude@2019');
    await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
    await waitFor(element(by.id(LOGIN.form.submitButton)))
      .toBeVisible()
      .withTimeout(2000);
    await expect(element(by.id(LOGIN.form.submitButton))).toExist();
    await expect(element(by.text('Entrar')));
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Desabilitado',
    //   );
    await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
    await element(by.id(LOGIN.form.termsCheckbox)).tap();
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Habilitado',
    // );
    await element(by.id(LOGIN.form.submitButton)).tap();
    await element(by.id('BOTTOMTABBAR.tab.Profile')).tap();
    await expect(element(by.id(PROFILEUSER.itemsList.logout))).toExist();
    await element(by.id(PROFILEUSER.itemsList.logout)).tap();
  });

  // feito
  it("Valid login using 'CPF'", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).clearText();
    await element(by.id(LOGIN.form.userInput)).typeText('99711160064');
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).clearText();
    await element(by.id(LOGIN.form.passwordInput)).typeText('1355@31ADz');
    await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
    await expect(element(by.id(LOGIN.form.submitButton))).toExist();
    await expect(element(by.text('Entrar')));
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Desabilitado',
    // );
    await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
    await element(by.id(LOGIN.form.termsCheckbox)).tap();
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Habilitado',
    // );
    await element(by.id(LOGIN.form.submitButton)).tap();
    await element(by.id('BOTTOMTABBAR.tab.Profile')).tap();
    await expect(element(by.id(PROFILEUSER.itemsList.logout))).toExist();
    await element(by.id(PROFILEUSER.itemsList.logout)).tap();
  });

  // FEITO
  it("Invalid login using invalid 'ID carteirinha' and correct password", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).typeText('22');
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).typeText('1355@31ADz');
    await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
    await expect(element(by.id(`${LOGIN.form.userInput}.error`))).toHaveText(
      'Use 11 dígitos para CPF ou 16 dígitos para ID',
    );
    await expect(element(by.id(LOGIN.form.submitButton))).toExist();
    // await expect(element(by.id(`${LOGIN.form.submitButton}.label`))).toBeVisible();
    await expect(element(by.text('Entrar')));
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Desabilitado',
    // );
    await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
    await element(by.id(LOGIN.form.termsCheckbox)).tap();
    await element(by.id(LOGIN.form.submitButton)).tap();
  });

  // FEITO
  it("Invalid login using a valid 'ID carteirinha' and wrong password", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).typeText('000010000127008');
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).typeText('135');
    await element(by.id(LOGIN.form.passwordInput)).tapBackspaceKey();
    await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
    await expect(element(by.id(`${LOGIN.form.userInput}.error`))).toHaveText(
      'Use 11 dígitos para CPF ou 16 dígitos para ID',
    );
    await expect(element(by.id(`${LOGIN.form.passwordInput}.error`))).toHaveText(
      'Senha deve ter no mínimo 6 caracteres',
    );
    await expect(element(by.id(LOGIN.form.submitButton))).toExist();
    await expect(element(by.text('Entrar')));
    await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
      'Entrar Desabilitado',
    );
    await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
    await element(by.id(LOGIN.form.termsCheckbox)).tap(2);
    await element(by.id(LOGIN.form.submitButton)).tap();
  });

  // FEITO
  it("Invalid login using invalid 'CPF' number and correct password", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).typeText('12');
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).typeText('1355@31ADz');
    await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
    await expect(element(by.id(`${LOGIN.form.userInput}.error`))).toHaveText(
      'Use 11 dígitos para CPF ou 16 dígitos para ID',
    );
    await expect(element(by.id(LOGIN.form.submitButton))).toExist();
    await expect(element(by.text('Entrar')));
    await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
      'Entrar Desabilitado',
    );
    await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
    await element(by.id(LOGIN.form.termsCheckbox)).tap();
    await element(by.id(LOGIN.form.submitButton)).tap();
  });

  // FEITO
  it("Invalid login using a valid 'CPF' and wrong password", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).typeText('10692250883');
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).typeText('135');
    await element(by.id(LOGIN.form.passwordInput)).tapBackspaceKey();
    await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
    await expect(element(by.id(`${LOGIN.form.passwordInput}.error`))).toHaveText(
      'Senha deve ter no mínimo 6 caracteres',
    );
    await expect(element(by.id(LOGIN.form.submitButton))).toExist();
    await expect(element(by.text('Entrar')));
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Desabilitado',
    // );
    await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
    await element(by.id(LOGIN.form.termsCheckbox)).tap();
    await element(by.id(LOGIN.form.submitButton)).tap();
  });
});

// FEITO
describe.skip('Termos de uso', () => {
  before(async () => {
    await device.launchApp({ newInstance: false });
    // await device.reloadReactNative();
  });

  it("Should check 'termos de uso'", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await expect(element(by.id(LOGIN.form.termsLink))).toBeVisible();
    await expect(element(by.id(LOGIN.form.termsLink))).toExist();
    await element(by.id(LOGIN.form.termsLink)).tap();
    await waitFor(element(by.id(LOGIN.form.termsText)))
      .toBeVisible()
      .withTimeout(3000);
    await element(by.id(LOGIN.form.termsText)).swipe('up');
    await element(by.id(LOGIN.form.termsText)).swipe('down');
    await element(by.id(LOGIN.form.termsText)).swipe('down');
    await expect(element(by.id(LOGIN.form.backButton))).toExist();
    await expect(element(by.id(LOGIN.form.backButton))).toBeVisible();
    await element(by.id(LOGIN.form.backButton)).tap();
  });
});

// FEITO
describe('Esqueceu a senha', () => {
  beforeEach(async () => {
    await device.launchApp({ newInstance: false });
    // await device.reloadReactNative();
  });

  it("Should check 'Esqueceu a senha' e envio de email", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await expect(element(by.id(LOGIN.form.forgotPassword))).toExist();
    await expect(element(by.id(LOGIN.form.forgotPassword))).toBeVisible();
    await element(by.id(LOGIN.form.forgotPassword)).tap();
    // await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).tap();
    // await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).typeText(
    //   'EMAIL@DOMINIO',
    // );
    // await element(by.id(FORGOTPASSWORD.form.forgotPasswordSubmitButton)).tap();
    // await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).clearText();
    // await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).typeText(
    //   'EMAILDOMINIO.COM',
    // );
    // await element(by.id(FORGOTPASSWORD.form.forgotPasswordSubmitButton)).tap();
    // await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).clearText();
    // await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).typeText(
    //   'EMAIL@DOMINIO',
    // );
    // await element(by.id(FORGOTPASSWORD.form.forgotPasswordSubmitButton)).tap();
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).clearText();
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).typeText(
      'EMAIL@DOMINIO.COM',
    );
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordSubmitButton)).tap();
    await element(by.id(LOGIN.form.navBackButtonSimple)).tap();
  });
});
