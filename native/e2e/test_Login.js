/* eslint-env detox/detox */
import {
  LOGIN,
  PROFILEUSER,
  FORGOTPASSWORD,
  BOTTOMTABBAR,
  MEMBERS,
  USER
} from '../src/constants/TestIds';


export const termosDeUso = () => {
  it("Should check 'termos de uso'", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await expect(element(by.id(LOGIN.form.termsLink))).toBeVisible();
    await expect(element(by.id(LOGIN.form.termsLink))).toExist();
    await element(by.id(LOGIN.form.termsLink)).tap();
    await waitFor(element(by.id(LOGIN.form.termsText)))
      .toBeVisible()
      .withTimeout(3000);
    await element(by.id(LOGIN.form.termsText)).swipe('up');
    await element(by.id(LOGIN.form.termsText)).swipe('down');
    await element(by.id(LOGIN.form.termsText)).swipe('down');
    await expect(element(by.id(LOGIN.form.navBackButtonSimple))).toExist();
    await expect(element(by.id(LOGIN.form.navBackButtonSimple))).toBeVisible();
    await element(by.id(LOGIN.form.navBackButtonSimple)).tap();
  });
};

export const esqueceuSenha = () => {
  it("Should check 'Esqueceu a senha' e envio de email", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await expect(element(by.id(LOGIN.form.forgotPassword))).toExist();
    await expect(element(by.id(LOGIN.form.forgotPassword))).toBeVisible();
    await element(by.id(LOGIN.form.forgotPassword)).tap();
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).tap();
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).typeText(
      'EMAIL@DOMINIO',
    );
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordSubmitButton)).tap();
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).clearText();
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).typeText(
      'EMAILDOMINIO.COM',
    );
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordSubmitButton)).tap();
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).clearText();
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).typeText(
      'EMAIL@DOMINIO',
    );
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordSubmitButton)).tap();
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).clearText();
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).typeText(
      'EMAIL@DOMINIO.COM',
    );
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordSubmitButton)).tap();
    await element(by.id(LOGIN.form.navBackButtonSimple)).tap();
  });
};

export const validLoginIDCarterinha = () => {

  it("Valid login using 'ID Carteirinha'", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).clearText();
    await element(by.id(LOGIN.form.userInput)).typeText(USER.numcardValido);
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).clearText();
    await element(by.id(LOGIN.form.passwordInput)).typeText(USER.senhaValida);
    await element(by.id(`${LOGIN.form.passwordInput}.label`)).tap();
    await waitFor(element(by.id(LOGIN.form.submitButton)))
      .toBeVisible()
      .withTimeout(2000);
    await expect(element(by.id(LOGIN.form.submitButton))).toExist();
    await expect(element(by.text('Entrar')));
    await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
      'Entrar Habilitado',
    );
    await element(by.id(LOGIN.form.submitButton)).tap();
  });
};

export const validLoginCPF = () => {
  it("Valid login using 'CPF'", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).clearText();
    await element(by.id(LOGIN.form.userInput)).typeText('29641911821');
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).clearText();
    await element(by.id(LOGIN.form.passwordInput)).typeText('Qsaude@2019');
    await element(by.id(`${LOGIN.form.passwordInput}.label`)).tap();
    await expect(element(by.id(LOGIN.form.submitButton))).toExist();
    await expect(element(by.text('Entrar')));
    await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
      'Entrar Desabilitado',
    );
    await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
    await element(by.id(LOGIN.form.termsCheckbox)).tap();
    await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
      'Entrar Habilitado',
    );
    await element(by.id(LOGIN.form.submitButton)).tap();
    await element(by.id('BOTTOMTABBAR.tab.Profile')).tap();
    await expect(element(by.id(PROFILEUSER.itemsList.logout))).toExist();
    await element(by.id(PROFILEUSER.itemsList.logout)).tap();
  });
};

export const invalidLoginIncorretIDCorrectPWD = () => {
  it("Invalid login using invalid 'ID carteirinha' and correct password", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).typeText('22');
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).typeText('1355@31ADz');
    await element(by.id(`${LOGIN.form.passwordInput}.label`)).tap();
    await expect(element(by.id(`${LOGIN.form.userInput}.error`))).toHaveText(
      'Mínimo de 11 dígitos',
    );
    await expect(element(by.id(LOGIN.form.submitButton))).toExist();
    // await expect(element(by.id(`${LOGIN.form.submitButton}.label`))).toBeVisible();
    await expect(element(by.text('Entrar')));
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Desabilitado',
    // );
    await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
    await element(by.id(LOGIN.form.termsCheckbox)).tap();
    await element(by.id(LOGIN.form.submitButton)).tap();
  });
};

export const invalidLoginCorrectIDIncorrectPWD = () => {
  it("Invalid login using a valid 'ID carteirinha' and wrong password", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).clearText();
    await element(by.id(LOGIN.form.userInput)).typeText('000010000127008');
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).clearText();
    await element(by.id(LOGIN.form.passwordInput)).typeText('135');
    await element(by.id(LOGIN.form.passwordInput)).tapBackspaceKey();
    await element(by.id(`${LOGIN.form.passwordInput}.label`)).tap();
    await expect(element(by.id(`${LOGIN.form.passwordInput}.error`))).toHaveText(
      'Mínimo de 6 caracteres',
    );
    await expect(element(by.id(LOGIN.form.submitButton))).toExist();
    await expect(element(by.text('Entrar')));
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Desabilitado',
    // );
    await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
    await element(by.id(LOGIN.form.termsCheckbox)).tap();
    await element(by.id(LOGIN.form.submitButton)).tap();
  });
};

export const invalidLoginInvalidCPFCorrectPWD = () => {
  it("Invalid login using invalid 'CPF' number and correct password", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).clearText();
    await element(by.id(LOGIN.form.userInput)).typeText('12');
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).clearText();
    await element(by.id(LOGIN.form.passwordInput)).typeText('1355@31ADz');
    await element(by.id(`${LOGIN.form.passwordInput}.label`)).tap();
    await expect(element(by.id(`${LOGIN.form.userInput}.error`))).toHaveText(
      'Mínimo de 11 dígitos',
    );
    await expect(element(by.id(LOGIN.form.submitButton))).toExist();
    await expect(element(by.text('Entrar'))).toExist();
    await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
      'Entrar Desabilitado',
    );
    await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
    await element(by.id(LOGIN.form.termsCheckbox)).tap();
    await element(by.id(LOGIN.form.submitButton)).tap();
  });
};

export const invalidLoginInvalidCPFIncorrectPWD = () => {
  it("Invalid login using a valid 'CPF' and wrong password", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).clearText();
    await element(by.id(LOGIN.form.userInput)).typeText('10692250883');
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).clearText();
    await element(by.id(LOGIN.form.passwordInput)).typeText('135');
    await element(by.id(LOGIN.form.passwordInput)).tapBackspaceKey();
    await element(by.id(`${LOGIN.form.passwordInput}.label`)).tap();
    await expect(element(by.id(`${LOGIN.form.passwordInput}.error`))).toHaveText(
      'Mínimo de 6 caracteres',
    );
    await expect(element(by.id(LOGIN.form.submitButton))).toExist();
    await expect(element(by.text('Entrar')));
    await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
      'Entrar Desabilitado',
    );
    await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
    await element(by.id(LOGIN.form.termsCheckbox)).tap();
    await element(by.id(LOGIN.form.submitButton)).tap();
  });
};

export const validLoginIDCarterinhaSecundario = () => {

  it("Valid login using 'ID Carteirinha'", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).clearText();
    await element(by.id(LOGIN.form.userInput)).typeText(USER.numcardValidoSecunario);
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).clearText();
    await element(by.id(LOGIN.form.passwordInput)).typeText(USER.senhaValidaSecunaria);
    await element(by.id(`${LOGIN.form.passwordInput}.label`)).tap();
    await waitFor(element(by.id(LOGIN.form.submitButton)))
      .toBeVisible()
      .withTimeout(2000);
    await expect(element(by.id(LOGIN.form.submitButton))).toExist();
    await expect(element(by.text('Entrar')));
    await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
      'Entrar Habilitado',
    );
    await element(by.id(LOGIN.form.submitButton)).tap();
  });
};