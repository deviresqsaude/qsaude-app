import {
  termosDeUso,
  esqueceuSenha,
  validLoginIDCarterinha,
  // validLoginCPF,
  // invalidLoginIncorretIDCorrectPWD,
  // invalidLoginCorrectIDIncorrectPWD,
  // invalidLoginInvalidCPFCorrectPWD,
  // invalidLoginInvalidCPFIncorrectPWD,
} from './test_Login';

import { qSaudeUser } from './test_qSaudeUser';

import {
  // ChangeUserPassword, 
  // checkVirtualCard, 
  changeUserData 
} from './test_Profile'

import {
  questSuaSaude,
  questAutoCuidado,
  questHabitos,
  saudeFichaMedica,
  saudeDadosVitais,
  saudeMedicamentos,
} from './test_TabHealth';

import { customerCare } from './test_Agenda';


describe('All tests', () => {
  // beforeEach(async () => {
  //   await device.launchApp({ newInstance: false });
    // await device.reloadReactNative({ newInstance: false, delete: true });
    // await device.launchApp({delete:true, newInstance: true});
  // });

  describe.skip('Componentes tela de login', () => {
    esqueceuSenha();
    termosDeUso();
  });

  describe.skip('Testes de Login', () => {
    // beforeEach(async () => {
      // await device.launchApp({ newInstance: false });
      // await device.reloadReactNative();
      // await device.reloadReactNative({ newInstance: false });
      // await device.launchApp({delete:true, newInstance: true});
    // });
    // invalidLoginIncorretIDCorrectPWD();
    // invalidLoginCorrectIDIncorrectPWD();
    // invalidLoginInvalidCPFCorrectPWD();
    // invalidLoginInvalidCPFIncorrectPWD();
    // validLoginCPF();
    validLoginIDCarterinha();
  });

  describe.skip('Aba Agenda', () => {
    customerCare();
  });
  
  describe.skip('Aba Saude', () => {
    qSaudeUser()
    questSuaSaude();
    questAutoCuidado();
    questHabitos()
    saudeFichaMedica();
    saudeDadosVitais();
    saudeMedicamentos();
  });

  describe.skip('Aba Atendimento', () => {
    customerCare();
  });

  // Testes aba Perfil
      describe('Aba Perfil', () => {
        // ChangeUserPassword()
        // checkVirtualCard()
        changeUserData()
      })
});

