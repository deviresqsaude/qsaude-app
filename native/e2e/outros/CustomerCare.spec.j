import { LOGIN, PROFILECUSTOMERCARE, PROFILEUSER } from '../../src/constants/TestIds';

// OK
/* eslint-disable no-undef */

describe('Customer Care button', () => {
  beforeEach(async () => {
    await device.reloadReactNative({ newInstance: true });
    // await device.launchApp({ delete: true, newInstance: true });
  });

  // OK
  it("Check 'Agendar uma consulta' button", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).typeText('000010000127008');
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).typeText('Qsaude@2019');
    await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
    await element(by.id(LOGIN.form.termsCheckbox)).tap();
    await element(by.id(LOGIN.form.submitButton)).tap();
    await waitFor(element(by.id(LOGIN.form.submitButton)))
      .toBeNotVisible()
      .withTimeout(15000);
    await element(by.id('BOTTOMTABBAR.tab.CustomerCare')).tap();
    await expect(
      element(by.id(PROFILECUSTOMERCARE.itemsList.ScheduleAppointment)),
    ).toExist();
    await element(by.id(PROFILECUSTOMERCARE.itemsList.ScheduleAppointment)).tap();
    await element(by.id(PROFILECUSTOMERCARE.AppointmentCategory.personalDoctor)).tap();
    await waitFor(element(by.id(PROFILECUSTOMERCARE.AppointmentCategory.closeModal)))
      .toBeVisible()
      .withTimeout(10000);
    await element(by.id(PROFILECUSTOMERCARE.AppointmentCategory.closeModal)).tap();

    // await element(by.id('BOTTOMTABBAR.tab.Profile')).tap();
    // await expect(element(by.id(PROFILEUSER.itemsList.logout))).toExist();
    // await element(by.id(PROFILEUSER.itemsList.logout)).tap();
  });

  it.skip("Check 'Agendar um exame' button", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).typeText('000010000127008');
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).typeText('Qsaude@2019');
    await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
    await element(by.id(LOGIN.form.termsCheckbox)).tap();
    await element(by.id(LOGIN.form.submitButton)).tap();
    await waitFor(element(by.id(LOGIN.form.submitButton)))
      .toBeNotVisible()
      .withTimeout(15000);
    await element(by.id('BOTTOMTABBAR.tab.CustomerCare')).tap();
    await expect(element(by.id(PROFILECUSTOMERCARE.itemsList.ScheduleExam))).toExist();
    await element(by.id(PROFILECUSTOMERCARE.itemsList.ScheduleExam)).tap();
    await expect(element(by.id(PROFILECUSTOMERCARE.laboratory.labDelvoni))).toExist();
    await element(by.id(PROFILECUSTOMERCARE.laboratory.labDelvoni)).tap();
    await element(by.id(PROFILECUSTOMERCARE.laboratory.labLavoiser)).tap();
    await element(by.id(PROFILECUSTOMERCARE.laboratory.labZoppi)).tap();
    await element(by.id(LOGIN.form.backButton)).tap();

    await element(by.id('BOTTOMTABBAR.tab.Profile')).tap();
    await expect(element(by.id(PROFILEUSER.itemsList.logout))).toExist();
    await element(by.id(PROFILEUSER.itemsList.logout)).tap();
    // await expect(element(by.id(PROFILEUSER.itemsList.logout))).toExist()
    // await element(by.id(PROFILEUSER.itemsList.logout)).tap()
  });
});
