import {
  LOGIN,
  PROFILECUSTOMERCARE,
  QUEST,
  BOTTOMTABBAR,
  FORGOTPASSWORD,
  PROFILEUSER,
} from '../../src/constants/TestIds';

// OK
/* eslint-disable no-undef */

describe('Teste completo', () => {
  beforeEach(async () => {
    await device.launchApp({ newInstance: false });
    // await device.reloadReactNative({ newInstance: true });
    // await device.launchApp({ delete: true, newInstance: true });
  });

  // OK
  it('Checando - Termos de Servico', async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await expect(element(by.id(LOGIN.form.termsLink))).toBeVisible();
    await expect(element(by.id(LOGIN.form.termsLink))).toExist();
    await element(by.id(LOGIN.form.termsLink)).tap();
    await waitFor(element(by.id(LOGIN.form.termsText)))
      .toBeVisible()
      .withTimeout(5000);
    await element(by.id(LOGIN.form.termsText)).swipe('up');
    await element(by.id(LOGIN.form.termsText)).swipe('down');
    await element(by.id(LOGIN.form.termsText)).swipe('down');
    await expect(element(by.id(LOGIN.form.backButton))).toExist();
    await expect(element(by.id(LOGIN.form.backButton))).toBeVisible();
    await element(by.id(LOGIN.form.backButton)).tap();
  });

  // ESQUECEU A SENHA
  it('Checando - Esqueceu a senha', async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await expect(element(by.id(LOGIN.form.forgotPassword))).toExist();
    await expect(element(by.id(LOGIN.form.forgotPassword))).toBeVisible();
    await element(by.id(LOGIN.form.forgotPassword)).tap();
    console.log('Email sem .com');
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).tap();
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).typeText(
      'EMAIL@DOMINIO',
    );
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordSubmitButton)).tap();
    console.log('Email sem @');
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordSubmitButton)).tap();
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).clearText();
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).typeText(
      'EMAILDOMINIO.COM',
    );
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordSubmitButton)).tap();
    console.log('Email com espacos');
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordSubmitButton)).tap();
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).clearText();
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).typeText(
      'EMAIL DOMINIO . COM',
    );
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordSubmitButton)).tap();

    console.log('Email completo');
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordSubmitButton)).tap();
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).clearText();
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordEmailField)).typeText(
      'EMAIL@DOMINIO.COM',
    );
    await element(by.id(FORGOTPASSWORD.form.forgotPasswordSubmitButton)).tap();
    await element(by.id(LOGIN.form.backButton)).tap();
  })


  it('Checando - Login invalido usando \'ID carteirinha e senha correta\'', async () => {
  await expect(element(by.id(LOGIN.form.form))).toBeVisible();
  await element(by.id(LOGIN.form.userInput)).tap();
  await element(by.id(LOGIN.form.userInput)).typeText('22');
  await element(by.id(LOGIN.form.passwordInput)).tap();
  await element(by.id(LOGIN.form.passwordInput)).typeText('1355@31ADz');
  await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
  await expect(element(by.id(`${LOGIN.form.userInput}.error`))).toHaveText(
    'Use 11 dígitos para CPF ou 16 dígitos para ID',
  );
  await expect(element(by.id(LOGIN.form.submitButton))).toExist();
  await expect(element(by.id(`${LOGIN.form.submitButton}.label`))).toBeVisible();
  await expect(element(by.text('Entrar')));
  await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    'Entrar Desabilitado',
  );
  await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
  await element(by.id(LOGIN.form.termsCheckbox)).tap();
  await element(by.id(LOGIN.form.submitButton)).tap();
  await element(by.id(LOGIN.form.userInput)).clearText();
  await element(by.id(LOGIN.form.passwordInput)).clearText();
  });

  it("Checando - Login invalido usando - 'ID carteirinha' valida e senha errada", async () => {
  await expect(element(by.id(LOGIN.form.form))).toBeVisible();
  await element(by.id(LOGIN.form.userInput)).tap();
  await element(by.id(LOGIN.form.userInput)).typeText('22');
  await element(by.id(LOGIN.form.passwordInput)).tap();
  await element(by.id(LOGIN.form.passwordInput)).typeText('135');
  await element(by.id(LOGIN.form.passwordInput)).tapBackspaceKey();
  await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
  await expect(element(by.id(`${LOGIN.form.userInput}.error`))).toHaveText(
    'Use 11 dígitos para CPF ou 16 dígitos para ID',
  );
  await expect(element(by.id(`${LOGIN.form.passwordInput}.error`))).toHaveText(
    'Senha deve ter no mínimo 6 caracteres',
  );
  await expect(element(by.id(LOGIN.form.submitButton))).toExist();
  await expect(element(by.text('Entrar')));
  await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    'Entrar Desabilitado',
  );
  await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
  await element(by.id(LOGIN.form.termsCheckbox)).tap();
  await element(by.id(LOGIN.form.submitButton)).tap();
  await element(by.id(LOGIN.form.userInput)).clearText();
  await element(by.id(LOGIN.form.passwordInput)).clearText();
  });

  it("Checando - Login invalido usando - CPF invalido e senha correta", async () => {
  await expect(element(by.id(LOGIN.form.form))).toBeVisible();
  await element(by.id(LOGIN.form.userInput)).tap();
  await element(by.id(LOGIN.form.userInput)).typeText('12');
  await element(by.id(LOGIN.form.passwordInput)).tap();
  await element(by.id(LOGIN.form.passwordInput)).typeText('1355@31ADz');
  await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
  await expect(element(by.id(`${LOGIN.form.userInput}.error`))).toHaveText(
    'Use 11 dígitos para CPF ou 16 dígitos para ID',
  );
  await expect(element(by.id(LOGIN.form.submitButton))).toExist();
  await expect(element(by.text('Entrar')));
  await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    'Entrar Desabilitado',
  );
  await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
  await element(by.id(LOGIN.form.termsCheckbox)).tap();
  await element(by.id(LOGIN.form.submitButton)).tap();
  await element(by.id(LOGIN.form.userInput)).clearText();
  await element(by.id(LOGIN.form.passwordInput)).clearText();
  });

  it("Checando - Login invalido usando - CPF valido e senha errada", async () => {
  await expect(element(by.id(LOGIN.form.form))).toBeVisible();
  await element(by.id(LOGIN.form.userInput)).tap();
  await element(by.id(LOGIN.form.userInput)).typeText('10692250883');
  await element(by.id(LOGIN.form.passwordInput)).tap();
  await element(by.id(LOGIN.form.passwordInput)).typeText('135');
  await element(by.id(LOGIN.form.passwordInput)).tapBackspaceKey();
  await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
  await expect(element(by.id(`${LOGIN.form.passwordInput}.error`))).toHaveText(
    'Senha deve ter no mínimo 6 caracteres',
  );
  await expect(element(by.id(LOGIN.form.submitButton))).toExist();
  await expect(element(by.text('Entrar')));
  await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    'Entrar Desabilitado',
  );
  await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
  await element(by.id(LOGIN.form.termsCheckbox)).tap();
  await element(by.id(LOGIN.form.submitButton)).tap();
  await element(by.id(LOGIN.form.userInput)).clearText();
  await element(by.id(LOGIN.form.passwordInput)).clearText();
  });

  it("Checando - Login invalido usando - CPF valido e senha errada", async () => {
  await expect(element(by.id(LOGIN.form.form))).toBeVisible();
  await element(by.id(LOGIN.form.userInput)).tap();
  await element(by.id(LOGIN.form.userInput)).typeText('000010000127008');
  await element(by.id(LOGIN.form.passwordInput)).tap();
  await element(by.id(LOGIN.form.passwordInput)).typeText('Qsaude@2019');
  await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
  // await element(by.id(LOGIN.form.termsCheckbox)).tap();
  await element(by.id(LOGIN.form.submitButton)).tap();
  await waitFor(element(by.id(LOGIN.form.submitButton)))
    .toBeNotVisible()
    .withTimeout(15000);

  // QSAUDE USER
  await element(by.text('Qsaude User')).tap();
  await element(by.id(LOGIN.form.backButton)).tap();
  await element(by.id('BOTTOMTABBAR.tab.Health')).tap();

  // QUESTIONARIO
  await expect(element(by.id(BOTTOMTABBAR.tab.health.itemQuestionario))).toExist();
  await element(by.id(BOTTOMTABBAR.tab.health.itemQuestionario)).tap();
  await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.entrar)).tap();
  await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.peso)).tap();
  await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.peso)).typeText('20');
  await element(by.id(`${BOTTOMTABBAR.questionarios.suaSaude.peso}.label`)).tap();
  await element(by.id(QUEST.nextQuestion)).tap();
  await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.altura)).tap();
  await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.altura)).typeText('165');
  await element(by.id(`${BOTTOMTABBAR.questionarios.suaSaude.altura}.label`)).tap();
  await element(by.id(QUEST.nextQuestion)).tap();
  await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.imc)).tap();
  await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.imc)).typeText('30');
  await element(by.id(`${BOTTOMTABBAR.questionarios.suaSaude.imc}.label`)).tap();
  await element(by.id(QUEST.nextQuestion)).tap();
  await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMaxima)).tap();
  await element(
    by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMaxima),
  ).tapBackspaceKey();
  await element(
    by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMaxima),
  ).tapBackspaceKey();
  await element(
    by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMaxima),
  ).tapBackspaceKey();
  await element(
    by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMaxima),
  ).typeText('122');
  await element(
    by.id(`${BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMaxima}.label`),
  ).tap();
  await element(by.id(QUEST.nextQuestion)).tap();
  await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMinima)).tap();
  await element(
    by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMinima),
  ).typeText('122');
  await element(
    by.id(`${BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMinima}.label`),
  ).tap();
  await element(by.id(QUEST.nextQuestion)).tap();
  await element(by.label('Até 80 cm')).tap();
  await element(by.id(QUEST.nextQuestion)).tap();
  await element(by.label('Muito boa')).tap();
  await element(by.id(QUEST.nextQuestion)).tap();
  await element(by.label('Diabetes Mellitus')).tap();
  await element(by.label('Fibromialgia')).tap();
  await element(by.id(BOTTOMTABBAR.modal.body)).swipe('up');
  await waitFor(element(by.label('Osteoporose')))
    .toBeVisible()
    .withTimeout(1000);
  await element(by.label('Osteoporose')).tap();
  await element(by.id(QUEST.nextQuestion)).tap();
  await element(by.label('Sim')).tap();
  await element(by.id(QUEST.nextQuestion)).tap();
  await element(by.label('Auditiva')).tap();
  await element(by.id(QUEST.nextQuestion)).tap();
  await element(by.label('Sim')).tap();
  await element(by.id(QUEST.nextQuestion)).tap();
  await element(by.label('Diabetes Mellitus ')).tap();
  await element(by.label('Câncer de útero')).tap();
  await element(by.id(BOTTOMTABBAR.modal.body)).swipe('up');
  await element(by.id(QUEST.nextQuestion)).tap();
  await element(by.label('Sim')).tap();
  await element(by.id(QUEST.nextQuestion)).tap();
  await element(by.label('1 a 3 vezes')).tap();
  await element(by.id(QUEST.nextQuestion)).tap();
  await element(by.label('Problema cardiovascular')).tap();
  await element(by.id(BOTTOMTABBAR.modal.body)).swipe('up');
  await waitFor(element(by.id(QUEST.nextQuestion)))
    .toBeVisible()
    .withTimeout(1000);
  await element(by.label('Outros')).tap();
  await element(by.id(QUEST.nextQuestion)).tap();
  await element(by.label('Sim')).tap();
  await element(by.id(QUEST.nextQuestion)).tap();
  await element(by.label('1 vez')).tap();
  await element(by.id(QUEST.nextQuestion)).tap();
  await element(by.label('Problema cardiovascular')).tap();
  await element(by.label('Diabetes Mellitus')).tap();
  await element(by.id(BOTTOMTABBAR.modal.body)).swipe('up');
  await waitFor(element(by.id(QUEST.nextQuestion)))
    .toBeVisible()
    .withTimeout(1000);
  await element(by.id(QUEST.nextQuestion)).tap();
  await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.questao_28__1)).tap();
  await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.questao_28__1)).typeText(
    'Depressao',
  );
  await element(
    by.id(`${BOTTOMTABBAR.questionarios.suaSaude.questao_28__1}.label`),
  ).tap();
  await element(by.id(BOTTOMTABBAR.modal.close)).tap();
  await element(by.text('Sair sem salvar')).tap();

  // FICHA MEDICA
  await expect(element(by.id(BOTTOMTABBAR.tab.health.itemFichaMedica))).toExist();
  await element(by.id('BOTTOMTABBAR.tab.Health')).tap();
  await element(by.id(BOTTOMTABBAR.tab.health.itemFichaMedica)).tap();
  await element(by.text('Dipirona'));
  await element(by.text('Transtorno depressivo'));
  await element(by.id('BOTTOMTABBAR.tab.Health')).tap();
  await element(by.id(BOTTOMTABBAR.tab.health.itemMedicamentos)).tap();
  await element(by.id('BOTTOMTABBAR.tab.Health')).tap();
  await element(by.id('BOTTOMTABBAR.tab.Health')).tap();

  // ATENDIMENTO
  await element(by.id('BOTTOMTABBAR.tab.CustomerCare')).tap();
  await expect(
    element(by.id(PROFILECUSTOMERCARE.itemsList.ScheduleAppointment)),
  ).toExist();
  await element(by.id(PROFILECUSTOMERCARE.itemsList.ScheduleAppointment)).tap();
  await element(by.id(PROFILECUSTOMERCARE.AppointmentCategory.personalDoctor)).tap();
  await element(by.text('ALFREDO JOSE'));
  await element(by.text('PERDIZES'));
  await element(by.text('Aline'));
  await waitFor(element(by.id(PROFILECUSTOMERCARE.AppointmentCategory.closeModal)))
    .toBeVisible()
    .withTimeout(1500);
  await element(by.id(PROFILECUSTOMERCARE.AppointmentCategory.closeModal)).tap();

  // LABORATORIOS
  await element(by.id('BOTTOMTABBAR.tab.CustomerCare')).tap();
  await expect(element(by.id(PROFILECUSTOMERCARE.itemsList.ScheduleExam))).toExist();
  await element(by.id(PROFILECUSTOMERCARE.itemsList.ScheduleExam)).tap();
  await expect(element(by.id(PROFILECUSTOMERCARE.laboratory.labDelvoni))).toExist();
  await element(by.id(PROFILECUSTOMERCARE.laboratory.labDelvoni)).tap();
  await element(by.id(PROFILECUSTOMERCARE.laboratory.labLavoiser)).tap();
  await element(by.id(PROFILECUSTOMERCARE.laboratory.labZoppi)).tap();
  await element(by.id(LOGIN.form.backButton)).tap();

  // await waitFor(element(by.id(BOTTOMTABBAR.itemFichaMedica.nome)))
  //   .toBeVisible()
  //   .withTimeout(1500);
  // await expect(element(by.id(BOTTOMTABBAR.itemFichaMedica.nome))).toExist();
  // await expect(element(by.id(BOTTOMTABBAR.itemFichaMedica.nome))).toHaveText('Anderson');

  // await waitFor(element(by.id(PROFILECUSTOMERCARE.AppointmentCategory.closeModal)))
  //   .toBeVisible()
  //   .withTimeout(10000);
  // await element(by.id(PROFILECUSTOMERCARE.AppointmentCategory.closeModal)).tap();

  await element(by.id('BOTTOMTABBAR.tab.Profile')).tap();
  await element(by.id(PROFILEUSER.itemsList.checkVcard)).tap();
  await element(by.id('virar')).tap();
  await element(by.id(LOGIN.form.backButton)).tap();
  // await element(by.id(LOGIN.form.backButton)).tap();

  // await expect(element(by.id(PROFILEUSER.itemsList.logout))).toExist();
  // await element(by.id(PROFILEUSER.itemsList.logout)).tap();
});

});
