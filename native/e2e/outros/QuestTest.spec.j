import {
  LOGIN,
  PROFILEUSER,
  HEADERNAVFULL
} from '../../src/constants/TestIds';

/* eslint-disable no-undef */

// OK
describe.skip('Tab Health', () => {
  beforeEach(async () => {
    await device.reloadReactNative({ newInstance: true });
    // await device.launchApp({delete:true, newInstance: true});
  });

  it.skip('Login e entrar em Saude > Questionario ', async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).typeText('000010000127008');
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).typeText('Qsaude@2019');
    await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
    await waitFor(element(by.id(LOGIN.form.submitButton)))
      .toBeVisible()
      .withTimeout(2000);
    await expect(element(by.id(LOGIN.form.submitButton))).toExist();
    await expect(element(by.text('Entrar')));
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Desabilitado',
    //   );
    await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
    await element(by.id(LOGIN.form.termsCheckbox)).tap();
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Habilitado',
    //   );
    await element(by.id(LOGIN.form.submitButton)).tap();
    await waitFor(element(by.id('BOTTOMTABBAR.tab.Health')))
      .toBeVisible()
      .withTimeout(2000);
      await element(by.id('BOTTOMTABBAR.tab.Health')).tap();
    await expect(element(by.id(BOTTOMTABBAR.tab.itemQuestionario))).toExist();
    await expect(element(by.id(BOTTOMTABBAR.tab.itemQuestionario))).toBeVisible();
    await element(by.id(BOTTOMTABBAR.tab.itemQuestionario)).tap();
    // await expect(element(by.id(BOTTOMTABBAR.tab.itemFichaMedica))).toExist();
    // await expect(element(by.id(BOTTOMTABBAR.tab.itemFichaMedica))).toBeVisible();
    // await element(by.id(BOTTOMTABBAR.tab.itemFichaMedica)).tap();
    // await element(by.id(HEADERNAVFULL.items.backButton)).tap();

    // await element(by.id('BOTTOMTABBAR.tab.Profile')).tap();
    // await expect(element(by.id(PROFILEUSER.itemsList.logout))).toExist();
    // await element(by.id(PROFILEUSER.itemsList.logout)).tap();
  });
  
  it.skip('Login e entrar em Saude > Medicamentos', async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).typeText('000010000127008');
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).typeText('Qsaude@2019');
    await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
    await waitFor(element(by.id(LOGIN.form.submitButton)))
      .toBeVisible()
      .withTimeout(2000);
    await expect(element(by.id(LOGIN.form.submitButton))).toExist();
    await expect(element(by.text('Entrar')));
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Desabilitado',
    //   );
    await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
    await element(by.id(LOGIN.form.termsCheckbox)).tap();
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Habilitado',
    //   );
    await element(by.id(LOGIN.form.submitButton)).tap();
    await waitFor(element(by.id('BOTTOMTABBAR.tab.Health')))
      .toBeVisible()
      .withTimeout(2000);
      await element(by.id('BOTTOMTABBAR.tab.Health')).tap();
    // await expect(element(by.id(BOTTOMTABBAR.tab.itemQuestionario))).toExist();
    // await expect(element(by.id(BOTTOMTABBAR.tab.itemQuestionario))).toBeVisible();
    // await element(by.id(BOTTOMTABBAR.tab.itemQuestionario)).tap();
    await expect(element(by.id(BOTTOMTABBAR.tab.itemMedicamentos))).toExist();
    await expect(element(by.id(BOTTOMTABBAR.tab.itemMedicamentos))).toBeVisible();
    await element(by.id(BOTTOMTABBAR.tab.itemMedicamentos)).tap();
    await element(by.id(HEADERNAVFULL.items.backButton)).tap();

    // await element(by.id('BOTTOMTABBAR.tab.Profile')).tap();
    // await expect(element(by.id(PROFILEUSER.itemsList.logout))).toExist();
    // await element(by.id(PROFILEUSER.itemsList.logout)).tap();
  });
});

