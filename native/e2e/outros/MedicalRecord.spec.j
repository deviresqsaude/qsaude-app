import { LOGIN, PROFILECUSTOMERCARE, PROFILEHEALTH, PROFILEUSER } from '../../src/constants/TestIds';

// OK
/* eslint-disable no-undef */

describe.skip('Customer Care button', () => {
  beforeEach(async () => {
    await device.reloadReactNative({newInstance: true});
    // await device.launchApp({ delete: true, newInstance: true });
  });

  // OK
  it.skip("Check 'Agendar uma consulta' button", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).typeText('000010000127008');
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).typeText('Qsaude@2019');
    await element(by.id(LOGIN.form.termsCheckbox)).multiTap(2);
    await element(by.id(LOGIN.form.submitButton)).tap();
    await waitFor(element(by.id(LOGIN.form.submitButton))).toBeNotVisible().withTimeout(15000)
    await element(by.id('BOTTOMTABBAR.tab.CustomerCare')).tap()
    await expect(element(by.id(PROFILECUSTOMERCARE.itemsList.ScheduleAppointment))).toExist()
    await element(by.id(PROFILECUSTOMERCARE.itemsList.ScheduleAppointment)).tap()
    await element(by.id(LOGIN.form.backButton)).tap();

   
   
   
   
    await element(by.id("BOTTOMTABBAR.tab.Profile")).tap()
    await expect(element(by.id(PROFILEUSER.itemsList.logout))).toExist()
    await element(by.id(PROFILEUSER.itemsList.logout)).tap()   
    // await expect(element(by.id(PROFILEUSER.itemsList.logout))).toExist() 
    // await element(by.id(PROFILEUSER.itemsList.logout)).tap()
  });

});
