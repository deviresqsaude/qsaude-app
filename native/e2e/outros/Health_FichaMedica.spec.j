import { LOGIN, PROFILECUSTOMERCARE, BOTTOMTABBAR } from '../../src/constants/TestIds';

// OK
/* eslint-disable no-undef */

describe.skip('Tab Health', () => {
  beforeEach(async () => {
    await device.reloadReactNative({ newInstance: true });
    // await device.launchApp({ delete: true, newInstance: true });
  });

  // OK
  it("Check 'Ficha Medica' ", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).typeText('000010000127008');
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).typeText('Qsaude@2019');
    await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
    await element(by.id(LOGIN.form.termsCheckbox)).tap();
    await element(by.id(LOGIN.form.submitButton)).tap();
    await waitFor(element(by.id(LOGIN.form.submitButton)))
      .toBeNotVisible()
      .withTimeout(15000);
    await element(by.id('BOTTOMTABBAR.tab.Health')).tap();
    await expect(
      element(by.id(BOTTOMTABBAR.tab.health.itemFichaMedica)),
    ).toExist();
    await element(by.id(BOTTOMTABBAR.tab.health.itemFichaMedica)).tap();
    await waitFor(element(by.id(BOTTOMTABBAR.itemFichaMedica.nome)))
      .toBeVisible()
      .withTimeout(1500);
    await expect(element(by.id(BOTTOMTABBAR.itemFichaMedica.nome))).toExist();
    await expect(element(by.id(BOTTOMTABBAR.itemFichaMedica.nome))).toHaveText('Anderson');

    // await waitFor(element(by.id(PROFILECUSTOMERCARE.AppointmentCategory.closeModal)))
    //   .toBeVisible()
    //   .withTimeout(10000);
    // await element(by.id(PROFILECUSTOMERCARE.AppointmentCategory.closeModal)).tap();

    // await element(by.id('BOTTOMTABBAR.tab.Profile')).tap();
    // await expect(element(by.id(PROFILEUSER.itemsList.logout))).toExist();
    // await element(by.id(PROFILEUSER.itemsList.logout)).tap();
  })
});
