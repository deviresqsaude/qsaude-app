import { LOGIN, BOTTOMTABBAR, QUEST } from '../../src/constants/TestIds';

// OK
/* eslint-disable no-undef */

describe.skip('Tab Health', () => {
  beforeEach(async () => {
    await device.reloadReactNative({ newInstance: true });
    // await device.launchApp({ delete: true, newInstance: true });
  });

  // OK
  it("Check 'Questionarios' ", async () => {
    await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    await element(by.id(LOGIN.form.userInput)).tap();
    await element(by.id(LOGIN.form.userInput)).typeText('000010000127008');
    await element(by.id(LOGIN.form.passwordInput)).tap();
    await element(by.id(LOGIN.form.passwordInput)).typeText('Qsaude@2019');
    await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
    await element(by.id(LOGIN.form.termsCheckbox)).tap();
    await element(by.id(LOGIN.form.submitButton)).tap();
    await waitFor(element(by.id(LOGIN.form.submitButton)))
    .toBeNotVisible()
    .withTimeout(15000);
    await element(by.id('BOTTOMTABBAR.tab.Health')).tap();
    await expect(
      element(by.id(BOTTOMTABBAR.tab.health.itemQuestionario)),
      ).toExist();
      await element(by.id(BOTTOMTABBAR.tab.health.itemQuestionario)).tap();
      await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.entrar)).tap();
      await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.peso)).tap();
      await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.peso)).typeText('20');
      await element(by.id(`${BOTTOMTABBAR.questionarios.suaSaude.peso}.label`)).tap();
      await element(by.id(QUEST.nextQuestion)).tap();
      await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.altura)).tap();
      await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.altura)).typeText('165');
      await element(by.id(`${BOTTOMTABBAR.questionarios.suaSaude.altura}.label`)).tap();
      await element(by.id(QUEST.nextQuestion)).tap();
      await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.imc)).tap();
      await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.imc)).typeText('30');
      await element(by.id(`${BOTTOMTABBAR.questionarios.suaSaude.imc}.label`)).tap();
      await element(by.id(QUEST.nextQuestion)).tap();
      await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMaxima)).tap();
      await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMaxima)).tapBackspaceKey();
      await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMaxima)).tapBackspaceKey();
      await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMaxima)).tapBackspaceKey();
      await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMaxima)).typeText('122');
      await element(by.id(`${BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMaxima}.label`)).tap();
      await element(by.id(QUEST.nextQuestion)).tap();
      await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMinima)).tap();
      await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMinima)).typeText('122');
      await element(by.id(`${BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMinima}.label`)).tap();
      await element(by.id(QUEST.nextQuestion)).tap();
      await element(by.label('Até 80 cm')).tap()
      await element(by.id(QUEST.nextQuestion)).tap();
      await element(by.label('Muito boa')).tap()
      await element(by.id(QUEST.nextQuestion)).tap();
      await element(by.label('Diabetes Mellitus')).tap()
      await element(by.label('Fibromialgia')).tap()
      await element(by.id(BOTTOMTABBAR.modal.body)).swipe('up');
      await waitFor(element(by.label('Osteoporose')))
      .toBeVisible()
      .withTimeout(1000);
      await element(by.label('Osteoporose')).tap()
      await element(by.id(QUEST.nextQuestion)).tap();
      await element(by.label('Sim')).tap();
      await element(by.id(QUEST.nextQuestion)).tap();
      await element(by.label('Auditiva')).tap();
      await element(by.id(QUEST.nextQuestion)).tap();
      await element(by.label('Sim')).tap();
      await element(by.id(QUEST.nextQuestion)).tap();
      await element(by.label('Diabetes Mellitus ')).tap();
      await element(by.label('Câncer de útero')).tap();
      await element(by.id(BOTTOMTABBAR.modal.body)).swipe('up');
      await element(by.id(QUEST.nextQuestion)).tap();
      await element(by.label('Sim')).tap();
      await element(by.id(QUEST.nextQuestion)).tap();
      await element(by.label('1 a 3 vezes')).tap();
      await element(by.id(QUEST.nextQuestion)).tap();
      await element(by.label('Problema cardiovascular')).tap();
      await element(by.id(BOTTOMTABBAR.modal.body)).swipe('up');
      await waitFor(element(by.id(QUEST.nextQuestion)))
      .toBeVisible()
      .withTimeout(1000);
      await element(by.label("Outros")).tap();
      await element(by.id(QUEST.nextQuestion)).tap();
      await element(by.label('Sim')).tap();
      await element(by.id(QUEST.nextQuestion)).tap();
      await element(by.label('1 vez')).tap();
      await element(by.id(QUEST.nextQuestion)).tap();
      await element(by.label('Problema cardiovascular')).tap();
      await element(by.label('Diabetes Mellitus')).tap();
      await element(by.id(BOTTOMTABBAR.modal.body)).swipe('up');
      await waitFor(element(by.id(QUEST.nextQuestion)))
      .toBeVisible()
      .withTimeout(1000);
      await element(by.id(QUEST.nextQuestion)).tap();
      await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.questao_28__1)).tap();
      await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.questao_28__1)).typeText('Depressao');
      await element(by.id(`${BOTTOMTABBAR.questionarios.suaSaude.questao_28__1}.label`)).tap();
      await element(by.id(BOTTOMTABBAR.modal.close)).tap();
      await element(by.text('Sair sem salvar')).tap();
  })
});
