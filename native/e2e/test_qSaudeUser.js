/* eslint-env detox/detox */
import {
    LOGIN,
    MEMBERS,
  } from '../src/constants/TestIds';
  
// QSAUDE user
export const qSaudeUser = () => {
    it("Check 'Usuario' button", async () => {
      await element(by.id('BOTTOMTABBAR.tab.Health')).tap();
      await expect(element(by.id(MEMBERS.tabHealth.topo.avatar))).toExist();
      await element(by.id(MEMBERS.tabHealth.topo.avatar)).tap();
      await element(by.id(LOGIN.form.navBackButtonSimple)).tap();
      await element(by.id('BOTTOMTABBAR.tab.Care')).tap();
    });
  };