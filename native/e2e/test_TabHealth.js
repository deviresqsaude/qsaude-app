/* eslint-env detox/detox */
import { BOTTOMTABBAR, QUEST } from '../src/constants/TestIds';

export const questSuaSaude = () => {
  it("Check 'Saude > Questionarios > Sua Saude'", async () => {
    // await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    // await element(by.id(LOGIN.form.userInput)).tap();
    // await element(by.id(LOGIN.form.userInput)).clearText();
    // await element(by.id(LOGIN.form.userInput)).typeText('000010000127008');
    // await element(by.id(LOGIN.form.passwordInput)).tap();
    // await element(by.id(LOGIN.form.passwordInput)).clearText();
    // await element(by.id(LOGIN.form.passwordInput)).typeText('Qsaude@2019');
    // await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
    // await waitFor(element(by.id(LOGIN.form.submitButton)))
    //   .toBeVisible()
    //   .withTimeout(2000);
    // await expect(element(by.id(LOGIN.form.submitButton))).toExist();
    // await expect(element(by.text('Entrar')));
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Desabilitado',
    //   );
    // await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
    // await element(by.id(LOGIN.form.termsCheckbox)).tap();
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Habilitado',
    // );
    // await element(by.id(LOGIN.form.submitButton)).tap();
    // await waitFor(element(by.id(LOGIN.form.submitButton)))
    //   .toBeNotVisible()
    //   .withTimeout(10000);
    await element(by.id('BOTTOMTABBAR.tab.Health')).tap();
    await expect(element(by.id(BOTTOMTABBAR.tab.health.itemQuestionario))).toExist();
    await element(by.id(BOTTOMTABBAR.tab.health.itemQuestionario)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.entrar)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.peso)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.peso)).clearText();
    await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.peso)).typeText('20');
    await element(by.id(`${BOTTOMTABBAR.questionarios.suaSaude.peso}.label`)).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.altura)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.altura)).clearText();
    await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.altura)).typeText('165');
    await element(by.id(`${BOTTOMTABBAR.questionarios.suaSaude.altura}.label`)).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.imc)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.imc)).clearText();
    await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.imc)).typeText('30');
    await element(by.id(`${BOTTOMTABBAR.questionarios.suaSaude.imc}.label`)).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMaxima)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMaxima),
    ).clearText();
    await element(
      by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMaxima),
    ).typeText('122');
    await element(
      by.id(`${BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMaxima}.label`),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMinima)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMinima),
    ).clearText();
    await element(
      by.id(BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMinima),
    ).typeText('122');
    await element(
      by.id(`${BOTTOMTABBAR.questionarios.suaSaude.pressaoArterialMinima}.label`),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    // await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.ate80ID)).tap();
    await element(by.id('questao_244')).tap();
    await element(by.id('questao_244')).clearText();
    await element(by.id('questao_244')).typeText('80');
    // await element(by.id('questao_244').label)).tap();
    await element(by.id(QUEST.nextQuestion)).multiTap(2);
    await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.muitoBoaID)).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.diabetesMellitusID)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.fibromialgiaID)).tap();
    await element(by.id(BOTTOMTABBAR.modal.body)).swipe('up');
    await waitFor(element(by.id(BOTTOMTABBAR.questionarios.suaSaude.osteoporoseID)))
      .toBeVisible()
      .withTimeout(1000);
    await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.osteoporoseID)).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.label(`${BOTTOMTABBAR.questionarios.suaSaude.respostaSimTexto}`),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.label(`${BOTTOMTABBAR.questionarios.suaSaude.respostaSimTexto}`),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.defAuditivaID)).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.label(`${BOTTOMTABBAR.questionarios.suaSaude.respostaSimTexto}`),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    // await element(
    //   by.id(BOTTOMTABBAR.questionarios.suaSaude.paisDoencaCardioVascularID),
    // ).tap();
    // await element(
    //   by.id(BOTTOMTABBAR.questionarios.suaSaude.paisAlteracaoEmocionalID),
    // ).tap();
    // await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.paisCancerMamaID)).tap();
    // await element(by.id(BOTTOMTABBAR.modal.body)).swipe('up');
    // await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.label(`${BOTTOMTABBAR.questionarios.suaSaude.respostaSimTexto}`),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.prontoSocorro13ID)).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.suaSaude.motivosPSCancerIntestinoID),
    ).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.suaSaude.motivosPSCancerOvarioID),
    ).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.suaSaude.motivosPSCancerProstataID),
    ).tap();
    await element(by.id(BOTTOMTABBAR.modal.body)).swipe('up');
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.label(`${BOTTOMTABBAR.questionarios.suaSaude.respostaSimTexto}`),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.suaSaude.vezesInternado23ID)).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.suaSaude.motivosInternacaoCancerIntestinoID),
    ).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.suaSaude.motivosInternacaoCancerProstataID),
    ).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.suaSaude.motivosInternacaoDiabeteMellitusID),
    ).tap();
    await element(by.id(BOTTOMTABBAR.modal.body)).swipe('up');
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.suaSaude.motivosInternacaoOutrosMotivosTextoID),
    ).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.suaSaude.motivosInternacaoOutrosMotivosTextoID),
    ).clearText();
    await element(
      by.id(BOTTOMTABBAR.questionarios.suaSaude.motivosInternacaoOutrosMotivosTextoID),
    ).typeText('Depressao');
    await element(by.id(BOTTOMTABBAR.modal.close)).tap();
    await element(by.text('Sair sem salvar')).tap();
    await element(by.id('BOTTOMTABBAR.tab.Care')).tap();

    // await element(by.id(QUEST.nextQuestion)).tap();
  });
};

export const questAutoCuidado = () => {
  it("Check 'Saude > Questionarios > Auto Cuidado' ", async () => {
    // await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    // await element(by.id(LOGIN.form.userInput)).tap();
    // await element(by.id(LOGIN.form.userInput)).clearText();
    // await element(by.id(LOGIN.form.userInput)).typeText('000010000127008');
    // await element(by.id(LOGIN.form.passwordInput)).tap();
    // await element(by.id(LOGIN.form.passwordInput)).clearText();
    // await element(by.id(LOGIN.form.passwordInput)).typeText('Qsaude@2019');
    // await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
    // await waitFor(element(by.id(LOGIN.form.submitButton)))
    //   .toBeVisible()
    //   .withTimeout(2000);
    // await expect(element(by.id(LOGIN.form.submitButton))).toExist();
    // await expect(element(by.text('Entrar')));
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Desabilitado',
    //   );
    // await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
    // await element(by.id(LOGIN.form.termsCheckbox)).tap();
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Habilitado',
    // );
    // await element(by.id(LOGIN.form.submitButton)).tap();
    // await waitFor(element(by.id(LOGIN.form.submitButton)))
    //   .toBeNotVisible()
    //   .withTimeout(10000);
    await element(by.id('BOTTOMTABBAR.tab.Health')).multiTap(2);
    await expect(element(by.id(BOTTOMTABBAR.tab.health.itemQuestionario))).toExist();
    await element(by.id(BOTTOMTABBAR.tab.health.itemQuestionario)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.autoCuidado.entrar)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.autoCuidado.acompanhamentoNaoID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.autoCuidado.medicamentoRotinaSimID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.autoCuidado.recomendacaoMedicaEsporadicoID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.autoCuidado.examePapanicolauNaoID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.autoCuidado.exameMamografiaNaoID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.autoCuidado.exameDentistaNaoID)).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.autoCuidado.exameAtividades15DiasTodosID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.autoCuidado.exameNivelStressAltoID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.autoCuidado.exameQualidadeSonoUmaSemanaMaisID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.autoCuidado.exameTempoSonoOcasionalmenteID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.autoCuidado.exameFaltaApetiteUmaSemanaMaisID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.autoCuidado.exameFracassoDecepcaoUmaSemanaMaisID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.autoCuidado.exameDificuldadeConcentracaoUmaSemanaMaisID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.autoCuidado.exameLentidaoMovimentarUmaSemanaMaisID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.autoCuidado.examePensamentosPrejudiciaisUmaSemanaMaisID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.autoCuidado.exameSintomasAnterioresMuitaID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.autoCuidado.examePraticaExercicioNenhumDiaID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.autoCuidado.exameTempoDuracaoAtividade),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.autoCuidado.exameEmocionalTodosID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();

    await element(by.id(BOTTOMTABBAR.modal.close)).tap();
    await element(by.text('Sair sem salvar')).tap();
    await element(by.id('BOTTOMTABBAR.tab.Care')).tap();
  });
};

export const questHabitos = () => {
  it("Check 'Saude > Questionarios > Habitos' ", async () => {
    // await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    // await element(by.id(LOGIN.form.userInput)).tap();
    // await element(by.id(LOGIN.form.userInput)).clearText();
    // await element(by.id(LOGIN.form.userInput)).typeText('000010000127008');
    // await element(by.id(LOGIN.form.passwordInput)).tap();
    // await element(by.id(LOGIN.form.passwordInput)).clearText();
    // await element(by.id(LOGIN.form.passwordInput)).typeText('Qsaude@2019');
    // await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
    // await waitFor(element(by.id(LOGIN.form.submitButton)))
    //   .toBeVisible()
    //   .withTimeout(2000);
    // await expect(element(by.id(LOGIN.form.submitButton))).toExist();
    // await expect(element(by.text('Entrar')));
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Desabilitado',
    //   );
    // await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
    // await element(by.id(LOGIN.form.termsCheckbox)).tap();
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Habilitado',
    // );
    // await element(by.id(LOGIN.form.submitButton)).tap();
    // await waitFor(element(by.id(LOGIN.form.submitButton)))
    //   .toBeNotVisible()
    //   .withTimeout(10000);
    await element(by.id('BOTTOMTABBAR.tab.Health')).multiTap(2);
    await expect(element(by.id(BOTTOMTABBAR.tab.health.itemQuestionario))).toExist();
    await element(by.id(BOTTOMTABBAR.tab.health.itemQuestionario)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.habitos.entrar)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.habitos.alimentacaoFrutasLegumesNaoID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.habitos.tresRefeicoesDiariasNaoID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.habitos.habitoSalSimID)).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.habitos.aguaDiariaNaoID)).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.habitos.bebidasAlcoolicasSemana15MaisID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(
      by.id(BOTTOMTABBAR.questionarios.habitos.bebidasAlcoolicasFDS6MaisMaisID),
    ).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.habitos.fumanteSimID)).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.habitos.tempoFumanteMais15ID)).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.habitos.estiloVidaNaoID)).tap();
    await element(by.id(QUEST.nextQuestion)).tap();
    // await element(
    //   by.id(BOTTOMTABBAR.questionarios.habitos.atividadeFisicaSemanalNenhumaID),
    // ).tap();
    // await element(by.id(QUEST.nextQuestion)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.habitos.outrosHabitosID)).tap();
    await element(by.id(BOTTOMTABBAR.questionarios.habitos.outrosHabitosID)).clearText();
    await element(by.id(BOTTOMTABBAR.questionarios.habitos.outrosHabitosID)).typeText(
      'Comidas gordurosas',
    );
    // await element(by.id(QUEST.nextQuestion)).tap();

    // await element(by.id(QUEST.nextQuestion)).tap();
    await element(by.id(BOTTOMTABBAR.modal.close)).tap();
    await element(by.text('Sair sem salvar')).tap();

    await element(by.id('BOTTOMTABBAR.tab.Care')).tap();
  });
};

export const saudeFichaMedica = () => {
  it("Check 'Saude > Ficha Medica' ", async () => {
    // await expect(element(by.id(LOGIN.form.form))).toBeVisible();
    // await element(by.id(LOGIN.form.userInput)).tap();
    // await element(by.id(LOGIN.form.userInput)).clearText();
    // await element(by.id(LOGIN.form.userInput)).typeText('000010000127008');
    // await element(by.id(LOGIN.form.passwordInput)).tap();
    // await element(by.id(LOGIN.form.passwordInput)).clearText();
    // await element(by.id(LOGIN.form.passwordInput)).typeText('Qsaude@2019');
    // await element(by.id(`${LOGIN.form.passwordInput}.label`)).multiTap(2);
    // await waitFor(element(by.id(LOGIN.form.submitButton)))
    //   .toBeVisible()
    //   .withTimeout(2000);
    // await expect(element(by.id(LOGIN.form.submitButton))).toExist();
    // await expect(element(by.text('Entrar')));
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Desabilitado',
    //   );
    // await expect(element(by.id(LOGIN.form.termsCheckbox))).toExist();
    // await element(by.id(LOGIN.form.termsCheckbox)).tap();
    // await expect(element(by.id(LOGIN.form.submitButton))).toHaveLabel(
    //   'Entrar Habilitado',
    // );
    // await element(by.id(LOGIN.form.submitButton)).tap();
    // await waitFor(element(by.id(LOGIN.form.submitButton)))
    //   .toBeNotVisible()
    //   .withTimeout(10000);
    await element(by.id('BOTTOMTABBAR.tab.Health')).multiTap(2);
    await expect(element(by.id(BOTTOMTABBAR.tab.health.itemFichaMedica))).toExist();
    await element(by.id(BOTTOMTABBAR.tab.health.itemFichaMedica)).tap();
    await waitFor(element(by.id(BOTTOMTABBAR.tabHealth.itemMedicalRecord.name)))
      .toBeVisible()
      .withTimeout(1000);
    await expect(element(by.id(BOTTOMTABBAR.tabHealth.itemMedicalRecord.name))).toExist();
    await expect(
      element(by.id(BOTTOMTABBAR.tabHealth.itemMedicalRecord.allergies)),
    ).toExist();
    await expect(
      element(by.id(BOTTOMTABBAR.tabHealth.itemMedicalRecord.vaccines)),
    ).toExist();
    await expect(
      element(by.id(BOTTOMTABBAR.tabHealth.itemMedicalRecord.illnesses)),
    ).toExist();

    console.log('Conferindo', BOTTOMTABBAR.tabHealth.itemMedicalRecord.name);
    await element(by.id(BOTTOMTABBAR.tabHealth.itemMedicalRecord.name)).tap();
    await element(
      by.id(BOTTOMTABBAR.tabHealth.itemMedicalRecord.name).and(by.label('Anderson Doe')),
    );
    console.log('Conferindo', BOTTOMTABBAR.tabHealth.itemMedicalRecord.allergies);
    await element(by.id(BOTTOMTABBAR.tabHealth.itemMedicalRecord.allergies)).tap();
    await element(
      by.id(BOTTOMTABBAR.tabHealth.itemMedicalRecord.allergies).and(by.label('Dipirona')),
    );
    console.log('Conferindo', BOTTOMTABBAR.tabHealth.itemMedicalRecord.vaccines);
    await element(by.id(BOTTOMTABBAR.tabHealth.itemMedicalRecord.vaccines)).tap();
    await element(
      by
        .id(BOTTOMTABBAR.tabHealth.itemMedicalRecord.vaccines)
        .and(by.label('Não há registros')),
    );
    console.log('Conferindo', BOTTOMTABBAR.tabHealth.itemMedicalRecord.illnesses);
    await element(by.id(BOTTOMTABBAR.tabHealth.itemMedicalRecord.illnesses)).tap();
    await element(
      by
        .id(BOTTOMTABBAR.tabHealth.itemMedicalRecord.illnesses)
        .and(by.label('Transtorno depressivo recorrente')),
    );
    console.log('Conferindo', BOTTOMTABBAR.tabHealth.itemMedicalRecord.medicalHistory);
    await element(by.id(BOTTOMTABBAR.tabHealth.itemMedicalRecord.medicalHistory)).tap();
    await element(
      by
        .id(BOTTOMTABBAR.tabHealth.itemMedicalRecord.medicalHistory)
        .and(by.label('Acidente vascular cerebral')),
    );

    await element(by.id('BOTTOMTABBAR.tab.Care')).tap();
  });
};

export const saudeDadosVitais = () => {
  it("Check 'Dados Vitais' ", async () => {
    await element(by.id('BOTTOMTABBAR.tab.Health')).multiTap(2);
    // IMC
    await element(by.id('healt_card_1')).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.addNew)).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.weight)).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.weight)).clearText();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.weight)).typeText('100');
    await element(by.id(`${BOTTOMTABBAR.tabHealth.dadosVitaisIMC.weight}.label`)).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.height)).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.height)).clearText();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.height)).typeText('185');
    await element(by.id(`${BOTTOMTABBAR.tabHealth.dadosVitaisIMC.height}.label`)).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.submit)).tap();
    await element(by.text('OK')).tap();
    await element(by.id('BOTTOMTABBAR.tab.Health')).tap();

    // Pressao arterial
    await element(by.id('healt_card_2')).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.addNew)).tap();
    await element(
      by.id(BOTTOMTABBAR.tabHealth.dadosVitaisPressaoArterial.minDiastolica),
    ).tap();
    await element(
      by.id(BOTTOMTABBAR.tabHealth.dadosVitaisPressaoArterial.minDiastolica),
    ).clearText();
    await element(
      by.id(BOTTOMTABBAR.tabHealth.dadosVitaisPressaoArterial.minDiastolica),
    ).typeText('100');
    await element(
      by.id(`${BOTTOMTABBAR.tabHealth.dadosVitaisPressaoArterial.minDiastolica}.label`),
    ).tap();
    await element(
      by.id(BOTTOMTABBAR.tabHealth.dadosVitaisPressaoArterial.maxSistolica),
    ).tap();
    await element(
      by.id(BOTTOMTABBAR.tabHealth.dadosVitaisPressaoArterial.maxSistolica),
    ).clearText();
    await element(
      by.id(BOTTOMTABBAR.tabHealth.dadosVitaisPressaoArterial.maxSistolica),
    ).typeText('185');
    await element(
      by.id(`${BOTTOMTABBAR.tabHealth.dadosVitaisPressaoArterial.maxSistolica}.label`),
    ).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisPressaoArterial.submit)).tap();
    await element(by.text('OK')).tap();
    await element(by.id('BOTTOMTABBAR.tab.Health')).tap();

    // Peso
    await element(by.id('BOTTOMTABBAR.tab.Health')).tap();
    await element(by.id(BOTTOMTABBAR.tab.health.dadosVitaisScrollView)).swipe(
      'left',
      'fast',
    );
    await element(by.id('healt_card_3')).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.addNew)).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.weight)).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.weight)).clearText();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.weight)).typeText('100');
    await element(by.id(`${BOTTOMTABBAR.tabHealth.dadosVitaisIMC.weight}.label`)).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.height)).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.height)).clearText();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.height)).typeText('185');
    await element(by.id(`${BOTTOMTABBAR.tabHealth.dadosVitaisIMC.height}.label`)).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.submit)).tap();
    await element(by.text('OK')).tap();
    await element(by.id('BOTTOMTABBAR.tab.Health')).tap();

    // Altura
    await element(by.id('BOTTOMTABBAR.tab.Health')).tap();
    await element(by.id(BOTTOMTABBAR.tab.health.dadosVitaisScrollView)).swipe(
      'left',
      'fast',
    );
    await element(by.id('healt_card_4')).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.addNew)).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.weight)).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.weight)).clearText();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.weight)).typeText('100');
    await element(by.id(`${BOTTOMTABBAR.tabHealth.dadosVitaisIMC.weight}.label`)).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.height)).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.height)).clearText();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.height)).typeText('185');
    await element(by.id(`${BOTTOMTABBAR.tabHealth.dadosVitaisIMC.height}.label`)).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.submit)).tap();
    await element(by.text('OK')).tap();
    await element(by.id('BOTTOMTABBAR.tab.Health')).tap();

    // Glicemia
    await element(by.id('BOTTOMTABBAR.tab.Health')).tap();
    await element(by.id(BOTTOMTABBAR.tab.health.dadosVitaisScrollView)).swipe(
      'left',
      'fast',
    );
    await element(by.id('healt_card_5')).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisIMC.addNew)).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisGlicemia.glicemia)).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisGlicemia.glicemia)).clearText();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisGlicemia.glicemia)).typeText(
      '100',
    );
    await element(
      by.id(`${BOTTOMTABBAR.tabHealth.dadosVitaisGlicemia.glicemia}.label`),
    ).tap();
    await element(by.id(BOTTOMTABBAR.tabHealth.dadosVitaisGlicemia.submit)).tap();
    await element(by.text('OK')).tap();
    await element(by.id('BOTTOMTABBAR.tab.Health')).tap();

    await element(by.id('BOTTOMTABBAR.tab.Care')).tap();
  });
};

export const saudeMedicamentos = () => {
  it("Check 'Medicamentos' ", async () => {
    await element(by.id('BOTTOMTABBAR.tab.Health')).tap();
    await expect(element(by.id(BOTTOMTABBAR.tab.health.itemMedicamentos))).toExist();
    await element(by.id(BOTTOMTABBAR.tab.health.itemMedicamentos)).tap();
    await expect(element(by.id('medicamento_1'))).toExist();
    await element(by.id('medicamento_1')).tap();
    await element(by.label('Manhã')).tap();
    await element(by.label('Via Oral')).tap();
    await element(by.id('back_MedicationDetails')).tap();
    await element(by.id('medicamento_2')).tap();
    await element(by.label('Manhã')).tap();
    await element(by.id('back_MedicationDetails')).tap();
    await element(by.id('BOTTOMTABBAR.tab.Health')).tap();
    // await expect(element(by.id(PROFILEUSER.itemsList.logout))).toExist();
    // await element(by.id(PROFILEUSER.itemsList.logout)).tap();
    // try {
    // } catch (error) {
    //   console.log('\n================= ');
    //   console.log(R.slice(0, 5000, error.toString()));
    //   console.log('=================\n');
    // }
  });
};
