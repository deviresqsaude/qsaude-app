/* eslint-env detox/detox */
import { PROFILEUSER, LOGIN } from '../src/constants/TestIds';

// QSAUDE user
export const ChangeUserPassword = () => {
  it("Test 'Alterar minha Senha'", async () => {
    await element(by.id('BOTTOMTABBAR.tab.Profile')).tap();
    await expect(element(by.id(PROFILEUSER.itemsList.changePassword))).toExist();
    await element(by.id(PROFILEUSER.itemsList.changePassword)).tap();
    await waitFor(element(by.id('formPassword')))
      .toBeVisible()
      .withTimeout(2000);
    await element(
      by.id(PROFILEUSER.changePassword.changeMyPassword.currentPassword),
    ).tap();
  });
};

export const checkVirtualCardMichelle = () => {
  it("Test 'virtual card MICHELLE'", async () => {
    await element(by.text('Dismiss All')).tap();
    await element(by.id('BOTTOMTABBAR.tab.Profile')).tap();
    await expect(element(by.id(PROFILEUSER.itemsList.checkVcard))).toExist();
    await element(by.id(PROFILEUSER.itemsList.checkVcard)).tap();
    await waitFor(element(by.id(PROFILEUSER.checkVcard.clientName)))
      .toBeVisible()
      .withTimeout(2000);
    await element(
      by.id(PROFILEUSER.checkVcard.clientName).and(by.text('MICHELE BARBOSA')),
    ).tap();
    await element(by.id(PROFILEUSER.checkVcard.healthPlanCode).and(by.text('QS1'))).tap();
    await element(
      by.id(PROFILEUSER.checkVcard.startingDate).and(by.text('20/12/20')),
    ).tap();
    await element(
      by.id(PROFILEUSER.checkVcard.segmentPlan).and(by.text('Ambulatorial')),
    ).tap();
    await element(by.id(PROFILEUSER.checkVcard.flipCard)).tap();
    await element(by.id(PROFILEUSER.checkVcard.birthday).and(by.text('27/04/96'))).tap();
    await element(
      by.id(PROFILEUSER.checkVcard.typeContract).and(by.text('Adesao')),
    ).tap();
    await element(by.id(PROFILEUSER.checkVcard.coverage).and(by.text('---'))).tap();
    await element(by.id(PROFILEUSER.checkVcard.contract).and(by.text('APM'))).tap();
    await element(
      by.id(PROFILEUSER.checkVcard.cns).and(by.text('925449545170009')),
    ).tap();
    await element(by.id(PROFILEUSER.checkVcard.contractor).and(by.text('---'))).tap();
    await element(
      by.id(PROFILEUSER.checkVcard.accommodation).and(by.text('Enfermaria')),
    ).tap();

    //   await element(by.id(LOGIN.form.navBackButtonSimple)).tap();
    //   await element(by.id('BOTTOMTABBAR.tab.Care')).tap();
  });
};
export const checkVirtualCardSecundario = () => {
  it("Test 'virtual card Secundario'", async () => {
    await element(by.text('Dismiss All')).tap();
    await element(by.id('BOTTOMTABBAR.tab.Profile')).tap();
    await expect(element(by.id(PROFILEUSER.itemsList.checkVcard))).toExist();
    await element(by.id(PROFILEUSER.itemsList.checkVcard)).tap();
    await waitFor(element(by.id(PROFILEUSER.checkVcard.clientName)))
      .toBeVisible()
      .withTimeout(2000);
    await element(
      by.id(PROFILEUSER.checkVcard.clientName).and(by.text('MICHELE BARBOSA')),
    ).tap();
    await element(by.id(PROFILEUSER.checkVcard.healthPlanCode).and(by.text('QS1'))).tap();
    await element(
      by.id(PROFILEUSER.checkVcard.startingDate).and(by.text('20/12/20')),
    ).tap();
    await element(
      by.id(PROFILEUSER.checkVcard.segmentPlan).and(by.text('Ambulatorial')),
    ).tap();
    await element(by.id(PROFILEUSER.checkVcard.flipCard)).tap();
    await element(by.id(PROFILEUSER.checkVcard.birthday).and(by.text('27/04/96'))).tap();
    await element(
      by.id(PROFILEUSER.checkVcard.typeContract).and(by.text('Adesao')),
    ).tap();
    await element(by.id(PROFILEUSER.checkVcard.coverage).and(by.text('---'))).tap();
    await element(by.id(PROFILEUSER.checkVcard.contract).and(by.text('APM'))).tap();
    await element(
      by.id(PROFILEUSER.checkVcard.cns).and(by.text('925449545170009')),
    ).tap();
    await element(by.id(PROFILEUSER.checkVcard.contractor).and(by.text('---'))).tap();
    await element(
      by.id(PROFILEUSER.checkVcard.accommodation).and(by.text('Enfermaria')),
    ).tap();

    //   await element(by.id(LOGIN.form.navBackButtonSimple)).tap();
    //   await element(by.id('BOTTOMTABBAR.tab.Care')).tap();
  });
};

export const changeUserData = () => {
  it("Test - Alterar dados do usuario logado - OSVALDO", async () => {
    await element(by.text('Dismiss All')).tap();
    await waitFor(element(by.id('BOTTOMTABBAR.tab.Account')))
      .toBeVisible()
      .withTimeout(2000);
    await element(by.id('BOTTOMTABBAR.tab.Account')).tap();
    await waitFor(element(by.id(PROFILEUSER.itemsList.changeUserData)))
      .toBeVisible()
      .withTimeout(2000);
    await expect(element(by.id(PROFILEUSER.itemsList.changeUserData))).toExist();
    await element(by.id(PROFILEUSER.itemsList.changeUserData)).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldEmail')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldEmail')).clearText();
    await element(by.id('PROFILEUSER.changeUserData.fieldEmail')).typeText(
      'fabrizio.andre@devires.com.br',
    );
    await element(by.text('E-mail')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldPhoneNumber')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldPhoneNumber')).clearText();
    await element(by.id('PROFILEUSER.changeUserData.fieldPhoneNumber')).typeText(
      '11959843574',
    );
    await element(by.text('Telefone celular')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldZipCode')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldZipCode')).clearText();
    await element(by.id('PROFILEUSER.changeUserData.fieldZipCode')).typeText('04707000');
    await element(by.text('CEP')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldAddress')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldAddress')).clearText();
    await element(by.id('PROFILEUSER.changeUserData.fieldAddress')).typeText(
      'Avenida Roque Petroni Júnior',
    );
    await element(by.text('Rua')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldAddressNumber')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldAddressNumber')).clearText();
    await element(by.id('PROFILEUSER.changeUserData.fieldAddressNumber')).typeText('850');
    await element(by.text('Número')).tap();
    await element(by.id('scrollView')).swipe('up', 'slow', 0.54);
    await element(by.id('PROFILEUSER.changeUserData.addressComplement')).tap();
    await element(by.id('PROFILEUSER.changeUserData.addressComplement')).clearText();
    await element(by.id('PROFILEUSER.changeUserData.addressComplement')).typeText(
      'Torre Jaceru',
    );
    await element(by.text('Complemento (opcional)')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldNeighborhood')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldNeighborhood')).clearText();
    await element(by.id('PROFILEUSER.changeUserData.fieldNeighborhood')).typeText(
      'Jardim das Acacias',
    );
    await element(by.text('Bairro')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldState')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldState')).clearText();
    await element(by.id('PROFILEUSER.changeUserData.fieldState')).typeText('SP');
    await element(by.text('Estado')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldCity')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldCity')).clearText();
    await element(by.id('PROFILEUSER.changeUserData.fieldCity')).typeText('São Paulo');
    await element(by.text('Cidade')).tap();
    await element(by.id('scrollView')).swipe('up', 'slow', 0.54);
    await element(by.text('Nome')).tap();
    await element(
      by
        .id('PROFILEUSER.changeUserData.fieldName')
        .and(by.text('OSVALDO OLIVEIRA SILVA')),
    );
    await element(by.text('CPF')).tap();
    await element(
      by.id('PROFILEUSER.changeUserData.fieldCpf').and(by.text('34960573007')),
    );
    await element(by.text('ID da carterinha')).tap();
    await element(
      by.id('PROFILEUSER.changeUserData.fieldNumCard').and(by.text('00010001000311009')),
    );
    await element(by.text('Data de nascimento')).tap();
    await element(
      by.id('PROFILEUSER.changeUserData.fieldBirthday').and(by.text('07-05-1971')),
    );
    await element(by.id(PROFILEUSER.changeUserData.submitData)).tap();
    await element(by.text('Ok')).tap();
    await expect(element(by.id(PROFILEUSER.itemsList.changeUserData))).toExist();
    await element(by.id(PROFILEUSER.itemsList.changeUserData)).tap();
    await element(
      by
        .id('PROFILEUSER.changeUserData.fieldEmail')
        .and(by.text('fabrizio.andre@devires.com.br')),
    );
    await element(by.text('E-mail')).tap();
    await element(
      by
        .id('PROFILEUSER.changeUserData.fieldPhoneNumber')
        .and(by.text('fabrizio.andre@devires.com.br')),
    );
    await element(by.text('Telefone celular')).tap();
    await element(
      by.id('PROFILEUSER.changeUserData.fieldZipCode').and(by.text('04707000')),
    );
    await element(by.text('CEP')).tap();
    await element(
      by
        .id('PROFILEUSER.changeUserData.fieldAddress')
        .and(by.text('Avenida Roque Petroni Júnior')),
    );
    await element(by.text('Rua')).tap();
    await element(
      by.id('PROFILEUSER.changeUserData.fieldAddressNumber').and(by.text('850')),
    );
    await element(by.text('Número')).tap();
    await element(by.id('scrollView')).swipe('up', 'slow', 0.54);
    await element(
      by.id('PROFILEUSER.changeUserData.addressComplement').and(by.text('Torre Jaceru')),
    );
    await element(by.text('Complemento (opcional)')).tap();
    await element(
      by
        .id('PROFILEUSER.changeUserData.fieldNeighborhood')
        .and(by.text('Jardim das Acacias')),
    );
    await element(by.text('Bairro')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldState').and(by.text('SP')));
    await element(by.text('Estado')).tap();
    await element(
      by.id('PROFILEUSER.changeUserData.fieldCity').and(by.text('São Paulo')),
    );
    await element(by.text('Cidade')).tap();
    await element(by.id('scrollView')).swipe('up', 'slow', 0.54);
    await element(by.text('Nome')).tap();
    await element(
      by
        .id('PROFILEUSER.changeUserData.fieldName')
        .and(by.text('OSVALDO OLIVEIRA SILVA')),
    );
    await element(by.text('CPF')).tap();
    await element(
      by.id('PROFILEUSER.changeUserData.fieldCpf').and(by.text('34960573007')),
    );
    await element(by.text('ID da carterinha')).tap();
    await element(
      by.id('PROFILEUSER.changeUserData.fieldNumCard').and(by.text('00010001000311009')),
    );
    await element(by.text('Data de nascimento')).tap();
    await element(
      by.id('PROFILEUSER.changeUserData.fieldBirthday').and(by.text('07-05-1971')),
    );
    await element(by.id('back_UpdateInfo')).tap();
    await element(by.id('BOTTOMTABBAR.tab.Care')).tap();
  });
};
export const changeUserDataSecundary = () => {
  it("Test 'Alterar dados do usuario logado - OSVALDO'", async () => {
    await element(by.text('Dismiss All')).tap();
    await waitFor(element(by.id('BOTTOMTABBAR.tab.Account')))
      .toBeVisible()
      .withTimeout(2000);
    await element(by.id('BOTTOMTABBAR.tab.Account')).tap();
    await waitFor(element(by.id(PROFILEUSER.itemsList.changeUserData)))
      .toBeVisible()
      .withTimeout(2000);
    await expect(element(by.id(PROFILEUSER.itemsList.changeUserData))).toExist();
    await element(by.id(PROFILEUSER.itemsList.changeUserData)).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldEmail')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldEmail')).clearText();
    await element(by.id('PROFILEUSER.changeUserData.fieldEmail')).typeText(
      'fabrizio.andre@devires.com.br',
    );
    await element(by.text('E-mail')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldPhoneNumber')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldPhoneNumber')).clearText();
    await element(by.id('PROFILEUSER.changeUserData.fieldPhoneNumber')).typeText(
      '11959843574',
    );
    await element(by.text('Telefone celular')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldZipCode')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldZipCode')).clearText();
    await element(by.id('PROFILEUSER.changeUserData.fieldZipCode')).typeText('04707000');
    await element(by.text('CEP')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldAddress')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldAddress')).clearText();
    await element(by.id('PROFILEUSER.changeUserData.fieldAddress')).typeText(
      'Avenida Roque Petroni Júnior',
    );
    await element(by.text('Rua')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldAddressNumber')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldAddressNumber')).clearText();
    await element(by.id('PROFILEUSER.changeUserData.fieldAddressNumber')).typeText('850');
    await element(by.text('Número')).tap();
    await element(by.id('scrollView')).swipe('up', 'slow', 0.54);
    await element(by.id('PROFILEUSER.changeUserData.addressComplement')).tap();
    await element(by.id('PROFILEUSER.changeUserData.addressComplement')).clearText();
    await element(by.id('PROFILEUSER.changeUserData.addressComplement')).typeText(
      'Torre Jaceru',
    );
    await element(by.text('Complemento (opcional)')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldNeighborhood')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldNeighborhood')).clearText();
    await element(by.id('PROFILEUSER.changeUserData.fieldNeighborhood')).typeText(
      'Jardim das Acacias',
    );
    await element(by.text('Bairro')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldState')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldState')).clearText();
    await element(by.id('PROFILEUSER.changeUserData.fieldState')).typeText('SP');
    await element(by.text('Estado')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldCity')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldCity')).clearText();
    await element(by.id('PROFILEUSER.changeUserData.fieldCity')).typeText('São Paulo');
    await element(by.text('Cidade')).tap();
    await element(by.id('scrollView')).swipe('up', 'slow', 0.54);
    await element(by.text('Nome')).tap();
    await element(
      by
        .id('PROFILEUSER.changeUserData.fieldName')
        .and(by.text('OSVALDO OLIVEIRA SILVA')),
    );
    await element(by.text('CPF')).tap();
    await element(
      by.id('PROFILEUSER.changeUserData.fieldCpf').and(by.text('34960573007')),
    );
    await element(by.text('ID da carterinha')).tap();
    await element(
      by.id('PROFILEUSER.changeUserData.fieldNumCard').and(by.text('00010001000311009')),
    );
    await element(by.text('Data de nascimento')).tap();
    await element(
      by.id('PROFILEUSER.changeUserData.fieldBirthday').and(by.text('07-05-1971')),
    );
    await element(by.id(PROFILEUSER.changeUserData.submitData)).tap();
    await element(by.text('Ok')).tap();
    await expect(element(by.id(PROFILEUSER.itemsList.changeUserData))).toExist();
    await element(by.id(PROFILEUSER.itemsList.changeUserData)).tap();
    await element(
      by
        .id('PROFILEUSER.changeUserData.fieldEmail')
        .and(by.text('fabrizio.andre@devires.com.br')),
    );
    await element(by.text('E-mail')).tap();
    await element(
      by
        .id('PROFILEUSER.changeUserData.fieldPhoneNumber')
        .and(by.text('fabrizio.andre@devires.com.br')),
    );
    await element(by.text('Telefone celular')).tap();
    await element(
      by.id('PROFILEUSER.changeUserData.fieldZipCode').and(by.text('04707000')),
    );
    await element(by.text('CEP')).tap();
    await element(
      by
        .id('PROFILEUSER.changeUserData.fieldAddress')
        .and(by.text('Avenida Roque Petroni Júnior')),
    );
    await element(by.text('Rua')).tap();
    await element(
      by.id('PROFILEUSER.changeUserData.fieldAddressNumber').and(by.text('850')),
    );
    await element(by.text('Número')).tap();
    await element(by.id('scrollView')).swipe('up', 'slow', 0.54);
    await element(
      by.id('PROFILEUSER.changeUserData.addressComplement').and(by.text('Torre Jaceru')),
    );
    await element(by.text('Complemento (opcional)')).tap();
    await element(
      by
        .id('PROFILEUSER.changeUserData.fieldNeighborhood')
        .and(by.text('Jardim das Acacias')),
    );
    await element(by.text('Bairro')).tap();
    await element(by.id('PROFILEUSER.changeUserData.fieldState').and(by.text('SP')));
    await element(by.text('Estado')).tap();
    await element(
      by.id('PROFILEUSER.changeUserData.fieldCity').and(by.text('São Paulo')),
    );
    await element(by.text('Cidade')).tap();
    await element(by.id('scrollView')).swipe('up', 'slow', 0.54);
    await element(by.text('Nome')).tap();
    await element(
      by
        .id('PROFILEUSER.changeUserData.fieldName')
        .and(by.text('OSVALDO OLIVEIRA SILVA')),
    );
    await element(by.text('CPF')).tap();
    await element(
      by.id('PROFILEUSER.changeUserData.fieldCpf').and(by.text('34960573007')),
    );
    await element(by.text('ID da carterinha')).tap();
    await element(
      by.id('PROFILEUSER.changeUserData.fieldNumCard').and(by.text('00010001000311009')),
    );
    await element(by.text('Data de nascimento')).tap();
    await element(
      by.id('PROFILEUSER.changeUserData.fieldBirthday').and(by.text('07-05-1971')),
    );
    await element(by.id('back_UpdateInfo')).tap();
    await element(by.id('BOTTOMTABBAR.tab.Care')).tap();
  });
};
