package com.qsaude;

import android.util.Log;

import androidx.annotation.Nullable;

import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import br.com.conexasdk.ConexaSDK;
import us.zoom.sdk.MeetingStatus;

public class ConexaIntegration extends ReactContextBaseJavaModule  {
    private ConexaSDK conexa;
    private ReactContext reactC;
    private ConexaSDK.OnMeetingListener listener;

  public ConexaIntegration(ReactApplicationContext reactContext) {
        super(reactContext);
        reactC = reactContext;

        listener = new ConexaSDK.OnMeetingListener() {
          @Override
          public void onErrorStartMeeting(String s, int i, int i1) {

          }

          @Override
          public void onMeetingStatusChanged(MeetingStatus meetingStatus, int i) {
            Log.d("meetingStatus", meetingStatus.toString() );
            if(meetingStatus == MeetingStatus.MEETING_STATUS_DISCONNECTING) {
              sendMeetingStatus();
            }
          }
        };

        conexa = new ConexaSDK(reactContext, listener);
    }

    @Override
    public String getName() {
        return "ConexaIntegration";
    }

  private void sendEvent(ReactContext reactContext,
                         String eventName,
                         @Nullable WritableMap params) {
    reactContext
      .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
      .emit(eventName, params);
  }

    @ReactMethod
    public void sendMeetingStatus() {
      WritableMap params = Arguments.createMap();
      params.putString("status", "finished");
      this.sendEvent(this.reactC, "endMeeting", params);
    }

    @ReactMethod
    public void getConexaConnection(
            int meetting,
            String participant,
            Callback successCallback,
            Callback errorCallback) {
        reactC.getCurrentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    conexa.startMeeting(String.valueOf(meetting), participant);
                    successCallback.invoke(meetting);

                }
                catch (Exception e){
                    errorCallback.invoke(e.toString());
                }
            }
        });
    }
}
