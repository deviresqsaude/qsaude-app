# QSaúde App 

## Installing
----------------------------------

Install Node dependencies: 
```bash
  cd native && yarn run install:clean
```

Install iOS dependencies:
```bash
  yarn run install:ios
```
> ⚠️ Do not run ```react-native link```  anymore!
> 
> Since RN 0.60, iOS and Android libs are now linked automatically

## Running the Project (local-dev)
----------------------------------

### Android

- <strong>Start emulator or connect phisycal device</strong>

To list A.V.Ds
```bash
  emulator -list-avds
```
Start avd:
```bash
  emulator -avd "Device_Name_API_00"
```
- <strong>Start packager</strong>
```bash
  yarn start:android
```


### iOS

```bash
  yarn start:ios 
  # PS: You can change the device in package.json, "start:ios" script. 
  # default: iPhone 7
```